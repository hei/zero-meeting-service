package com.imflea.zero.controller.pay;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.model.entity.pay.dto.PayOrderDetailDto;
import com.imflea.zero.service.wx.IWxPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.wx
 * @ClassName: WxPayController
 * @Description
 * @date 2021-09-26  11:10:26
 */
@RequestMapping("/pay/wx")
@RestController
@Api(value = "支付-微信支付", tags = "支付-微信支付")
public class WxPayController {
    @Autowired
    IWxPayService wxPayService;


    /**
     * @param orderDetail
     * @return com.css.aegis.entity.JsonResult
     * @author 祥保玉
     * @description 微信支付统一下单
     * @date 2021/9/26  17:33
     */


    @ApiOperation(value = "API10001：微信支付统一下单", notes = "微信支付")
    @PostMapping(value = "/unifiedAddOrder")
    @ApiOperationSupport(order = 1)
    public JsonResult<Map<String, Object>> unifiedOrder(@RequestBody PayOrderDetailDto orderDetail) throws Exception {
        return new JsonResult<>(ZeroContant.getSuccessCode(), wxPayService.addOrder4Unified(orderDetail));
    }

    /**
     * 验证是否完成支付
     */
    @ApiOperation(value = "API10002：验证是否完成支付", notes = "微信支付")
    @ApiOperationSupport(order = 2)
    @PostMapping("/queryOrderIsPayFinish")
    public JsonResult<Map<String, Object>> updateOrQueryPayResult(@RequestBody PayOrderDetailDto orderDetail) throws Exception {
        return new JsonResult(ZeroContant.getSuccessCode(), wxPayService.updateIsPayFinish4QueryOrder(orderDetail));
    }

    /**
     * 支付回调
     */
    @ApiOperation(value = "API10003：支付回调", notes = "微信支付")
    @ApiOperationSupport(order = 3)
    @PostMapping("/notify")
    public JsonResult<String> notify(HttpServletRequest request) throws Exception {
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", wxPayService.update4WxCallBack(request));
    }

    /**
     * 关闭订单
     */
    @ApiOperation(value = "API10004：关闭订单", notes = "微信支付")
    @ApiOperationSupport(order = 4)
    @PostMapping("/closeOrders")
    public JsonResult<Boolean> closeOrders(@RequestBody PayOrderDetailDto orderDetail) throws Exception {
        return new JsonResult<>(ZeroContant.getSuccessCode(), wxPayService.closeOrders(orderDetail));
    }

    /**
     * 退款
     */
    @ApiOperation(value = "API10005：退款", notes = "微信支付")
    @ApiOperationSupport(order = 5)
    @PostMapping("/refund")
    public JsonResult<Map<String, Object>> refund(String orderNumber, BigDecimal refundAmount, BigDecimal totalFee) {
        return new JsonResult(ZeroContant.getSuccessCode(), wxPayService.doRefund(orderNumber, refundAmount, totalFee));
    }

}
