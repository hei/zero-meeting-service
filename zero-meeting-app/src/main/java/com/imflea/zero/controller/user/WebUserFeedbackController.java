package com.imflea.zero.controller.user;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.model.entity.user.WebUserFeedback;
import com.imflea.zero.service.user.IWebUserFeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zhaoyang
 * @PackageName: com.imflea.zero.controller
 * @ClassName: WebUserFeedbackController
 * @Description
 * @since 2021-09-24
 */
@RestController
@Api(value = "产品建议", tags = "产品建议")
@RequestMapping("/web/user/feedback")
public class WebUserFeedbackController {

    @Autowired
    private IWebUserFeedbackService webUserFeedbackService;


    @PostMapping("/save")
    @ApiOperation(value = "保存与修改接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<WebUserFeedback> save(@RequestBody WebUserFeedback webUserFeedback) {
        WebUserFeedback entity = webUserFeedbackService.saveUserFeedback(webUserFeedback);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

}
