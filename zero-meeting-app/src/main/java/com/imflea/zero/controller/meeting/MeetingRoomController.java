package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.meeting.dto.MeetingRoomDto;
import com.imflea.zero.model.entity.meeting.dto.RoomStatProvinceDto;
import com.imflea.zero.model.entity.meeting.vo.MeetingAreaStatVo;
import com.imflea.zero.model.entity.meeting.vo.MeetingRoomDetailVO;
import com.imflea.zero.service.meeting.IMeetingRoomService;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 空间表 前端控制器
 * </p>
 *
 * @author xby
 * @PackageName: com.imflea.zero.controller
 * @ClassName: MeetingRoomController
 * @Description
 * @since 2024-01-25
 */
@RestController
@Api(value = "空间表管理", tags = "空间表管理")
@RequestMapping("/meeting/room")
public class MeetingRoomController {

    @Autowired
    private IMeetingRoomService meetingRoomService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<MeetingRoomDetailVO> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        MeetingRoomDetailVO entity = meetingRoomService.getDetailById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }


    @PostMapping("/page")
    @ApiOperation(value = "查询空间表分页列表")
    @ApiOperationSupport(order = 2)
    public JsonResult<PageInfo<MeetingRoomDetailVO>> queryPage(@RequestBody MeetingRoomDto meetingRoomDto) {
        meetingRoomDto.setStatus("1");
        if (CommonUtils.isNull(meetingRoomDto.getProvince())) {
            meetingRoomDto.setProvince("110000000000");
        }
        PageInfo<MeetingRoomDetailVO> pageInfo = meetingRoomService.queryPage(meetingRoomDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/statRoomProvinceByRoomType")
    @ApiOperation(value = "根据省份和roomType统计数量")
    @ApiOperationSupport(order = 3)
    public JsonResult<List<MeetingAreaStatVo>> statRoomProvinceByRoomType(@RequestBody RoomStatProvinceDto meetingRoomDto) {
        List<MeetingAreaStatVo> pageInfo = meetingRoomService.statRoomProvinceByRoomType(meetingRoomDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
