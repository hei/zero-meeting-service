package com.imflea.zero.controller.comments;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.anno.NoRepeatSubmit;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.comments.CommentsInfo;
import com.imflea.zero.model.entity.comments.dto.SaveOrUpdateCommentsInfoDto;
import com.imflea.zero.model.entity.comments.vo.QueryListVo;
import com.imflea.zero.service.comments.ICommentsInfoService;
import com.imflea.zero.service.comments.SingleAdoptSwitch;
import com.imflea.zero.service.comments.SingleAreatSwitch;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 互动评论表	 前端控制器
 * </p>
 *
 * @author guohui
 * @PackageName: com.imflea.zero.controller
 * @ClassName: CommentsInfoController
 * @Description
 * @since 2021-10-08
 */
@RestController
@Api(value = "互动评论表管理", tags = "互动评论表管理")
@RequestMapping("/commentsInfo")
public class CommentsInfoController {

    @Autowired
    private ICommentsInfoService commentsInfoService;


    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<CommentsInfo> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        CommentsInfo entity = commentsInfoService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }
    @NoRepeatSubmit(key = "createComment:arg[0]")
    @PostMapping("/save")
    @ApiOperation(value = "保存与修改互动评论表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<CommentsInfo> save( @RequestBody SaveOrUpdateCommentsInfoDto pam) {
        if (CommonUtils.isNull(pam) ||CommonUtils.isNull(pam.getComments()) ||CommonUtils.isNull(pam.getCommentBusinessType())) {
            return new JsonResult<>(ZeroContant.getFailCode(), "内容为空！");
        }
        String message = "评论成功";
        CommentsInfo entity = commentsInfoService.saveOrUpdateCommentsInfo(pam);
        if ("0".equals(pam.getCommentBusinessType())) {
            boolean open = SingleAreatSwitch.getInstance().getWordSwitch();
            if (open) {
                message = "评论成功，待审核";
            }
        }
        if ("1".equals(pam.getCommentBusinessType())) {
            boolean open = SingleAdoptSwitch.getInstance().getWordSwitch();
            if (open) {
                message = "评论成功，待审核";
            }
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), message, entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id删除互动评论表")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> removeByIdAndUserId(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        Boolean result = commentsInfoService.removeByIdAndUserId(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！", result);
    }

    @PostMapping("/queryList")
    @ApiOperation(value = "查看评论列表")
    @ApiOperationSupport(order = 3)
    public JsonResult<List<QueryListVo>> queryList(@RequestBody IdDto idDto) {
        if (CommonUtils.isNull(idDto) ||CommonUtils.isNull(idDto.getId())) {
            return new JsonResult<>(ZeroContant.getFailCode(), "参数为空！");
        }
        List<QueryListVo> result = commentsInfoService.queryList(idDto.getId());
        return new JsonResult<>(ZeroContant.getSuccessCode(), "查询成功！", result);
    }

}
