package com.imflea.zero.controller.invoice;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.imflea.zero.model.entity.invoice.InvoiceInfo;
import com.imflea.zero.model.entity.invoice.dto.InsertWxApplayDto;
import com.imflea.zero.model.entity.invoice.dto.InvoiceInfoDto;
import com.imflea.zero.model.entity.invoice.vo.InvoiceInfoDetailVo;
import com.imflea.zero.service.invoice.IInvoiceHeaderService;
import com.imflea.zero.service.invoice.IInvoiceInfoService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 发票信息 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: InvoiceInfoController
 * @Description
 * @since 2021-09-25
 */
@RestController
@Api(value = "发票信息管理", tags = "发票管理")
@RequestMapping("/invoice/info")
public class InvoiceInfoController {

    @Autowired
    private IInvoiceInfoService invoiceInfoService;
    @Autowired
    private IInvoiceHeaderService iInvoiceHeaderService;

    @PostMapping("/page")
    @ApiOperation(value = "发票信息--查询发票信息分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<InvoiceInfo>> queryByUserIdAndTenantId(@RequestBody InvoiceInfoDto invoiceInfoDto) {
        PageInfo<InvoiceInfo> pageInfo = invoiceInfoService.selectPage(invoiceInfoDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/save")
    @ApiOperation(value = "发票信息--保存与修改发票信息接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<InvoiceInfo> save(@RequestBody InsertWxApplayDto insertWxApplayDto) {
        if (CommonUtils.isNull(insertWxApplayDto) ||CommonUtils.isNull(insertWxApplayDto.getInvoiceHeaderId()) ||CommonUtils.isNull(insertWxApplayDto.getOrderId())) {
            return new JsonResult<>(ZeroContant.getSuccessCode(), "参数为空！");
        }
        InvoiceInfo entity = invoiceInfoService.insertWxApplay(insertWxApplayDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/queryByOrderId")
    @ApiOperation(value = "发票信息--根据订单id获取发票详细信息")
    @ApiOperationSupport(order = 3)
    public JsonResult<InvoiceInfoDetailVo> queryByOrderId(@RequestBody IdDto orderIdDto) {

        InvoiceInfo entity = invoiceInfoService.queryByOrderId(orderIdDto.getId());
        InvoiceHeader headerInfo = new InvoiceHeader();
        InvoiceInfoDetailVo vo = new InvoiceInfoDetailVo();
        if (CommonUtils.isNull(entity)) {
            vo.setHasReplay(false);
        } else {
            headerInfo = iInvoiceHeaderService.getById(entity.getInvoiceHeaderId());
            vo.setHasReplay(true);
            vo.setInvoiceHeader(headerInfo);
            vo.setInvoiceInfo(entity);
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "发票信息！", vo);
    }


}
