package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.meeting.MeetingShop;
import com.imflea.zero.model.entity.meeting.dto.MeetingShopDto;
import com.imflea.zero.service.meeting.IMeetingShopService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xby
 * @PackageName: com.imflea.zero.controller
 * @ClassName: MeetingShopController
 * @Description
 * @since 2024-01-24
 */
@RestController
@Api(value = "门店管理", tags = "门店管理")
@RequestMapping("/meeting/shop")
public class MeetingShopController {

    @Autowired
    private IMeetingShopService meetingShopService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<MeetingShop> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        MeetingShop entity = meetingShopService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }



    @PostMapping("/page")
    @ApiOperation(value = "查询分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<MeetingShop>> queryPage(@RequestBody MeetingShopDto meetingShopDto) {
        PageInfo<MeetingShop> pageInfo = meetingShopService.queryPage(meetingShopDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }


    @PostMapping("/list")
    @ApiOperation(value = "全部列表")
    @ApiOperationSupport(order = 5)
    public JsonResult<List<MeetingShop>> list(@RequestBody MeetingShopDto meetingShopDto) {
        List<MeetingShop> list = meetingShopService.queryAll(meetingShopDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), list);
    }

}
