package com.imflea.zero.controller.user;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.imflea.zero.model.entity.user.WebUserAccount;
import com.imflea.zero.model.entity.user.dto.WebUserAccountDto;
import com.imflea.zero.service.user.IWebUserAccountService;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.dto.IdDto;
import java.util.List;

/**
 * <p>
 * 微信端用户账户 前端控制器
 * </p>
 *
 * @author zzz
 * @since 2024-02-22
 * @PackageName: com.imflea.zero.controller
 * @ClassName: WebUserAccountController
 * @Description
 */
@RestController
@Api(value = "微信端用户账户管理", tags = "微信端用户账户管理")
@RequestMapping("/web-user-account")
public class WebUserAccountController {

    @Autowired
    private IWebUserAccountService webUserAccountService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<WebUserAccount> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        WebUserAccount entity = webUserAccountService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改微信端用户账户接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<WebUserAccount> save(@RequestBody WebUserAccount webUserAccount) {
        WebUserAccount entity = webUserAccountService.saveOrUpdateWebUserAccount(webUserAccount);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除微信端用户账户")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        webUserAccountService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询微信端用户账户分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<WebUserAccount>> queryPage(@RequestBody WebUserAccountDto webUserAccountDto) {
        PageInfo<WebUserAccount> pageInfo = webUserAccountService.queryPage(webUserAccountDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
