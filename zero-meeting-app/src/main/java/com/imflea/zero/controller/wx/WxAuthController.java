package com.imflea.zero.controller.wx;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.CodeDto;
import com.imflea.zero.dto.wx.WxInfoDto;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.entity.user.WebUserInfo;
import com.imflea.zero.model.entity.wx.vo.WxUserLoginVo;
import com.imflea.zero.service.wx.IWxAuthService;
import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller
 * @ClassName: WxAuthController
 * @Description 微信授权登录
 * @date 2021-09-13  10:16:44
 */
@RequestMapping("/wx/")
@RestController
@Api(value = "微信授权及分享", tags = "微信授权及分享")
public class WxAuthController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private IWxAuthService wxAuthService;


    @ApiOperation(value = "API10001：根据登录授权的code值,进行登录", notes = "授权登录")
    @PostMapping(value = "/login")
    @ApiOperationSupport(order = 1)
    public JsonResult<WxUserLoginVo> login(@RequestBody CodeDto codeDto) {
        // 授权（必填）
        String code = codeDto.getCode();
        if (CommonUtils.isNull(code)) {
            throw new BizException("授权code不能为空");
        }
        try {
            if (CommonUtils.isNull(ZeroCacheUtils.getString(code))) {
                ZeroCacheUtils.setString(code, code, 10);
            } else {
                throw new BizException("网络延时，请稍等");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), wxAuthService.saveOrLogin4WxAuth(code));


    }

    @ApiOperation(value = "API10002：获取手机号", notes = "授权登录-获取手机号")
    @PostMapping(value = "/getUserCellNum")
    @ApiOperationSupport(order = 2)
    public JsonResult<WebUserInfo> queryCellNumByEncodeStr(@RequestBody WxInfoDto reqInfo) {

        String encryptedData = reqInfo.getEncryptedData();
        String iv = reqInfo.getIv();
        String code = reqInfo.getCode();
        Map<String, Object> map = new HashMap<String, Object>();

        // 登录凭证不能为空
        if (CommonUtils.isNull(encryptedData)) {
            throw new BizException("密文不可为空");
        }
        if (CommonUtils.isNull(iv)) {
            throw new BizException("iv不能为空");
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.wxAuthService.updateUserDetailInfo(code, encryptedData, iv));
    }
}
