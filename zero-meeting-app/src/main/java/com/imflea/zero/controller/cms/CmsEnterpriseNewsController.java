package com.imflea.zero.controller.cms;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.model.entity.cms.dto.CmsEnterpriseNewsDto;
import com.imflea.zero.model.entity.cms.vo.EnterpriseNewsVo;
import com.imflea.zero.service.cms.ICmsEnterpriseNewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 * 企业资讯 前端控制器
 * </p>
 *
 * @author zhaoyang
 * @PackageName: com.imflea.zero.controller
 * @ClassName: CmsEnterpriseNewsController
 * @Description
 * @since 2021-09-24
 */
@RestController
@Api(value = "企业资讯管理", tags = "企业资讯管理")
@RequestMapping("/cms/enterprise/news")
public class CmsEnterpriseNewsController {

    @Autowired
    private ICmsEnterpriseNewsService cmsEnterpriseNewsService;
    @PostMapping("/page")
    @ApiOperation(value = "查询分页列表")
    @ApiOperationSupport(order = 1)
    public JsonResult<EnterpriseNewsVo> queryPage(@RequestBody CmsEnterpriseNewsDto cmsEnterpriseNewsDto) {
        EnterpriseNewsVo newsVo = cmsEnterpriseNewsService.queryPageFormEs(cmsEnterpriseNewsDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), newsVo);
    }


    @PostMapping("/content")
    @ApiOperation(value = "查询正文")
    @ApiOperationSupport(order = 2)
    public JsonResult<Map<String, Object>> detail(@RequestBody CmsEnterpriseNewsDto cmsEnterpriseNewsDto) {
        Map<String, Object> news = cmsEnterpriseNewsService.queryNewsFromEs(cmsEnterpriseNewsDto.getNewsId());
        return new JsonResult<>(ZeroContant.getSuccessCode(), news);
    }


}
