//package com.imflea.zero.controller.price;
//
//import com.imflea.zero.constants.ZeroContant;
//import com.imflea.zero.util.base.JsonResult;
//import com.imflea.zero.model.entity.price.PriceCalculateDto;
//import com.imflea.zero.model.entity.price.SemCouponCalculateDto;
//import com.imflea.zero.model.entity.price.vo.PriceDetailVo;
//import com.imflea.zero.service.price.IPriceService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.math.BigDecimal;
//
//@RestController
//@Api(value = "价格计算前端接口", tags = "价格计算前端接口")
//@RequestMapping("/price")
//public class PriceController {
//    private final Logger logger = LoggerFactory.getLogger(PriceController.class);
//
//    @Autowired
//    IPriceService priceService;
//
//    @PostMapping("/calculatePrice")
//    @ApiOperation(value = "计算价格")
//    public JsonResult<BigDecimal> calculatePrice(@RequestBody PriceCalculateDto priceCalculateDto) {
//        BigDecimal result = priceService.calculatePrice(priceCalculateDto);
//        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功", result);
//    }
//
//    @PostMapping("/calculate4SemCoupon")
//    @ApiOperation(value = "促销计算价格")
//    public JsonResult<BigDecimal> calculate4SemCoupon(@RequestBody SemCouponCalculateDto semCouponCalculateDto) {
//        BigDecimal result = priceService.calculatePrice(semCouponCalculateDto);
//        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功", result);
//    }
//
//    @PostMapping("/listAndCalculate4SemCoupon")
//    @ApiOperation(value = "促销计算价格:优惠券；且返回 可用，不可用区券")
//    public JsonResult<PriceDetailVo> listAndCalculate4SemCoupon(@RequestBody SemCouponCalculateDto semCouponCalculateDto) {
//        PriceDetailVo result = priceService.listAndCalculate4SemCoupon(semCouponCalculateDto);
//        return new JsonResult<>(ZeroContant.getSuccessCode(), "成功计算价格", result);
//    }
//
//    @PostMapping("/listAndCalculate4GiftCard")
//    @ApiOperation(value = "促销计算价格: 礼金卡")
//    public JsonResult<PriceDetailVo> listAndCalculate4GiftCard(@RequestBody SemCouponCalculateDto semCouponCalculateDto) {
//        PriceDetailVo result = priceService.listAndCalculate4GiftCard(semCouponCalculateDto);
//        return new JsonResult<>(ZeroContant.getSuccessCode(), "成功计算价格", result);
//    }
//
//    @PostMapping("/calculate4Sem")
//    @ApiOperation(value = "促销计算价格: 礼金卡|优惠券|原价")
//    public JsonResult<PriceDetailVo> calculate4Sem(@RequestBody SemCouponCalculateDto semCouponCalculateDto) {
////        logger.error("参数信息：------------------");
////
////        try {
////            logger.error(ZeroJsonUtils.entityToJson(semCouponCalculateDto));
////            logger.error("tenantId:" + SessionUtils.getTenantId());
////            logger.error("token:" + SessionUtils.getAccessToken());
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//
//        PriceDetailVo result = priceService.calculate(semCouponCalculateDto);
//        return new JsonResult<>(ZeroContant.getSuccessCode(), "成功计算价格", result);
//    }
//}
