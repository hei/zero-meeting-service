package com.imflea.zero.controller.user;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.user.WebUserFriend;
import com.imflea.zero.model.entity.user.dto.FriendQueryDto;
import com.imflea.zero.model.entity.user.vo.UserFriendVo;
import com.imflea.zero.service.user.IWebUserFriendService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 好友关系表 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: WebUserFriendController
 * @Description
 * @since 2021-09-23
 */
@RestController
@Api(value = "好友关系表管理", tags = "好友关系表管理")
@RequestMapping("/web/friend")
public class WebUserFriendController {

    @Autowired
    private IWebUserFriendService webUserFriendService;


    @PostMapping("/save")
    @ApiOperation(value = "保存好友关系表接口")
    @ApiOperationSupport(order = 1)
    public JsonResult<WebUserFriend> save(@RequestBody WebUserFriend webUserFriend) {
        WebUserFriend entity = webUserFriendService.saveFriend(webUserFriend);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }


    @PostMapping("/remove")
    @ApiOperation(value = "根据id删除好友关系表")
    @ApiOperationSupport(order = 2)
    public JsonResult<Boolean> remove(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        boolean result = webUserFriendService.removeFriendsById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), result);
    }

    @PostMapping("/queryAll")
    @ApiOperation(value = "查询好友关系表列表,不分页")
    @ApiOperationSupport(order = 3)
    public JsonResult<List<UserFriendVo>> queryAll(@RequestBody FriendQueryDto friendQueryDto) {
        List<UserFriendVo> friends = webUserFriendService.queryAll(friendQueryDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), friends);
    }

    @PostMapping("/updateRemarksName")
    @ApiOperation(value = "修改昵称")
    @ApiOperationSupport(order = 4)
    public JsonResult<WebUserFriend> updateRemarksName(@RequestBody WebUserFriend webUserFriend) {
        WebUserFriend entity = webUserFriendService.saveOrUpdateWebUserFriend(webUserFriend);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

}
