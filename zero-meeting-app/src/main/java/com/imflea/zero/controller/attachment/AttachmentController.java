package com.imflea.zero.controller.attachment;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.qx.QxAttachment;
import com.imflea.zero.model.entity.qx.dto.AttachmentDetailDto;
import com.imflea.zero.model.entity.qx.dto.BusinessIdDto;
import com.imflea.zero.service.qx.IQxAttachmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: AttachmentController
 * @Description
 * @date 2021-08-27  10:59:00
 */
@RestController
@RequestMapping("/attachment")
@Api(value = "附件管理-公共部分", tags = "附件管理-公共部分")
public class AttachmentController {

    @Autowired
    private IQxAttachmentService attachmentService;

    @ApiOperation(value = "API10000：保存-资源文件", notes = "API10000：保存-资源文件")
    @PostMapping("/saveOrUpdateAttachment")
    @ApiOperationSupport(order = 10000)
    public JsonResult<QxAttachment> saveOrUpdateAttachment(@RequestBody @Valid QxAttachment attchment) {

        return new JsonResult(ZeroContant.getSuccessCode(), attachmentService.saveOrUpdateAttachment(attchment));

    }

    @ApiOperation(value = "API10001：批量保存-资源文件", notes = "API10001：批量保存-资源文件")
    @PostMapping("/saveOrUpdateAttachmentBatch")
    @ApiOperationSupport(order = 10001)
    public JsonResult<Boolean> saveOrUpdateAttachmentBatch(@RequestBody @Valid List<QxAttachment> attchments) {
        return new JsonResult(ZeroContant.getSuccessCode(), attachmentService.saveAttachmentBatch(attchments));
    }

    @ApiOperation(value = "API100002：根据资源ID删除资源", notes = "API100002：根据资源ID删除资源")
    @PostMapping("/removeById")
    @ApiOperationSupport(order = 10002)
    public JsonResult<Boolean> removeById(@RequestBody @Valid IdDto idReq) {
        return new JsonResult(ZeroContant.getSuccessCode(), attachmentService.removeByAttachmentById(idReq.getId()));
    }

    @ApiOperation(value = "API100003：根据业务ID，模块code，系统code 删除资源", notes = "API100003：根据业务ID，模块code，系统code 删除资源")
    @PostMapping("/removeBuinessId")
    @ApiOperationSupport(order = 10003)
    public JsonResult<Boolean> removeBusinessId(@RequestBody @Valid AttachmentDetailDto delReq) {

        return new JsonResult(ZeroContant.getSuccessCode(), this.attachmentService.removeByBusinessId(delReq));

    }

    @ApiOperation(value = "API100004：根据业务ID，获取资源列表", notes = "API100004：根据业务ID，获取资源列表")
    @PostMapping("/queryAttachmentsByBuinessId")
    @ApiOperationSupport(order = 10004)
    public JsonResult<List<QxAttachment>> queryAttachmentsByBuinessId(@RequestBody @Valid BusinessIdDto businessIdDto) {
        return new JsonResult(ZeroContant.getSuccessCode(), this.attachmentService.queryAttachmentsByBuinessId(businessIdDto.getBusinessId()));
    }

    @ApiOperation(value = "API100005：根据业务ID，模块code，系统code 请求资源", notes = "API100005：根据业务ID，模块code，系统code 请求资源")
    @PostMapping("/queryAttachmentsByBuinessIdAndModule")
    @ApiOperationSupport(order = 10005)
    public JsonResult<List<QxAttachment>> queryAttachmentsByBuinessIdAndModule(@RequestBody @Valid AttachmentDetailDto queryReq) {

        return new JsonResult(ZeroContant.getSuccessCode(), this.attachmentService.queryAttachmentsByBuinessIdAndModule(queryReq));

    }

    @ApiOperation(value = "API100006：根据资源ID，获取资源", notes = "API100006：根据资源ID，获取资源")
    @PostMapping("/queryAttachmentId")
    @ApiOperationSupport(order = 10006)
    public JsonResult<QxAttachment> queryAttachmentsById(@RequestBody @Valid IdDto idReq) {
        return new JsonResult(ZeroContant.getSuccessCode(), this.attachmentService.queryAttachmentsById(idReq.getId()));

    }
}
