package com.imflea.zero.controller.pay;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.model.entity.pay.dto.OrderSnDto;
import com.imflea.zero.model.entity.pay.vo.PayRecorderDetailVo;
import com.imflea.zero.service.pay.IPayTransportProofService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 支付记录 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: PayRecoderInfoController
 * @Description
 * @since 2021-09-27
 */
@RestController
@Api(value = "支付记录管理", tags = "支付记录管理")
@RequestMapping("/pay/recorderInfo")
public class PayRecorderInfoController {

    @Autowired
    private IPayTransportProofService payTransportProofService;

    @PostMapping("/queryByOrderSn")
    @ApiOperation(value = "根据订单号获取支付记录结果")
    @ApiOperationSupport(order = 1)
    public JsonResult<PayRecorderDetailVo> queryByOrderSn(@RequestBody OrderSnDto orderSn) {
        String sn = orderSn.getOrderSn();
        PayRecorderDetailVo vo = payTransportProofService.queryPayDetailRecorderInfo(sn);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", vo);
    }

}
