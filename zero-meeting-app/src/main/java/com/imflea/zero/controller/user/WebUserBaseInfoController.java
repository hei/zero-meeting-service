package com.imflea.zero.controller.user;


import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.entity.user.WebUserInfo;
import com.imflea.zero.model.entity.user.dto.OpenIdDto;
import com.imflea.zero.model.entity.user.vo.WebUserInfoDetailVo;
import com.imflea.zero.service.user.IWebUserInfoService;
import com.imflea.zero.service.wx.IWxAuthService;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.utils.SessionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 微信端用户基本信息 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: WebUserInfoController
 * @Description
 * @since 2021-09-13
 */
@RestController
@Api(value = "微信端用户", tags = "微信端用户")
@RequestMapping("/web/user")
public class WebUserBaseInfoController {

    @Autowired
    private IWebUserInfoService webUserInfoService;
    @Autowired
    private IWxAuthService wxAuthService;

    @PostMapping("/save")
    @ApiOperation(value = "微信端用户基本信息修改接口")
    @ApiOperationSupport(order = 1)
    public JsonResult<WebUserInfo> save(@RequestBody WebUserInfo webUserInfo) {
        webUserInfoService.updateWebUserInfo(webUserInfo);
        try {
            wxAuthService.refreshlogInUtils(SessionUtils.getAccessToken(), SessionUtils.getTenantId(), webUserInfo.getNickName(), webUserInfo.getAvatarUrl());
        } catch (Exception e) {
            throw new BizException("刷新异常");
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！");
    }

    @PostMapping("/queryById")
    @ApiOperation(value = "根据Id获取用户的基本信息")
    @ApiOperationSupport(order = 2)
    public JsonResult<WebUserInfoDetailVo> queryById(@RequestBody IdDto idDto) {
        WebUserInfoDetailVo webUserInfo = webUserInfoService.getWebUserDetailById(idDto);

        return new JsonResult<>(ZeroContant.getSuccessCode(), webUserInfo);
    }

    @PostMapping("/queryByOpenId")
    @ApiOperation(value = "根据OpendId获取用户的基本信息")
    @ApiOperationSupport(order = 3)
    public JsonResult<WebUserInfo> queryByOpenId(@RequestBody OpenIdDto openIdDto) {
        WebUserInfo webUserInfo = webUserInfoService.queryByOpenId(openIdDto.getOpenId());

        return new JsonResult<>(ZeroContant.getSuccessCode(), webUserInfo);
    }

}
