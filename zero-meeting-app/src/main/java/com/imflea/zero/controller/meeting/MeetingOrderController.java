package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.meeting.MeetingOrderInfo;
import com.imflea.zero.model.entity.meeting.dto.*;
import com.imflea.zero.model.entity.meeting.vo.DateTime4ItemStatusVO;
import com.imflea.zero.service.meeting.IMeetingOrderInfoService;
import com.imflea.zero.service.user.IWebUserInfoService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "空间租赁订单", tags = "空间租赁订单")
@RequestMapping("/meeting/order")
public class MeetingOrderController {

    @Autowired
    IMeetingOrderInfoService meetingOrderInfoService;

    @Autowired
    private IWebUserInfoService webUserInfoService;


    @PostMapping("/generateOrder")
    @ApiOperation(value = "空间--生成订单")
    public JsonResult<MeetingOrderInfo> saveOrder(@RequestBody MeetingOrderAppSaveDto meetingOrderAppSaveDto) {
        MeetingOrderInfo result = meetingOrderInfoService.saveOrUpdateMeetingRoomInfo(meetingOrderAppSaveDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }

    @PostMapping("/generateRechargeOrder")
    @ApiOperation(value = "充值--生成订单")
    public JsonResult<MeetingOrderInfo> generateRechargeOrder(@RequestBody RechargeOrderAppSaveDto meetingOrderAppSaveDto) {
        MeetingOrderInfo result = meetingOrderInfoService.saveOrUpdateRechargeInfo(meetingOrderAppSaveDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }

    @PostMapping("/update2Cancel")
    @ApiOperation(value = "空间/充值--订单--取消订单")
    public JsonResult<Boolean> update2Cancel(@RequestBody IdDto idDto) {
        String orderId = idDto.getId();
        boolean result = meetingOrderInfoService.changeOrder2Cancel(orderId);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }

    @PostMapping("/pay2RoomOrder")
    @ApiOperation(value = "空间--订单-支付")
    public JsonResult<Boolean> updatePay2RoomOrder(@RequestBody IdDto idDto) {
        String orderId = idDto.getId();
        boolean result = meetingOrderInfoService.updatePay2RoomOrder(orderId);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }


    @PostMapping("/queryOrderList")
    @ApiOperation(value = "空间/充值--获取订单集合")
    public JsonResult<PageInfo<MeetingOrderInfo>> queryOrderList(@RequestBody MeetingOrderInfoDto dto) {
//        Integer orderStatus = adoptOrderQueryDto.getOrderStatus();
//        MeetingOrderInfoDto dto = new MeetingOrderInfoDto();
//        dto.setPageNum(adoptOrderQueryDto.getPageNum());
//        dto.setPageSize(adoptOrderQueryDto.getPageSize());
//        dto.setOrderStatus(orderStatus);
        PageInfo<MeetingOrderInfo> result = meetingOrderInfoService.queryPage4App(dto);
//        List<MeetingOrderInfo> curPageData = result.getList();
//        for (MeetingOrderInfo info : curPageData) {
//            List<MeetingAlbum> zhutuList = adoptAlbumService.queryList(info.getAdoptId(), "0");
//            String url = "";
//            if (zhutuList != null && zhutuList.size() == 1) {
//                url = zhutuList.get(0).getPicTotalUrl();
//            }
//            info.setImgUrl(url);
//        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }

    @PostMapping("/queryOrderById")
    @ApiOperation(value = "空间/充值--根据ID获取订单")
    public JsonResult<MeetingOrderInfo> queryOrderById(@RequestBody IdDto idDto) {
        String orderId = idDto.getId();
        MeetingOrderInfo order = meetingOrderInfoService.getById(orderId);
//        List<MeetingAlbum> zhutuList = adoptAlbumService.queryList(order.getAdoptId(), "0");
//        String url = "";
//        if (zhutuList != null && zhutuList.size() == 1) {
//            url = zhutuList.get(0).getPicTotalUrl();
//        }
//        order.setImgUrl(url);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", order);
    }

    @PostMapping("/getCanOrderTimeByDay")
    @ApiOperation(value = "根据日期获取可预定时间")
    public JsonResult<List<DateTime4ItemStatusVO>> getCanOrderTimeByDay(@RequestBody MeetingRoomDayTimeDto dayTimeDto) {
        List<DateTime4ItemStatusVO> result = meetingOrderInfoService.getCanOrderTimeByDate(dayTimeDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }

    @PostMapping("/checkRoomTime")
    @ApiOperation(value = "根据开始结束时间判断是否可用")
    public JsonResult<Boolean> check(@RequestBody MeetingRoomTimeCheckDto dayTimeDto) {
    Boolean result = meetingOrderInfoService.check(dayTimeDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }


//    @PostMapping("/queryAllOrders")
//    @ApiOperation(value = "空间--获取所有订单")
//    public JsonResult<PageInfo<MeetingOrderInfo>> queryAllOrders(@RequestBody MeetingOrderPageQueryDto dto) {
//        String adoptId = dto.getId();
//        QueryWrapper<MeetingOrderInfo> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("biz_id", adoptId);
//        queryWrapper.eq("order_status", 3);
//        queryWrapper.orderByDesc("commit_time");
//        Integer pageIndex = dto.getPageNum();
//        Integer pageSize = dto.getPageSize();
//        PageHelper.startPage(pageIndex, pageSize);
//
//        List<MeetingOrderInfo> result = meetingOrderInfoService.list(queryWrapper);
//        PageInfo<MeetingOrderInfo> pageInfo = new PageInfo<>(result);
//        PageInfo<MeetingOrderInfo> pageInfoResult = new PageInfo<>();
//
//        pageInfoResult.setTotal(pageInfo.getTotal());
//        pageInfoResult.setPageNum(pageInfo.getPageNum());
//        pageInfoResult.setPageSize(pageInfo.getPageSize());
//        if (!CommonUtils.isNull(result)) {
//            List<String> memberIds = result.stream().map(item -> item.getMemberId()).collect(Collectors.toList());
//            List<WebUserInfo> members = webUserInfoService.listByIds(memberIds);
//            Map<String, String> memberMap = members.stream().collect(Collectors.toMap(WebUserInfo::getId, WebUserInfo::getAvatarUrl));
//            List<MeetingOrderInfo> resultVo = new ArrayList<>();
//            for (MeetingOrderInfo orderInfo : result) {
//                MeetingOrderInfo vo = new MeetingOrderInfo();
//                BeanUtil.copyProperties(orderInfo, vo);
////               vo.setUserAvater(memberMap.get(orderInfo.getMemberId()));
//                resultVo.add(vo);
//            }
//            pageInfoResult.setList(resultVo);
//        }
//        return new JsonResult<>(ZeroContant.getSuccessCode(), "", pageInfoResult);
//    }

}
