package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.meeting.MeetingAlbum;
import com.imflea.zero.model.entity.meeting.dto.MeetingAlbumDto;
import com.imflea.zero.service.meeting.IMeetingAlbumService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 画廊 前端控制器
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 * @PackageName: com.imflea.zero.controller
 * @ClassName: MeetingAlbumController
 * @Description
 */
@RestController
@Api(value = "画廊管理", tags = "画廊管理")
@RequestMapping("/meeting/album")
public class MeetingAlbumController {

    @Autowired
    private IMeetingAlbumService meetingAlbumService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<MeetingAlbum> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        MeetingAlbum entity = meetingAlbumService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }
    @PostMapping("/pageAlb")
    @ApiOperation(value = "查询画廊分页列表-正确的接口")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<MeetingAlbum>> pageAlb(@RequestBody MeetingAlbumDto meetingAlbumDto) {
        PageInfo<MeetingAlbum> pageInfo = meetingAlbumService.queryPage(meetingAlbumDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询画廊分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<MeetingAlbum>> queryPage(@RequestBody MeetingAlbumDto meetingAlbumDto) {
        PageInfo<MeetingAlbum> pageInfo = meetingAlbumService.queryPage(meetingAlbumDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
