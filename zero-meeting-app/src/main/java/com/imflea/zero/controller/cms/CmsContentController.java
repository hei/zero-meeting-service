package com.imflea.zero.controller.cms;


import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.model.entity.cms.dto.CmsTemplateDto;
import com.imflea.zero.model.entity.cms.vo.CmsContentVo;
import com.imflea.zero.service.cms.ICmsTemplateService;
import com.imflea.zero.utils.RestUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 发布模板信息 前端控制器
 * </p>
 *
 * @author zhaoyang
 * @PackageName: com.imflea.zero.controller
 * @ClassName: CmsContentAPI
 * @Description 查询发布内容信息
 * @since 2021-09-17
 */
@RestController
@Api(value = "内容发布", tags = "内容发布")
@RequestMapping("/cms/content")
public class CmsContentController {

    @Autowired
    private ICmsTemplateService cmsTemplateService;


    @ApiOperation(value = "根据publishType,contentType,scope找到符合条件的单条记录")
    @ApiOperationSupport(order = 1)
    @PostMapping("/info")
    public JsonResult<CmsContentVo> queryOneFromRedis(@RequestBody CmsTemplateDto dto) {
        CmsContentVo entity = cmsTemplateService.queryOneFromRedis(dto);
        return RestUtil.success(entity);
    }


    @ApiOperation(value = "根据publishType,contentType,scope,topN找到符合条件的n条记录")
    @ApiOperationSupport(order = 2)
    @PostMapping("/list")
    public JsonResult<List<CmsContentVo>> queryListFromRedis(@RequestBody CmsTemplateDto dto) {
        List<CmsContentVo> contentVoList = cmsTemplateService.queryListFromRedis(dto);
        return RestUtil.success(contentVoList);
    }


    @ApiOperation(value = "查询详情")
    @ApiOperationSupport(order = 2)
    @PostMapping("/detail")
    public JsonResult<CmsContentVo> queryContentFromRedis(@RequestBody CmsTemplateDto dto) {
        CmsContentVo vo = cmsTemplateService.queryContentFromRedis(dto);
        return RestUtil.success(vo);
    }

}
