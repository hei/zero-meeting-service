package com.imflea.zero.controller.invoice;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.imflea.zero.model.entity.invoice.dto.SaveDto;
import com.imflea.zero.service.invoice.IInvoiceHeaderService;
import com.imflea.zero.service.invoice.impl.InvoiceHeaderServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 发表抬头 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: InvoiceHeaderController
 * @Description
 * @since 2021-09-25
 */
@RestController
@Api(value = "发票抬头管理", tags = "发票管理")
@RequestMapping("/invoice/header")
public class InvoiceHeaderController {

    @Autowired
    private IInvoiceHeaderService invoiceHeaderService;

    @PostMapping("/queryById")
    @ApiOperation(value = "发票抬头--根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<InvoiceHeader> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        InvoiceHeader entity = invoiceHeaderService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "发票抬头--保存与修改发表抬头接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<InvoiceHeader> save(@RequestBody SaveDto saveDto) {
        if (CommonUtils.isNull(saveDto.getEmail()) || !InvoiceHeaderServiceImpl.isEmail(saveDto.getEmail())) {
            return new JsonResult<>(ZeroContant.getFailCode(), "邮箱格式不正确", null);
        }
        InvoiceHeader entity = invoiceHeaderService.saveOrUpdateInvoiceHeader(saveDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/list")
    @ApiOperation(value = "发票抬头--查询发表抬头列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<List<InvoiceHeader>> queryList() {
        List<InvoiceHeader> pageInfo = invoiceHeaderService.queryList();
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/removeById")
    @ApiOperation(value = "发票抬头--根据发票抬头Id删除抬头")
    @ApiOperationSupport(order = 5)
    public JsonResult<Boolean> save(@RequestBody IdDto idDto) {
        if (CommonUtils.isNull(idDto) ||CommonUtils.isNull(idDto.getId())) {
            return new JsonResult<>(ZeroContant.getSuccessCode(), "参数为空！");
        }
        boolean entity = invoiceHeaderService.removeById(idDto.getId());
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！", entity);
    }
}
