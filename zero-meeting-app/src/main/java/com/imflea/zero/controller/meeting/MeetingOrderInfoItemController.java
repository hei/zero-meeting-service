package com.imflea.zero.controller.meeting;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.imflea.zero.model.entity.meeting.MeetingOrderInfoItem;
import com.imflea.zero.model.entity.meeting.dto.MeetingOrderInfoItemDto;
import com.imflea.zero.service.meeting.IMeetingOrderInfoItemService;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.dto.IdDto;
import java.util.List;

/**
 * <p>
 * 空间订单表-分项表 前端控制器
 * </p>
 *
 * @author zzz
 * @since 2024-03-03
 * @PackageName: com.imflea.zero.controller
 * @ClassName: MeetingOrderInfoItemController
 * @Description
 */
@RestController
@Api(value = "空间订单表-分项表管理", tags = "空间订单表-分项表管理")
@RequestMapping("/meetingOrder/item")
public class MeetingOrderInfoItemController {

    @Autowired
    private IMeetingOrderInfoItemService meetingOrderInfoItemService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<MeetingOrderInfoItem> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        MeetingOrderInfoItem entity = meetingOrderInfoItemService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改空间订单表-分项表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<MeetingOrderInfoItem> save(@RequestBody MeetingOrderInfoItem meetingOrderInfoItem) {
        MeetingOrderInfoItem entity = meetingOrderInfoItemService.saveOrUpdateMeetingOrderInfoItem(meetingOrderInfoItem);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除空间订单表-分项表")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        meetingOrderInfoItemService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询空间订单表-分项表分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<MeetingOrderInfoItem>> queryPage(@RequestBody MeetingOrderInfoItemDto meetingOrderInfoItemDto) {
        PageInfo<MeetingOrderInfoItem> pageInfo = meetingOrderInfoItemService.queryPage(meetingOrderInfoItemDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
