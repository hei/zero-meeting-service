package com.imflea.zero.controller.user;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.user.UserFarmUpdateAvatarDto;
import com.imflea.zero.dto.user.UserFarmUpdateNameDto;
import com.imflea.zero.dto.user.UserFarmUpdateSloganDto;
import com.imflea.zero.model.entity.user.WebUserFarmyard;
import com.imflea.zero.service.user.IWebUserFarmyardService;
import com.imflea.zero.utils.SessionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户农场表 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: WebUserFarmyardController
 * @Description
 * @since 2021-09-24
 */
@RestController
@Api(value = "用户农场表管理", tags = "用户农场表管理")
@RequestMapping("/web/user/farmyard")
public class WebUserFarmyardController {

    @Autowired
    private IWebUserFarmyardService webUserFarmyardService;

    @PostMapping("/queryFarm")
    @ApiOperation(value = "获取当前用户农场")
    @ApiOperationSupport(order = 1)
    public JsonResult<WebUserFarmyard> queryFarm() {
        String userId = SessionUtils.getUserId();
        Map<String, Object> columnMap = new HashMap<>();
        columnMap.put("web_user_id", userId);
        columnMap.put("tenant_id", SessionUtils.getTenantId());
        List<WebUserFarmyard> farmyards = webUserFarmyardService.listByMap(columnMap);
        WebUserFarmyard result = new WebUserFarmyard();
        if (farmyards != null && farmyards.size() > 0) {
            result = farmyards.get(0);
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }

    @PostMapping("/updateFarmName")
    @ApiOperation(value = "更新农场名")
    @ApiOperationSupport(order = 2)
    public JsonResult<Boolean> updateFarmName(@RequestBody UserFarmUpdateNameDto userFarmUpdateNameDto) {
        WebUserFarmyard entity = webUserFarmyardService.getById(userFarmUpdateNameDto.getFarmId());
        entity.setFarmName(userFarmUpdateNameDto.getFarmName());
        Boolean result = webUserFarmyardService.saveOrUpdate(entity);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", result);
    }

    @PostMapping("/updateFarmSlogan")
    @ApiOperation(value = "更新农场个性签名")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> updateFarmSlogan(@RequestBody UserFarmUpdateSloganDto userFarmUpdateSloganDto) {
        WebUserFarmyard entity = webUserFarmyardService.getById(userFarmUpdateSloganDto.getFarmId());
        entity.setFarmSlogan(userFarmUpdateSloganDto.getFarmSlogan());
        Boolean result = webUserFarmyardService.saveOrUpdate(entity);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", result);
    }

    @PostMapping("/updateFarmAvatar")
    @ApiOperation(value = "更新农场头像")
    @ApiOperationSupport(order = 4)
    public JsonResult<Boolean> updateFarmAvatar(@RequestBody UserFarmUpdateAvatarDto userFarmUpdateAvatarDto) {
        WebUserFarmyard entity = webUserFarmyardService.getById(userFarmUpdateAvatarDto.getFarmId());
        entity.setFarmAvatarId(userFarmUpdateAvatarDto.getFarmAvatarId());
        entity.setFarmAvatarUrl(userFarmUpdateAvatarDto.getFarmAvatarUrl());
        Boolean result = webUserFarmyardService.saveOrUpdate(entity);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", result);
        //废品率 =报废数量/汇报任务生产数量
        //disNumRate = cdiscardqty/cplanqty
    }

}
