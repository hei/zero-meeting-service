package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.meeting.MeetingCouponRule;
import com.imflea.zero.model.entity.meeting.dto.MeetingCouponRuleDto;
import com.imflea.zero.service.meeting.IMeetingCouponRuleService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 规则表 前端控制器
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 * @PackageName: com.imflea.zero.controller
 * @ClassName: MeetingCouponRuleController
 * @Description
 */
@RestController
@Api(value = "规则表管理", tags = "规则表管理")
@RequestMapping("/meeting/couponRule")
public class MeetingCouponRuleController {

    @Autowired
    private IMeetingCouponRuleService meetingCouponRuleService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<MeetingCouponRule> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        MeetingCouponRule entity = meetingCouponRuleService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改规则表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<MeetingCouponRule> save(@RequestBody MeetingCouponRule meetingCouponRule) {
        MeetingCouponRule entity = meetingCouponRuleService.saveOrUpdateMeetingCouponRule(meetingCouponRule);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除规则表")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        meetingCouponRuleService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询规则表分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<MeetingCouponRule>> queryPage(@RequestBody MeetingCouponRuleDto meetingCouponRuleDto) {
        PageInfo<MeetingCouponRule> pageInfo = meetingCouponRuleService.queryPage(meetingCouponRuleDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
