package com.imflea.zero.controller.tenant;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.model.entity.qx.QxTenantConfig;
import com.imflea.zero.service.qx.IQxTenantConfigService;
import com.imflea.zero.utils.SessionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 租户配置表 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: QxTenantConfigController
 * @Description
 * @since 2021-09-23
 */
@RestController
@Api(value = "租户配置表管理", tags = "租户配置表管理")
@RequestMapping("/tenant/config")
public class QxTenantConfigController {

    @Autowired
    private IQxTenantConfigService qxTenantConfigService;

    @PostMapping("/queryTransferInfoBank")
    @ApiOperation(value = "获取当前租户的对公账户信息")
    @ApiOperationSupport(order = 1)
    public JsonResult<QxTenantConfig> queryTransferInfoBankByTenantId() {
        String tenantId = SessionUtils.getTenantId();
        QxTenantConfig entity = qxTenantConfigService.queryTransferInfoBankByTenantId(tenantId);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }


}
