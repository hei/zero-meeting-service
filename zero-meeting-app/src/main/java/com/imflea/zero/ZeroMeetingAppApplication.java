package com.imflea.zero;


import com.imflea.zero.filter.WxSessionFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
//@ComponentScan(basePackages = {"com.imflea.*"})
@EnableTransactionManagement
public class ZeroMeetingAppApplication {

    public static void main(String[] args) {

        SpringApplication.run(ZeroMeetingAppApplication.class, args);
    }

    @Bean
    public FilterRegistrationBean<WxSessionFilter> registSSOAuthenticationFilter() {
        FilterRegistrationBean<WxSessionFilter> registration = new FilterRegistrationBean<>();
        // 注入过滤器
        registration.setFilter(new WxSessionFilter());
        // 初始化参数 |.*(meeting).*
        registration.addInitParameter("ignorePattern", ".*(doc.html).*|.*(login).*|.*(api).*|.*(/meeting/room/queryById).*");
        // 拦截规则
        registration.addUrlPatterns("/*");
        // 过滤器名称
        registration.setName("WxSessionFilter");
        // 过滤器顺序
        registration.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE);
        return registration;
    }

    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", buildConfig());
        return new CorsFilter(source);
    }

}
