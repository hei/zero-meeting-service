package com.imflea.zero.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableKnife4j
@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
// 在 Swagger 3.X 以下版本报错时可以加此注解解决，但是在3.X版本以上的，加此注解会导致页面无法打开
public class Knife4jConfig {
    private String title = "会议室项目";
    private String description = "会议室项目";
    private String termsOfServiceUrl = "https://www.imflea.com";
    private String version = "1.0";
    private String author = "祥保玉";
    private String email = "xiangbaoyu0504@163.com";
    private String groupName = "会议室-manager";

    @Bean
    public Docket createUser() {
        return new Docket(DocumentationType.OAS_30)
                .enable(true)
                // .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
//                .groupName(this.groupName)
                .groupName("微信用户")
                .select()
                // 方式一: 配置扫描 所有想在swagger界面的统一管理接口，都必须在此包下
                .apis(RequestHandlerSelectors.basePackage("com.imflea.zero.controller.user"))
//                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                // 方式二: 只有当方法上有  @ApiOperation 注解时才能生成对应的接口文档
                // .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket createInvoice() {
        return new Docket(DocumentationType.OAS_30)
                .enable(true)
                // .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
//                .groupName(this.groupName)
                .groupName("发票抬头管理")
                .select()
                // 方式一: 配置扫描 所有想在swagger界面的统一管理接口，都必须在此包下
                .apis(RequestHandlerSelectors.basePackage("com.imflea.zero.controller.invoice"))
//                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                // 方式二: 只有当方法上有  @ApiOperation 注解时才能生成对应的接口文档
                // .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket createWx() {
        return new Docket(DocumentationType.OAS_30)
                .enable(true)
                // .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
//                .groupName(this.groupName)
                .groupName("微信登录")
                .select()
                // 方式一: 配置扫描 所有想在swagger界面的统一管理接口，都必须在此包下
                .apis(RequestHandlerSelectors.basePackage("com.imflea.zero.controller.wx"))
//                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                // 方式二: 只有当方法上有  @ApiOperation 注解时才能生成对应的接口文档
                // .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket createBaseApi() {
        return new Docket(DocumentationType.OAS_30)
                .enable(true)
                .apiInfo(apiInfo())
                .groupName("OpenAPI")
                .select()
                // 方式一: 配置扫描 所有想在swagger界面的统一管理接口，都必须在此包下
                .apis(RequestHandlerSelectors.basePackage("com.imflea.zero.api"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket createCMSApi() {
        return new Docket(DocumentationType.OAS_30)
                .enable(true)
                .apiInfo(apiInfo())
                .groupName("CMS接口")
                .select()
                // 方式一: 配置扫描 所有想在swagger界面的统一管理接口，都必须在此包下
                .apis(RequestHandlerSelectors.basePackage("com.imflea.zero.controller.cms"))
                .paths(PathSelectors.any())
                .build();
    }
    @Bean
    public Docket wxPayApi() {
        return new Docket(DocumentationType.OAS_30)
                .enable(true)
                .apiInfo(apiInfo())
                .groupName("微信支付接口")
                .select()
                // 方式一: 配置扫描 所有想在swagger界面的统一管理接口，都必须在此包下
                .apis(RequestHandlerSelectors.basePackage("com.imflea.zero.controller.pay"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket createMeetingApi() {
        return new Docket(DocumentationType.OAS_30)
                .enable(true)
                .apiInfo(apiInfo())
                .groupName("会议室相关接口")
                .select()
                // 方式一: 配置扫描 所有想在swagger界面的统一管理接口，都必须在此包下
                .apis(RequestHandlerSelectors.basePackage("com.imflea.zero.controller.meeting"))
                .paths(PathSelectors.any())
                .build();
    }

    @Bean
    public Docket createTaskApi() {
        return new Docket(DocumentationType.OAS_30)
                .enable(true)
                .apiInfo(apiInfo())
                .groupName("定时任务管理器")
                .select()
                // 方式一: 配置扫描 所有想在swagger界面的统一管理接口，都必须在此包下
                .apis(RequestHandlerSelectors.basePackage("com.imflea.zero.controller.task"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(this.title)
                .description(this.description)
                .termsOfServiceUrl(this.termsOfServiceUrl)
                .contact(new Contact(this.author, this.termsOfServiceUrl, this.email))
                .version(this.version)
                .build();
    }
}