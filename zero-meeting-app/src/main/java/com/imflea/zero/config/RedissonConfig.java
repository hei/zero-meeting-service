package com.imflea.zero.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.config
 * @filename 创建时间: 2021/10/25
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Configuration
public class RedissonConfig {

    @Value("${spring.redis.host:localhost}")
    private String host;

    @Value("${spring.redis.port:6379}")
    private int port;

    @Value("${spring.redis.password}")
    private String password;


    // 当前尝试引入分布式锁解决扣库存问题
    @Bean(destroyMethod = "shutdown")
    RedissonClient redisson() {
        //1、创建配置
        Config config = new Config();

        config.useSingleServer()
                .setAddress("redis://" + host + ":" + port)
                .setDatabase(5)
                .setPassword(password);
        System.out.println("redis://" + host + ":" + port);
        System.out.println(password);
        return Redisson.create(config);
    }
}