package com.imflea.zero.api;

import com.imflea.zero.service.wx.IWxPayService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.api
 * @ClassName: WxAPI
 * @Description
 * @date 2021-10-28  15:16:49
 */
@RestController
@RequestMapping("/api/wx")
@Api(value = "微信回调-api", tags = "基础公共api")
public class WxAPI {
    @Autowired
    private IWxPayService wxPayService;

    @PostMapping("/notify")
    public String notify(HttpServletRequest request) throws Exception {
        return wxPayService.update4WxCallBack(request);
    }

}
