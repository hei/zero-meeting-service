package com.imflea.zero.api;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.PidDto;
import com.imflea.zero.model.entity.qx.BaseArea;
import com.imflea.zero.service.qx.IBaseAreaService;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.api
 * @ClassName: AreaAPI
 * @Description
 * @date 2021-08-24  16:51:28
 */
@RestController
@RequestMapping("/api/areaAPI")
@Api(value = "地区字典库-基础公共api", tags = "基础公共api")
public class AreaAPI {

    @Autowired
    private IBaseAreaService baseAreaService;


    @ApiOperation(value = "根据parentId获取地区信息，parendId 为空则为省份", notes = "根据parentId获取地区信息")
    @PostMapping("/queryByParendId")
    @ApiOperationSupport(order = 1)
    public JsonResult<List<BaseArea>> queryByParendId(@RequestBody PidDto pidReq) {

        String parendId = pidReq.getPid();

        JsonResult<List<BaseArea>> areas = new JsonResult<>();
        try {
            areas.setInfo(baseAreaService.queryByParendId(parendId));
            areas.setCode(ZeroContant.getSuccessCode());
        } catch (Exception e) {
            areas.setCode(ZeroContant.getFailCode());
            areas.setMsg(e.getMessage());
        }
        return areas;
    }

    @ApiOperation(value = "根据pcode获取字典子项-北京-市辖区/天津市辖区、广东/深圳", notes = "根据pcode获取字典子项-北京-市辖区/天津市辖区、广东/深圳")
    @PostMapping("/queryByAppPcode")
    @ApiOperationSupport(order = 1)
    public JsonResult<List<BaseArea>> queryByAppPcode(@RequestBody PidDto pidReq) {

        String parendId = pidReq.getPid();

        JsonResult<List<BaseArea>> areas = new JsonResult<>();
        try {
            List<BaseArea> baseAreas = baseAreaService.queryByParendId(parendId);
            if(!CommonUtils.isNull(baseAreas)){
                baseAreas = baseAreas.stream().filter(item->item.getAppUsed().equals(1)).collect(Collectors.toList());
            }
            areas.setInfo(baseAreas);
            areas.setCode(ZeroContant.getSuccessCode());
        } catch (Exception e) {
            areas.setCode(ZeroContant.getFailCode());
            areas.setMsg(e.getMessage());
        }
        return areas;
    }


}
