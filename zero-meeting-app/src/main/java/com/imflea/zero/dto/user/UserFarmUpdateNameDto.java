package com.imflea.zero.dto.user;

import lombok.Data;

@Data
public class UserFarmUpdateNameDto {

    private String farmId;
    private String farmName;

}
