package com.imflea.zero.dto.user;

import lombok.Data;

@Data
public class UserFarmUpdateSloganDto {

    private String farmId;
    private String farmSlogan;

}
