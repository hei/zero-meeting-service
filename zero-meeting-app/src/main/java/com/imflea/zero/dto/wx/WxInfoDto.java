package com.imflea.zero.dto.wx;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("获取微信用户信息后端封装类")
public class WxInfoDto implements Serializable {
    /**
    * @description 字段功能描述
    * @value value:serialVersionUID
    */
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "加密数据", required = true)
    String encryptedData;
    @ApiModelProperty(value = "前端iv", required = true)
    String iv;
    @ApiModelProperty(value = "临时授权码", required = true)
    String code;

}
