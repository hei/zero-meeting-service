package com.imflea.zero.dto.user;

import lombok.Data;

@Data
public class UserFarmUpdateAvatarDto {

    private String farmId;
    private String farmAvatarId;
    private String farmAvatarUrl;

}
