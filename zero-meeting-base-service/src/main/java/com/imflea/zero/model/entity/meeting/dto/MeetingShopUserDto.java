package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * MeetingShopUserDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="MeetingShopUserDto对象", description="MeetingShopUserDto")
public class MeetingShopUserDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "门店id",example = "")
    private String meetingShopId;

    @ApiModelProperty(value = "用户id",example = "")
    private String userId;

    @ApiModelProperty(value = "删除标志（0正常，1删除）",example = "")
    private String delFlag;

    @ApiModelProperty(value = "修改时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id",example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id",example = "")
    private String tenantId;


}
