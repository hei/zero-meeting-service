package com.imflea.zero.model.entity.qx.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * QxTenantConfigDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "QxTenantConfigDto对象", description = "QxTenantConfigDto")
public class QxTenantConfigDto extends PageDto {

    @ApiModelProperty(value = "主键", example = "")
    private String Id;

    @ApiModelProperty(value = "租户id", example = "")
    private String tenantPid;

    @ApiModelProperty(value = "微信小程序id", example = "")
    private String wxAppId;

    @ApiModelProperty(value = "微信小程序秘钥", example = "")
    private String wxSecret;

    @ApiModelProperty(value = "对公支付账户", example = "")
    private String payBankNum;

    private String mchId;
    @ApiModelProperty(value = "微信支付接口key")
    private String apiKey;
    @ApiModelProperty(value = "微信支付：退款证书base64")
    private String certContent;

    @ApiModelProperty(value = "收款单位开户银行名称")
    private String payBankName;

    @ApiModelProperty(value = "收款单位名称")
    private String payCompanyName;

    @ApiModelProperty(value = "备注信息", example = "")
    private String Remarks;

    @ApiModelProperty(value = "删除标志（0正常，1删除）", example = "")
    private String delFlag;

    @ApiModelProperty(value = "修改时间", example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id", example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名", example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间", example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名", example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id", example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id", example = "")
    private String tenantId;


}
