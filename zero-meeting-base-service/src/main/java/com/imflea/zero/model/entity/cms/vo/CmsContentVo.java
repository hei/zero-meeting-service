package com.imflea.zero.model.entity.cms.vo;

import com.imflea.zero.model.entity.cms.CmsTemplate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.cms.vo
 * @filename 创建时间: 2021/9/17
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value = "CmsContentVo", description = "发布正文信息[文字,图片,图文]")
public class CmsContentVo {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "发布类型")
    private String publishType;

    @ApiModelProperty(value = "正文类型")
    private String contentType;

    @ApiModelProperty(value = "发布范围")
    private String scope;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "发布时间")
    private LocalDateTime publishTime;

    @ApiModelProperty(value = "失效时间")
    private LocalDateTime deadlineTime;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "正文信息")
    private String content;

    @ApiModelProperty(value = "外链")
    private String outerLink;

    @ApiModelProperty(value = "模板信息")
    private CmsTemplate template;

    @ApiModelProperty(value = "发布正文")
    private List<String> contents;

}
