package com.imflea.zero.model.entity.pay.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @param
 * @author 祥保玉
 * @description
 * @date 2021/9/26  15:45
 * @return null
 */


@Data
@ApiModel(value = "统一支付订单接收信息", description = "支付订单")
public class PayOrderDetailDto implements Serializable {
    @ApiModelProperty(value = "订单id")
    private String orderId;
    @ApiModelProperty(value = "订单ids")
    private List<String> orderIds;
    @ApiModelProperty(value = "订单类型:会议室订单 ORDER_TYPE_MEETING_ROOM：充值订单:ORDER_TYPE_MEETING_CZ")
    private String orderType;
    @ApiModelProperty(value = "支付方式：微信:PAY_TYPE_WX")
    private String payType;
}
