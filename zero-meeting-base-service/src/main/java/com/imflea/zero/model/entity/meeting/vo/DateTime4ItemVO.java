package com.imflea.zero.model.entity.meeting.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

/**
 * <p>
 * 空间订单表-分项表
 * </p>
 *
 * @author zzz
 * @since 2024-03-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DateTime4ItemVO", description = "DateTime4ItemVO-分项表")
public class DateTime4ItemVO {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "订单占用时间")
    private String theTime;


    @ApiModelProperty(value = "订单所占日期")
    private LocalDate theDay;


}
