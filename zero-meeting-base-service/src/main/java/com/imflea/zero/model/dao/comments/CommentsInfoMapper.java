package com.imflea.zero.model.dao.comments;

import com.imflea.zero.model.entity.comments.CommentsInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 互动评论表Mapper 接口
 * </p>
 *
 * @author guohui
 * @since 2021-10-08
 */
public interface CommentsInfoMapper extends BaseMapper<CommentsInfo> {

}
