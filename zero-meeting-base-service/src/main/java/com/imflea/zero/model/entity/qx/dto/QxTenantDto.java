package com.imflea.zero.model.entity.qx.dto;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 租户表
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "租户信息接口", description = "租户复合关系表")
public class QxTenantDto extends Model<QxTenantDto> {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "租户名")
    private String tenantName;

    @ApiModelProperty(value = "营业执照编号")
    private String businessLicense;

    @ApiModelProperty(value = "负责人")
    private String chargeUser;

    @ApiModelProperty(value = "统一信用编码")
    private String socialCreditCode;

    @ApiModelProperty(value = "联系电话")
    private String chargeUserMobile;

    @ApiModelProperty(value = "是否启用：0 正常，1 停服")
    private String status;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "富文本信息Id")
    private String contentId;

    @ApiModelProperty(value = "富文本内容")
    private String content;

    @ApiModelProperty(value = "租户id",example = "")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
