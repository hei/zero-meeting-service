package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * MeetingAlbumDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="MeetingAlbumDto对象", description="MeetingAlbumDto")
public class MeetingAlbumDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "业务主表id",example = "")
    private String bizId;

    @ApiModelProperty(value = "图片的附件主键",example = "")
    private String imgAttachmentId;

    @ApiModelProperty(value = "图片url",example = "")
    private String imgUrl;

    @ApiModelProperty(value = "小视频的附件主键",example = "")
    private String videoAttachmentId;

    @ApiModelProperty(value = "小视频url",example = "")
    private String videoUrl;

    @ApiModelProperty(value = "小视频名称",example = "")
    private String videoName;

    @ApiModelProperty(value = "是否作为业务的图标,1(是),0(否)",example = "")
    private Integer iconFlag;

    @ApiModelProperty(value = "删除标志（0正常，1删除）",example = "")
    private String delFlag;

    @ApiModelProperty(value = "修改时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id",example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id",example = "")
    private String tenantId;


}
