package com.imflea.zero.model.entity.invoice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 发表抬头
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SaveDto对象", description="发表抬头")
public class SaveDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private String id;

    @ApiModelProperty(value = "抬头种类(GR,GS)")
    private String type;

    @ApiModelProperty(value = "单位税号")
    private String taxNumber;

    @ApiModelProperty(value = "开户银行")
    private String bank;

    @ApiModelProperty(value = "银行账户")
    private String bankAccount;

    @ApiModelProperty(value = "公司地址")
    private String companyAddress;

    @ApiModelProperty(value = "公司电话")
    private String companyPhone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "抬头名称")
    private String invoiceName;

}
