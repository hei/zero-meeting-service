package com.imflea.zero.model.entity.pay.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * PayTransportProofDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "PayTransportProofDto对象", description = "PayTransportProofDto")
public class PayTransportProofDto implements Serializable {

    private static final long serialVersionUID = 4569198797441527431L;
    @ApiModelProperty(value = "订单号", example = "")
    private String orderSn;
    @ApiModelProperty(value = "支付凭证ID：附件表中的id")
    private String proofId;

    @ApiModelProperty(value = "支付凭证URL")
    private String proofUrl;

    @ApiModelProperty(value = "支付凭证类型：app端：PAY_RECODER_PROOF_TYPE_PAY 管理端：PAY_RECODER_PROOF_TYPE_RECEIVE")
    private String proofType;

}
