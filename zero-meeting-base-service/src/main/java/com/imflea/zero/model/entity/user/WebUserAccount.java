package com.imflea.zero.model.entity.user;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 微信端用户账户
 * </p>
 *
 * @author zzz
 * @since 2024-02-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "WebUserAccount对象", description = "微信端用户账户")
public class WebUserAccount extends Model<WebUserAccount> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "web用户id")
    private String userId;

    @ApiModelProperty(value = "账户余额（元）")
    private BigDecimal userAccount;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "注册时间")
    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
