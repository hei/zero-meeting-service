package com.imflea.zero.model.dao.user;

import com.imflea.zero.model.entity.user.WebUserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 微信端用户基本信息 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-13
 */
public interface WebUserInfoMapper extends BaseMapper<WebUserInfo> {

}
