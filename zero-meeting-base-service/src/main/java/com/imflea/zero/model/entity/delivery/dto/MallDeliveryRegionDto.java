package com.imflea.zero.model.entity.delivery.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * MallDeliveryRegionDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="MallDeliveryRegionDto对象", description="MallDeliveryRegionDto")
public class MallDeliveryRegionDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "省份",example = "")
    private String province;

    @ApiModelProperty(value = "城市",example = "")
    private String city;

    @ApiModelProperty(value = "默认标记[0非默认1默认]",example = "")
    private Integer defaultFlag;

    @ApiModelProperty(value = "起步价",example = "")
    private BigDecimal startPrice;

    @ApiModelProperty(value = "阶梯价",example = "")
    private  BigDecimal normalPrice;

    @ApiModelProperty(value = "规则ID",example = "")
    private String ruleId;


}
