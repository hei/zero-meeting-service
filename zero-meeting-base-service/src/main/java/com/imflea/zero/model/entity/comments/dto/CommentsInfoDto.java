package com.imflea.zero.model.entity.comments.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * CommentsInfoDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="CommentsInfoDto对象", description="CommentsInfoDto")
public class CommentsInfoDto extends PageDto {

    @ApiModelProperty(value = "主键ID",example = "")
    private String id;

    @ApiModelProperty(value = "评论内容",example = "")
    private String comments;

    @ApiModelProperty(value = "评论类型",example = "")
    private String type;

    @ApiModelProperty(value = "是否精选",example = "")
    private String selected;

    @ApiModelProperty(value = "用户ID",example = "")
    private String userId;

    @ApiModelProperty(value = "业务模块ID")
    private String businessId;

    @ApiModelProperty(value = "评论业务类型（0点位，1认养物品类）")
    private String commentBusinessType;

    @ApiModelProperty(value = "商户ID",example = "")
    private String tenantId;

    @ApiModelProperty(value = "创建人ID",example = "")
    private String createUserId;

    @ApiModelProperty(value = "创建人名称",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人ID",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "更新人名称",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "更新时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否审核通过(0 通过，1未通过)",example = "")
    private String status;

    @ApiModelProperty(value = "删除标志",example = "")
    private String delFlag;

    @ApiModelProperty(value = "评论者的昵称")
    private String nickName;

    @ApiModelProperty(value = "评论人的头像")
    private String headImage;

}
