package com.imflea.zero.model.entity.qx;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 附件表
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="QxAttachment对象", description="附件表")
public class QxAttachment extends Model<QxAttachment> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "文件类型")
    private String fileType;

    @ApiModelProperty(value = "文件名称")
    private String fileName;

    @ApiModelProperty(value = "文件id（文件服务请求参数）")
    private String fileId;

    @ApiModelProperty(value = "业务系统编码")
    private String businessSysCode;

    @ApiModelProperty(value = "业务系统模块编码")
    private String businessSysModuleCode;

    @ApiModelProperty(value = "业务id")
    private String businessId;

    @ApiModelProperty(value = "0：open；1：必须登录（暂时无权限验证）")
    private String fileOperQx;

    @ApiModelProperty(value = "文件无权限地址")
    private String fileOpenUrl;


    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
