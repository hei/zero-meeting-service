package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import com.imflea.zero.model.entity.meeting.MeetingShopUser;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * MeetingShopDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="MeetingShopUserObjsDto对象", description="MeetingShopUserObjsDto对象")
public class MeetingShopUserObjsDto extends PageDto {

   List<MeetingShopUser> objs ;



}
