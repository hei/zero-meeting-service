package com.imflea.zero.model.entity.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.vo.user
 * @ClassName: FriendVo
 * @Description
 * @date 2021-09-23  15:14:43
 */
@Data
@ApiModel("好友列表vo")
public class UserFriendVo implements Serializable {
    private static final long serialVersionUID = 1336146263133275503L;
    @ApiModelProperty(value = "好友关系表中id")
    private String id;
    @ApiModelProperty(value = "好友ID")
    private String friendId;
    @ApiModelProperty(value = "备注")
    private String remarksName;
    @ApiModelProperty(value = "昵称")
    private String nickName;
    @ApiModelProperty(value = "手机号")
    private String cellNum;
    @ApiModelProperty(value = "好友创建时间")
    private LocalDateTime createTime;
    @ApiModelProperty(value = "好友头像")
    private String avatarUrl;

}
