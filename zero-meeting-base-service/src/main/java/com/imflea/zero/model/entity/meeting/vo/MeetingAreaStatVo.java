package com.imflea.zero.model.entity.meeting.vo;

import com.imflea.zero.model.entity.qx.BaseArea;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * MeetingAlbumDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "MeetingAlbumDto对象", description = "MeetingAlbumDto")
public class MeetingAreaStatVo extends BaseArea {

    @ApiModelProperty(value = "数量", example = "")
    private Integer count;


}
