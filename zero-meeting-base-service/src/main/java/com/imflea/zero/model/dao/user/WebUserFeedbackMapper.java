package com.imflea.zero.model.dao.user;

import com.imflea.zero.model.entity.user.WebUserFeedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-24
 */
public interface WebUserFeedbackMapper extends BaseMapper<WebUserFeedback> {

}
