package com.imflea.zero.model.entity.comments.vo;

import com.imflea.zero.model.entity.comments.CommentsInfo;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@EqualsAndHashCode
@ApiModel(value="QueryListVo对象", description="")
public class QueryPageVo {
    @ApiModelProperty(value = "评论内容",example = "")
    private PageInfo<CommentsInfo> list;
    @ApiModelProperty(value = "开关是否开启",example = "")
    private Boolean isSwitch;
}
