package com.imflea.zero.model.entity.user;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 微信端用户基本信息
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "WebUserInfo对象", description = "微信端用户基本信息")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebUserInfo extends Model<WebUserInfo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户UUID")
    private String id;

    private String unionId;

    @ApiModelProperty(value = "用户openId")
    private String openId;

    @ApiModelProperty(value = "性别")
    private String gender;

    private String avatarUrl;

    private String nickName;

    @ApiModelProperty(value = "电话号码")
    private String cellNum;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "注册时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    private String city;

    private String province;

    private String country;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
