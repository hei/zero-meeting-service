package com.imflea.zero.model.dao.pay;

import com.imflea.zero.model.entity.pay.PayTransportProof;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 转账支付凭证 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-27
 */
public interface PayTransportProofMapper extends BaseMapper<PayTransportProof> {

}
