package com.imflea.zero.model.entity.qx.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * QxUserAccessDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "QxUserAccessDto对象", description = "QxUserAccessDto")
public class QxUserAccessDto extends PageDto {

    @ApiModelProperty(value = "主键", example = "")
    private String id;

    @ApiModelProperty(value = "访问数量", example = "")
    private Long total;

    @ApiModelProperty(value = "具体日期", example = "")
    private LocalDate accessDay;

    @ApiModelProperty(value = "修改时间", example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id", example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名", example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间", example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名", example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id", example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id", example = "")
    private String tenantId;


}
