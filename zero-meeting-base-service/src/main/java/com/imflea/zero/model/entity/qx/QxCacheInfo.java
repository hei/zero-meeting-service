package com.imflea.zero.model.entity.qx;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 缓存基本信息
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="QxCacheInfo对象", description="缓存基本信息")
public class QxCacheInfo extends Model<QxCacheInfo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "缓存索引列")
    private String indexColumns;

    @ApiModelProperty(value = "缓存索引对应名称")
    private String indexCaption;

    @ApiModelProperty(value = "数据查询sql")
    private String dataQuerySql;

    @ApiModelProperty(value = "'MAP||LISTALL||LISTGROUP||DM2MC'")
    private String redisType;

    @ApiModelProperty(value = "分组字段")
    private String groupIndex;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
