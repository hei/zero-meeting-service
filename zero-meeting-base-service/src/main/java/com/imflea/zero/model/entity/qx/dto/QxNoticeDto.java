package com.imflea.zero.model.entity.qx.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * QxNoticeDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "QxNoticeDto对象", description = "QxNoticeDto")
public class QxNoticeDto extends PageDto {

    @ApiModelProperty(value = "主键", example = "")
    private String id;

    @ApiModelProperty(value = "消息通知标题", example = "")
    private String title;


    @ApiModelProperty(value = "通知类型", example = "")
    private String noticeType;

    @ApiModelProperty(value = "发布时间", example = "")
    private String postTime;

    @ApiModelProperty(value = "有效期开始时间：十一假期通知公告", example = "")
    private String startTime;

    @ApiModelProperty(value = "有效期结束时间", example = "")
    private String endTime;

    @ApiModelProperty(value = "排序：可置顶，获取当前最高的数字，用倒排", example = "")
    private Integer orderNum;


}
