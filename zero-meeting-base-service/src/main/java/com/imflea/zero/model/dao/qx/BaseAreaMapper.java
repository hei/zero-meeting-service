package com.imflea.zero.model.dao.qx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.qx.BaseArea;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-24
 */
public interface BaseAreaMapper extends BaseMapper<BaseArea> {

}
