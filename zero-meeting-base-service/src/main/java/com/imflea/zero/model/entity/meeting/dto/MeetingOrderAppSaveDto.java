package com.imflea.zero.model.entity.meeting.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class MeetingOrderAppSaveDto {

    @ApiModelProperty(value = "会员id")
    private String memberId;

    @ApiModelProperty(value = "会员名称")
    private String memberName;

    @ApiModelProperty(value = "空间ID")
    private String bizId;
    @ApiModelProperty(value = "会议室开始租赁时间")
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;
    @ApiModelProperty(value = "会议室结束租赁时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;


    @ApiModelProperty(value = "订单前端预计算价格：用于和后端再次计算时对照，避免前端价格与总计算价格冲突")
    private BigDecimal preTotalAmount;


    @ApiModelProperty(value = "发票抬头ID")
    private String invoiceHeadId;




}
