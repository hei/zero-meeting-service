package com.imflea.zero.model.entity.user;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="WebUserFeedback对象", description="")
public class WebUserFeedback extends Model<WebUserFeedback> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "主题",example = "")
    private String theme;

    @ApiModelProperty(value = "会员iD")
    private String memberId;

    @ApiModelProperty(value = "会员名")
    private String memberName;

    @ApiModelProperty(value = "建议信息")
    private String content;

    @ApiModelProperty(value = "回复信息")
    private String reply;

    @ApiModelProperty(value = "回复人")
    private String replId;

    @ApiModelProperty(value = "回复人名称")
    private String replyName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
