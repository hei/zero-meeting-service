package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req.qx
 * @ClassName: UpdatePasswordReq
 * @Description
 * @date 2021-08-19  14:42:36
 */
@Data
@ApiModel(description = "用户密码修改数据请求对象")
@EqualsAndHashCode(callSuper=false)
public class UpdatePasswordDto implements Serializable {
    private static final long serialVersionUID = 2363349842617846214L;
    @ApiModelProperty(name = "旧密码",required = true)
    private String oldPassword;
    @ApiModelProperty(name="新密码",required = true)
    private String newPassword;


}
