package com.imflea.zero.model.entity.systempiclib.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * SystemPictureLibDtoDTO
 * </p>
 *
 * @author gh
 */
@Data
@ApiModel(value="SystemPictureLibDto对象", description="SystemPictureLibDto")
public class SystemPictureLibDto extends PageDto {

    @ApiModelProperty(value = "主键ID",example = "")
    private String id;

    @ApiModelProperty(value = "图片Id",example = "")
    private String picId;

    @ApiModelProperty(value = "图片地址",example = "")
    private String picUrl;

    @ApiModelProperty(value = "业务模块标识（字典项）",example = "")
    private String businessType;

    @ApiModelProperty(value = "业务ID（预留）",example = "")
    private String businessId;

    @ApiModelProperty(value = "创建人名称",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人ID",example = "")
    private String createUserId;

    @ApiModelProperty(value = "",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人名称",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "更新人ID",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标志",example = "")
    private String delFlag;

    @ApiModelProperty(value = "排序字段")
    private Integer sort;

}
