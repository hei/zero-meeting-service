package com.imflea.zero.model.entity.wx.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.wx.vo
 * @ClassName: OrderTemplateMessageVO
 * @Description
 * @date 2022-03-08  16:40:44
 */
@Data
@ApiModel(value = "微信订单订阅消息")
public class OrderTemplateMessageVo {
    private String phrase3;
    private String phrase2;
    private String thing8;






}
