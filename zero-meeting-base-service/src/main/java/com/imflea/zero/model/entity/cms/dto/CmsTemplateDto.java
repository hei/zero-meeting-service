package com.imflea.zero.model.entity.cms.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * CmsTemplateDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "CmsTemplateDto对象", description = "CmsTemplateDto")
public class CmsTemplateDto extends PageDto {

    @ApiModelProperty(value = "主键", example = "")
    private String id;

    @ApiModelProperty(value = "发布类型", example = "")
    private String publishType;

    @ApiModelProperty(value = "正文类型")
    private String contentType;

    @ApiModelProperty(value = "发布范围")
    private String scope;

    @ApiModelProperty(value = "标题", example = "")
    private String title;

    @ApiModelProperty(value = "最新的多少条记录", example = "")
    private Integer topN;

    @ApiModelProperty(value = "发布时间起", example = "")
    private LocalDateTime publishTimeEnd;

    @ApiModelProperty(value = "发布时间止", example = "")
    private LocalDateTime publishTimeStart;

}
