package com.imflea.zero.model.dao.keywords;

import com.imflea.zero.model.entity.keywords.Keywords;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 敏感词表 Mapper 接口
 * </p>
 *
 * @author guohui
 * @since 2021-10-09
 */
public interface KeywordsMapper extends BaseMapper<Keywords> {

}
