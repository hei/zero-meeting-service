package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MeetingOrderQueryDto extends PageDto {

    @ApiModelProperty(value = "订单状态")
    private Integer orderStatus;

    @ApiModelProperty(value = "订单类型：1办公空间订单 2 充值订单")
    private String orderType;

}
