package com.imflea.zero.model.dao.qx;

import com.imflea.zero.model.entity.qx.QxNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统通知 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-22
 */
public interface QxNoticeMapper extends BaseMapper<QxNotice> {

}
