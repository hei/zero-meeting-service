package com.imflea.zero.model.dao.qx;

import com.imflea.zero.model.entity.qx.QxUserCertification;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户认证密码表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
public interface QxUserCertificationMapper extends BaseMapper<QxUserCertification> {

}
