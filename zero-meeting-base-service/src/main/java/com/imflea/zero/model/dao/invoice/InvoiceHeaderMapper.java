package com.imflea.zero.model.dao.invoice;

import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 发表抬头 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 */
public interface InvoiceHeaderMapper extends BaseMapper<InvoiceHeader> {

    InvoiceHeader queryByOrderIdAndOrderType(@Param("orderId") String orderId,  @Param("orderType") String orderType);
}
