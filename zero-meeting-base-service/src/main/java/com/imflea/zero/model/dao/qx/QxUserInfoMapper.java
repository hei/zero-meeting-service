package com.imflea.zero.model.dao.qx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.qx.QxUserInfo;

/**
 * <p>
 * 管理端客户用户 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
public interface QxUserInfoMapper extends BaseMapper<QxUserInfo> {

}
