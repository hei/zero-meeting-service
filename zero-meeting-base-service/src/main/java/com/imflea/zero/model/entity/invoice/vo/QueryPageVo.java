package com.imflea.zero.model.entity.invoice.vo;

import com.imflea.zero.model.entity.invoice.InvoiceInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value="QueryPageVo对象", description="QueryPageVo")
public class QueryPageVo {

    @ApiModelProperty(value = "主键ID",example = "")
    private String Id;

    @ApiModelProperty(value = "抬头ID",example = "")
    private String invoiceHeaderId;

    @ApiModelProperty(value = "订单ID",example = "")
    private String orderId;

    @ApiModelProperty(value = "申请人ID")
    private String userId;

    @ApiModelProperty(value = "订单类型(adopt,mall)",example = "")
    private String orderType;

    @ApiModelProperty(value = "发票状态(已申请，已开具)",example = "")
    private String Status;

    @ApiModelProperty(value = "文件地址",example = "")
    private String contentUrl;

    @ApiModelProperty(value = "删除标志",example = "")
    private String delFlag;

    @ApiModelProperty(value = "申请时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "申请人名称",example = "")
    private String createUserName;

    @ApiModelProperty(value = "申请时间",example = "")
    private String createUserId;

    @ApiModelProperty(value = "开票时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "开票人ID",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "开票人名称",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "商户ID",example = "")
    private String tenantId;

    @ApiModelProperty(value = "订单号",example = "")
    private String orderNum;
}
