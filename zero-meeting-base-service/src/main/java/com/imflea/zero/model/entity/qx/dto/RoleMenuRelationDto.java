package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req.qx
 * @ClassName: RoleMenuRelationReq
 * @Description
 * @date 2021-08-23  10:43:28
 */
@Data
@ApiModel("角色菜单关联关系维护请求数据格式")
@EqualsAndHashCode(callSuper = false)
public class RoleMenuRelationDto implements Serializable {
    private static final long serialVersionUID = 1926967870307704058L;
    @ApiModelProperty(value = "角色id", required = true)
    private String roleId;
    @ApiModelProperty(value = "菜单id", required = true)
    private List<String> menuIds;
}
