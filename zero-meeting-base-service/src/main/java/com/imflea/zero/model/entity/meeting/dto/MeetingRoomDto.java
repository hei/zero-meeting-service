package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * MeetingRoomDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="MeetingRoomDto对象", description="MeetingRoomDto")
public class MeetingRoomDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "空间类型：0小型办公空间，1：多功能办公空间",example = "")
    private String roomType;

    @ApiModelProperty(value = "多功能办公空间 详细分类：见字典",example = "")
    private String roomCategory;

    @ApiModelProperty(value = "空间名称",example = "")
    private String roomName;

    @ApiModelProperty(value = "容纳人数",example = "")
    private Integer capNum;

    @ApiModelProperty(value = "配置tag，用逗号分割",example = "")
    private String configTag;

    @ApiModelProperty(value = "电话号码",example = "")
    private String phoneNum;

    @ApiModelProperty(value = "坐标-经度",example = "")
    private String zbJd;

    @ApiModelProperty(value = "坐标-维度",example = "")
    private String zbWd;

    @ApiModelProperty(value = "营业开始日期",example = "")
    private LocalDate startDate;

    @ApiModelProperty(value = "营业结束日期",example = "")
    private LocalDate  endDate;

    @ApiModelProperty(value = "营业每天开始时间",example = "")
    private String dayStartTime;

    @ApiModelProperty(value = "营业每天结束时间",example = "")
    private String dayEndTime;

    @ApiModelProperty(value = "所属门店",example = "")
    private String shopId;
    @ApiModelProperty(value = "是否上架：0 未上架，1上架")
    private String status;
    @ApiModelProperty(value = "是否长租：0 不是，1是长租房")
    private String longRent;

    @ApiModelProperty(value = "单价/小时",example = "")
    private  Float price;

    @ApiModelProperty(value = "详细地址",example = "")
    private String address;

    @ApiModelProperty(value = "面积-平方米（㎡）")
    private String area;

    @ApiModelProperty(value = "缩略图地址",example = "")
    private String thumbUrl;

    @ApiModelProperty(value = "简介",example = "")
    private  String descript;

    @ApiModelProperty(value = "删除标志（0正常，1删除）",example = "")
    private String delFlag;

    @ApiModelProperty(value = "修改时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id",example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id",example = "")
    private String tenantId;

    @ApiModelProperty(value = "空间所在省",example = "")
    private String province;
    @ApiModelProperty(value = "空间所城市",example = "")
    private String city;
    @ApiModelProperty(value = "区",example = "")
    private String region;

}
