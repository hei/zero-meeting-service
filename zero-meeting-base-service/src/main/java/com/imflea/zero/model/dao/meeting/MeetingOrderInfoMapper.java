package com.imflea.zero.model.dao.meeting;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.meeting.MeetingOrderInfo;

import java.util.List;

/**
 * <p>
 * 订单主表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-24
 */
public interface MeetingOrderInfoMapper extends BaseMapper<MeetingOrderInfo> {

    List<MeetingOrderInfo> listOrderByIds(List<String> orderIds);
}
