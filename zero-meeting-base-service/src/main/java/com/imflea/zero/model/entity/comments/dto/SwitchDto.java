package com.imflea.zero.model.entity.comments.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value="SwitchDto对象", description="")
public class SwitchDto {

    @ApiModelProperty(value = "主键ID",example = "")
    private Boolean isSwitch;

    @ApiModelProperty(value = "评论业务类型（0点位，1认养物品类）")
    private String commentBusinessType;
}
