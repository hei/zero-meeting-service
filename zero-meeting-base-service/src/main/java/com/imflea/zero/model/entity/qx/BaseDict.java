package com.imflea.zero.model.entity.qx;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="BaseDict对象", description="字典表")
public class BaseDict extends Model<BaseDict> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "字典code")
    private String code;

    @ApiModelProperty(value = "字典上级code")
    private String pcode;

    @ApiModelProperty(value = "字典上级id")
    private String dictPid;

    @ApiModelProperty(value = "菜单路由")
    private String dictName;

    @ApiModelProperty(value = "菜单排序")
    private Integer orderNum;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
