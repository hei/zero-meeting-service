package com.imflea.zero.model.entity.meeting;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author zzz
 * @since 2024-02-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MeetingOrderInfo对象", description = "订单表")
public class MeetingOrderInfo extends Model<MeetingOrderInfo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "订单号")
    private String orderSn;

    @ApiModelProperty(value = "会员id")
    private String memberId;

    @ApiModelProperty(value = "会员名称")
    private String memberName;

    @ApiModelProperty(value = "订单开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "订单结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "业务表ID")
    private String bizId;

    @ApiModelProperty(value = "门店ID")
    private String shopId;

    @ApiModelProperty(value = "会议室或门店名称")
    private String bizName;

    @ApiModelProperty(value = "会议订购时的json")
    private String bizJson;

    @ApiModelProperty(value = "单价金额/小时")
    private BigDecimal singleAmount;

    @ApiModelProperty(value = "订购数量")
    private Integer adoptAmount;

    @ApiModelProperty(value = "订单总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "支付金额")
    private BigDecimal payAmount;

    @ApiModelProperty(value = "支付类型")
    private String payType;
    @ApiModelProperty(value = "订单类型：1办公空间订单 2 充值订单")
    private String orderType;

    @ApiModelProperty(value = "支付单号：微信或转账支付流水")
    private String paySn;

    @ApiModelProperty(value = "管理员调整的折扣价格")
    private BigDecimal discountAmount;

    @ApiModelProperty(value = "优惠金额")
    private BigDecimal semAmount;

    @ApiModelProperty(value = "订单状态：0->待付款；3->已完成；4->已关闭；5->无效订单")
    private Integer orderStatus;

    @ApiModelProperty(value = "订单提交时间")
    private LocalDateTime commitTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }
}