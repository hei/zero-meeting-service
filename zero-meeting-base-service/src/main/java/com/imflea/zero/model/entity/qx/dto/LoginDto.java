package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req.qx
 * @ClassName: LoginReq
 * @Description
 * @date 2021-08-23  14\:29:20
 */
@Data
@ApiModel("登录认证请求数据格式")
@EqualsAndHashCode(callSuper = false)
public class LoginDto implements Serializable {
    private static final long serialVersionUID = 5538791746563447316L;
    @ApiModelProperty(value = "登录账号", required = true)
    private String userName;
    @ApiModelProperty(value = "密码", required = true)
    private String password;
    @ApiModelProperty(value = "验证码", required = true)
    private String captchaCode;
    @ApiModelProperty(value = "序列码", required = true)
    private String serialNum;

}
