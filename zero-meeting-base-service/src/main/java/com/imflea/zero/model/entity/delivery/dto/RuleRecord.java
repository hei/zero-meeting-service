package com.imflea.zero.model.entity.delivery.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.delivery.dto
 * @filename 创建时间: 2021/11/4
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value = "RuleRecord对象", description = "规则记录")
public class RuleRecord {

    @ApiModelProperty(value = "起步价数量")
    private Integer startCount;

    @ApiModelProperty(value = "免邮数量")
    private Integer freeCount;

    @ApiModelProperty(value = "商品数量")
    private Integer quantity;

    @ApiModelProperty(value = "起步价")
    private BigDecimal startPrice;

    @ApiModelProperty(value = "阶梯价")
    private BigDecimal normalPrice;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "运费")
    private BigDecimal money;

    private boolean match;

    @Override
    public String toString() {
        return "RuleRecord{" +
                "startCount=" + startCount +
                ", freeCount=" + freeCount +
                ", startPrice=" + startPrice +
                ", normalPrice=" + normalPrice +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", money=" + money +
                ", match=" + match +
                '}';
    }
}
