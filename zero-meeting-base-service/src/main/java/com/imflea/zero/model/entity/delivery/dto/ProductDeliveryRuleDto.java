package com.imflea.zero.model.entity.delivery.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.delivery.dto
 * @filename 创建时间: 2021/11/4
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value = "ProductDeliveryRuleDto对象", description = "ProductDeliveryRuleDto")
public class ProductDeliveryRuleDto {

    @ApiModelProperty(value = "商品物流规则ID", example = "")
    private String ruleId;

    @ApiModelProperty(value = "商品数量", example = "")
    private Integer quantity;

    @ApiModelProperty(value = "物流模式", example = "0物流,1自提")
    private Integer deliveryMode;

    @ApiModelProperty(value = "收货省份", example = "")
    private String receiverProvince;

    @ApiModelProperty(value = "收获城市", example = "")
    private String receiverCity;

    @Override
    public String toString() {
        return "ProductDeliveryRuleDto{" +
                "ruleId='" + ruleId + '\'' +
                ", quantity=" + quantity +
                ", receiverProvince='" + receiverProvince + '\'' +
                ", receiverCity='" + receiverCity + '\'' +
                '}';
    }
}
