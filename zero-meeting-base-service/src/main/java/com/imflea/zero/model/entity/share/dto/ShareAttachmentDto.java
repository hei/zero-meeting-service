package com.imflea.zero.model.entity.share.dto;

import com.imflea.zero.model.entity.qx.dto.AttachmentDetailDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value="ShareAttachmentDto对象", description="ShareAttachmentDto")
public class ShareAttachmentDto {

    @ApiModelProperty(value = "记录Id",example = "")
    private String id;
    @ApiModelProperty(value = "文件Id",example = "")
    private String attachmentId;
    @ApiModelProperty(value = "文件路径",example = "")
    private String icon;
    @ApiModelProperty(value = "排序",example = "")
    private Integer sort;
    @ApiModelProperty(value = "文件跳转路径",example = "")
    private String uri;


}
