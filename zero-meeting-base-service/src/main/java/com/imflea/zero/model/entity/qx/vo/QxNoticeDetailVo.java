package com.imflea.zero.model.entity.qx.vo;

import com.imflea.zero.model.entity.cms.CmsCommonContent;
import com.imflea.zero.model.entity.qx.QxNotice;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * QxNoticeDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "通知详情", description = "通知详情")
public class QxNoticeDetailVo implements Serializable {
    @ApiModelProperty(value = "通知基本信息")
    private QxNotice notice;
    @ApiModelProperty(value = "通知详情")
    private CmsCommonContent content;


}
