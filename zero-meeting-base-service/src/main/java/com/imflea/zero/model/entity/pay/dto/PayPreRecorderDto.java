package com.imflea.zero.model.entity.pay.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.pay.dto
 * @ClassName: PayPreRecorderDto
 * @Description
 * @date 2021-10-14  19:56:28
 */
@Data
public class PayPreRecorderDto {

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderSn;

    @ApiModelProperty(value = "支付流水号")
    private String paySn;

    @ApiModelProperty(value = "支付方式：微信，银行转账")
    private String payType;

    @ApiModelProperty(value = "订单类型：牧场订单，商城订单")
    private String orderType;

    @ApiModelProperty(value = "总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "支付人")
    private String payUserId;

    @ApiModelProperty(value = "支付人openId")
    private String payUserOpenId;

    @ApiModelProperty(value = "支付人姓名")
    private String payUserName;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "租户id")
    private String tenantId;
}
