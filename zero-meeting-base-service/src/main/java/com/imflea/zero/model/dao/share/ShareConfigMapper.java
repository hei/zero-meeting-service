package com.imflea.zero.model.dao.share;

import com.imflea.zero.model.entity.share.ShareConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 分享界面图片配置表 Mapper 接口
 * </p>
 *
 * @author guohui
 * @since 2021-10-21
 */
public interface ShareConfigMapper extends BaseMapper<ShareConfig> {

    List<ShareConfig> listByMaxSort(@Param("businessId") String businessId, @Param("tenantId") String tenantId);
}
