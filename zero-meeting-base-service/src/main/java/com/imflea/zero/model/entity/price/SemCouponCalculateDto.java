package com.imflea.zero.model.entity.price;

import com.imflea.zero.model.entity.delivery.dto.ProductDeliveryRuleDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@ApiModel("优惠价格计算-认养券-换购券-礼金卡等请求参数")
public class SemCouponCalculateDto implements Serializable {


    //原认养券接口参数
    @ApiModelProperty("原认养券请求参数模型")
    private PriceCalculateDto priceCalculateDto;

    @ApiModelProperty("skuIds和对应数量的集合")
    private List<SkuAndAmountDto> skuList;
    @ApiModelProperty("商品类别，商品id，商品skuId等")
    private List<String> businessIds;
    @ApiModelProperty("已选且可用券id集合")
    private List<String> semCoupItemIds;

    @ApiModelProperty(value = "订单类型：认养：ORDER_TYPE_ADOPT 商城： ORDER_TYPE_MALL", example = "认养：ORDER_TYPE_ADOPT 商城： ORDER_TYPE_MALL")
    private String orderType;

    @ApiModelProperty("礼金卡支付金额")
    private Integer giftCardAmount;

    @ApiModelProperty("订单提交中的部分信息")
    private List<ProductDeliveryRuleDto> productDeliveryRuleDto;

    @ApiModelProperty(value = "优惠计算类型：0：不参与任何优惠 1:semCoupon（优惠券）  2:giftCard（礼金卡）3 换购券")
    private String semType;
    @ApiModelProperty("是否使用礼金卡：0：不使用，1：使用")
    private String useGiftCard;


}
