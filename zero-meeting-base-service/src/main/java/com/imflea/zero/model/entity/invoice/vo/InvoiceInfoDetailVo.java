package com.imflea.zero.model.entity.invoice.vo;

import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.imflea.zero.model.entity.invoice.InvoiceInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.invoice.vo
 * @ClassName: InvoiceInfoDetatilVo
 * @Description
 * @date 2022-01-18  09:53:16
 */
@Data
@ApiModel(value = "发票详情", description = "发票详情")
public class InvoiceInfoDetailVo implements Serializable {
    @ApiModelProperty(value = "发票", example = "")
    private InvoiceInfo invoiceInfo;
    @ApiModelProperty(value = "抬头", example = "")
    private InvoiceHeader invoiceHeader;
    @ApiModelProperty(value = "是否已经申请了发票", example = "")
    private Boolean hasReplay;
}
