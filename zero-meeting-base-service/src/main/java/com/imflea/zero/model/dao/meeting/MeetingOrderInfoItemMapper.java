package com.imflea.zero.model.dao.meeting;

import com.imflea.zero.model.entity.meeting.MeetingOrderInfoItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 空间订单表-分项表 Mapper 接口
 * </p>
 *
 * @author zzz
 * @since 2024-03-03
 */
public interface MeetingOrderInfoItemMapper extends BaseMapper<MeetingOrderInfoItem> {

}
