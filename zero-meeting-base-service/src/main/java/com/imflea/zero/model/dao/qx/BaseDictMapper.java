package com.imflea.zero.model.dao.qx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.qx.BaseDict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-25
 */
public interface BaseDictMapper extends BaseMapper<BaseDict> {

}
