package com.imflea.zero.model.entity.user.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * WebUserInfoDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "WebUserInfoDto对象", description = "WebUserInfoDto")
public class WebUserInfoDto extends PageDto {


    @ApiModelProperty(value = "", example = "")
    private String nickName;

    @ApiModelProperty(value = "电话号码", example = "")
    private String cellNum;


}
