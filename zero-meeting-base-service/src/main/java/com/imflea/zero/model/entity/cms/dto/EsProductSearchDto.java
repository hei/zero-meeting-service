package com.imflea.zero.model.entity.cms.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.cms.dto
 * @filename CmsProductInfoDto 创建时间: 2021/9/13
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@ApiModel(value = "CmsProductInfoDto小程序商品信息", description = "小程序商品信息")
@Data
public class EsProductSearchDto extends PageDto {

    @ApiModelProperty(value = "商品")
    private String id;

    @ApiModelProperty(value = "商品名称")
    private String name;

    @ApiModelProperty(value = "搜索词")
    private String searchWord;

    @ApiModelProperty(value = "商品分类ID")
    private String categoryId;

    @ApiModelProperty(value = "商品分类名称")
    private String categoryName;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
