package com.imflea.zero.model.entity.cms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.cms.vo
 * @filename ProductSkuVo 创建时间: 2021/9/14
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ProductSkuVo对象", description = "")
public class ProductSkuStockVo {

    @ApiModelProperty(value = "SKU主键")
    private String skuId;

    @ApiModelProperty(value = "库存")
    private Integer stock;

    @ApiModelProperty(value = "销量")
    private Integer sale;

    @ApiModelProperty(value = "商品规格JSON")
    private String spData;

    @ApiModelProperty(value = "SKU编码")
    private String skuCode;

    @ApiModelProperty(value = "销售价")
    private BigDecimal price;
}
