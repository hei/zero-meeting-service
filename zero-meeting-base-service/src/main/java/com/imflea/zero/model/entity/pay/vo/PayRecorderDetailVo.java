package com.imflea.zero.model.entity.pay.vo;

import com.imflea.zero.model.entity.pay.PayRecorderInfo;
import com.imflea.zero.model.entity.pay.PayTransportProof;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.pay.vo
 * @ClassName: PayRecorderDetailVo
 * @Description
 * @date 2021-10-12  18:53:51
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "支付记录信息对象信息", description = "支付记录")
public class PayRecorderDetailVo extends  PayRecorderInfo implements Serializable {
    private static final long serialVersionUID = 6479902949708677999L;

    @ApiModelProperty(value = "支付凭证")
    private PayTransportProof payTransportProof;
    @ApiModelProperty(value = "收款凭证")
    private PayTransportProof reciviedTransportProof;
}
