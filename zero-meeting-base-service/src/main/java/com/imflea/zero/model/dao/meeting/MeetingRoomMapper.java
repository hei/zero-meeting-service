package com.imflea.zero.model.dao.meeting;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.meeting.MeetingRoom;

/**
 * <p>
 * 空间表 Mapper 接口
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 */
public interface MeetingRoomMapper extends BaseMapper<MeetingRoom> {

}
