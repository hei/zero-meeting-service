package com.imflea.zero.model.dao.qx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.qx.QxRoleMenu;

/**
 * <p>
 * 角色菜单关联表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
public interface QxRoleMenuMapper extends BaseMapper<QxRoleMenu> {

}
