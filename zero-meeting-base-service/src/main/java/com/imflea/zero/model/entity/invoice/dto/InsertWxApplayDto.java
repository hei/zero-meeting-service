package com.imflea.zero.model.entity.invoice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value="InsertWxApplayDto对象", description="InsertWxApplayDto")
public class InsertWxApplayDto {

    @ApiModelProperty(value = "抬头ID")
    private String invoiceHeaderId;

    @ApiModelProperty(value = "订单ID")
    private String orderId;

    @ApiModelProperty(value = "订单类型(adopt,mall)")
    private String orderType;
    @ApiModelProperty(value = "订单号")
    private String orderSn;
}
