package com.imflea.zero.model.entity.system.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * SystemVersionDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "SystemVersionDto对象", description = "SystemVersionDto")
public class SystemVersionDto extends PageDto {

    @ApiModelProperty(value = "主键ID", example = "")
    private String id;

    @ApiModelProperty(value = "版本号", example = "")
    private String versionNo;

    @ApiModelProperty(value = "版本更新内容", example = "")
    private String remarks;

    @ApiModelProperty(value = "版本发布时间", example = "")
    private LocalDate postTime;

    @ApiModelProperty(value = "创建人名称", example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人ID", example = "")
    private String createUserId;

    @ApiModelProperty(value = "", example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新人名称", example = "")
    private String updateUserName;

    @ApiModelProperty(value = "更新人ID", example = "")
    private String updateUserId;

    @ApiModelProperty(value = "更新时间", example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标志", example = "")
    private String delFlag;


}
