package com.imflea.zero.model.dao.invoice;

import com.imflea.zero.model.entity.invoice.InvoiceInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 发票信息 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 */
public interface InvoiceInfoMapper extends BaseMapper<InvoiceInfo> {

}
