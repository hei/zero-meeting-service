package com.imflea.zero.model.entity.wx.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "pay")
public class WxPayBaseConfig {

    private String wxNotifyUrl;

}