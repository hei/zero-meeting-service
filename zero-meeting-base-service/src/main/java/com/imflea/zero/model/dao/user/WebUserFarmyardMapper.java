package com.imflea.zero.model.dao.user;

import com.imflea.zero.model.entity.user.WebUserFarmyard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户农场表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-24
 */
public interface WebUserFarmyardMapper extends BaseMapper<WebUserFarmyard> {

}
