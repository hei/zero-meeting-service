package com.imflea.zero.model.entity.qx.vo;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.imflea.zero.util.base.CommonUtils;;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 租户配置表
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QxTenantConfigVo对象", description = "租户配置表VO")
public class QxTenantConfigVo extends Model<QxTenantConfigVo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "租户id")
    private String tenantPid;

    @ApiModelProperty(value = "微信小程序id")
    private String wxAppId;

    @ApiModelProperty(value = "微信小程序秘钥")
    private String wxSecret;
    @ApiModelProperty(value = "微信支付商户id")
    private String mchId;
    @ApiModelProperty(value = "微信支付接口key")
    private String apiKey;
    @ApiModelProperty(value = "微信支付：退款证书base64")
    private String certContent;


    @ApiModelProperty(value = "收款账号")
    private String payBankNum;

    @ApiModelProperty(value = "收款单位开户银行名称")
    private String payBankName;

    @ApiModelProperty(value = "收款单位名称")
    private String payCompanyName;


    @ApiModelProperty(value = "备注信息")
    private String remarks;


    @ApiModelProperty(value = "租户id")
    private String tenantId;


    public String getWxSecret() {
        if (CommonUtils.isNull(wxSecret)) {
            return "0";
        }
        return wxSecret.length() + "";
    }

    public String getMchId() {
        if (CommonUtils.isNull(mchId)) {
            return "0";
        }
        return mchId.length() + "";
    }

    public String getApiKey() {
        if (CommonUtils.isNull(apiKey)) {
            return "0";
        }
        return apiKey.length() + "";
    }

    public String getCertContent() {

        if (CommonUtils.isNull(certContent)) {
            return "0";
        }
        return "1";
    }

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
