package com.imflea.zero.model.entity.keywords;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 敏感词表
 * </p>
 *
 * @author guohui
 * @since 2021-10-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Keywords对象", description="敏感词表")
public class Keywords extends Model<Keywords> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private String id;

    @ApiModelProperty(value = "敏感词")
    private String words;

    @ApiModelProperty(value = "商户ID")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
