package com.imflea.zero.model.entity.cms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.cms.vo
 * @filename ProductGroupVo 创建时间: 2021/9/14
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value = "ProductGroupVo对象", description = "")
public class ProductGroupVo {

    @ApiModelProperty(value = "分类Id")
    private String categoryId;

    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    @ApiModelProperty(value = "上级分类,默认空串")
    private String parentId;

    @ApiModelProperty(value = "所处级别 1(一级),2(二级)")
    private Integer level;
}
