package com.imflea.zero.model.entity.cms.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 * CmsEnterpriseNewsDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "CmsEnterpriseNewsDto对象", description = "CmsEnterpriseNewsDto")
public class CmsEnterpriseNewsDto extends PageDto {

    @ApiModelProperty(value = "主键", example = "")
    private String id;

    @ApiModelProperty(value = "主键的同义词,具有业务含义", example = "")
    private String newsId;

    @ApiModelProperty(value = "标题", example = "")
    private String Title;

    @ApiModelProperty(value = "作者", example = "")
    private String Author;

    @ApiModelProperty(value = "发布日期", example = "")
    private Date publishDate;

    @ApiModelProperty(value = "发布正文", example = "")
    private String Content;

    @ApiModelProperty(value = "文章的标签", example = "")
    private String Label;

    @ApiModelProperty(value = "发布状态", example = "")
    private Integer publishStatus;

}
