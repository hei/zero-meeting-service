package com.imflea.zero.model.entity.qx.vo;

import com.imflea.zero.model.entity.qx.QxRole;
import com.imflea.zero.model.entity.qx.QxUserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * <p>
 * 管理端客户用户
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QxUserInfoVO对象", description = "管理端客户用户展示")
public class QxUserInfoVo extends QxUserInfo {


    private static final long serialVersionUID = -3302796355444810088L;

    @ApiModelProperty(value = "主键")
    private List<QxRole> roleList;
    @ApiModelProperty(value = "关联id")
    private String relationId ;
}
