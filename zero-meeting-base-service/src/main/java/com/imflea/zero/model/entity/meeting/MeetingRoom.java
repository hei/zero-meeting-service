package com.imflea.zero.model.entity.meeting;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 空间表
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MeetingRoom对象", description = "空间表")
public class MeetingRoom extends Model<MeetingRoom> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "空间类型：0小型办公空间，1：多功能办公空间")
    private String roomType;

    @ApiModelProperty(value = "多功能办公空间 详细分类：见字典")
    private String roomCategory;

    @ApiModelProperty(value = "空间名称")
    private String roomName;

    @ApiModelProperty(value = "容纳人数")
    private Integer capNum;

    @ApiModelProperty(value = "配置tag，用逗号分割")
    private String configTag;

    @ApiModelProperty(value = "电话号码")
    private String phoneNum;

    @ApiModelProperty(value = "坐标-经度")
    private String zbJd;

    @ApiModelProperty(value = "坐标-维度")
    private String zbWd;

    @ApiModelProperty(value = "营业开始日期")
    private LocalDate startDate;

    @ApiModelProperty(value = "营业结束日期")
    private LocalDate endDate;

    @ApiModelProperty(value = "营业每天开始时间")
    private String dayStartTime;

    @ApiModelProperty(value = "营业每天结束时间")
    private String dayEndTime;

    @ApiModelProperty(value = "所属门店")
    private String shopId;
    @ApiModelProperty(value = "是否上架：0 未上架，1上架")
    private String status;
    @ApiModelProperty(value = "是否长租：0 不是，1是长租房")
    private String longRent;

    @ApiModelProperty(value = "单价/小时")
    private Float price;

    @ApiModelProperty(value = "详细地址")
    private String address;

    @ApiModelProperty(value = "面积-平方米（㎡）")
    private String area;

    @ApiModelProperty(value = "缩略图地址")
    private String thumbUrl;

    @ApiModelProperty(value = "简介")
    private String descript;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
