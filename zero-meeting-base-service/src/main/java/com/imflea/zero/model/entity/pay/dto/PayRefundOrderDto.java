package com.imflea.zero.model.entity.pay.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.pay.dto
 * @ClassName: PayRefundOrderDto
 * @Description
 * @date 2021-11-02  14:19:09
 */
@Data
@ApiModel(value = "PayRefundOrderDto对象", description = "退款order对象")
public class PayRefundOrderDto implements Serializable {
    @ApiModelProperty(value = "订单类型", example = "")
    private String orderType;
    @ApiModelProperty(value = "退款单id", example = "")
    private String refundOrderId;
    @ApiModelProperty(value = "附件扩展id", example = "")
    private String extend;
}
