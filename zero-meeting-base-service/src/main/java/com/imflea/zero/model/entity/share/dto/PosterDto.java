package com.imflea.zero.model.entity.share.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * PosterDto
 * </p>
 *
 * @author gh
 */
@Data
@ApiModel(value="PosterDto对象", description="PosterDto")
public class PosterDto{

    @ApiModelProperty(value = "海报的商品区Url",example = "")
    private String goodsUrl;

    @ApiModelProperty(value = "海报抬头名称",example = "")
    private String goodsTitle;

    @ApiModelProperty(value = "物品名称",example = "")
    private String goodsName;

    @ApiModelProperty(value = "物品描述",example = "")
    private String description;

    @ApiModelProperty(value = "海报的跳转Url",example = "")
    private String jumpUrl;
}
