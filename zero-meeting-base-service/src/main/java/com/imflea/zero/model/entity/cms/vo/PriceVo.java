package com.imflea.zero.model.entity.cms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.cms.vo
 * @filename PriceVo 创建时间: 2021/9/14
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value = "PriceVo对象", description = "")
public class PriceVo {

    @ApiModelProperty(value = "最高价")
    private BigDecimal maxPrice;

    @ApiModelProperty(value = "最低价")
    private BigDecimal minPrice;

}
