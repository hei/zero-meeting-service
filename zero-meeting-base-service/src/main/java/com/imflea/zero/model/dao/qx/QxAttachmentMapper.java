package com.imflea.zero.model.dao.qx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.qx.QxAttachment;

/**
 * <p>
 * 附件表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-27
 */
public interface QxAttachmentMapper extends BaseMapper<QxAttachment> {

}
