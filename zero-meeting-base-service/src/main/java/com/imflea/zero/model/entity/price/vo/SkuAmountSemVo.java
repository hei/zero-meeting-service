package com.imflea.zero.model.entity.price.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.price
 * @ClassName: SkuAndAmountDto
 * @Description
 * @date 2021-11-04  15:02:40
 */
@Data
public class SkuAmountSemVo implements Serializable {

    private static final long serialVersionUID = 2564508880180345966L;
    @ApiModelProperty(value = "商品sku")
    private String skuId;
    @ApiModelProperty(value = "数量")
    private Integer amount;
    @ApiModelProperty(value = "总优惠值")
    private String totalSemAmount;
    @ApiModelProperty(value = "邮费")
    private String deliveryFee;

}
