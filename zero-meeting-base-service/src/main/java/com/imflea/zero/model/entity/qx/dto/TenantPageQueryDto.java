package com.imflea.zero.model.entity.qx.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req.qx
 * @ClassName: TenantPageQueryReq
 * @Description
 * @date 2021-08-24  15:23:21
 */
@Data
@ApiModel(value = "租户条件查询且分页")
@EqualsAndHashCode(callSuper = false)
public class TenantPageQueryDto extends PageDto {
    private static final long serialVersionUID = -4040087329436761803L;
    @ApiModelProperty(value = "租户状态：0正常 1：停服状态", required = true)
    private String status;
    @ApiModelProperty(value = "租户名称（模糊条件）", required = true)
    private String tenantName;
}
