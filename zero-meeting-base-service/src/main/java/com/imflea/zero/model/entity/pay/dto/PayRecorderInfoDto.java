package com.imflea.zero.model.entity.pay.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * PayRecoderInfoDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "PayRecoderInfoDto对象", description = "PayRecoderInfoDto")
public class PayRecorderInfoDto extends PageDto {

    @ApiModelProperty(value = "主键", example = "")
    private String id;

    @ApiModelProperty(value = "订单id", example = "")
    private String orderId;

    @ApiModelProperty(value = "订单号", example = "")
    private String orderSn;

    @ApiModelProperty(value = "支付流水号", example = "")
    private String paySn;

    @ApiModelProperty(value = "支付方式：微信，银行转账，认养券", example = "")
    private String payType;

    @ApiModelProperty(value = "订单类型：牧场订单，商城订单", example = "")
    private String orderType;

    @ApiModelProperty(value = "订单状态：待支付，待上传证明，待确认，支付完成")
    private String payStatus;

    @ApiModelProperty(value = "总金额", example = "")
    private String totalAmount;

    @ApiModelProperty(value = "支付人", example = "")
    private String payUserId;

    @ApiModelProperty(value = "支付人openId", example = "")
    private String payUserOpenId;

    @ApiModelProperty(value = "支付人姓名", example = "")
    private String payUserName;

    @ApiModelProperty(value = "支付时间", example = "")
    private LocalDateTime payTime;

    @ApiModelProperty(value = "支付详情:对公账户，认养卡，商户等")
    private String payDetail;

    @ApiModelProperty(value = "租户id", example = "")
    private String tenantId;


}
