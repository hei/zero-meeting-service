package com.imflea.zero.model.dao.qx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.qx.QxTenant;

/**
 * <p>
 * 租户表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-24
 */
public interface QxTenantMapper extends BaseMapper<QxTenant> {

}
