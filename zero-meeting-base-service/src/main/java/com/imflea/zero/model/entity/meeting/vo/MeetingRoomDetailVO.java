package com.imflea.zero.model.entity.meeting.vo;

import com.imflea.zero.model.entity.meeting.MeetingRoom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MeetingRoomDetailVO对象", description = "MeetingRoomDetailVO")
public class MeetingRoomDetailVO extends MeetingRoom {
    @ApiModelProperty(value = "门店名称")
    private String shopName;
    @ApiModelProperty(value = "标签名称")
    private String configTagName;
    @ApiModelProperty(value = "省名称")
    private String provinceName;
    @ApiModelProperty(value = "市名称")
    private String cityName;
    @ApiModelProperty(value = "区。县名称")
    private String regionName;
}
