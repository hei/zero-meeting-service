package com.imflea.zero.model.entity.invoice.dto;

import com.imflea.zero.utils.SessionUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * InvoiceHeaderDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="InvoiceHeaderDto对象", description="InvoiceHeaderDto")
public class InvoiceHeaderDto extends PageDto {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID",example = "")
    private String Id;

    @ApiModelProperty(value = "用户ID",example = "")
    private String userId;

    @ApiModelProperty(value = "抬头种类(GR,GS)",example = "")
    private String Type;

    @ApiModelProperty(value = "单位税号",example = "")
    private String taxNumber;

    @ApiModelProperty(value = "开户银行",example = "")
    private String Bank;

    @ApiModelProperty(value = "银行账户",example = "")
    private String bankAccount;

    @ApiModelProperty(value = "公司地址",example = "")
    private String companyAddress;

    @ApiModelProperty(value = "公司电话",example = "")
    private String companyPhone;

    @ApiModelProperty(value = "邮箱",example = "")
    private String Email;

    @ApiModelProperty(value = "删除标志",example = "")
    private String delFlag;

    @ApiModelProperty(value = "更新时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "更新人ID",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "更新人姓名",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人姓名",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人ID",example = "")
    private String createUserId;

    @ApiModelProperty(value = "商户ID",example = "")
    private String tenantId;

    @ApiModelProperty(value = "抬头名称")
    private String invoiceName;
}
