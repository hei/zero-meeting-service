package com.imflea.zero.model.entity.user;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户农场表
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="WebUserFarmyard对象", description="用户农场表")
public class WebUserFarmyard extends Model<WebUserFarmyard> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "农场名")
    private String farmName;

    @ApiModelProperty(value = "农场标语")
    private String farmSlogan;

    @ApiModelProperty(value = "点赞数（劣后）")
    private Long farmThumbsUp;

    @ApiModelProperty(value = "关注数（劣后）")
    private Long farmFollow;

    @ApiModelProperty(value = "粉丝数（劣后）")
    private Long farmFans;

    @ApiModelProperty(value = "农场等级")
    private Integer farmLevel;

    @ApiModelProperty(value = "农场头像附件ID")
    private String farmAvatarId;

    @ApiModelProperty(value = "农场头像URL")
    private String farmAvatarUrl;

    @ApiModelProperty(value = "农场类型（劣后）")
    private String farmType;

    @ApiModelProperty(value = "APP用户ID")
    private String webUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标记")
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
