package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * QxCacheInfoDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "根据表名称刷新缓存", description = "根据表名称刷新缓存")
public class CacheInfoTableNamesDto {

    @ApiModelProperty(value = "表名", example = "")
    private List<String> tableNames;


}
