package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * MeetingOrderInfoDtoDTO
 * </p>
 *
 * @author zzz
 */
@Data
@ApiModel(value = "MeetingOrderInfoDto对象", description = "MeetingOrderInfoDto")
public class MeetingOrderInfoDto extends PageDto {

    @ApiModelProperty(value = "主键", example = "")
    private String id;

    @ApiModelProperty(value = "订单号", example = "")
    private String orderSn;

    @ApiModelProperty(value = "会员id", example = "")
    private String memberId;

    @ApiModelProperty(value = "会员名称", example = "")
    private String memberName;

    @ApiModelProperty(value = "订单开始时间", example = "")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "订单结束时间", example = "")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "业务表ID", example = "")
    private String bizId;

    @ApiModelProperty(value = "会议室或门店名称", example = "")
    private String bizName;

    @ApiModelProperty(value = "会议订购时的json", example = "")
    private String bizJson;

    @ApiModelProperty(value = "单个金额", example = "")
    private BigDecimal singleAmount;

    @ApiModelProperty(value = "订购数量", example = "")
    private Integer adoptAmount;

    @ApiModelProperty(value = "订单总金额", example = "")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "支付金额", example = "")
    private BigDecimal payAmount;

    @ApiModelProperty(value = "支付类型", example = "")
    private String payType;
    @ApiModelProperty(value = "订单类型：1办公空间订单 2 充值订单")
    private String orderType;


    @ApiModelProperty(value = "支付单号：微信或转账支付流水", example = "")
    private String paySn;

    @ApiModelProperty(value = "管理员调整的折扣价格", example = "")
    private BigDecimal discountAmount;

    @ApiModelProperty(value = "优惠金额", example = "")
    private BigDecimal semAmount;

    @ApiModelProperty(value = "订单状态：0->待付款；3->已完成；4->已关闭；5->无效订单", example = "")
    private Integer orderStatus;

    @ApiModelProperty(value = "订单提交时间", example = "")
    private LocalDateTime commitTime;

    @ApiModelProperty(value = "删除标志（0正常，1删除）", example = "")
    private String delFlag;

    @ApiModelProperty(value = "创建时间", example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "租户id", example = "")
    private String tenantId;

    @ApiModelProperty(value = "修改时间", example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id", example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名", example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建人账户名", example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id", example = "")
    private String createUserId;


}
