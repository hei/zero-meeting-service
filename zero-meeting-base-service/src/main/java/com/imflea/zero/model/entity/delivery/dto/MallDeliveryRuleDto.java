package com.imflea.zero.model.entity.delivery.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * MallDeliveryRuleDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="MallDeliveryRuleDto对象", description="MallDeliveryRuleDto")
public class MallDeliveryRuleDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "规则名称")
    private String name;

}
