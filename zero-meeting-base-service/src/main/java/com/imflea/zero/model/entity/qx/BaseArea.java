package com.imflea.zero.model.entity.qx;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="BaseArea对象", description="")
public class BaseArea extends Model<BaseArea> {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty(value = "地区名称")
    private String areaName;

    @ApiModelProperty(value = "父级id")
    private String parentId;

    private Integer level;

    private Integer appUsed;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
