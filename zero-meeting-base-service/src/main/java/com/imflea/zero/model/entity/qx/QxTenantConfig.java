package com.imflea.zero.model.entity.qx;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 租户配置表
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QxTenantConfig对象", description = "租户配置表")
public class QxTenantConfig extends Model<QxTenantConfig> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "租户id")
    private String tenantPid;

    @ApiModelProperty(value = "微信小程序id")
    private String wxAppId;

    @ApiModelProperty(value = "微信小程序秘钥")
    private String wxSecret;
    @ApiModelProperty(value = "微信支付商户id")
    private String mchId;
    @ApiModelProperty(value = "微信支付接口key")
    private String apiKey;
    @ApiModelProperty(value = "微信支付：退款证书base64")
    private String certContent;


    @ApiModelProperty(value = "收款账号")
    private String payBankNum;

    @ApiModelProperty(value = "收款单位开户银行名称")
    private String payBankName;

    @ApiModelProperty(value = "收款单位名称")
    private String payCompanyName;
    @ApiModelProperty(value = "默认认养介绍字段")
    private String adoptDefaultComment;

    @ApiModelProperty(value = "微信订阅消息模板ID")
    private String orderTemplateId;


    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
