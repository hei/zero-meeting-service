package com.imflea.zero.model.entity.logistics;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.logistics
 * @filename LogisticsProperties 创建时间: 2021/9/18
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Data
@Component
@ConfigurationProperties(prefix = "logistics",ignoreInvalidFields=true)
public class LogisticsProperties {

    private String domain;

    private String path;

    private String appcode;

    // appsecret 和  appkey 是基于 签名认证 实现调用
    private String appsecret;

    private String appkey;
}
