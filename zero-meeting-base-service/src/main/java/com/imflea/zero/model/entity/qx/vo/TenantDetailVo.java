package com.imflea.zero.model.entity.qx.vo;

import com.imflea.zero.model.entity.cms.CmsCommonContent;
import com.imflea.zero.model.entity.qx.QxTenant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.qx.vo
 * @ClassName: TenantDetailVo
 * @Description
 * @date 2021-09-24  19:00:23
 */
@Data
@ApiModel(value = "租户信息信息对象", description = "")
public class TenantDetailVo implements Serializable {

    private static final long serialVersionUID = 584120823835354140L;
    @ApiModelProperty(value = "租户基本信息")
    private QxTenant tenant;
    @ApiModelProperty(value = "租户简介信息")
    private CmsCommonContent content;

}
