package com.imflea.zero.model.entity.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "RoomStatProvinceDto对象", description = "RoomStatProvinceDto")
public class RoomStatProvinceDto {

    @ApiModelProperty(value = "空间类型：0小型办公空间，1：多功能办公空间", example = "", required = true)
    private String roomType;
}