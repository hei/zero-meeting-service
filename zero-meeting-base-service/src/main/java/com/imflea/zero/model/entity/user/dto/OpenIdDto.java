package com.imflea.zero.model.entity.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * WebUserFriendDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "OpenId", description = "OpenId")
public class OpenIdDto implements Serializable {

    private static final long serialVersionUID = -22373676243176131L;


    @ApiModelProperty(value = "openId", example = "")
    private String openId;


}
