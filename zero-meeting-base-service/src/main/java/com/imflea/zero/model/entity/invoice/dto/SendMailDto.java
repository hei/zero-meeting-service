package com.imflea.zero.model.entity.invoice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value="SendMailDto对象", description="SendMailDto")
public class SendMailDto implements Serializable {
    private static final long serialVersionUID = -40613732207236907L;
    @ApiModelProperty(name = "请求id", required = true)
    private String id;
    @ApiModelProperty(name = "发票地址", required = true)
    private String contentUrl;
    @ApiModelProperty(name = "文件Id", required = true)
    private String contentId;
}
