package com.imflea.zero.model.entity.user;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 好友关系表
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="WebUserFriend对象", description="好友关系表")
public class WebUserFriend extends Model<WebUserFriend> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    private String id;

    @ApiModelProperty(value = "自己id")
    private String userId;

    @ApiModelProperty(value = "外键，好友用户id")
    private String friendId;

    @ApiModelProperty(value = "备注名称")
    private String remarksName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "注册时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
