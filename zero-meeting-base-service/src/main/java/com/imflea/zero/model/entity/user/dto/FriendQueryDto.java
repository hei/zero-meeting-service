package com.imflea.zero.model.entity.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * WebUserFriendDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "FriendDto对象", description = "好友查询，非分页条件")
public class FriendQueryDto implements Serializable {

    private static final long serialVersionUID = -2237367624317613841L;


    @ApiModelProperty(value = "好友备注名称或昵称", example = "")
    private String remarksName;


}
