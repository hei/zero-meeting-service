package com.imflea.zero.model.dao.delivery;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.delivery.MallDeliveryRegion;
import com.imflea.zero.model.entity.delivery.dto.RuleRecord;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyang
 * @since 2021-11-02
 */
public interface MallDeliveryRegionMapper extends BaseMapper<MallDeliveryRegion> {

    List<RuleRecord> queryRuleRecord(String ruleId);
}
