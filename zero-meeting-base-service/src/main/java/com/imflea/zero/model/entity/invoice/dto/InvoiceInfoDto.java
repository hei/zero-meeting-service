package com.imflea.zero.model.entity.invoice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * InvoiceInfoDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="InvoiceInfoDto对象", description="InvoiceInfoDto")
public class InvoiceInfoDto extends PageDto {

    @ApiModelProperty(value = "主键ID",example = "")
    private String Id;

    @ApiModelProperty(value = "抬头ID",example = "")
    private String invoiceHeaderId;

    @ApiModelProperty(value = "订单ID",example = "")
    private String orderId;

    @ApiModelProperty(value = "申请人ID")
    private String userId;

    @ApiModelProperty(value = "订单类型(adopt,mall)",example = "")
    private String orderType;

    @ApiModelProperty(value = "发票状态(已申请，已开具)",example = "")
    private String status;

    @ApiModelProperty(value = "文件地址",example = "")
    private String contentUrl;

    @ApiModelProperty(value = "删除标志",example = "")
    private String delFlag;

    @ApiModelProperty(value = "申请时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "申请人名称",example = "")
    private String createUserName;

    @ApiModelProperty(value = "申请时间",example = "")
    private String createUserId;

    @ApiModelProperty(value = "开票时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "开票人ID",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "开票人名称",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "商户ID",example = "")
    private String tenantId;

    @ApiModelProperty(value = "抬头的JSON字符串")
    private String headerJson;
}
