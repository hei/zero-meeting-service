package com.imflea.zero.model.entity.pay.vo;

import com.imflea.zero.model.entity.pay.dto.PayPreRecorderDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.pay.vo
 * @ClassName: PayPreDataVo
 * @Description
 * @date 2021-10-14  19:46:50
 */
@Data
public class PayPreDataVo {

    private BigDecimal payTotal;

    private List<PayPreRecorderDto> preRecorders;

    private String tenantId;

    private String payType;

    private String paySn;

    private String goodsName;

}
