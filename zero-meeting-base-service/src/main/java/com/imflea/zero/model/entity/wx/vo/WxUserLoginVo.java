package com.imflea.zero.model.entity.wx.vo;

import com.imflea.zero.model.entity.user.WebUserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.vo.wx
 * @ClassName: WxUserLoginVo
 * @Description
 * @date 2021-09-14  10:43:55
 */
@Data
@ApiModel(value = "登录成功后，返回结果")
public class WxUserLoginVo extends WebUserInfo {
    @ApiModelProperty(value = "accessToken")
    private String accessToken;

}
