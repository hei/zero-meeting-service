package com.imflea.zero.model.dao.cms;

import com.imflea.zero.model.entity.cms.CmsEnterpriseNews;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业资讯 Mapper 接口
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-24
 */
public interface CmsEnterpriseNewsMapper extends BaseMapper<CmsEnterpriseNews> {

}
