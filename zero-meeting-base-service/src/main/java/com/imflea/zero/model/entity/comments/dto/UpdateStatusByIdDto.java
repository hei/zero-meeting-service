package com.imflea.zero.model.entity.comments.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value="UpdateStatusByIdDto对象", description="UpdateStatusByIdDto")
public class UpdateStatusByIdDto {

    @ApiModelProperty(value = "主键ID",example = "")
    private String id;

    @ApiModelProperty(value = "评论状态(是否通过)",example = "")
    private String status;
}
