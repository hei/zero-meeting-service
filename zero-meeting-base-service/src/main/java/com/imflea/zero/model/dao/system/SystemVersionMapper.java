package com.imflea.zero.model.dao.system;

import com.imflea.zero.model.entity.system.SystemVersion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统发布版本 Mapper 接口
 * </p>
 *
 * @author xby
 * @since 2022-04-06
 */
public interface SystemVersionMapper extends BaseMapper<SystemVersion> {

}
