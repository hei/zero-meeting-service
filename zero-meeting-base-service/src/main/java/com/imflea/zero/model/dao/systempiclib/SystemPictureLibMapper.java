package com.imflea.zero.model.dao.systempiclib;

import com.imflea.zero.model.entity.systempiclib.SystemPictureLib;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统图库 Mapper 接口
 * </p>
 *
 * @author guohui
 * @since 2021-10-28
 */
public interface SystemPictureLibMapper extends BaseMapper<SystemPictureLib> {

}
