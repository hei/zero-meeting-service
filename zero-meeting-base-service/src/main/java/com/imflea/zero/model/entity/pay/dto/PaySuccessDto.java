package com.imflea.zero.model.entity.pay.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.pay.dto
 * @ClassName: PaySuccessDto
 * @Description
 * @date 2021-11-01  14:44:33
 */
@Data
public class PaySuccessDto implements Serializable {

    @ApiModelProperty(value = "订单id", example = "")
    private String orderId;

    @ApiModelProperty(value = "微信支付订单号/转账支付流水号", example = "")
    private String paySn;
    @ApiModelProperty(value = "支付类型", example = "")
    private String payType;
}
