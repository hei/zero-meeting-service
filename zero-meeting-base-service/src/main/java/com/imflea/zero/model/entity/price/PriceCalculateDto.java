package com.imflea.zero.model.entity.price;

import lombok.Data;

import java.util.List;

@Data

public class PriceCalculateDto {

    private String skuId;

    private Integer amount;

    private List<String> semAdoptIdList;

//    private List<String> semIdList;

//    private List<String> cashIdList;

}
