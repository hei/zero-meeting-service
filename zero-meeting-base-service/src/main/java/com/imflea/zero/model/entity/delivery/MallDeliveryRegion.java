package com.imflea.zero.model.entity.delivery;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyang
 * @since 2021-11-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="MallDeliveryRegion对象", description="")
public class MallDeliveryRegion extends Model<MallDeliveryRegion> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "城市")
    private String city;

    @Deprecated
    @ApiModelProperty(value = "默认标记[0非默认1默认]")
    private Integer defaultFlag;

    @ApiModelProperty(value = "起步价")
    private BigDecimal startPrice;

    @ApiModelProperty(value = "阶梯价")
    private BigDecimal normalPrice;

    @ApiModelProperty(value = "规则ID")
    private String ruleId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
