package com.imflea.zero.model.entity.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * MeetingOrderInfoItemDtoDTO
 * </p>
 *
 * @author zzz
 */
@Data
@ApiModel(value = "MeetingOrderInfoItemDto对象", description = "MeetingOrderInfoItemDto")
public class MeetingOrderInfoItemDto extends PageDto {

    @ApiModelProperty(value = "创建时间", example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人id", example = "")
    private String createUserId;

    @ApiModelProperty(value = "创建人账户名", example = "")
    private String createUserName;

    @ApiModelProperty(value = "删除标志（0正常，1删除）", example = "")
    private String delFlag;

    @ApiModelProperty(value = "订单结束时间", example = "")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "主键", example = "")
    private String id;

    @ApiModelProperty(value = "订单号", example = "")
    private String orderId;

    @ApiModelProperty(value = "会员id", example = "")
    private String roomId;

    @ApiModelProperty(value = "订单开始时间", example = "")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "租户id", example = "")
    private String tenantId;

    @ApiModelProperty(value = "订单所占日期", example = "")
    private LocalDate theDay;

    @ApiModelProperty(value = "修改时间", example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id", example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名", example = "")
    private String updateUserName;


}
