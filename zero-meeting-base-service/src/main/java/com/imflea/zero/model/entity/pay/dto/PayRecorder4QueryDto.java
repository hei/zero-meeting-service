package com.imflea.zero.model.entity.pay.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * PayRecoderInfoDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "PayRecoderInfo查询对象", description = "PayRecoderInfo查询对象")
public class PayRecorder4QueryDto extends PageDto {

    private static final long serialVersionUID = 6688148735459177362L;


    @ApiModelProperty(value = "订单号", example = "")
    private String orderSn;

    @ApiModelProperty(value = "支付方式：微信，银行转账，认养券", example = "")
    private String payType;

    @ApiModelProperty(value = "订单类型：牧场订单，商城订单", example = "")
    private String orderType;

    @ApiModelProperty(value = "订单状态：待支付，待上传证明，待确认，支付完成")
    private String payStatus;


}
