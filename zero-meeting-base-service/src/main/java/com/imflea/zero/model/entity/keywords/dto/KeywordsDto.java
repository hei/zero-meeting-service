package com.imflea.zero.model.entity.keywords.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * KeywordsDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="KeywordsDto对象", description="KeywordsDto")
public class KeywordsDto extends PageDto {

    @ApiModelProperty(value = "主键ID",example = "")
    private String id;

    @ApiModelProperty(value = "敏感词",example = "")
    private String words;

    @ApiModelProperty(value = "商户ID",example = "")
    private String tenantId;


}
