package com.imflea.zero.model.entity.sharestatistics.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * ShareStatisticsDtoDTO
 * </p>
 *
 * @author gh
 */
@Data
@ApiModel(value="ShareStatisticsDto对象", description="ShareStatisticsDto")
public class ShareStatisticsDto extends PageDto {

    @ApiModelProperty(value = "主键ID",example = "")
    private String id;

    @ApiModelProperty(value = "分享次数",example = "")
    private Integer shareNum;

    @ApiModelProperty(value = "分享后点进来的次数",example = "")
    private Integer comingNum;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "分享类型")
    private String businessType;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志")
    private String delFlag;

}
