package com.imflea.zero.model.dao.user;

import com.imflea.zero.model.entity.user.WebUserFriend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 好友关系表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
public interface WebUserFriendMapper extends BaseMapper<WebUserFriend> {

}
