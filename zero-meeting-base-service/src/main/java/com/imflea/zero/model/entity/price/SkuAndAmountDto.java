package com.imflea.zero.model.entity.price;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.price
 * @ClassName: SkuAndAmountDto
 * @Description
 * @date 2021-11-04  15:02:40
 */
@Data
public class SkuAndAmountDto implements Serializable {

    private String skuId;

    private Integer amount;

    @ApiModelProperty(value = "商品物流规则ID", example = "")
    private String ruleId;

    @ApiModelProperty(value = "物流模式", example = "")
    private Integer deliveryMode;


    @ApiModelProperty(value = "收货省份", example = "")
    private String receiverProvince;

    @ApiModelProperty(value = "收获城市", example = "")
    private String receiverCity;

}
