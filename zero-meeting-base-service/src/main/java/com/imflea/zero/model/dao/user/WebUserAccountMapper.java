package com.imflea.zero.model.dao.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.user.WebUserAccount;

/**
 * <p>
 * 微信端用户账户 Mapper 接口
 * </p>
 *
 * @author zzz
 * @since 2024-02-22
 */
public interface WebUserAccountMapper extends BaseMapper<WebUserAccount> {

}
