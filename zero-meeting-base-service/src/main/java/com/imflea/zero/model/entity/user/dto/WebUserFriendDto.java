package com.imflea.zero.model.entity.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * WebUserFriendDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="WebUserFriendDto对象", description="WebUserFriendDto")
public class WebUserFriendDto extends PageDto {

    @ApiModelProperty(value = "用户ID",example = "")
    private String Id;

    @ApiModelProperty(value = "自己id",example = "")
    private String userId;

    @ApiModelProperty(value = "外键，好友用户id",example = "")
    private String friendId;

    @ApiModelProperty(value = "备注名称",example = "")
    private String remarksName;


}
