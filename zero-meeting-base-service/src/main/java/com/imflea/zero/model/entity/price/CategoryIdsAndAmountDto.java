package com.imflea.zero.model.entity.price;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.price
 * @ClassName: SkuAndAmountDto
 * @Description
 * @date 2021-11-04  15:02:40
 */
@Data
public class CategoryIdsAndAmountDto implements Serializable {

    private String categoryId;

    private Integer amount;
}
