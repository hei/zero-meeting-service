package com.imflea.zero.model.entity.share.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import java.util.List;

import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * ShareConfigDtoDTO
 * </p>
 *
 * @author gh
 */
@Data
@ApiModel(value="ShareConfigDto对象", description="ShareConfigDto")
public class ShareConfigDto extends PageDto {

    @ApiModelProperty(value = "主键ID",example = "")
    private String id;

    @ApiModelProperty(value = "图片地址",example = "")
    private String pic;

    @ApiModelProperty(value = "标题",example = "")
    private String title;

    @ApiModelProperty(value = "业务模块Id",example = "")
    private String businessId;

    @ApiModelProperty(value = "跳转路由",example = "")
    private String uri;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人ID",example = "")
    private String createUserId;

    @ApiModelProperty(value = "创建人姓名",example = "")
    private String createUserName;

    @ApiModelProperty(value = "修改人名称",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人姓名",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "修改人id",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "删除标志",example = "")
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "文件路径List")
    List<ShareAttachmentDto> list;
}
