package com.imflea.zero.model.dao.logistics;

import com.imflea.zero.model.entity.logistics.BaseLogisticsCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-18
 */
public interface BaseLogisticsCodeMapper extends BaseMapper<BaseLogisticsCode> {

}
