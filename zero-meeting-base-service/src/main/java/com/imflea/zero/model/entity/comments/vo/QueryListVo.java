package com.imflea.zero.model.entity.comments.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.imflea.zero.model.entity.comments.CommentsInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@EqualsAndHashCode
@ApiModel(value="QueryListVo对象", description="")
public class QueryListVo {

    private static final long serialVersionUID = 1L;

    {
        this.nextList = new ArrayList<>();
    }

    @ApiModelProperty(value = "主键ID")
    private String id;

    @ApiModelProperty(value = "评论内容",example = "")
    private String comments;

    @ApiModelProperty(value = "评论类型",example = "")
    private String type;

    @ApiModelProperty(value = "是否精选",example = "")
    private String selected;

    @ApiModelProperty(value = "用户ID")
    private String userId;

    @ApiModelProperty(value = "业务模块ID")
    private String businessId;

    @ApiModelProperty(value = "评论业务类型（0点位，1认养物品类）")
    private String commentBusinessType;

    @ApiModelProperty(value = "商户ID")
    private String tenantId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人ID")
    private String createUserId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人名称")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新人ID")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新人名称")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否审核通过(0 通过，1未通过)")
    private String status;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志")
    private String delFlag;

    @ApiModelProperty(value = "评论的评论ID")
    private String toCommentsId;

    @ApiModelProperty(value = "对评论的评论")
    List<QueryListVo> nextList;

    @ApiModelProperty(value = "评论者的昵称")
    private String nickName;

    @ApiModelProperty(value = "评论人的头像")
    private String headImage;
}
