package com.imflea.zero.model.entity.share;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 分享界面图片配置表
 * </p>
 *
 * @author guohui
 * @since 2021-10-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ShareConfig对象", description="分享界面图片配置表")
public class ShareConfig extends Model<ShareConfig> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private String id;

    @ApiModelProperty(value = "图片地址")
    private String pic;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "业务模块Id")
    private String businessId;

    @ApiModelProperty(value = "跳转路由")
    private String uri;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人ID")
    private String createUserId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人姓名")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人名称")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人姓名")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志")
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "文件Id")
    private String attachmentId;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
