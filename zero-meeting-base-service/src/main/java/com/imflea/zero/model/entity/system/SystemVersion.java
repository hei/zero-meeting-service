package com.imflea.zero.model.entity.system;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统发布版本
 * </p>
 *
 * @author xby
 * @since 2022-04-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SystemVersion对象", description="系统发布版本")
public class SystemVersion extends Model<SystemVersion> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private String id;

    @ApiModelProperty(value = "版本号")
    private String versionNo;

    @ApiModelProperty(value = "版本更新内容")
    private String remarks;

    @ApiModelProperty(value = "版本发布时间")
    private LocalDate postTime;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人名称")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人ID")
    private String createUserId;
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新人名称")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新人ID")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志")
    private String delFlag;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
