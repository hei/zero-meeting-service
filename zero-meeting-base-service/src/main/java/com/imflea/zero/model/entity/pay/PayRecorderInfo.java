package com.imflea.zero.model.entity.pay;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 支付记录
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PayRecoderInfo对象", description = "支付记录")
public class PayRecorderInfo extends Model<PayRecorderInfo> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "订单号")
    private String orderSn;

    @ApiModelProperty(value = "支付流水号")
    private String paySn;

    @ApiModelProperty(value = "支付方式：微信，银行转账")
    private String payType;

    @ApiModelProperty(value = "订单类型：牧场订单，商城订单")
    private String orderType;
    @ApiModelProperty(value = "订单状态：待支付，待上传证明，待确认，支付完成")
    private String payStatus;

    @ApiModelProperty(value = "总金额")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "支付详情:对公账户，微信收款商户等")
    private String payDetail;

    @ApiModelProperty(value = "支付人")
    private String payUserId;

    @ApiModelProperty(value = "支付人openId")
    private String payUserOpenId;

    @ApiModelProperty(value = "支付人姓名")
    private String payUserName;

    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
