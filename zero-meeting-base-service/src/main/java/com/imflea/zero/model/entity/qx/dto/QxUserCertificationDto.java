package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * QxUserCertificationDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="QxUserCertificationDto对象", description="QxUserCertificationDto")
public class QxUserCertificationDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String Id;

    @ApiModelProperty(value = "用户id",example = "")
    private String userId;

    @ApiModelProperty(value = "盐",example = "")
    private String Salt;

    @ApiModelProperty(value = "密码",example = "")
    private String Password;

    @ApiModelProperty(value = "删除标志（0正常，1删除）",example = "")
    private String delFlag;

    @ApiModelProperty(value = "修改时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id",example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id",example = "")
    private String tenantId;


}
