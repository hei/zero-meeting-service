package com.imflea.zero.model.dao.qx;

import com.imflea.zero.model.entity.qx.QxTenantConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 租户配置表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
public interface QxTenantConfigMapper extends BaseMapper<QxTenantConfig> {

}
