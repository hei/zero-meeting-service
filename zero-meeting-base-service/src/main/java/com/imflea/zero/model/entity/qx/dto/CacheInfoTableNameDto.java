package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * QxCacheInfoDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "缓存查询条件-name", description = "缓存查询条件")
public class CacheInfoTableNameDto {

    @ApiModelProperty(value = "表名", example = "")
    private String tableName;


}
