package com.imflea.zero.model.entity.pay;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 转账支付凭证
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "PayTransportProof对象", description = "转账支付凭证")
public class PayTransportProof extends Model<PayTransportProof> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "订单号")
    private String orderSn;

    @ApiModelProperty(value = "支付记录id")
    private String payRecorderId;

    @ApiModelProperty(value = "订单提交时间")
    private LocalDateTime commitTime;

    @ApiModelProperty(value = "支付凭证ID")
    private String proofId;

    @ApiModelProperty(value = "支付凭证URL")
    private String proofUrl;

    @ApiModelProperty(value = "凭证类型")
    private String proofType;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
