package com.imflea.zero.model.dao.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.cms.CmsTemplate;
import com.imflea.zero.model.entity.cms.vo.CmsContentVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 发布模板信息 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-17
 */
public interface CmsTemplateMapper extends BaseMapper<CmsTemplate> {

    List<CmsContentVo> seleContent(@Param("publishType") String publishType, @Param("contentType") String contentType, @Param("scope") String scope);

    List<CmsContentVo> selectContent(String id);
}
