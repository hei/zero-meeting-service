package com.imflea.zero.model.dao.sharestatistics;

import com.imflea.zero.model.entity.sharestatistics.ShareStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分享的统计分析 Mapper 接口
 * </p>
 *
 * @author guohui
 * @since 2021-10-22
 */
public interface ShareStatisticsMapper extends BaseMapper<ShareStatistics> {

}
