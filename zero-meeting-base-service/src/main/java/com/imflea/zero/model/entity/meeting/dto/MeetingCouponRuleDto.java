package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * MeetingCouponRuleDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value = "MeetingCouponRuleDto对象", description = "MeetingCouponRuleDto")
public class MeetingCouponRuleDto extends PageDto {

    @ApiModelProperty(value = "主键", example = "")
    private String id;

    @ApiModelProperty(value = "规则名称", example = "")
    private String name;

    @ApiModelProperty(value = "满足的条件：1000；5000等", example = "")
    private Integer conditionBase;

    @ApiModelProperty(value = "满足的条件的结果：送1000；送5000等", example = "")
    private Integer conditionResult;

    @ApiModelProperty(value = "0启用1关闭", example = "")
    private String status;

    @ApiModelProperty(value = "备注", example = "")
    private String remarks;

    @ApiModelProperty(value = "删除标志（0生效，1不生效）", example = "")
    private String delFlag;

    @ApiModelProperty(value = "修改时间", example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id", example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名", example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间", example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名", example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id", example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id", example = "")
    private String tenantId;


}
