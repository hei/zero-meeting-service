package com.imflea.zero.model.entity.sharestatistics;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 分享的统计分析
 * </p>
 *
 * @author guohui
 * @since 2021-10-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ShareStatistics对象", description="分享的统计分析")
public class ShareStatistics extends Model<ShareStatistics> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private String id;

    @ApiModelProperty(value = "分享次数")
    private Integer shareNum;

    @ApiModelProperty(value = "分享后点进来的次数")
    private Integer comingNum;

    @ApiModelProperty(value = "租户id")
    private String tenantId;

    @ApiModelProperty(value = "分享类型")
    private String businessType;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志")
    private String delFlag;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
