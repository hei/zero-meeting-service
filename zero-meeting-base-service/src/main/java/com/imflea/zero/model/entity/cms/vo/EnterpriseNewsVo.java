package com.imflea.zero.model.entity.cms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.cms.vo
 * @filename 创建时间: 2021/9/24
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value = "EnterpriseNewsVo对象", description = "企业新闻信息")
public class EnterpriseNewsVo {

    @ApiModelProperty(value = "新闻记录")
    private List<Map<String, Object>> list;

    @ApiModelProperty(value = "新闻总数")
    private Long total;
}
