package com.imflea.zero.model.entity.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value="RechargeOrderAppSaveDto对象", description="RechargeOrderAppSaveDto对象")
public class RechargeOrderAppSaveDto {

    @ApiModelProperty(value = "会员id")
    private String memberId;

    @ApiModelProperty(value = "会员名称")
    private String memberName;

    @ApiModelProperty(value = "充值金额")
    private BigDecimal preTotalAmount;







}
