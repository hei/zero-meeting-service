package com.imflea.zero.model.entity.pay.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @param
 * @author 祥保玉
 * @description
 * @date 2021/9/26  15:45
 * @return null
 */


@Data
@ApiModel(value = "订单号dto", description = "订单号dto")
public class OrderSnDto implements Serializable {
    private static final long serialVersionUID = -8317109327401232779L;
    @ApiModelProperty(value = "订单编号")
    private String orderSn;


}
