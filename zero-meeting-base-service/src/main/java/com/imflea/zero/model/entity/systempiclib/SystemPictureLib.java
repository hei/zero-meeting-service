package com.imflea.zero.model.entity.systempiclib;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统图库
 * </p>
 *
 * @author guohui
 * @since 2021-10-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="SystemPictureLib对象", description="系统图库")
public class SystemPictureLib extends Model<SystemPictureLib> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private String id;

    @ApiModelProperty(value = "图片Id")
    private String picId;

    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    @ApiModelProperty(value = "业务模块标识（字典项）")
    private String businessType;

    @ApiModelProperty(value = "业务ID（预留）")
    private String businessId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人名称")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人ID")
    private String createUserId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新人名称")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新人ID")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志")
    private String delFlag;

    @ApiModelProperty(value = "排序字段")
    private Integer sort;

    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
