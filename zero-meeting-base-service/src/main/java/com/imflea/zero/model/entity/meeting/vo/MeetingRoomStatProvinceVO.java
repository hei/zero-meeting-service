package com.imflea.zero.model.entity.meeting.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "MeetingRoomStatProvinceVO", description = "MeetingRoomStatProvinceVO")
public class MeetingRoomStatProvinceVO {
    @ApiModelProperty(value = "省-名称")
    private String provinceName;
    @ApiModelProperty(value = "省-代码")
    private String province;
    @ApiModelProperty(value = "个数")
    private Integer count;
    @ApiModelProperty(value = "空间类型：0小型办公空间，1：多功能办公空间")
    private String roomType;
}
