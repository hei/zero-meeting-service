package com.imflea.zero.model.dao.qx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.qx.QxRole;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
public interface QxRoleMapper extends BaseMapper<QxRole> {

}
