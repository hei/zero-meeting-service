package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.adopt.dto
 * @ClassName: AdoptedPageQueryDto
 * @Description
 * @date 2021-10-25  15:16:08
 */
@Data
@ApiModel("认养物id。查询所有订单信息")
@EqualsAndHashCode(callSuper = false)
public class MeetingOrderPageQueryDto extends PageDto {
    @ApiModelProperty(name = "已认养物id", required = true)
    private String id;

}
