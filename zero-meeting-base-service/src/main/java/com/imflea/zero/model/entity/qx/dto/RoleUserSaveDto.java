package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req.qx
 * @ClassName: RoleUserSaveReq
 * @Description
 * @date 2021-08-19  15:50:58
 */
@Data
@ApiModel(value = "人员角色授权请求对象")
@EqualsAndHashCode(callSuper = false)
public class RoleUserSaveDto implements Serializable {
    private static final long serialVersionUID = 2240278393581350516L;
    @ApiModelProperty(value = "用户id", required = true)
    private String userId;
    @ApiModelProperty(value = "授权的角色的id集合", required = true)
    private List<String> roleIds;

}
