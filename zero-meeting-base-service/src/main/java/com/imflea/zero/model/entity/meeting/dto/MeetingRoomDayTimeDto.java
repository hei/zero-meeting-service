package com.imflea.zero.model.entity.meeting.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

@Data
@ApiModel("计算会议室可使用时间")
public class MeetingRoomDayTimeDto {


    @ApiModelProperty(value = "空间ID")
    private String roomId;
    @ApiModelProperty(value = "会议室租赁日期")
    private LocalDate theDay;


}
