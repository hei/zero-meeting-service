package com.imflea.zero.model.entity.systempiclib.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Data
@ApiModel(value="QueryListDto对象", description="QueryListDto")
public class QueryListDto {
    @ApiModelProperty(value = "业务模块标识（字典项）",example = "")
    private String businessType;
}
