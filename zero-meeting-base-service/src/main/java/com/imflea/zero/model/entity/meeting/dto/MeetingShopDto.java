package com.imflea.zero.model.entity.meeting.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>
 * MeetingShopDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="MeetingShopDto对象", description="MeetingShopDto")
public class MeetingShopDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "省/直辖市",example = "")
    private String province;

    @ApiModelProperty(value = "市",example = "")
    private String city;

    @ApiModelProperty(value = "区",example = "")
    private String region;

    @ApiModelProperty(value = "详细地址",example = "")
    private String detailAddress;

    @ApiModelProperty(value = "排序",example = "")
    private Integer sort;

    @ApiModelProperty(value = "是否正常营业（0 正常，1 歇业）",example = "")
    private String status;

    @ApiModelProperty(value = "门店名称（简称）",example = "")
    private String companyName;

    @ApiModelProperty(value = "营业开始时间",example = "")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "歇业时间",example = "")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "删除标志（0正常，1删除）",example = "")
    private String delFlag;

    @ApiModelProperty(value = "修改时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id",example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id",example = "")
    private String tenantId;



}
