package com.imflea.zero.model.entity.task.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * ScheduleTaskDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="ScheduleTaskDto对象", description="ScheduleTaskDto")
public class ScheduleTaskDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "任务key值（使用bean名称）",example = "")
    private String taskKey;

    @ApiModelProperty(value = "任务名称",example = "")
    private String name;

    @ApiModelProperty(value = "任务表达式",example = "")
    private String cron;

    @ApiModelProperty(value = "状态(0.禁用; 1.启用)",example = "")
    private String status;

    @ApiModelProperty(value = "修改时间",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "修改人id",example = "")
    private String updateUserId;

    @ApiModelProperty(value = "修改人账户名",example = "")
    private String updateUserName;

    @ApiModelProperty(value = "创建时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "创建人账户名",example = "")
    private String createUserName;

    @ApiModelProperty(value = "创建人id",example = "")
    private String createUserId;

    @ApiModelProperty(value = "租户id",example = "")
    private String tenantId;



}
