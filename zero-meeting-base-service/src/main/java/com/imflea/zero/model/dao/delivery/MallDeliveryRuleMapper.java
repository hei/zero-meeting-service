package com.imflea.zero.model.dao.delivery;

import com.imflea.zero.model.entity.delivery.MallDeliveryRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhaoyang
 * @since 2021-11-02
 */
public interface MallDeliveryRuleMapper extends BaseMapper<MallDeliveryRule> {

}
