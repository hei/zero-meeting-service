package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * QxCacheInfoDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="QxCacheInfoDto对象", description="QxCacheInfoDto")
public class QxCacheInfoDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "表名",example = "")
    private String tableName;

    @ApiModelProperty(value = "缓存索引列",example = "")
    private String indexColumns;

    @ApiModelProperty(value = "缓存索引对应名称",example = "")
    private String indexCaption;

    @ApiModelProperty(value = "数据查询sql",example = "")
    private String dataQuerySql;

    @ApiModelProperty(value = "'MAP||LISTALL||LISTGROUP||DM2MC'",example = "")
    private String redisType;

    @ApiModelProperty(value = "分组字段",example = "")
    private String groupIndex;


}
