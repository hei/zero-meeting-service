package com.imflea.zero.model.entity.logistics;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="BaseLogisticsCode对象", description="")
public class BaseLogisticsCode extends Model<BaseLogisticsCode> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "快递公司的编码")
    private String cpCode;

    @ApiModelProperty(value = "快递公司名称")
    private String company;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标记")
    private String delFlag;


    @Override
    public Serializable pkVal() {
        return null;
    }

}
