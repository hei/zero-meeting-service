package com.imflea.zero.model.entity.cms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.model.entity.cms.dto
 * @filename CmsProductGroupDto 创建时间: 2021/9/13
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Deprecated
@Data
@ApiModel(value = "ProductGroupDto对象", description = "商品的分组信息,小程序查询参数")
public class ProductGroupDto {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "分组名称")
    private String name;

    @ApiModelProperty(value = "上级分类,默认空串")
    private String parentId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;
}
