package com.imflea.zero.model.dao.pay;

import com.imflea.zero.model.entity.pay.PayRecorderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 支付记录 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-27
 */
public interface PayRecoderInfoMapper extends BaseMapper<PayRecorderInfo> {

    List<PayRecorderInfo> queryByOrderIds(List<String> list);
}
