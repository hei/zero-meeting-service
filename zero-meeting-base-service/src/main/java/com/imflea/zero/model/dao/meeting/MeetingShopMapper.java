package com.imflea.zero.model.dao.meeting;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.meeting.MeetingShop;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xby
 * @since 2024-01-24
 */
public interface MeetingShopMapper extends BaseMapper<MeetingShop> {

}
