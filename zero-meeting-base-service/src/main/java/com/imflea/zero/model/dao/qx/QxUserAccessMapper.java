package com.imflea.zero.model.dao.qx;

import com.imflea.zero.model.entity.qx.QxUserAccess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户每天访问数量表 Mapper 接口
 * </p>
 *
 * @author zhaoyang
 * @since 2022-01-13
 */
public interface QxUserAccessMapper extends BaseMapper<QxUserAccess> {

}
