package com.imflea.zero.model.dao.cms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.cms.CmsContentInfo;

/**
 * <p>
 * 发布正文信息表 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-17
 */
public interface CmsContentInfoMapper extends BaseMapper<CmsContentInfo> {

}
