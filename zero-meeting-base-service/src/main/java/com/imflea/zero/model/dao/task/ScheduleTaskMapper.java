package com.imflea.zero.model.dao.task;

import com.imflea.zero.model.entity.task.ScheduleTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务配置表 Mapper 接口
 * </p>
 *
 * @author zhaoyang
 * @since 2022-01-14
 */
public interface ScheduleTaskMapper extends BaseMapper<ScheduleTask> {

}
