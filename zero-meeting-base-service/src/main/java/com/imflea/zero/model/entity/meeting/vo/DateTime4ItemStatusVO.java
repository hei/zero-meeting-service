package com.imflea.zero.model.entity.meeting.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

/**
 * <p>
 * 空间订单表-分项表
 * </p>
 *
 * @author zzz
 * @since 2024-03-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DateTime4ItemStatusVO", description = "DateTime4ItemStatusVO-分项表")
public class DateTime4ItemStatusVO {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "订单占用时间")
    private String theTime;


    @ApiModelProperty(value = "订单所占日期")
    private LocalDate theDay;
    @ApiModelProperty(value = "所占用时间，是否可用：0：可用；1已预定；2未营业")
    private String useState;


}
