package com.imflea.zero.model.entity.user.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * WebUserAccountDtoDTO
 * </p>
 *
 * @author zzz
 */
@Data
@ApiModel(value="WebUserAccountDto对象", description="WebUserAccountDto")
public class WebUserAccountDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String id;

    @ApiModelProperty(value = "web用户id",example = "")
    private String userId;

    @ApiModelProperty(value = "账户余额（元）",example = "")
    private BigDecimal userAccount;

    @ApiModelProperty(value = "注册时间",example = "")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "",example = "")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "删除标志（0正常，1删除）",example = "")
    private String delFlag;

    @ApiModelProperty(value = "租户id",example = "")
    private String tenantId;


}
