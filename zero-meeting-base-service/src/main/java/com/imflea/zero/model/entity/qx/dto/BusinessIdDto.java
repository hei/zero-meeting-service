package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@ApiModel("业务主键id")
public class BusinessIdDto implements Serializable {

    /**
     * @description 字段功能描述
     * @value value:serialVersionUID
     */
    private static final long serialVersionUID = -966924196376852L;
    @NotBlank(message="业务ID不可为空")
    @ApiModelProperty(value="请求数据编号",required = true)
    private String businessId;

}
