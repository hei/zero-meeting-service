package com.imflea.zero.model.entity.user.vo;

import com.imflea.zero.model.entity.user.WebUserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "WebUserInfoDetailVo对象", description = "微信端用户基本信息-详细信息")
public class WebUserInfoDetailVo extends WebUserInfo {
    @ApiModelProperty(value = "账户余额（元）")
    private BigDecimal userAccount;

}
