package com.imflea.zero.model.entity.pay.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.model.entity.pay.vo
 * @ClassName: PayPreRefundVo
 * @Description
 * @date 2021-11-02  16:59:21
 */
@Data
@ApiModel(value = "退款预处理数据对象", description = "退款预处理数据对象")
public class PayPreRefundVo implements Serializable {

    @ApiModelProperty(value = "订单中原支付统一流水号")
    private String paySn;
    @ApiModelProperty(value = "退款金额")
    private BigDecimal refundAmount;
    @ApiModelProperty(value = "原支付金额")
    private BigDecimal totalFee;
    @ApiModelProperty(value = "原订单支付方式",example = "PAY_TYPE_WX PAY_TYPE_TRANSFER ")
    private String payType;
}
