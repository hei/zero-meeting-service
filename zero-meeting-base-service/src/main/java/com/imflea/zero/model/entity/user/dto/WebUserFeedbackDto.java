package com.imflea.zero.model.entity.user.dto;

import com.imflea.zero.dto.PageDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * WebUserFeedbackDtoDTO
 * </p>
 *
 * @author xby
 */
@Data
@ApiModel(value="WebUserFeedbackDto对象", description="WebUserFeedbackDto")
public class WebUserFeedbackDto extends PageDto {

    @ApiModelProperty(value = "主键",example = "")
    private String Id;

    @ApiModelProperty(value = "主题",example = "")
    private String theme;

    @ApiModelProperty(value = "会员iD",example = "")
    private String memberId;

    @ApiModelProperty(value = "会员名",example = "")
    private String memberName;

    @ApiModelProperty(value = "回复人",example = "")
    private String replId;

    @ApiModelProperty(value = "回复人名称",example = "")
    private String replyName;


}
