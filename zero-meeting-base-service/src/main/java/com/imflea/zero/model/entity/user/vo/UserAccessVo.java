package com.imflea.zero.model.entity.user.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.vo.user
 * @ClassName: FriendVo
 * @Description
 * @date 2021-09-23  15:14:43
 */
@Data
@ApiModel("用户访问基本数据vo")
public class UserAccessVo implements Serializable {
    private static final long serialVersionUID = 1336146578783L;
    @ApiModelProperty(value = "租户id")
    private String tenantId;
    @ApiModelProperty(value = "用户id")
    private String userId;


}
