package com.imflea.zero.model.dao.qx;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.imflea.zero.model.entity.qx.QxCacheInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 缓存基本信息 Mapper 接口
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-17
 */
public interface QxCacheInfoMapper extends BaseMapper<QxCacheInfo> {

    List<Map<String, Object>> queryAllTableData(String sql) ;

}
