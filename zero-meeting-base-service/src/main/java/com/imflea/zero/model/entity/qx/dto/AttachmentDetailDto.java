package com.imflea.zero.model.entity.qx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 附件
 * </p>
 *
 * @author xiangbaoyu
 * @since 2020-11-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="attachment业务细节操作对象模型", description="附件")
public class AttachmentDetailDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "系统名称")
    private String businessSysCode;

    @ApiModelProperty(value = "系统业务模块名称")
    private String businessSysModuleCode;

    @ApiModelProperty(value = "系统业务主键id")
    private String businessId;



}
