package com.imflea.zero.model.entity.qx;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 系统通知
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "QxNotice对象", description = "系统通知")
public class QxNotice extends Model<QxNotice> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "通知标题")
    private String title;
    @ApiModelProperty(value = "0 未发布，1 发布")
    private String status;

    @ApiModelProperty(value = "通知类型")
    private String noticeType;

    @ApiModelProperty(value = "发布时间")
    private LocalDateTime postTime;

    @ApiModelProperty(value = "有效期开始时间：十一假期通知公告")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "有效期结束时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "排序：可置顶，获取当前最高的数字，用倒排")
    private Integer orderNum;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "删除标志（0正常，1删除）")
    private String delFlag;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人id")
    private String updateUserId;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "修改人账户名")
    private String updateUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人账户名")
    private String createUserName;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人id")
    private String createUserId;

    @ApiModelProperty(value = "租户id")
    private String tenantId;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
