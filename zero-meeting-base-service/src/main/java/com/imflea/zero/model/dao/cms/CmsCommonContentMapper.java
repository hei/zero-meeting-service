package com.imflea.zero.model.dao.cms;

import com.imflea.zero.model.entity.cms.CmsCommonContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公共富文本信息表 Mapper 接口
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-23
 */
public interface CmsCommonContentMapper extends BaseMapper<CmsCommonContent> {

}
