package com.imflea.zero.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;


@Component
//@ConditionalOnMissingBean(name = "zeroApplicationContextUtil")
//@DependsOn("applicationContext")
public final class ZeroApplicationContextUtil implements ApplicationContextAware {

    /**
     *
     */
    private static ApplicationContext applicationContext;

    /**
     * @constructor 构造方法
     */
    private ZeroApplicationContextUtil() {
    }

    /**
     * @param applicationContext applicationContext
     * @throws BeansException
     * @name 中文名称
     * @description 相关说明
     * @time 创建时间:2019年3月2日下午2:58:52
     * @author 史雪涛
     * @history 修订历史（历次修订内容、修订人、修订时间等）
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ZeroApplicationContextUtil.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static <T> T getBean(String name, Class<T> aClass) {

        return applicationContext.getBean(name, aClass);
    }

    public static <T> T getBean(Class<T> aClass) {

        return applicationContext.getBean(aClass);
    }


}