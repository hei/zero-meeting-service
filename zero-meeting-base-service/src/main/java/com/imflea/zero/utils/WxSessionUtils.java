package com.imflea.zero.utils;


import com.imflea.zero.constant.QxContant;
import com.imflea.zero.util.base.ZeroThreadLocalUtils;

import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.utils
 * @ClassName: WxSessionUtils
 * @Description
 * @date 2021-09-13  14:45:35
 */
@Deprecated
public class WxSessionUtils {

//    public static String getTenantId() {
//        return (String)ZeroThreadLocalUtils.get("tenantId");
//    }

    public static String getUserId() {
        return (String) ZeroThreadLocalUtils.get("userId");
    }

    public static String getOpenId() {
        return (String)ZeroThreadLocalUtils.get("openId");
    }

    public static String accessToken() {
        return (String)ZeroThreadLocalUtils.get("accessToken");
    }


/*    public static void makeSession(Map<String, Object> token) {
        String accessToken = (String) token.get(QxContant.getWxLoginAccessTokenKey4front());
        String tenantId = (String) token.get("tenantId");
        String userId = (String) token.get("userId");
        String openId = (String) token.get("openId");

       ZeroThreadLocalUtils.add("accessToken", accessToken);
       ZeroThreadLocalUtils.add("userId", userId);
       ZeroThreadLocalUtils.add("tenantId", tenantId);
       ZeroThreadLocalUtils.add("openId", openId);
    }*/
}
