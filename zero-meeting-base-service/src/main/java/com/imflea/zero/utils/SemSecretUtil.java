package com.imflea.zero.utils;

import java.nio.charset.StandardCharsets;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.utils
 * @ClassName: SemSecrtUtil
 * @Description
 * @date 2021-12-22  10:53:17
 */
public class SemSecretUtil {

    // 盐值千万不要修改
    private static final String SALT = "THERE IS NO SMOKE WITHOUT FIRE!";

    // 生成密钥
    public static String generateSecret(String salt, byte[] md5) {
        String hmacMd5Encrypt = JdkHmacUtils.encryptHmacMD5(salt.getBytes(StandardCharsets.UTF_8), md5);
        String secret = hmacMd5Encrypt.substring(0, 8);
        return secret;
    }

    public static String generateSecret(String salt) {
        byte[] md5 = JdkHmacUtils.getHmacMd5Key();
        return generateSecret(SALT, md5);
    }

    public static String generateSecret() {
        byte[] md5 = JdkHmacUtils.getHmacMd5Key();
        return generateSecret(SALT, md5);
    }


}
