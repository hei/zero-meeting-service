package com.imflea.zero.utils;

import java.util.Random;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.utils
 * @filename 创建时间: 2021/10/28
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
public class GoodsCardUtil {

    private static final String sources = "0123456789";

    public static String gen() {
        Random rand = new Random();
        StringBuilder flag = new StringBuilder();
        for (int j = 0; j < 6; j++) {
            flag.append(sources.charAt(rand.nextInt(9)));
        }
        return flag.toString();
    }

    public static void main(String[] args) {

        for (int i = 0; i <= 100; i++) {
            gen();
        }

    }

    // 数据领货码后 将订单的状态置为完成即可
}
