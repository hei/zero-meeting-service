package com.imflea.zero.utils;

import java.util.Random;

public class OrderUtils {

    public static String generateOrderSn(){
        String timeStr = String.valueOf(System.currentTimeMillis());
        String random = String.format("%02d",new Random().nextInt(99));
        return timeStr + random;
    }

}
