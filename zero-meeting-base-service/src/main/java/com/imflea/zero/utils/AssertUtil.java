package com.imflea.zero.utils;

import com.imflea.zero.exception.CustomIllegalArgumentException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Map;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.utils
 * @filename null.java 创建时间: 2021/9/9
 * @description 描述（参数校验断言处理类）
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
public final class AssertUtil {

    private AssertUtil() {
        throw new IllegalStateException("Util class no instance ");
    }

    public static void mustEmpty(String info, String msg) {
        if (info == null || info.isEmpty()) {
            return;
        }
        throw new CustomIllegalArgumentException(msg);
    }

    public static void mustNotEmpty(Object info, String msg) {
        if (info == null) {
            throw new CustomIllegalArgumentException(msg);
        }
        if (info instanceof String) {
            String temp = (String) info;
            if (temp.isEmpty()) {
                throw new CustomIllegalArgumentException(msg);
            }
        } else if (info instanceof Collection) {
            Collection<?> temp = (Collection) info;
            if (temp.isEmpty()) {
                throw new CustomIllegalArgumentException(msg);
            }
        } else if (info instanceof Map) {
            Map<?,?> temp = (Map) info;
            if (temp.isEmpty()) {
                throw new CustomIllegalArgumentException(msg);
            }
        }
    }

    public static void mustNotNull(Object info, String msg) {
        Assert.notNull(info, msg);
    }

    public static void main(String[] args) {
        String number = desensitizedPhoneNumber("15210925844");
        System.out.println(number);
    }

    // 脱敏规则: 保留前三后四, 比如15638296218置换为156****6218
    private static String desensitizedPhoneNumber(String phoneNumber) {
        if (StringUtils.isNotEmpty(phoneNumber)) {
            phoneNumber = phoneNumber.replaceAll("(\\w{3})\\w*(\\w{4})", "$1****$2");
        }
        return phoneNumber;
    }
}
