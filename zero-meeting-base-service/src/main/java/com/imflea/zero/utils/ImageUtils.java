package com.imflea.zero.utils;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.Map;

public class ImageUtils {

	static int targetLength = 66;// 小图长
	static int targetWidth = 66;// 小图宽
	static int circleR = 6;// 半径
	static int r1 = 3;// 距离点

	@RequestMapping(value = "/test", method = RequestMethod.POST)
	public Object insertTemplateFile() throws Exception {
		return createImage(new FileInputStream(new File("C:\\Users\\admin\\Desktop\\1111.jpg")));
	}

	/**
	 * 生成小图片、给大图片添加阴影
	 * 
	 * @Title: cutByTemplate
	 * @author 张伟
	 * @data 2020年3月2日 下午9:08:02
	 * @param oriImage
	 * @param targetImage
	 * @param templateImage
	 * @param x
	 * @param y             void
	 */
	private static void cutByTemplate(BufferedImage oriImage, BufferedImage targetImage, int x, int y) {
		for (int i = 0; i < targetLength; i++) {
			for (int j = 0; j < targetWidth; j++) {
				// 原图中对应位置变色处理
				int rgb_ori = 0;
				try {
					rgb_ori = oriImage.getRGB(y + j, x + i);
				} catch (Exception e) {
					throw new RuntimeException((y + j) + "---------------" + (x + i));
				}

				// 抠图上复制对应颜色值
				targetImage.setRGB(j, i, rgb_ori);
				// 原图对应位置颜色变化
				oriImage.setRGB(y + j, x + i, 16777215);
			}

		}
	}

	/**
	 * 获取大图，小图Base64码
	 * 
	 * @Title: createImage
	 * @author 张伟
	 * @data 2020年3月3日 上午9:27:20
	 * @param url
	 * @param L
	 * @param W
	 * @param resultMap
	 * @return Map<String,String>
	 * @throws IOException
	 */
	@SuppressWarnings("unused")
    public static Map<String, String> createImage(InputStream is) throws IOException {
		Map<String, String> resultMap = new HashMap<String, String>();
		BufferedImage bufferedImage = ImageIO.read(is);

		int maxHeight = bufferedImage.getHeight();
		int maxWidth = bufferedImage.getWidth();

		int L = (int) (Math.random() * 110) + 10;
		int W = (int) (Math.random() * 300) + 100;

		BufferedImage target = new BufferedImage(targetLength, targetWidth, BufferedImage.TYPE_4BYTE_ABGR);
		cutByTemplate(bufferedImage, target, L, W);
		resultMap.put("bigImage", getImageBASE64(bufferedImage));// 大图
		resultMap.put("smallImage", getImageBASE64(target));// 小图
		resultMap.put("height", (L - 2) + "");// 高度
		resultMap.put("width", W + "");// 宽度

		return resultMap;
	}
	@SuppressWarnings("unused")
    public static Map<String, String> createImage(BufferedImage bufferedImage) throws IOException {
		Map<String, String> resultMap = new HashMap<String, String>();
		
		int maxHeight = bufferedImage.getHeight();
		int maxWidth = bufferedImage.getWidth();
		
		int L = (int) (Math.random() * 110) + 10;
		int W = (int) (Math.random() * 300) + 100;
		
		BufferedImage target = new BufferedImage(targetLength, targetWidth, BufferedImage.TYPE_4BYTE_ABGR);
		cutByTemplate(bufferedImage, target, L, W);
		resultMap.put("bigImage", getImageBASE64(bufferedImage));// 大图
		resultMap.put("smallImage", getImageBASE64(target));// 小图
		resultMap.put("height", (L - 2) + "");// 高度
		resultMap.put("width", W + "");// 宽度
		
		return resultMap;
	}

	/**
	 * 图片转BASE64
	 * 
	 * @Title: getImageBASE64
	 * @author 张伟
	 * @data 2020年3月3日 上午9:27:14
	 * @param image
	 * @return
	 * @throws IOException String
	 */
	public static String getImageBASE64(BufferedImage image) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(image, "png", out);
		byte[] b = out.toByteArray();// 转成byte数组
		out.close();
		Encoder encoder = Base64.getEncoder();
		return encoder.encodeToString(b);
	}
}
