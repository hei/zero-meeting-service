package com.imflea.zero.utils;

import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.util.base.ZeroThreadLocalUtils;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.utils
 * @ClassName: SessionUtils
 * @Description
 * @date 2021-08-19  14:08:31
 */
@Slf4j
public class SessionUtils {


    public static String getTenantId() {
        return (String) ZeroThreadLocalUtils.get("tenantId");
    }

    public static String getOpenId() {
        return (String) ZeroThreadLocalUtils.get("openId");
    }


    public static String getUserId() {
        return (String) ZeroThreadLocalUtils.get("userId");
    }

    public static List<String> getRoleIds() {
        return (List<String>) ZeroThreadLocalUtils.get("roleIds");
    }

    public static Boolean isMdAdmin() {

        List<String> roleIds = getRoleIds();
        if (!CommonUtils.isNull(roleIds) && roleIds.contains(QxContant.getMdAdminRoleId())) {
            return true;
        } else {
            return false;
        }
    }

    public static Boolean isZbAdmin() {
        List<String> roleIds = getRoleIds();
        if (!CommonUtils.isNull(roleIds) && roleIds.contains(QxContant.getZbAdminRoleId())) {
            return true;
        } else {
            return false;
        }
    }

    public static Boolean isTenantAdmin() {
        List<String> roleIds = getRoleIds();
        if (!CommonUtils.isNull(roleIds) && roleIds.contains(QxContant.getTenantAdminRoleId())) {
            return true;
        } else {
            return false;
        }
    }

    public static Boolean isTenantBuinessAdmin() {
        List<String> roleIds = getRoleIds();
        if (!CommonUtils.isNull(roleIds) && roleIds.contains(QxContant.getTenantBuinessRoleId())) {
            return true;
        } else {
            return false;
        }
    }

    public static String getAvatarUrl() {
        String avatarUrl = (String) ZeroThreadLocalUtils.get("avatarUrl");
//        if (CommonUtils.isNull(avatarUrl)) {
//            return "https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLJYn0kl8JgNdrzWpzdTthBQMS6WqjRqISXWfnIsv5dQtnUlSiboHMQ6TJxDW8WyCyYj1G33NyZajg/132";
//        }
        return avatarUrl;
    }

    public static String getAccessToken() {

        return (String) ZeroThreadLocalUtils.get("Authorization");

    }

    public static String getWxNickName() {
//        return "jjjjo";
        String wxNickName = (String) ZeroThreadLocalUtils.get("wxNickName");
//        if (CommonUtils.isNull(wxNickName)) {
//            return "jjjj";
//        }
        return wxNickName;
    }

    public static String getUserName() {
        return (String) ZeroThreadLocalUtils.get("userName");
    }


    public static void makeSession(Map<String, Object> token, HttpServletRequest req) {

        final Map<String, String> header = CommonUtils.getHeadersInfo(req);
        final String sessionId = header.get("sessionid");
        String tranceId = header.get("tranceid");
        if (CommonUtils.isNull(tranceId)) {
            tranceId = CommonUtils.generateRandomString();
        }
        ZeroThreadLocalUtils.add("sessionId", CommonUtils.generateRandomString());
        ZeroThreadLocalUtils.add("parentSessionId", sessionId);
        ZeroThreadLocalUtils.add("tranceId", tranceId);
        String accessToken = (String) token.get(QxContant.getWebAccessTokenKey());
        String userName = (String) token.get("userName");
        String userId = (String) token.get("userId");
        String realName = (String) token.get("realName");
        String mobileNum = (String) token.get("mobileNum");
        String email = (String) token.get("email");
        String tenantId = (String) token.get("tenantId");
        String scope = (String) token.get("scope");
        List<String> roleIds = (List<String>) token.get("roleIds");
        ZeroThreadLocalUtils.add("Authorization", accessToken);
        ZeroThreadLocalUtils.add("userName", userName);
        ZeroThreadLocalUtils.add("userId", userId);
        ZeroThreadLocalUtils.add("realName", realName);
        ZeroThreadLocalUtils.add("mobileNum", mobileNum);
        ZeroThreadLocalUtils.add("email", email);
        ZeroThreadLocalUtils.add("tenantId", tenantId);
        ZeroThreadLocalUtils.add("scope", scope);
        ZeroThreadLocalUtils.add("roleIds", roleIds);
    }

    public static void makeWxSession(Map<String, Object> token, HttpServletRequest req) {

        final Map<String, String> header = CommonUtils.getHeadersInfo(req);
        final String sessionId = header.get("sessionid");
        String tranceId = header.get("tranceid");
        if (CommonUtils.isNull(tranceId)) {
            tranceId = CommonUtils.generateRandomString();
        }
        ZeroThreadLocalUtils.add("sessionId", CommonUtils.generateRandomString());
        ZeroThreadLocalUtils.add("parentSessionId", sessionId);
        ZeroThreadLocalUtils.add("tranceId", tranceId);

        String accessToken = (String) token.get("accessToken");
        String tenantId = (String) token.get("tenantId");
        String userId = (String) token.get("userId");
        String openId = (String) token.get("openId");
        String wxNickName = (String) token.get("wxNickName");
        String avatarUrl = (String) token.get("avatarUrl");
        ZeroThreadLocalUtils.add("Authorization", accessToken);
        ZeroThreadLocalUtils.add("userId", userId);
        ZeroThreadLocalUtils.add("tenantId", tenantId);
        ZeroThreadLocalUtils.add("openId", openId);
        ZeroThreadLocalUtils.add("wxNickName", wxNickName);
        ZeroThreadLocalUtils.add("avatarUrl", avatarUrl);
    }

    public static void makeWxSessionForPay(String tenantId, String userId, String wxNickName, HttpServletRequest req) {

        final Map<String, String> header = CommonUtils.getHeadersInfo(req);
        final String sessionId = header.get("sessionid");
        String tranceId = header.get("tranceid");
        if (CommonUtils.isNull(tranceId)) {
            tranceId = CommonUtils.generateRandomString();
        }
        ZeroThreadLocalUtils.add("sessionId", CommonUtils.generateRandomString());
        ZeroThreadLocalUtils.add("parentSessionId", sessionId);
        ZeroThreadLocalUtils.add("tranceId", tranceId);

        ZeroThreadLocalUtils.add("userId", userId);
        ZeroThreadLocalUtils.add("tenantId", tenantId);
        ZeroThreadLocalUtils.add("wxNickName", wxNickName);
        ZeroThreadLocalUtils.add("userName", wxNickName);
    }

    public static String getAopStackParentServiceName() throws Exception {
        final String sessionId = getSessionId();
        final LinkedList<String> serviceNameList = (LinkedList<String>) ZeroThreadLocalUtils.get(sessionId);
        String parentServiceName = "";
        if (CommonUtils.isNull(serviceNameList) || serviceNameList.size() < 2) {
            return parentServiceName;
        }
        serviceNameList.removeLast();
        parentServiceName = serviceNameList.get(serviceNameList.size() - 1);
        return parentServiceName;
    }

    public static String getSessionId() throws Exception {
        return (String) ZeroThreadLocalUtils.get("sessionId");
    }

    public static String getTranceId() throws Exception {
        return (String) ZeroThreadLocalUtils.get("tranceId");
    }

    public static String getParentSessionId() throws Exception {
        return (String) ZeroThreadLocalUtils.get("parentSessionId");
    }


    public static void setAopStackParentServiceName(String serviceName) throws Exception {
        final String sessionId = getSessionId();
        LinkedList<String> serviceNameList = (LinkedList<String>) ZeroThreadLocalUtils.get(sessionId);
        if (CommonUtils.isNull(serviceNameList)) {
            serviceNameList = new LinkedList<String>();
        }
        serviceNameList.add(serviceName);
        ZeroThreadLocalUtils.add(sessionId, serviceNameList);
    }
}
