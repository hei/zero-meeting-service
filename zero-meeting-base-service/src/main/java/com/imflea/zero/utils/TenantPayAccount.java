package com.imflea.zero.utils;

import lombok.Data;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.utils
 * @ClassName: TenantPayAccount
 * @Description
 * @date 2021-09-26  17:29:17
 */
@Data
public class TenantPayAccount {
    private String appId;
    private String mchId;

}
