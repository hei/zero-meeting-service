package com.imflea.zero.utils;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.utils
 * @filename 创建时间: 2021/12/21
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
public final class FarmPlusQrCodeUtil {

    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(FarmPlusQrCodeUtil.class);


    private static QrConfig initQrConfig() {
        QrConfig config = new QrConfig(200, 200);
        // 设置边距，既二维码和背景之间的边距
        config.setMargin(2);
        // 设置前景色，既二维码颜色（青色）
        config.setForeColor(Color.BLACK);
        // 设置背景色（灰色）
        config.setBackColor(Color.WHITE);
        return config;
    }


    public static String generateAsBase64(String data) {
        return QrCodeUtil.generateAsBase64(data, initQrConfig(), ImgUtil.IMAGE_TYPE_JPEG);
    }

    public static void main(String[] args) {
        System.out.println(generateAsBase64("http://www.iampeiyuan.com/login"));
    }
}
