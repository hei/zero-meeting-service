package com.imflea.zero.task;

import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.util.ZeroJsonUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;


@Component()
@Scope("prototype")
public class QxRedisTask extends Thread {


    /**
     * @description 字段功能描述
     * @value value:map
     */
    private Map<String, String> map;

    /**
     * 刷新的数据
     */
    private List<Map<String, Object>> dataList;

    /**
     * @param map 修改
     * @name 中文名称
     * @description 相关说明
     * @time 创建时间:2018年12月29日上午10:17:11
     * @author 史雪涛
     * @history 修订历史（历次修订内容、修订人、修订时间等）
     */
    public void setParam(Map<String, String> map) {
        this.map = map;
    }

    public void setDataList(List<Map<String, Object>> dataList) {
        this.dataList = dataList;
    }


    /**
     * @name 中文名称
     * @description 相关说明
     * @time 创建时间:2018年12月29日上午10:17:33
     * @author 史雪涛
     * @history 修订历史（历次修订内容、修订人、修订时间等）
     */
    @SuppressWarnings("unchecked")
    @Override
    public void run() {
        final String tableName = map.get("tableName");
        final String fl = "YWB";
        final String redisType = map.get("redisType");
        final String groupIndex = map.get("groupIndex");
        final String indexColumns = map.get("indexColumns");
        try {
            if (!CommonUtils.isNull(dataList)) {
                dataList =CommonUtils.getHumpListByUpUnderLineList(dataList); // 将Key转驼峰
                dataList = this.getToStringList(dataList); // value转String
                if (redisType.contains("LISTALL")) {
                    // 先组装ListRedis
                    final String redisListKey = "List:" + fl + ":" + tableName.toUpperCase() + ":all";
                   ZeroCacheUtils.setString(redisListKey,ZeroJsonUtils.listToJson(dataList));
                }
                if (redisType.toUpperCase().contains("MAP")) {
                    // 在组装MapRedis
                    // 在组装MapRedis
                    final String[] keyArray = indexColumns.split(",");
                    final Iterator<?> it1 = dataList.iterator();
                    while (it1.hasNext()) {
                        final Map<String, Object> dataMap = (Map<String, Object>) it1.next();
                        String key = "";
                        for (String column : keyArray) {
                            key += dataMap.get(column) + "&&";
                        }
                        key = key.substring(0, key.length() - 2);
                       ZeroCacheUtils.setMapToRedisByTableNameAndIndexColumnValue(tableName.toUpperCase(), key,
                               ZeroJsonUtils.mapToJson(dataMap));
                    }
                }
//                if (redisType.toUpperCase().contains("DM2MC")) {
//                    final String redisListKey = "Map:" + fl + ":" + tableName.toUpperCase() + ":all";
//                    final Map<String, Object> rMap = new HashMap<String, Object>();
//                    final String[] keyArray = indexColumns.split(",");
//                    for (Map<String, Object> dataMap : dataList) {
//                        String key = "";
//                        for (String column : keyArray) {
//                            key += dataMap.get(column) + "&&";
//                        }
//                        key = key.substring(0, key.length() - 2);
//                        final Object value = dataMap.get(indexCaption);
//                        rMap.put(key,CommonUtils.isNull(value) ? "" : value.toString());
//                    }
//                   ZeroCacheUtils.setString(redisListKey,ZeroJsonUtils.mapToJson(rMap));
//                }
                if (redisType.toUpperCase().contains("LISTGROUP") && !CommonUtils.isNull(groupIndex)) {
                    // 分组的
                    final String[] gIndex = groupIndex.split(",");
                    for (String key : gIndex) {
                        final Map<String, List<Map<String, Object>>> dataM = this.getMapByListAndMapKey(dataList,
                                key);
                        final Iterator<String> it = dataM.keySet().iterator();
                        while (it.hasNext()) {
                            final String value = it.next();
                            final String redisGroupListKey = "List:" + fl + ":" + tableName.toUpperCase() + ":" + key
                                    + ":" + value;
                           ZeroCacheUtils.setString(redisGroupListKey,ZeroJsonUtils.listToJson(dataM.get(value)));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static List<Map<String, Object>> getToStringList(List<Map<String, Object>> list) throws Exception {
        final List<Map<String, Object>> resList = new ArrayList<Map<String, Object>>();
        if (!CommonUtils.isNull(list)) {
            for (Map<String, Object> map : list) {
                final Map<String, Object> resMap = new HashMap<String, Object>();
                if (!CommonUtils.isNull(map)) {
                    final Iterator<String> iterator = map.keySet().iterator();
                    while (iterator.hasNext()) {
                        final String key = iterator.next();
                        final Object value = map.get(key);
                        if (!CommonUtils.isNull(value)) {
                            resMap.put(key, value.toString());
                        }
                    }
                }
                resList.add(resMap);
            }
        }
        return resList;
    }

    public static Map<String, List<Map<String, Object>>> getMapByListAndMapKey(List<Map<String, Object>> list,
                                                                               String key) throws Exception {
        final Map<String, List<Map<String, Object>>> resMap = new HashMap<String, List<Map<String, Object>>>();
        if (!CommonUtils.isNull(list)) {
            for (Map<String, Object> map : list) {
                final String value = (String) map.get(key);
                if (CommonUtils.isNull(resMap.get(value))) {
                    final List<Map<String, Object>> resList = new ArrayList<Map<String, Object>>();
                    resList.add(map);
                    resMap.put(value, resList);
                } else {
                    final List<Map<String, Object>> resList = resMap.get(value);
                    resList.add(map);
                    resMap.put(value, resList);
                }
            }
        }
        return resMap;
    }
}
