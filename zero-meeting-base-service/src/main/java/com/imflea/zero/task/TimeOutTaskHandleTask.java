package com.imflea.zero.task;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.utils
 * @filename 创建时间: 2021/12/31
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@FunctionalInterface
public interface TimeOutTaskHandleTask {

    void cancel(String rediskey);

}
