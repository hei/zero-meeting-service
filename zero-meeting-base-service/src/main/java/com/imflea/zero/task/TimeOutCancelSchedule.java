package com.imflea.zero.task;

import org.springframework.stereotype.Component;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.task
 * @filename 创建时间: 2021/10/20
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Deprecated
@Component
public class TimeOutCancelSchedule {

//    @Autowired
//    private IMallUnPayOrderTimeOutTask iMallUnPayOrderTimeOutTask;
//    @Autowired
//    private IMallTimeOutOrderService iMallTimeOutOrderService;
//    @Autowired
//    private AdoptUnPayOrderTimeOutTask adoptUnPayOrderTimeOutTask;
//    @Autowired
//    private ActivityUnPayTimeOutTask activityUnPayTimeOutTask;

//    @Scheduled(initialDelay = 10000, fixedDelay = 100000)
//    private void cancelTimeOutUnPayOrder() {
//        iMallUnPayOrderTimeOutTask.cancel(MallContant.UN_PAY_ORDER);
//    }

    //    @Scheduled(initialDelay = 10000, fixedDelay = 100000)
//    private void cancelAdoptTimeOutUnPayOrder() {
//        adoptUnPayOrderTimeOutTask.cancel(AdoptConstant.ADOPT_TIME_OUT_SQU.getCode());
//    }

    //    @Scheduled(initialDelay = 10000, fixedDelay = 100000)
//    private void cancelActiveTimeOutUnPayOrder() {
//        activityUnPayTimeOutTask.cancel(ActivityConstant.getActivitySquKey());
//    }


    // TODO 可以继续添加其他的
//    @Scheduled(initialDelay = 10000, fixedDelay = 300000)
//    private void timeOutConfirmStatus() {
//        iMallTimeOutOrderService.updateTimeOutConfirmStatus();
//    }

}
