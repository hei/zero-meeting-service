package com.imflea.zero.exception;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.exception
 * @filename 创建时间: 2022/1/13
 * @description
 * @copyright Copyright (c) 2022 中国软件与技术服务股份有限公司
 */
public class CustomIllegalArgumentException extends IllegalArgumentException {

    public CustomIllegalArgumentException() {
        super();
    }


    public CustomIllegalArgumentException(String s) {
        super(s);
    }
}
