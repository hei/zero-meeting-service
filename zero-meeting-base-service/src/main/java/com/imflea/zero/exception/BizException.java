package com.imflea.zero.exception;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.exception
 * @ClassName: BizException
 * @Description
 * @date 2021-09-10  14:27:52
 */
public class BizException extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(BizException.class);

    public BizException(Throwable cause) {
        super(cause);
    }

    public BizException(String message) {

        super(message);
        logger.error(message);
    }

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

}
