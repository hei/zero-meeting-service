package com.imflea.zero.exception;


/**
 * @author zhaoyang
 * @version 1.0
 * @project oa-parent
 * @package com.css.oa.component.exception
 * @filename BizException 创建时间: 2020-03-18 17:31
 * @description 描述（简要描述类的职责、实现方式、使用注意事项等）
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 */
public class MallServiceException extends BizException {

    public MallServiceException(Throwable cause) {
        super(cause);
    }

    public MallServiceException(String  message) {
        super(message);
    }

    public MallServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
