package com.imflea.zero.advice;

import com.imflea.zero.exception.BizException;
import com.imflea.zero.exception.CustomIllegalArgumentException;
import com.imflea.zero.exception.MallServiceException;
import com.github.jaemon.dinger.DingerSender;
import com.github.jaemon.dinger.core.entity.DingerRequest;
import com.github.jaemon.dinger.core.entity.enums.MessageSubType;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author zhaoyang
 * @version 1.0
 * @project oa-parent
 * @package com.css.oa.component.advice
 * @filename GlobalExceptionHandler 创建时间: 2020-03-18 17:23
 * @description 描述（全局业务异常处理类）
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 */
@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class BizExceptionHandler  {
    @Autowired
    private DingerSender dingerSender;


    @ExceptionHandler(value = {MallServiceException.class, BizException.class, CustomIllegalArgumentException.class})
    public JsonResult<String> customExceptionHandler(Exception e) {
        log.error(e.getMessage());
        dingerSender.send(MessageSubType.TEXT, DingerRequest.request("异常信息监控："+e.getMessage(), "监控"));
        return new JsonResult<>(ZeroContant.getFailCode(), e.getMessage(), null);
    }


}
