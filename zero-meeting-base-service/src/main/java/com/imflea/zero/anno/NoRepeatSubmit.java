package com.imflea.zero.anno;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.anno
 * @filename 创建时间: 2021/11/1
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */

import java.lang.annotation.*;

/**
 * 锁的注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface NoRepeatSubmit {

    //redis锁前缀
    String key() default "";

    //redis锁过期时间
    int expire() default 5;

}