//package com.imflea.zero.constant;
//
//import com.imflea.zero.util.base.CommonUtils;
//import com.imflea.zero.model.entity.mall.dto.DeliveryOrderDto;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author zhaoyang
// * @version 1.0
// * @project farmplus-service
// * @package com.imflea.zero.constant
// * @filename 创建时间: 2022/3/21
// * @description
// * @copyright Copyright (c) 2022 中国软件与技术服务股份有限公司
// */
//public enum KuaiDi100CompanyEnum {
//
//    SF("shunfeng", "顺丰"),
//    ZT("zhongtong", "中通"),
//    YT("yuantong", "园通"),
//    ST("shentong", "申通"),
//    YD("yunda", "韵达"),
//    ;
//
//    private String code;
//
//    private String label;
//
//
//    KuaiDi100CompanyEnum(String code, String label) {
//        this.code = code;
//        this.label = label;
//    }
//
//
//    public static List<Map<String, String>> list(DeliveryOrderDto dto) {
//
//        String company = dto.getCompany();
//
//        String cpCode = dto.getCpCode();
//
//        KuaiDi100CompanyEnum[] companyEnums = KuaiDi100CompanyEnum.values();
//
//        List<Map<String, String>> result = new ArrayList<>();
//
//        for (KuaiDi100CompanyEnum companyEnum : companyEnums) {
//
//            if (!CommonUtils.isNull(company) && !companyEnum.label.equals(company)) {
//                continue;
//            }
//
//            if (!CommonUtils.isNull(cpCode) && !companyEnum.code.equals(cpCode)) {
//                continue;
//            }
//
//            Map<String, String> map = new HashMap<>();
//
//            map.put("label", companyEnum.label);
//            map.put("value", companyEnum.code);
//
//            result.add(map);
//        }
//
//        return result;
//    }
//}
