package com.imflea.zero.constant;

public enum WxMessageConstant {
    WX_MESSAGE_ZERO_ORDER_NO_PRO("订单号", "character_string1"),
    WX_MESSAGE_ZERO_ORDER_TYPE_PRO("订单类型", "phrase8"),
    WX_MESSAGE_ZERO_ORDER_STATUS_PRO("订单状态", "phrase8"),
    WX_MESSAGE_ZERO_ORDER_TOTAL_PRO("订单金额", "amount24"),
    WX_MESSAGE_ZERO_ORDER_PRODUCT_NAME_PRO("产品名称", "thing16"),


    /**
     * 订单消息字段常量备注
     */

    WX_MESSAGE_ORDER_STATUS_TEST("订单状态", "phrase3"),
    WX_MESSAGE_ORDER_TYPE_TEST("类型", "phrase2"),
    WX_MESSAGE_ORDER_COMMENT_TEST("备注", "thing8"),

    WX_MESSAGE_ORDER_STATUS_PRO("订单状态", "phrase6"),
    WX_MESSAGE_ORDER_TYPE_PRO("类型", "thing11"),
    WX_MESSAGE_ORDER_COMMENT_PRO("备注", "thing3"),


    ;

    private String name;
    private String code;

    // 构造方法
    private WxMessageConstant(String name, String code) {
        this.name = name;
        this.code = code;
    }

    // 普通方法
    public static String getName(String code) {
        for (WxMessageConstant c : WxMessageConstant.values()) {
            if (c.getCode().equals(code)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
