package com.imflea.zero.constant;

public enum PayConstant {

    /**
     * 支付状态
     */

    PAY_STATUS_WAIT_PAY("待支付", "PAY_STATUS_WAIT_PAY"),
    PAY_STATUS_WAIT_UPLOAD_PAY_PROOF("待上传支付证明", "PAY_STATUS_WAIT_UPLOAD_PAY_PROOF"),
    PAY_STATUS_WAIT_UPLOAD_RECEIVE_PROOF("待上传收款证明", "PAY_STATUS_WAIT_UPLOAD_RECEIVE_PROOF"),
    PAY_STATUS_FINISH("支付完成", "PAY_STATUS_FINISH"),
    PAY_STATUS_WAIT_CONFIRM("待确认", "PAY_STATUS_WAIT_CONFIRM"),

    /**
     * 支付类型
     */


    PAY_TYPE_WX("微信支付", "PAY_TYPE_WX"),
    PAY_TYPE_TRANSFER("银行转账", "PAY_TYPE_TRANSFER"),
    PAY_TYPE_REN_CARD("认养券", "PAY_TYPE_REN_CARD"),
    /**
     * 订单类型
     */
    ORDER_TYPE_ADOPT("认养物", "ORDER_TYPE_ADOPT"),
    ORDER_TYPE_MALL("商城", "ORDER_TYPE_MALL"),
    ORDER_TYPE_ACTIVITY("活动", "ORDER_TYPE_ACTIVITY"),
    ORDER_TYPE_MEETING_CZ("会议室充值", "ORDER_TYPE_MEETING_CZ"),
    ORDER_TYPE_MEETING_ROOM("空间订单", "ORDER_TYPE_MEETING_ROOM"),
    ORDER_TYPE_OTHER("其他", "ORDER_TYPE_OTHER"),

    /**
     * 凭证类型
     */


    PAY_RECODER_PROOF_TYPE_PAY("支付凭证", "PAY_RECODER_PROOF_TYPE_PAY"),
    PAY_RECODER_PROOF_TYPE_RECEIVE("收款凭证", "PAY_RECODER_PROOF_TYPE_RECEIVE"),

    ;

    private String name;
    private String code;

    // 构造方法
    private PayConstant(String name, String code) {
        this.name = name;
        this.code = code;
    }

    // 普通方法
    public static String getName(String code) {
        for (PayConstant c : PayConstant.values()) {
            if (c.getCode().equals(code)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
