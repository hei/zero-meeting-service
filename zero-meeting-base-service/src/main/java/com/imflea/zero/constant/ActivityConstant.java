package com.imflea.zero.constant;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.constant
 * @ClassName: ActivityConstant
 * @Description
 * @date 2022-01-05  15:46:11
 */
public class ActivityConstant {
    private static final String ACTIVITY_SQU_KEY = "ACTIVITY_SQU_KEY";
    private static final Integer ACTIVITY_SQU_TIME_OUT = 300;

    public static String getActivitySquKey() {
        return ACTIVITY_SQU_KEY;
    }

    public static Integer getActivitySquTimeOut() {
        return ACTIVITY_SQU_TIME_OUT;
    }
}
