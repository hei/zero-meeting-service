package com.imflea.zero.constant;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.constant
 * @ClassName: SemConstant
 * @Description 促销相关的常量及枚举
 * @date 2021-10-20  11:19:34
 */
public enum SemConstant {

    //券基本配置状态

    SEM_COUPON_STATUS_OPEN("开启", "0"),
    SEM_COUPON_STATUS_CLOSE("关闭", "1"),

    //适用人群

    APPLY_TO_USER_SCOPE_NEW("新人", "APPLY_TO_USER_SCOPE_NEW"),
    APPLY_TO_USER_SCOPE_ALL("全员", "APPLY_TO_USER_SCOPE_ALL"),

    //所属业务模块

    BUSINESS_MODULE_ADOPT("认养", "BUSINESS_MODULE_ADOPT"),
    BUSINESS_MODULE_MALL("商城", "BUSINESS_MODULE_MALL"),
    //券分类

    SEM_COUPON_CATEGORY_COMMON("通用券", "SEM_COUPON_CATEGORY_COMMON"),
    SEM_COUPON_CATEGORY_KIND("品类券", "SEM_COUPON_CATEGORY_KIND"),
    SEM_COUPON_CATEGORY_INSTANCE("物品券", "SEM_COUPON_CATEGORY_INSTANCE"),
    SEM_COUPON_CATEGORY_PRODUCT("商品券", "SEM_COUPON_CATEGORY_PRODUCT"),
    //抵扣方式

    SEM_COUPON_SALE_TYPE_DISCOUNT("折扣", "SEM_COUPON_SALE_TYPE_DISCOUNT"),
    SEM_COUPON_SALE_TYPE_DEDUCT("抵扣", "SEM_COUPON_SALE_TYPE_DEDUCT"),
    SEM_COUPON_SALE_TYPE_EXCHANGE("换购", "SEM_COUPON_SALE_TYPE_EXCHANGE"),
    // status 0 待领取,1 待使用 ,2 已使用,3已过期，4已作废
    SEM_COUPON_ITEM_STATUS_WAIT_LQ("待领取", "0"),
    SEM_COUPON_ITEM_STATUS_WAIT_USE("待使用", "1"),
    SEM_COUPON_ITEM_STATUS_USED("已使用", "2"),
    SEM_COUPON_ITEM_STATUS_EXPIRE("已过期", "3"),
    SEM_COUPON_ITEM_STATUS_CANCEL("已作废", "4"),
    SEM_COUPON_ITEM_STATUS_LOCK("已占用", "5"),

    SEM_GIFT_CARD_STATUS_WAIT_BIND("待领取", "0"),
    SEM_GIFT_CARD_STATUS_WAIT_USE("待使用", "1"),
    SEM_GIFT_CARD_STATUS_USED("已使用", "2"),
    SEM_GIFT_CARD_STATUS_EXPIRE("已过期", "3"),
    SEM_GIFT_CARD_STATUS_CANCEL("已作废", "4"),

    //      `status` varchar(1) DEFAULT '0' COMMENT '状态：0锁定，1使用，2 取消',
    SEM_GIFT_CARD_USED_STATUS_LOCK("已锁定", "0"),
    SEM_GIFT_CARD_USED_STATUS_USE("已使用", "1"),
    SEM_GIFT_CARD_USED_STATUS_CANCEL("已取消", "2"),

    //券的使用方式，是否互斥

    SEM_COUPON_IS_MUTEX("互斥", "1"),
    SEM_COUPON_IS_NOT_MUTEX("不互斥", "0"),

    // 券的使用，订单金额是否有限制
    SEM_COUPON_RESTRICT("有限制", "1"),
    SEM_COUPON_NO_RESTRICT("无限制", "0"),
    // 券的领取数量限制
    SEM_COUPON_BIND_NUM_RESTRICT("有限制", "1"),
    SEM_COUPON_BIND_NUM_NO_RESTRICT("无限制", "0"),

    //券-规则引擎-规则-状态
    SEM_COUPON_RULE_STATUS_VALID("生效", "0"),
    SEM_COUPON_RULE_STATUS_INVALID("无效", "1"),


    //券-规则引擎-规则-类型
    SEM_COUPON_RULE_TYPE_INCLUDE("共融", "SEM_COUPON_RULE_TYPE_INCLUDE"),
    ;


    private String name;
    private String code;

    // 构造方法
    private SemConstant(String name, String code) {
        this.name = name;
        this.code = code;
    }

    // 普通方法
    public static String getName(String code) {
        for (SemConstant c : SemConstant.values()) {
            if (c.getCode().equals(code)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
