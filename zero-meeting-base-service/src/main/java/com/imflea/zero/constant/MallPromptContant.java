package com.imflea.zero.constant;

import com.imflea.zero.exception.MallServiceException;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.constant
 * @filename a 创建时间: 2021/9/10
 * @description
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
public final class MallPromptContant {

    public static final String SAVE_FAIL = "保存失败";
    public static final String UPDATE_FAIL = "更新失败";
    public static final String REMOVE_FAIL = "删除失败";

    //  商品分类信息
    private static final String MALL_PRODUCT_CATEGORY = "商品分类";
    public static final String MALL_PRODUCT_CATEGORY_SAVE_FAIL = MALL_PRODUCT_CATEGORY + SAVE_FAIL;
    public static final String MALL_PRODUCT_CATEGORY_UPDATE_FAIL = MALL_PRODUCT_CATEGORY + UPDATE_FAIL;
    public static final String MALL_PRODUCT_CATEGORY_REMOVE_FAIL = MALL_PRODUCT_CATEGORY + REMOVE_FAIL;

    // 商品信息
    private static final String MALL_PRODUCT = "商品信息";
    public static final String MALL_PRODUCT_SAVE_FAIL = MALL_PRODUCT + SAVE_FAIL;
    public static final String MALL_PRODUCT_UPDATE_FAIL = MALL_PRODUCT + UPDATE_FAIL;
    public static final String MALL_PRODUCT_REMOVE_FAIL = MALL_PRODUCT + REMOVE_FAIL;
    // 商品详情介绍
    private static final String MALL_PRODUCT_DETAIL = "商品详情简介";
    public static final String MALL_PRODUCT_DETAIL_SAVE_FAIL = MALL_PRODUCT_DETAIL + SAVE_FAIL;
    public static final String MALL_PRODUCT_DETAIL_UPDATE_FAIL = MALL_PRODUCT_DETAIL + UPDATE_FAIL;
    public static final String MALL_PRODUCT_DETAIL_REMOVE_FAIL = MALL_PRODUCT_DETAIL + REMOVE_FAIL;
    // 商品属性(规格/参数)
    private static final String MALL_PRODUCT_ATTR = "商品属性信息";
    public static final String MALL_PRODUCT_ATTR_SAVE_FAIL = MALL_PRODUCT_ATTR + SAVE_FAIL;
    public static final String MALL_PRODUCT_ATTR_REMOVE_FAIL = MALL_PRODUCT_ATTR + REMOVE_FAIL;
    // 商品相册
    private static final String MALL_PRODUCT_ALBUM = "商品相册信息";
    public static final String MALL_PRODUCT_ALBUM_SAVE_FAIL = MALL_PRODUCT_ALBUM + SAVE_FAIL;
    public static final String MALL_PRODUCT_ALBUM_UPDATE_FAIL = MALL_PRODUCT_ALBUM + UPDATE_FAIL;
    public static final String MALL_PRODUCT_ALBUM_REMOVE_FAIL = MALL_PRODUCT_ALBUM + REMOVE_FAIL;
    // 商品库存
    private static final String MALL_PRODUCT_SKU = "商品SKU";
    public static final String MALL_PRODUCT_SKU_SAVE_FAIL = MALL_PRODUCT_SKU + SAVE_FAIL;
    public static final String MALL_PRODUCT_SKU_UPDATE_FAIL = MALL_PRODUCT_SKU + UPDATE_FAIL;
    public static final String MALL_PRODUCT_SKU_REMOVE_FAIL = MALL_PRODUCT_SKU + REMOVE_FAIL;

    // 商户地址保存失败
    private static final String MALL_COMPANY_ADDRESS = "商户地址";
    public static final String MALL_COMPANY_ADDRESS_SAVE_FAIL = MALL_COMPANY_ADDRESS + SAVE_FAIL;
    public static final String MALL_COMPANY_ADDRESS_UPDATE_FAIL = MALL_COMPANY_ADDRESS + UPDATE_FAIL;
    public static final String MALL_COMPANY_ADDRESS_REMOVE_FAIL = MALL_COMPANY_ADDRESS + REMOVE_FAIL;

    // 商户地址保存失败
    private static final String MALL_MEMBER_ADDRESS = "会员地址";
    public static final String MALL_MEMBER_ADDRESS_SAVE_FAIL = MALL_MEMBER_ADDRESS + SAVE_FAIL;
    public static final String MALL_MEMBER_ADDRESS_UPDATE_FAIL = MALL_MEMBER_ADDRESS + UPDATE_FAIL;
    public static final String MALL_MEMBER_ADDRESS_REMOVE_FAIL = MALL_MEMBER_ADDRESS + REMOVE_FAIL;

    // 购物车业务提示
    private static final String MALL_ORDER_CART = "购物车商品";
    public static final String MALL_ORDER_CART_SAVE_FAIL = MALL_ORDER_CART + SAVE_FAIL;
    public static final String MALL_ORDER_CART_UPDATE_FAIL = MALL_ORDER_CART + UPDATE_FAIL;
    public static final String MALL_ORDER_CART_REMOVE_FAIL = MALL_ORDER_CART + REMOVE_FAIL;
    // 订单操作记录
    private static final String MALL_ORDER_HISTORY = "订单操作记录";
    public static final String MALL_ORDER_HISTORY_SAVE_FAIL = MALL_ORDER_HISTORY + SAVE_FAIL;

    // 发布模板
    private static final String CMS_TEMPLATE = "发布模板";
    public static final String CMS_TEMPLATE_SAVE_FAIL = CMS_TEMPLATE + SAVE_FAIL;
    public static final String CMS_TEMPLATE_UPDATE_FAIL = CMS_TEMPLATE + UPDATE_FAIL;
    public static final String CMS_TEMPLATE_REMOVE_FAIL = CMS_TEMPLATE + REMOVE_FAIL;
    // 发布内容
    private static final String CMS_CONTENT = "发布内容";
    public static final String CMS_CONTENT_SAVE_FAIL = CMS_CONTENT + SAVE_FAIL;
    public static final String CMS_CONTENT_UPDATE_FAIL = CMS_CONTENT + UPDATE_FAIL;
    public static final String CMS_CONTENT_REMOVE_FAIL = CMS_CONTENT + REMOVE_FAIL;

    // 物流编码
    private static final String BASE_LOGISTICS = "物流编码";
    public static final String BASE_LOGISTICS_SAVE_FAIL = BASE_LOGISTICS + SAVE_FAIL;
    public static final String BASE_LOGISTICS_UPDATE_FAIL = BASE_LOGISTICS + UPDATE_FAIL;
    public static final String BASE_LOGISTICS_REMOVE_FAIL = BASE_LOGISTICS + REMOVE_FAIL;

    public static final String ID_NOT_EMPTY = "ID不能为空";
    public static final String PRODUCT_ID_NOT_EMPTY = "商品ID不能为空";
    public static final String SKU_ID_NOT_EMPTY ="库存ID不能为空";
    public static final String ORDER_ID_NOT_EMPTY = "订单ID不能为空";
    public static final String BIZ_ID_NOT_EMPTY = "业务ID不能为空";



    public static MallServiceException error(String message) {
        return new MallServiceException(message);
    }


    private MallPromptContant() {
        throw new IllegalStateException("Contant class no instance ");
    }


}
