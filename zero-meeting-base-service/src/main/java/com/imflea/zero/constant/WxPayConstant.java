package com.imflea.zero.constant;

/**
 * 常量类
 *
 * @Date 2021/9/1 10:01
 */
public class WxPayConstant {


    /**
     * 查询状态信息展示
     **/
    public static final String PAY_STATUS_MSG_NOT_PAY = "交易创建";
    public static final String PAY_STATUS_MSG_SUCCESS = "支付成功";
    public static final String PAY_STATUS_MSG_FINISHED = "交易完成";
    public static final String PAY_STATUS_MSG_CLOSED = "交易关闭";
    public static final String PAY_STATUS_MSG_NOT_EXIST = "交易不存在";
    public static final String PAY_STATUS_MSG_FAIL = "请求失败，请刷新重试";


    /**
     * 成功
     */
    public static final String SUCCESS = "SUCCESS";

    /**
     * 失败
     */
    public static final String FAIL = "FAIL";


    /**
     * 业务正常状态
     */
    public static final String CHARACTER_ENCODING_UTF8 = "UTF-8";

    /**
     * 申请退款
     */
    public static final String WX_REFUND_URL = "";
    /**
     * 关闭订单
     */
    public static final String WX_CLOSE_ORDER_URL = "";
    /**
     * 统一下单
     */
    public static final String WX_UNIFIED_ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    /**
     * 微信异步回调
     */
//    public static final String WX_NOTIFY_URL = "https://www.iampeiyuan.com/farm-plus-app/api/wx/notify";
    /**
     * 查询订单状态
     */
    public static final String WX_ORDER_QUERY_URL = "";


    private static final String PAY_STATUS = "";

    private static final String PAY_STATUS_FINISH = "";

    private static final String PAY_STATUS_WAIT_UPLOAD_CERT = "";

    private static final String PAY_STATUS_WAIT_CONFIRM = "";


}