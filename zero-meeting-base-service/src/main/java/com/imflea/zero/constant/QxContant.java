package com.imflea.zero.constant;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.constant
 * @ClassName: QxContant
 * @Description
 * @date 2021-08-19  14:22:55
 */
public class QxContant {
    //用户锁定状态
    private static final String LOCK_STAUTS = "1";
    //用户正常状态
    private static final String UNLOCK_STATUS = "0";

    //菜单启用
    private static final String MENU_CLOSE_STATUS = "1";
    //菜单禁用
    private static final String MENU_OPEN_STATUS = "0";

    //租户启用
    private static final String TENANT_STATUS_OPEN = "0";
    //租户禁用
    private static final String TENANT_STATUS_CLOSE = "1";

    //通知未发布
    private static final String NOTICE_STATUS_UNPOST = "0";
    //通知发布
    private static final String NOTICE_STATUS_POST = "1";

    //单位秒   登录Token默认有效时间
    private static final Long SESSION_TIME = 1000 * 60 * 60L;

    //单位秒   微信端登录Token默认有效时间  3个月

    private static final Long WX_SESSION_TIME = 3 * 30 * 24 * 60 * 60L;

    private static final String LOGIN_TOKEN_PREFIX = "AccessToken:";
    //失败信息
    private static final String LOGIN_FAIL_POOL = "LoginFailPool:";

    //租户用户-商户业务员
    private static final String TENANT_BUINESS_ROLE_ID = "00000000000000000000000000000012";
    //租户用户-商户管理员
    private static final String TENANT_ADMIN_ROLE_ID = "00000000000000000000000000000011";
    //门店管理员
    private static final String MD_ADMIN_ROLE_ID = "oxmNzq0x";
    //总部管理员
    private static final String ZB_ADMIN_ROLE_ID = "HZLAflFX";


    private static final String WEB_ACCESS_TOKEN_KEY = "Authorization";

    private static final String WX_LOGIN_SESSION_KEY_PREFIX = "WX_LOGIN_SESSION_KEY:";

    private static final String WX_LOGIN_ACCESS_TOKEN_KEY_INDB = "WX_LOGINED_TOKEN_KEY";

    private static final String WX_LOGIN_ACCESS_TOKEN_KEY = "WxAccessToken:";

    private static final String WX_LOGIN_ACCESS_TOKEN_KEY_4FRONT = "accessToken";

    private static final String SINGLE_SIGN_ON_CAPTCHA = "SingleSignOn:Captcha:";

    private static final String WX_ACCESS_USER_NUM_PREFIX = "wxAccessUserNum:";


    private static final String DEF_PASSWORD = "Css@111111";

    public static String getLockStauts() {
        return LOCK_STAUTS;
    }

    public static String getUnlockStatus() {
        return UNLOCK_STATUS;
    }

    public static String getDefPassword() {
        return DEF_PASSWORD;
    }

    public static String getMenuCloseStatus() {
        return MENU_CLOSE_STATUS;
    }

    public static String getMenuOpenStatus() {
        return MENU_OPEN_STATUS;
    }

    public static Long getSessionTime() {
        return SESSION_TIME;
    }

    public static String getWebAccessTokenKey() {
        return WEB_ACCESS_TOKEN_KEY;
    }

    public static String getLoginTokenPrefix() {
        return LOGIN_TOKEN_PREFIX;
    }

    public static String getTenantStatusOpen() {
        return TENANT_STATUS_OPEN;
    }

    public static String getTenantStatusClose() {
        return TENANT_STATUS_CLOSE;
    }

    public static String getWxLoginSessionKeyPrefix() {
        return WX_LOGIN_SESSION_KEY_PREFIX;
    }

    public static String getWxLoginAccessTokenKeyIndb() {
        return WX_LOGIN_ACCESS_TOKEN_KEY_INDB;
    }

    public static String getWxLoginAccessTokenKey4front() {
        return WX_LOGIN_ACCESS_TOKEN_KEY_4FRONT;
    }

    public static String getTenantBuinessRoleId() {
        return TENANT_BUINESS_ROLE_ID;
    }

    public static String getTenantAdminRoleId() {
        return TENANT_ADMIN_ROLE_ID;
    }

    public static String getWxLoginAccessTokenKey() {
        return WX_LOGIN_ACCESS_TOKEN_KEY;
    }


    public static Long getWxSessionTime() {
        return WX_SESSION_TIME;
    }

    public static String getNoticeStatusUnpost() {
        return NOTICE_STATUS_UNPOST;
    }

    public static String getNoticeStatusPost() {
        return NOTICE_STATUS_POST;
    }

    public static String getWxAccessUserNumPrefix() {
        return WX_ACCESS_USER_NUM_PREFIX;
    }

    public static String getSingleSignOnCaptcha() {
        return SINGLE_SIGN_ON_CAPTCHA;
    }

    public static String getLoginFailPool() {
        return LOGIN_FAIL_POOL;
    }


    public static String getMdAdminRoleId() {
        return MD_ADMIN_ROLE_ID;
    }

    public static String getZbAdminRoleId() {
        return ZB_ADMIN_ROLE_ID;
    }
}
