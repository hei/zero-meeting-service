package com.imflea.zero.constant;

public final class MallContant {

    private MallContant() {
        throw new IllegalStateException("Contant class no instance ");
    }

    public static final int PRODUCT_SPEC = 1;
    public static final int PRODUCT_ATTR = 2;

    public static final int ONE = 1;
    public static final int ZERO = 0;


    /*===================================下单常量=====================================*/
    // 新建状态
    public static final int CREATE_ORDER_STATUS = 0;
    // 已支付状态
    public static final int ALREADY_PAY_ORDER_STATUS = 5;
    // 已发货状态
    public static final int ALREADY_SEND_ORDER_STATUS = 10;
    // 已完成状态
    public static final int ALREADY_FINISH_ORDER_STATUS = 15;
    // 已关闭状态
    public static final int ALREADY_CLOSE_ORDER_STATUS = 20;
    // 已支付订单,申请退单时审核状态
    public static final int REVIEW_ORDER_STATUS = 25;
    // 已支付订单申请退单时已退款状态
    public static final int ALREADY_REFUND_ORDER_STATUS = 30;
    // 失效订单
    public static final int ALREADY_INVALID_ORDER_STATUS = 35;
    /*===================================下单常量=====================================*/


    /*===================================退单常量=====================================*/

    public static final int CREATE_RETURN_ORDER_STATUS = 0;

    public static final int AGREE_RETURN_ORDER_STATUS = 5;

    public static final int PROCESSING_RETURN_ORDER_STATUS = 10;

    public static final int RECEIVE_RETURN_ORDER_STATUS = 15;

    public static final int HANDLE_RETURN_ORDER_STATUS = 20;

    public static final int END_RETURN_ORDER_STATUS = 25;

    public static final int REFUSE_RETURN_ORDER_STATUS = 30;

    public static final int CANCEL_RETURN_ORDER_STATUS = 35;

    /*===================================退单常量=====================================*/
    // 空白字符
    public static final String BLANK = " ";

    public static final String UNDELETE = "0";

    public static final String TENANT_SKU_STOCK_PREFIX = "MALL_TENANT_SKU_STOCK:";
    public static final String TENANT_SKU_STOCK_LOCK_PREFIX = "MALL_TENANT_SKU_LOCK:";

    // 物流追踪缓存前缀
    public static final String DELIVERY_TRACING_REDIS_KEY = "DELIVERYORDER:ORDERID:";

    public static final String UN_PAY_ORDER = "UNPAY_TIMEOUT_ORDER";
    public static final String SEND_ORDER = "SEND_ORDER_TIMEOUT_ORDER";


    // 商品分组信息
    public static final String PRODUCT_GROUP_KEY_PREFIX = "List:YWB:PRODUCT_GROUP:TENANTID:";

    public static final String PRODUCT_INFO_INDEX = "product_info_new";
    public static final String PRODUCT_SKU_INDEX = "product_sku";


    public static final int UNINITIALIZED_STOCK = -3;

    public static final int  LACK_OF_STOCK = -2;

    public static final int  ENOUGH_STOCK = 1;
}
