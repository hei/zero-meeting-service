package com.imflea.zero.constant;

public enum AdoptConstant {

    /**
     * 认养物状态：待确认 已确认
     */

    ADOPTED_STATUS_WAIT_CONFIRM("待确认", "0"),
    ADOPTED_STATUS_CONFIRMED("已确认", "1"),
    ADOPTED_STATUS_OPEN_REFUND("打开退款", "2"),

    ADOPTED_BASE_STATUS_GROWING("成长中", "0"),
    ADOPTED_BASE_STATUS_WAIT_HARVEST("待收获", "1"),
    ADOPTED_BASE_STATUS_HAVE_HARVEST("已收获", "2"),
    ADOPTED_BASE_STATUS_HAVING_REFUND("退养中", "3"),
    ADOPTED_BASE_STATUS_HAVE_REFUND("已退养", "4"),


    ADOPTED_RETURN_STATUS_WAIT_HANDLE("待处理", "0"),
    ADOPTED_RETURN_STATUS_RETURN_PAY("同意退款", "1"),
    ADOPTED_RETURN_STATUS_FINISH("已完成", "2"),
    ADOPTED_RETURN_STATUS_REFUSE("已拒绝", "3"),
    ADOPTED_RETURN_STATUS_WAIT_UPLOAD_PROOF("待上传退款证明", "4"),


    ADOPT_TIME_OUT_SQU("认养订单超时未支付", "ADOPT_UNPAY_TIMEOUT_ORDER"),
    MEETING_ORDER_TIME_OUT_SQU("订单超时未支付", "MEETING_ROOM_UNPAY_TIMEOUT_ORDER"),
    MEETING_ORDER_TIME_OUT_TIME_SECONDS("订单超时时间-微信", "1800"),

    //半小时
    ADOPT_TIME_OUT_TIME_SECONDS("认养订单超时时间-微信", "1800"),
    ADOPT_TIME_OUT_TIME_SECONDS_TRANSFER("认养订单超时时间-转账", "86400"),
    ;

    private String name;
    private String code;

    // 构造方法
    private AdoptConstant(String name, String code) {
        this.name = name;
        this.code = code;
    }

    // 普通方法
    public static String getName(String code) {
        for (AdoptConstant c : AdoptConstant.values()) {
            if (c.getCode().equals(code)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
