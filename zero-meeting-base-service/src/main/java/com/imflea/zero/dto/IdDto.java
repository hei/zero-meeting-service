package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req
 * @ClassName: IdReq
 * @Description
 * @date 2021-08-19  14:19:51
 */
@Data
@ApiModel("逻辑主键ID数据请求模型")
@EqualsAndHashCode(callSuper=false)
public class IdDto implements Serializable {
    private static final long serialVersionUID = -40613732207236907L;
    @ApiModelProperty(name = "请求id", required = true)
    private String id;
}
