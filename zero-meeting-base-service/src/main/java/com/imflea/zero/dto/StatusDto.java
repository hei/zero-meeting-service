package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel("状态字段通用模型")
public class StatusDto implements Serializable {
    private static final long serialVersionUID = -34817541143594906L;
    @ApiModelProperty(value = "状态")
    private String status ;
    @ApiModelProperty(value = "业务主键")
    private String buinessId;

}
