package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req
 * @ClassName: IdsReq
 * @Description
 * @date 2021-08-23  10:25:02
 */
@Data
@ApiModel("逻辑主键ID集合数据请求模型")
@EqualsAndHashCode(callSuper=false)
public class IdsDto implements Serializable {
    private static final long serialVersionUID = -3319713646097907130L;
    @ApiModelProperty(value = "逻辑主键ids",required = true)
    private List<String> ids ;
}
