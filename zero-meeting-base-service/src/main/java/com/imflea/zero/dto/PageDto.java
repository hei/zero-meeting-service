package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 
 */
@Data
@ApiModel("通用分页请求数据模型")
public class PageDto implements Serializable {
    private static final long serialVersionUID = -1485967233713121487L;
    @ApiModelProperty("当前页，列表用")
    private Integer pageNum = 1;// 当前页
    @ApiModelProperty("每页显示的记录数，列表用")
	private Integer pageSize = 10;// 每页显示的记录数
}