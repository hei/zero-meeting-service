package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req
 * @ClassName: PidReq
 * @Description
 * @date 2021-08-23  11:20:21
 */
@Data
@ApiModel("逻辑主键父级ID集合数据请求模型")
@EqualsAndHashCode(callSuper=false)
public class PidDto implements Serializable {

    @ApiModelProperty(name = "请求父级pid", required = true)
    private String pid;
}
