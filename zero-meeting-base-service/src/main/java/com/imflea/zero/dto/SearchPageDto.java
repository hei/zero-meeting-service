package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@ApiModel("搜索带分页数据请求模型")
@EqualsAndHashCode(callSuper=false)
public class SearchPageDto extends PageDto {
    /**
     * @description 字段功能描述
     * @value value:serialVersionUID
     */
    private static final long serialVersionUID = -5516664700936060306L;
    @ApiModelProperty("搜索关键字")
    private String searchContent;


}
