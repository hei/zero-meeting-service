package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req
 * @ClassName: IdsReq
 * @Description
 * @date 2021-08-23  10:25:02
 */
@Data
@ApiModel("业务相关ids集合")
@EqualsAndHashCode(callSuper=false)
public class BusinessIdsDto implements Serializable {

    private static final long serialVersionUID = -842512582843918100L;
    @ApiModelProperty(value = "业务ids",required = true)
    private List<String> businessIds ;
}
