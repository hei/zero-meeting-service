package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req
 * @ClassName: PCodeReq
 * @Description
 * @date 2021-08-25  14:12:57
 */
@Data
@ApiModel("统一pcode参数请求模型")
@EqualsAndHashCode(callSuper=false)
public class PCodeDto implements Serializable {
    private static final long serialVersionUID = 4563355901639842168L;
    @ApiModelProperty(value = "pcode参数接受字段",required = true)
    private String pcode;
}
