package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.req
 * @ClassName: CodeReq
 * @Description
 * @date 2021-08-25  14:12:57
 */
@Data
@ApiModel("统一code参数请求模型")
@EqualsAndHashCode(callSuper=false)
public class CodeDto implements Serializable {
    private static final long serialVersionUID = -3632494830636944230L;
    @ApiModelProperty(value = "code参数接受字段",required = true)
    private String code;
}
