package com.imflea.zero.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@ApiModel("编号带分页请求数据模型")
@EqualsAndHashCode(callSuper=false)
public class IdPageDto extends PageDto {

    /**
     * @description 字段功能描述
     * @value value:serialVersionUID
     */
    private static final long serialVersionUID = -406137330007236907L;
    @ApiModelProperty("请求数据编号")
    private String id;
}
