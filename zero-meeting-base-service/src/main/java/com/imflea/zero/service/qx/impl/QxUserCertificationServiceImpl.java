package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.dao.qx.QxUserCertificationMapper;
import com.imflea.zero.model.entity.qx.QxUserCertification;
import com.imflea.zero.model.entity.qx.dto.QxUserCertificationDto;
import com.imflea.zero.service.qx.IQxUserCertificationService;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lambdaworks.crypto.SCryptUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户认证密码表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
@Service
public class QxUserCertificationServiceImpl extends ServiceImpl<QxUserCertificationMapper, QxUserCertification> implements IQxUserCertificationService {

    @Override
    public boolean saveQxUserCertification(String salt, String defPassword, String userId, String tenantId) {

        String password = SCryptUtil.scrypt(salt + defPassword + salt, 2048, 1, 1);
        QxUserCertification certification = new QxUserCertification();
        certification.setId(CommonUtils.generateRandomString());
        certification.setUserId(userId);
        certification.setTenantId(tenantId);
        certification.setPassword(password);
        certification.setSalt(salt);
        boolean result = this.saveOrUpdate(certification);
        return result;
    }

    @Override
    public boolean updateQxUserCertification(String defPassword, String userId) {
        QueryWrapper<QxUserCertification> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        QxUserCertification certification = this.getOne(queryWrapper);
        if (CommonUtils.isNull(certification)) {
            return false;
        }
        String salt = certification.getSalt();
        String password = SCryptUtil.scrypt(salt + defPassword + salt, 2048, 1, 1);
        certification.setPassword(password);
        return this.updateById(certification);
    }

    @Override
    public boolean query4CheckPassword(String userId, String password) {
        QueryWrapper<QxUserCertification> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        QxUserCertification certification = this.getOne(queryWrapper);
        if (CommonUtils.isNull(certification)) {
            return false;
        }
        String salt = certification.getSalt();
        String dbPassword = certification.getPassword();
        boolean result = SCryptUtil.check(salt + password + salt, dbPassword);
        return result;
    }


    @Override
    public PageInfo<QxUserCertification> queryPage(QxUserCertificationDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<QxUserCertification> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<QxUserCertification> adoptInfos = this.list(queryWrapper);
        PageInfo<QxUserCertification> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

}
