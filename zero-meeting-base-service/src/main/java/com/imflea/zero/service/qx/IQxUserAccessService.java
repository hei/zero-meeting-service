package com.imflea.zero.service.qx;

import com.imflea.zero.model.entity.qx.QxUserAccess;
import com.imflea.zero.model.entity.qx.dto.QxUserAccessDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 用户每天访问数量表 服务类
 * </p>
 *
 * @author zhaoyang
 * @since 2022-01-13
 */
public interface IQxUserAccessService extends IService<QxUserAccess> {

    QxUserAccess saveOrUpdateQxUserAccess(QxUserAccess entity);

    PageInfo<QxUserAccess> queryPage(QxUserAccessDto dto);

    List<QxUserAccess> listAllAccessByTenantId(String tenantId);

    Boolean saveOrUpdateByTenantAndDate(QxUserAccess access);
}
