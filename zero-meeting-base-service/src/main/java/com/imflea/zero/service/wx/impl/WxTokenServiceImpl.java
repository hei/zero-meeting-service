package com.imflea.zero.service.wx.impl;


import com.imflea.zero.model.entity.qx.QxTenantConfig;
import com.imflea.zero.service.qx.IQxTenantConfigService;
import com.imflea.zero.service.wx.IWxTokenService;
import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.utils.SessionUtils;
import com.imflea.zero.utils.WxAuthUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.service.wx.impl
 * @ClassName: WxTokenServiceImpl
 * @Description
 * @date 2022-03-08  16:57:21
 */
@Service("wxTokenService")
public class WxTokenServiceImpl implements IWxTokenService {
    @Autowired
    private IQxTenantConfigService configService;

    public String getAccessToken() throws Exception {
        String tenantId = SessionUtils.getTenantId();
        QxTenantConfig config = configService.queryByTenantId(tenantId);
        String apiTokenCacheKey = tenantId + "API_TOKEN";
        String accessToken = ZeroCacheUtils.getString(apiTokenCacheKey);
        if (CommonUtils.isNull(accessToken)) {
            accessToken = WxAuthUtils.getAccessToken(config.getWxAppId(), config.getWxSecret());
           ZeroCacheUtils.setString(apiTokenCacheKey, accessToken);
           ZeroCacheUtils.expire(apiTokenCacheKey, 7100);
        }
        return accessToken;


    }


}
