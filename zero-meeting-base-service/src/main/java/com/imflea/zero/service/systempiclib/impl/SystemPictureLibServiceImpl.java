package com.imflea.zero.service.systempiclib.impl;

import com.imflea.zero.model.entity.systempiclib.SystemPictureLib;
import com.imflea.zero.model.dao.systempiclib.SystemPictureLibMapper;
import com.imflea.zero.service.systempiclib.ISystemPictureLibService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.model.entity.systempiclib.dto.SystemPictureLibDto;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.util.base.CommonUtils;
import java.util.List;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
* <p>
    * 系统图库 服务实现类
    * </p>
*
* @author guohui
* @since 2021-10-28
*/
@Service
public class SystemPictureLibServiceImpl extends ServiceImpl<SystemPictureLibMapper, SystemPictureLib> implements ISystemPictureLibService {

    @Override
    public SystemPictureLib saveOrUpdateSystemPictureLib(SystemPictureLib entity){
        if(CommonUtils.isNull(entity.getId())){
            entity.setId(CommonUtils.generateRandomString());
        }
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<SystemPictureLib> queryPage(SystemPictureLibDto dto){
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        QueryWrapper<SystemPictureLib> queryWrapper = new QueryWrapper<>();
        if(!CommonUtils.isNull(dto.getBusinessType())){
            queryWrapper.eq("business_type", dto.getBusinessType());
        }
        queryWrapper.orderByAsc("sort");
        PageHelper.startPage(pageIndex, pageSize);
        List<SystemPictureLib> adoptInfos = this.list(queryWrapper);
        PageInfo<SystemPictureLib> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

}
