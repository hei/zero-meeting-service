package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.qx.QxRoleMapper;
import com.imflea.zero.model.entity.qx.QxRole;
import com.imflea.zero.service.qx.IQxRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
@Service
public class QxRoleServiceImpl extends ServiceImpl<QxRoleMapper, QxRole> implements IQxRoleService {

    @Override
    public QxRole saveOrUpdateRole(QxRole role) {
        boolean result = false;
        String id = role.getId();
        if (CommonUtils.isNull(id)) {
            String tenantId = role.getTenantId();
            id =CommonUtils.generateRandomString();
            role.setTenantId(tenantId);
            role.setId(id);
            result = this.saveOrUpdate(role);
        } else {
            result = this.updateById(role);
        }
        if (result) {
            return role;
        } else {
            throw new BizException("角色信息更新失败");
        }

    }

    @Override
    public List<QxRole> queryRoles() {
        List<QxRole> roles = this.list();
        return roles;
    }

    @Override
    public Boolean removeRoleById(String id) {
        boolean result = this.removeById(id);
        if (result) {
            return result;

        } else {
            throw new BizException("用户状态更新失败");
        }
    }

    @Override
    public List<QxRole> queryTenantRoles() {
        QueryWrapper<QxRole> qxRoleQueryWrapper = new QueryWrapper<>();
        qxRoleQueryWrapper.eq("tenant_can_use", "1");
        List<QxRole> roles = this.list(qxRoleQueryWrapper);
        return roles;
    }
}
