package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.qx.BaseDictMapper;
import com.imflea.zero.model.entity.qx.BaseDict;
import com.imflea.zero.service.qx.IBaseDictService;
import com.imflea.zero.service.qx.IQxCacheInfoService;
import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 字典表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-25
 */
@Service
public class BaseDictServiceImpl extends ServiceImpl<BaseDictMapper, BaseDict> implements IBaseDictService {
    @Autowired
    private IQxCacheInfoService cacheInfoService;


    @Override
    public List<BaseDict> queryByPcode(String pcode) {


        List<BaseDict> dicts = null;
        try {
            dicts = ZeroCacheUtils.getEntityListCacheValueByTableNameAndGroupIndexAndIndexValue("base_dict", "pcode", pcode, BaseDict.class);
            if (CommonUtils.isNull(dicts)) {
                QueryWrapper<BaseDict> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("pcode", pcode);
                queryWrapper.orderByAsc("order_num");
                dicts = this.list(queryWrapper);
            }
        } catch (Exception e) {
            e.printStackTrace();
            QueryWrapper<BaseDict> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("pcode", pcode);
            queryWrapper.orderByAsc("order_num");
            dicts = this.list(queryWrapper);
        }
        return dicts;
    }

    @Override
    public BaseDict saveOrUpdateBaseDict(BaseDict baseDict) {
        String id = baseDict.getId();
        String dictPid = baseDict.getDictPid();
        boolean result = false;
        if (CommonUtils.isNull(dictPid)) {
            baseDict.setDictPid(null);
        }

        if (CommonUtils.isNull(id)) {
            id = CommonUtils.generateRandomString();
            baseDict.setId(id);
            result = this.save(baseDict);
        } else {
            result = this.updateById(baseDict);
        }
        if (result) {
            cacheInfoService.refreshCacheByTableName("base_dict");
            return baseDict;
        } else {
            throw new BizException("操作失败");
        }
    }

    @Override
    public Boolean queryDictByCode4Validate(String code) {
        QueryWrapper<BaseDict> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("code", code);
        BaseDict baseDict = this.getOne(queryWrapper);
        if (CommonUtils.isNull(baseDict)) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public Boolean removeBaseDictById(String id) {
        boolean result = this.removeById(id);
        if (result) {
            cacheInfoService.refreshCacheByTableName("base_dict");
            return result;
        } else {
            throw new BizException("字典项删除失败");
        }
    }

    @Override
    public List<BaseDict> queryRootDict(String name) {
        QueryWrapper<BaseDict> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNull("dict_pid");
        if (!CommonUtils.isNull(name)) {
            queryWrapper.like("dict_name", name);
        }
        queryWrapper.orderByAsc("order_num");
        List<BaseDict> baseDicts = this.list(queryWrapper);
        return baseDicts;
    }
}
