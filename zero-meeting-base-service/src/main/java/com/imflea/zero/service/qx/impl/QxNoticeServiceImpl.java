package com.imflea.zero.service.qx.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.qx.QxNoticeMapper;
import com.imflea.zero.model.entity.cms.CmsCommonContent;
import com.imflea.zero.model.entity.qx.QxNotice;
import com.imflea.zero.model.entity.qx.dto.QxNoticeDto;
import com.imflea.zero.model.entity.qx.dto.QxNoticeSaveDto;
import com.imflea.zero.model.entity.qx.vo.QxNoticeDetailVo;
import com.imflea.zero.service.cms.ICmsCommonContentService;
import com.imflea.zero.service.qx.IQxNoticeService;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 系统通知 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-22
 */
@Service
public class QxNoticeServiceImpl extends ServiceImpl<QxNoticeMapper, QxNotice> implements IQxNoticeService {
    @Autowired
    private ICmsCommonContentService contentService;

    @Override
    public QxNotice saveOrUpdateQxNotice(QxNotice entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
        }
        entity.setTenantId(SessionUtils.getTenantId());
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<QxNotice> queryPage(QxNoticeDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String title = dto.getTitle();
        String noticeType = dto.getNoticeType();
        String startTime = dto.getStartTime();
        String endTime = dto.getEndTime();
        String postTime = dto.getPostTime();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<QxNotice> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        if (!CommonUtils.isNull(title)) {
            queryWrapper.like("title", title);
        }
        if (!CommonUtils.isNull(noticeType)) {
            queryWrapper.eq("notice_type", noticeType);
        }
        if (!CommonUtils.isNull(postTime)) {
            queryWrapper.like("post_time", postTime);
        }
        if (!CommonUtils.isNull(startTime) && !CommonUtils.isNull(endTime)) {
            queryWrapper.ge("start_time", startTime);
            queryWrapper.le("end_time", endTime);
        }

        queryWrapper.orderByDesc("post_time");
        PageHelper.startPage(pageIndex, pageSize);
        List<QxNotice> adoptInfos = this.list(queryWrapper);
        PageInfo<QxNotice> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

    @Override
    public QxNotice queryByNoticeType(String noticeType) {
        return this.queryByTypeAndOrderNumBiggest(noticeType);
    }

    @Override
    public QxNoticeDetailVo saveOrUpdateQxNotice(QxNoticeSaveDto qxNoticeSaveDto) {
        QxNotice notice = new QxNotice();
        String noticeId = qxNoticeSaveDto.getId();
        if (CommonUtils.isNull(noticeId)) {
            noticeId =CommonUtils.generateRandomString();
            notice.setId(noticeId);
            notice.setStatus(QxContant.getNoticeStatusUnpost());
        }
        BeanUtil.copyProperties(qxNoticeSaveDto, notice);
        notice = this.saveOrUpdateQxNotice(notice);
        String bizId = notice.getId();
        CmsCommonContent content = contentService.queryCmsCommonContentByBizId(bizId);
        if (CommonUtils.isNull(content)) {
            content = new CmsCommonContent();
        }
        content.setBizId(bizId);
        content.setContent(qxNoticeSaveDto.getContent());
        content = contentService.saveOrUpdateCmsCommonContent(content);
        QxNoticeDetailVo vo = new QxNoticeDetailVo();
        vo.setContent(content);
        vo.setNotice(notice);
        return vo;
    }

    @Override
    public QxNoticeDetailVo queryDetailById(String id) {
        if (CommonUtils.isNull(id)) {
            throw new BizException("未获取到通知主键信息");
        }
        QxNotice notice = this.getById(id);
        CmsCommonContent content = contentService.queryCmsCommonContentByBizId(id);
        QxNoticeDetailVo vo = new QxNoticeDetailVo();
        vo.setContent(content);
        vo.setNotice(notice);
        return vo;
    }

    @Override
    public boolean removeBatchByIds(List<String> ids) {
        boolean result = this.removeByIds(ids);
        contentService.removeCmsCommonContentsByBizIds(ids);
        if (!result) {
            throw new BizException("删除失败");
        }
        return result;
    }

    @Override
    public QxNotice queryByTypeAndOrderNumBiggest(String type) {
        QueryWrapper<QxNotice> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(type)) {
            queryWrapper.eq("notice_type", type);
        }
        queryWrapper.eq("status", QxContant.getNoticeStatusPost());
        queryWrapper.orderByDesc("order_num").last("limit 1");
        QxNotice notice = this.getOne(queryWrapper);
        return notice;
    }

    @Override
    public List<QxNotice> queryListByNoticeType(String noticeType) {
        QueryWrapper<QxNotice> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(noticeType)) {
            queryWrapper.eq("notice_type", noticeType);
        }
        queryWrapper.eq("status", QxContant.getNoticeStatusPost());
        List<QxNotice> notice = this.list(queryWrapper);
        return notice;
    }

    @Override
    public List<QxNotice> queryListByNoticeTypeValid(String noticeType) {
        QueryWrapper<QxNotice> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(noticeType)) {
            queryWrapper.eq("notice_type", noticeType);
        }
        queryWrapper.ge("end_time", LocalDateTime.now());
        queryWrapper.le("start_time", LocalDateTime.now());
        queryWrapper.eq("status", QxContant.getNoticeStatusPost());
        List<QxNotice> notice = this.list(queryWrapper);
        return notice;
    }


    @Override
    public boolean updateStatus(String id, String status, Boolean updateOrderNum) {
        UpdateWrapper<QxNotice> updateWrapper = new UpdateWrapper<>();
        if (updateOrderNum) {
            QxNotice notice = this.queryByTypeAndOrderNumBiggest(null);
            Integer orderNum = 1;
            if (!CommonUtils.isNull(notice)) {
                orderNum = notice.getOrderNum();
            }
            orderNum++;
            updateWrapper.set("order_num", orderNum);
        }
        updateWrapper.set("post_time", LocalDateTime.now());
        updateWrapper.set("status", status);
        updateWrapper.eq("id", id);

        boolean result = this.update(updateWrapper);
        return result;
    }

}
