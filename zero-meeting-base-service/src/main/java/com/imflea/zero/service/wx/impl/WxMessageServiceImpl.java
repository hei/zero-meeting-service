package com.imflea.zero.service.wx.impl;

import com.imflea.zero.constant.WxMessageConstant;
import com.imflea.zero.service.qx.IQxTenantConfigService;
import com.imflea.zero.service.wx.IWxMessageService;
import com.imflea.zero.service.wx.IWxTokenService;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.utils.SessionUtils;
import com.imflea.zero.utils.WxAuthUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.service.wx.impl
 * @ClassName: WxMessageServiceImpl
 * @Description
 * @date 2022-03-08  16:32:29
 */
@Service("wxMessageService")
@Slf4j
public class WxMessageServiceImpl implements IWxMessageService {
    @Autowired
    private IQxTenantConfigService configService;
    @Autowired
    private IWxTokenService wxTokenService;

    @Override
    public String sendMessage(Map<String, Object> data, String accessToken, String touser, String templateId) {


        return WxAuthUtils.sendMessage(touser, templateId, data, "trial", accessToken);
    }

    @Override
    public String sendOrderMessage(String statusName, String typeName, String message, String touser) throws Exception {
        Map<String, Object> mapData2 = new HashMap<>();
        Map<String, Object> data = new HashMap<>();
        Map<String, Object> p3 = new HashMap<>();
        Map<String, Object> p4 = new HashMap<>();
        Map<String, Object> p5 = new HashMap<>();

        log.error("this is send message in this step----");
        p3.put("value", statusName);
        p4.put("value", typeName);
        p5.put("value", message);
        String orderTemplateId = configService.queryOrderTemplateId(SessionUtils.getTenantId());
        //原-生产环境
        if (!CommonUtils.isNull(orderTemplateId) && orderTemplateId.equals("72ISRbiW07DClIAlcTtsrSL9uKhG5bIurd_glZ9q3f8")) {
            mapData2.put(WxMessageConstant.WX_MESSAGE_ORDER_STATUS_PRO.getCode(), p3);
            mapData2.put(WxMessageConstant.WX_MESSAGE_ORDER_TYPE_PRO.getCode(), p4);
            mapData2.put(WxMessageConstant.WX_MESSAGE_ORDER_COMMENT_PRO.getCode(), p5);
        //原-测试环境
        } else if (!CommonUtils.isNull(orderTemplateId) && orderTemplateId.equals("MjkhLmzB4-VM2Y729sjEtQvIsTOXmiawe0LcR-lR2AA")) {
            mapData2.put(WxMessageConstant.WX_MESSAGE_ORDER_STATUS_TEST.getCode(), p3);
            mapData2.put(WxMessageConstant.WX_MESSAGE_ORDER_TYPE_TEST.getCode(), p4);
            mapData2.put(WxMessageConstant.WX_MESSAGE_ORDER_COMMENT_TEST.getCode(), p5);
        //智友云
        } else if (!CommonUtils.isNull(orderTemplateId) && orderTemplateId.equals("6JImbpS0Y-wf59UDLMioK_TZVggtyR7GGe9rwyMMktY")) {
            mapData2.put(WxMessageConstant.WX_MESSAGE_ZERO_ORDER_NO_PRO.getCode(), p3);
            mapData2.put(WxMessageConstant.WX_MESSAGE_ZERO_ORDER_TYPE_PRO.getCode(), p4);
            mapData2.put(WxMessageConstant.WX_MESSAGE_ZERO_ORDER_STATUS_PRO.getCode(), p5);
            mapData2.put(WxMessageConstant.WX_MESSAGE_ZERO_ORDER_TOTAL_PRO.getCode(), p5);
            mapData2.put(WxMessageConstant.WX_MESSAGE_ZERO_ORDER_PRODUCT_NAME_PRO.getCode(), p5);
        } else {
            return "";
        }


        data.put("data", mapData2);


        String accessToken = wxTokenService.getAccessToken();
        return this.sendMessage(data, accessToken, touser, orderTemplateId);
    }


}
