package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.qx.QxAttachmentMapper;
import com.imflea.zero.model.entity.qx.QxAttachment;
import com.imflea.zero.model.entity.qx.dto.AttachmentDetailDto;
import com.imflea.zero.service.qx.IQxAttachmentService;
import com.imflea.zero.utils.SessionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 附件表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-27
 */
@Service
public class QxAttachmentServiceImpl extends ServiceImpl<QxAttachmentMapper, QxAttachment> implements IQxAttachmentService {

    @Override
    public QxAttachment saveOrUpdateAttachment(QxAttachment attchment) {
        String id = attchment.getId();
        if (CommonUtils.isNull(id)) {
            attchment.setId(CommonUtils.generateRandomString());

            boolean result = this.save(attchment);
            if (!result) {
                throw new BizException("保存失败");
            }
        } else {

            boolean result = this.updateById(attchment);
            if (!result) {
                throw new BizException("更新失败");
            }
        }
        return attchment;
    }

    @Override
    public Boolean saveAttachmentBatch(List<QxAttachment> attchments) {
        List<QxAttachment> operList = new ArrayList<>();
        String tenantId = SessionUtils.getTenantId();
        for (QxAttachment attachment : attchments) {
            attachment.setTenantId(tenantId);
            attachment.setId(CommonUtils.generateRandomString());
            operList.add(attachment);
        }
        boolean result = this.saveBatch(operList);
        return result;
    }

    @Override
    public Boolean removeByAttachmentById(String id) {
        boolean result = removeById(id);
        return result;
    }

    @Override
    public Boolean removeByBusinessId(AttachmentDetailDto del) {


        QueryWrapper<QxAttachment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", del.getBusinessId());
        queryWrapper.eq("business_sys_code", del.getBusinessSysCode());
        queryWrapper.eq("business_sys_module_code", del.getBusinessSysModuleCode());

        boolean result = this.remove(queryWrapper);
        return result;

    }

    @Override
    public List<QxAttachment> queryAttachmentsByBuinessId(String businessId) {
        QueryWrapper<QxAttachment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", businessId);
        queryWrapper.orderByAsc("file_name");
        List<QxAttachment> resultData = this.list(queryWrapper);
        return resultData;
    }

    @Override
    public List<QxAttachment> queryAttachmentsByBuinessIdAndModule(AttachmentDetailDto queryReq) {
        QueryWrapper<QxAttachment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", queryReq.getBusinessId());
        queryWrapper.eq("business_sys_code", queryReq.getBusinessSysCode());
        queryWrapper.eq("business_sys_module_code", queryReq.getBusinessSysModuleCode());
        queryWrapper.orderByAsc("file_name");
        List<QxAttachment> resultData = this.list(queryWrapper);
        return resultData;
    }

    @Override
    public QxAttachment queryAttachmentsById(String id) {
        QxAttachment qxAttachment = this.getById(id);
        return qxAttachment;
    }
}
