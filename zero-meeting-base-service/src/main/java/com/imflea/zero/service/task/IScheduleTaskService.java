package com.imflea.zero.service.task;

import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.task.ScheduleTask;
import com.imflea.zero.model.entity.task.dto.ScheduleTaskDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * 定时任务配置表 服务类
 * </p>
 *
 * @author zhaoyang
 * @since 2022-01-14
 */
public interface IScheduleTaskService extends IService<ScheduleTask> {

    ScheduleTask saveOrUpdateScheduleTask(ScheduleTask entity);

    PageInfo<ScheduleTask> queryPage(ScheduleTaskDto dto);

    Boolean updateJobStatus(IdDto taskId,String status);
}
