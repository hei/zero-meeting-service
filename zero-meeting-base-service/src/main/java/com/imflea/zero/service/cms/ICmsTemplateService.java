package com.imflea.zero.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.cms.CmsTemplate;
import com.imflea.zero.model.entity.cms.dto.CmsTemplateDto;
import com.imflea.zero.model.entity.cms.vo.CmsContentVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 发布模板信息 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-17
 */
public interface ICmsTemplateService extends IService<CmsTemplate> {

    /**
     * 管理端: 保存CMS模板信息
     */
    CmsTemplate saveEntity(CmsTemplate entity);

    /**
     * 管理端: 保存CMS模板信息
     */
    CmsTemplate updateEntity(CmsTemplate entity);

    /**
     * 管理端: 保存CMS模板信息
     */
    Boolean removeEntity(String id);

    /**
     * 管理端: 保存CMS模板信息
     */
    CmsTemplate queryEntity(String id);

    /**
     * 管理端: 保存CMS模板信息
     */
    PageInfo<CmsTemplate> queryPage(CmsTemplateDto dto);

    /**
     * 管理端: 保存CMS模板信息
     */
    List<CmsTemplate> queryList(CmsTemplateDto cmsTemplateDto);

    /**
     * 移动端: 保存CMS模板信息
     */
    CmsContentVo queryOneFromRedis(CmsTemplateDto dto);

    /**
     * 移动端: 保存CMS模板信息
     */
    List<CmsContentVo> queryListFromRedis(CmsTemplateDto dto);

    /**
     * 移动端: 保存CMS模板信息
     */
    CmsContentVo queryContentFromRedis(CmsTemplateDto dto);
}
