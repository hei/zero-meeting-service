package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxRole;
import com.imflea.zero.model.entity.qx.QxRoleUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色用户关联表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
public interface IQxRoleUserService extends IService<QxRoleUser> {

    Boolean saveRoleUserBatch(String userId, List<String> roleIds);

    Boolean saveRoleUserBatch(String userId, List<String> roleIds, String tenantId);

    List<QxRoleUser> queryRolesByUserId(String userId);

    boolean query4CheckIsTenantAdmin(String userId);

    List<QxRoleUser> queryRolesByUserIds(List<String> userIds);

    Map<String, List<QxRole>> queryAllRoleByUserIds(List<String> userIds);
}
