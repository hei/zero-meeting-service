package com.imflea.zero.service.cms.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.model.dao.cms.CmsContentInfoMapper;
import com.imflea.zero.model.entity.cms.CmsContentInfo;
import com.imflea.zero.service.cms.ICmsContentInfoService;
import com.imflea.zero.utils.RestUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 发布正文信息表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-17
 */
@Service
public class CmsContentInfoServiceImpl extends ServiceImpl<CmsContentInfoMapper, CmsContentInfo> implements ICmsContentInfoService {

    @Override
    public Boolean removeByTemplateId(String templateId) {

        LambdaQueryWrapper<CmsContentInfo> lambdaQuery = Wrappers.lambdaQuery();
        lambdaQuery.eq(CmsContentInfo::getTemplateId, templateId);

        return this.remove(lambdaQuery);
    }

    @Override
    public List<CmsContentInfo> queryListByTemplateId(String templateId) {

        LambdaQueryWrapper<CmsContentInfo> lambdaQuery = Wrappers.lambdaQuery();
        lambdaQuery.eq(CmsContentInfo::getTemplateId, templateId);
        lambdaQuery.orderByAsc(CmsContentInfo::getSort);

        List<CmsContentInfo> list = this.list(lambdaQuery);
        return RestUtil.orEmptyList(list);
    }


}
