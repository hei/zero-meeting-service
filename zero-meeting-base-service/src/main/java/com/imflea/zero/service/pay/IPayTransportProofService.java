package com.imflea.zero.service.pay;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.pay.PayTransportProof;
import com.imflea.zero.model.entity.pay.dto.PayProofQueryDto;
import com.imflea.zero.model.entity.pay.dto.PayTransportProofDto;
import com.imflea.zero.model.entity.pay.vo.PayRecorderDetailVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 转账支付凭证 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-27
 */
public interface IPayTransportProofService extends IService<PayTransportProof> {

    PayTransportProof saveOrUpdatePayTransportProof(PayTransportProof entity);

    PageInfo<PayTransportProof> queryPage(PayProofQueryDto dto);

    PayTransportProof savePayTransportProof(PayTransportProofDto payTransportProof);

    List<PayTransportProof> getByOrderSn(String orderSn);

    PayTransportProof getByOrderSn(String orderSn, String proofType);

    PayRecorderDetailVo queryPayDetailRecorderInfo(String sn);
}
