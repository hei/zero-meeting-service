package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxMenu;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
public interface IQxMenuService extends IService<QxMenu> {

    QxMenu saveOrUpdateMenu(QxMenu qxMenu);

    Boolean removeMenuById(String id);

    Boolean updateMenuStatus(List<String> ids, String status);

    List<QxMenu> queryByParentId(String menuPid);

    List<QxMenu> queryMenuByIds(List<String> menuIds);

    List<Map<String, Object>> queryMenuTree();

    List<Map<String, Object>> queryMenuTreeNoStatus();
}
