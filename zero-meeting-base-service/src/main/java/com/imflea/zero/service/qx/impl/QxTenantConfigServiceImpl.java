package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.qx.QxTenantConfigMapper;
import com.imflea.zero.model.entity.qx.QxTenantConfig;
import com.imflea.zero.model.entity.qx.dto.QxTenantConfigDto;
import com.imflea.zero.service.qx.IQxTenantConfigService;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 租户配置表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
@Service
public class QxTenantConfigServiceImpl extends ServiceImpl<QxTenantConfigMapper, QxTenantConfig> implements IQxTenantConfigService {

    @Override
    public QxTenantConfig saveOrUpdateQxTenantConfig(QxTenantConfig entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
        }
        entity.setTenantId(SessionUtils.getTenantId());
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<QxTenantConfig> queryPage(QxTenantConfigDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<QxTenantConfig> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<QxTenantConfig> adoptInfos = this.list(queryWrapper);
        PageInfo<QxTenantConfig> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

    @Override
    public QxTenantConfig queryByTenantId(String tenantId) {
        QueryWrapper<QxTenantConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tenant_id", tenantId);
        QxTenantConfig tenantConfig = this.getOne(queryWrapper);
        return tenantConfig;
    }

    @Override
    public QxTenantConfig queryTransferInfoBankByTenantId(String tenantId) {
        QueryWrapper<QxTenantConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tenant_id", tenantId);
        QxTenantConfig tenantConfig = this.getOne(queryWrapper);
        if (CommonUtils.isNull(tenantConfig)) {
            throw new BizException("未获取到有效的支付配置信息");
        }
        QxTenantConfig newConfigResult = new QxTenantConfig();

        newConfigResult.setPayBankName(tenantConfig.getPayBankName());
        newConfigResult.setPayBankNum(tenantConfig.getPayBankNum());
        newConfigResult.setPayCompanyName(tenantConfig.getPayCompanyName());
        return newConfigResult;
    }

    @Override
    public QxTenantConfig queryByMchId(String mchId) {
        QueryWrapper<QxTenantConfig> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mch_id", mchId);
        QxTenantConfig tenantConfig = this.getOne(queryWrapper);
        if (CommonUtils.isNull(tenantConfig)) {
            throw new BizException("未获取到有效的支付配置信息");
        }
        return tenantConfig;
    }

    @Override
    public String queryOrderTemplateId(String tenantId) {
        QxTenantConfig config = this.queryByTenantId(tenantId);
        if (!CommonUtils.isNull(config)) {
            return config.getOrderTemplateId();
        }
        return null;

    }

    @Override
    public String queryDefaultComment(String tenantId) {
        QxTenantConfig config = this.queryByTenantId(tenantId);
        if (!CommonUtils.isNull(config)) {
            return config.getAdoptDefaultComment();
        }
        return null;

    }

}
