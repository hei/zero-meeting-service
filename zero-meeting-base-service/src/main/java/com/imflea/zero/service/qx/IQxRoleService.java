package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxRole;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
public interface IQxRoleService extends IService<QxRole> {

    QxRole saveOrUpdateRole(QxRole role);

    List<QxRole> queryRoles();

    Boolean removeRoleById(String id);

    List<QxRole> queryTenantRoles();
}
