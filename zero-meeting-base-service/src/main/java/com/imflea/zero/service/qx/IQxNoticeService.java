package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxNotice;
import com.imflea.zero.model.entity.qx.dto.QxNoticeDto;
import com.imflea.zero.model.entity.qx.dto.QxNoticeSaveDto;
import com.imflea.zero.model.entity.qx.vo.QxNoticeDetailVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 系统通知 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-22
 */
public interface IQxNoticeService extends IService<QxNotice> {

    QxNotice saveOrUpdateQxNotice(QxNotice entity);

    PageInfo<QxNotice> queryPage(QxNoticeDto dto);

    QxNotice queryByNoticeType(String noticeType);

    QxNoticeDetailVo saveOrUpdateQxNotice(QxNoticeSaveDto qxNoticeSaveDto);

    QxNoticeDetailVo queryDetailById(String id);

    boolean removeBatchByIds(List<String> ids);

    boolean updateStatus(String id, String status, Boolean updateOrderNum);

    QxNotice queryByTypeAndOrderNumBiggest(String noticeType);

    List<QxNotice> queryListByNoticeType(String noticeType);

    List<QxNotice> queryListByNoticeTypeValid(String noticeType);
}
