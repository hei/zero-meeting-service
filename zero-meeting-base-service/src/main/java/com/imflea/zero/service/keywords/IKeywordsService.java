package com.imflea.zero.service.keywords;

import com.imflea.zero.model.entity.keywords.Keywords;
import com.imflea.zero.model.entity.keywords.dto.KeywordsDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 敏感词表 服务类
 * </p>
 *
 * @author guohui
 * @since 2021-10-09
 */
public interface IKeywordsService extends IService<Keywords> {

    Keywords saveOrUpdateKeywords(Keywords entity);

    PageInfo<Keywords> queryPage(KeywordsDto dto);

    List<Keywords> queryAll();

}
