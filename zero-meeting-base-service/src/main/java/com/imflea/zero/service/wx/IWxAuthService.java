package com.imflea.zero.service.wx;

import com.imflea.zero.model.entity.user.WebUserInfo;
import com.imflea.zero.model.entity.wx.vo.WxUserLoginVo;


public interface IWxAuthService {
    WxUserLoginVo saveOrLogin4WxAuth(String code);

    WebUserInfo updateUserDetailInfo(String code, String encryptedData, String iv);

    boolean refreshlogInUtils(String accessToken, String tenantId, String nickName, String avatarUrl) throws Exception;
}
