package com.imflea.zero.service.invoice.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.util.ZeroJsonUtils;
import com.imflea.zero.model.dao.invoice.InvoiceInfoMapper;
import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.imflea.zero.model.entity.invoice.InvoiceInfo;
import com.imflea.zero.model.entity.invoice.dto.InsertWxApplayDto;
import com.imflea.zero.model.entity.invoice.dto.InvoiceInfoDto;
import com.imflea.zero.service.invoice.IInvoiceHeaderService;
import com.imflea.zero.service.invoice.IInvoiceInfoService;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

//import com.imflea.zero.service.adopt.IAdoptOrderInfoService;

/**
 * <p>
 * 发票信息 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 */
@Service
public class InvoiceInfoServiceImpl extends ServiceImpl<InvoiceInfoMapper, InvoiceInfo> implements IInvoiceInfoService {

    @Autowired
    IInvoiceHeaderService iInvoiceHeaderService;


    @Override
    public InvoiceInfo saveOrUpdateInvoiceInfo(InvoiceInfo entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
            this.setUserInfo(entity, 0);
        } else {
            this.setUserInfo(entity, 1);
        }
        entity.setTenantId(SessionUtils.getTenantId());
        InvoiceHeader header = iInvoiceHeaderService.getById(entity.getInvoiceHeaderId());
        try {
            entity.setHeaderJson(ZeroJsonUtils.entityToJson(header));
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.saveOrUpdate(entity);
        return entity;
    }

    private void setUserInfo(InvoiceInfo pam, int flag) {
        if (flag == 0) { // 新增（申请）
            pam.setCreateTime(LocalDateTime.now());
            pam.setCreateUserId(SessionUtils.getUserId());
            pam.setCreateUserName(SessionUtils.getWxNickName());
            pam.setUserId(SessionUtils.getUserId());
        } else { //更新（已开票）
            pam.setUpdateTime(LocalDateTime.now());
            pam.setUpdateUserId(SessionUtils.getUserId());
            pam.setUpdateUserName(SessionUtils.getWxNickName());
        }
    }

    @Override
    public InvoiceInfo insertWxApplay(InsertWxApplayDto insertWxApplayDto) {
        if (CommonUtils.isNull(insertWxApplayDto) ||CommonUtils.isNull(insertWxApplayDto.getInvoiceHeaderId()) ||CommonUtils.isNull(insertWxApplayDto.getOrderId())) {
            return null;
        }
        InvoiceInfo result = new InvoiceInfo();
        BeanUtils.copyProperties(insertWxApplayDto, result);
        result.setId(CommonUtils.generateRandomString());
        this.setUserInfo(result, 0);
        result.setStatus("1");
        result = this.saveOrUpdateInvoiceInfo(result);
        return result;
    }

    @Override
    public Boolean insertBath(List<InsertWxApplayDto> list) {
        if (CommonUtils.isNull(list)) {
            return false;
        }
        List<InvoiceInfo> result = new ArrayList<>();
        for (InsertWxApplayDto dto : list) {
            InvoiceInfo invoiceInfo = new InvoiceInfo();
            BeanUtils.copyProperties(dto, invoiceInfo);
            this.setUserInfo(invoiceInfo, 0);
            invoiceInfo.setStatus("1");
            result.add(invoiceInfo);
        }
        return this.saveBatch(result);
    }

    @Override
    public List<InvoiceInfo> listByOrderIdAndUserIdAndTentantId(String id) {
        QueryWrapper<InvoiceInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", id);
        return this.list(queryWrapper);
    }

    @Override
    public PageInfo<InvoiceInfo> selectPage(InvoiceInfoDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        QueryWrapper<InvoiceInfo> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(SessionUtils.getTenantId())) {
            queryWrapper.eq("tenant_id", SessionUtils.getTenantId());
        }
        if (!CommonUtils.isNull(dto.getOrderType())) {
            queryWrapper.eq("order_type", dto.getOrderType());
        }
        if (!CommonUtils.isNull(dto.getStatus())) {
            queryWrapper.eq("status", dto.getStatus());
        }
        queryWrapper.orderByDesc("update_time");
        PageHelper.startPage(pageIndex, pageSize);
        List<InvoiceInfo> result = this.list(queryWrapper);
        PageInfo<InvoiceInfo> pageInfo = new PageInfo<>(result);
        return pageInfo;
    }

    @Override
    public InvoiceInfo queryByOrderId(String id) {
        QueryWrapper<InvoiceInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", id);
        return this.getOne(queryWrapper);

    }


}
