package com.imflea.zero.service.user.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.dao.user.WebUserFeedbackMapper;
import com.imflea.zero.model.entity.user.WebUserFeedback;
import com.imflea.zero.model.entity.user.dto.WebUserFeedbackDto;
import com.imflea.zero.service.user.IWebUserFeedbackService;
import com.imflea.zero.utils.AssertUtil;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-24
 */
@Service
public class WebUserFeedbackServiceImpl extends ServiceImpl<WebUserFeedbackMapper, WebUserFeedback> implements IWebUserFeedbackService {

    // 小程序端接口
    @Override
    public WebUserFeedback saveUserFeedback(WebUserFeedback entity) {

        entity.setId(CommonUtils.generateRandomString());
        entity.setTenantId(SessionUtils.getTenantId());
        entity.setMemberId(SessionUtils.getUserId());
        entity.setMemberName(SessionUtils.getWxNickName());
//        entity.setCreateTime(LocalDateTime.now());
//        entity.setUpdateTime(LocalDateTime.now());
//        entity.setCreateUserId(SessionUtils.getUserId());
//        entity.setCreateUserName(SessionUtils.getWxNickName());
//        entity.setUpdateUserId(SessionUtils.getUserId());
//        entity.setUpdateUserName(SessionUtils.getWxNickName());

        this.save(entity);

        return entity;
    }


    //管理端接口
    @Override
    public WebUserFeedback updateUserFeedback(WebUserFeedback entity) {

        String id = entity.getId();
        AssertUtil.mustNotEmpty(id, "ID为空");

        WebUserFeedback feedback = new WebUserFeedback();
        feedback.setId(id);
        feedback.setReplId(SessionUtils.getUserId());
        feedback.setReplyName(SessionUtils.getUserName());
        feedback.setReply(entity.getReply());

        this.updateById(feedback);

        return feedback;
    }


    @Override
    public PageInfo<WebUserFeedback> queryPage(WebUserFeedbackDto dto) {

        QueryWrapper<WebUserFeedback> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("tenant_id", SessionUtils.getTenantId());

        String theme = dto.getTheme();
        queryWrapper.like(!CommonUtils.isNull(theme), "theme", theme);

        String memberName = dto.getMemberName();
        queryWrapper.like(!CommonUtils.isNull(memberName), "member_name", memberName);

        String replyName = dto.getReplyName();
        queryWrapper.like(!CommonUtils.isNull(replyName), "reply_name", replyName);

        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        PageHelper.startPage(pageIndex, pageSize);

        List<WebUserFeedback> adoptInfos = this.list(queryWrapper);

        PageInfo<WebUserFeedback> pageResult = new PageInfo<>(adoptInfos);

        return pageResult;
    }

}
