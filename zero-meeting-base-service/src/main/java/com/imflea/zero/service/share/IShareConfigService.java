package com.imflea.zero.service.share;

import com.imflea.zero.model.entity.share.ShareConfig;
import com.imflea.zero.model.entity.share.dto.PosterDto;
import com.imflea.zero.model.entity.share.dto.ShareConfigDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.share.vo.QueryPageVo;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * 分享界面图片配置表 服务类
 * </p>
 *
 * @author guohui
 * @since 2021-10-21
 */
public interface IShareConfigService extends IService<ShareConfig> {

    ShareConfigDto saveOrUpdateShareConfig(ShareConfigDto entity);

    PageInfo<QueryPageVo> queryPage(ShareConfigDto dto);

    ShareConfig queryByBusinessId(String id);

    String poster(PosterDto pam) throws Exception;
}
