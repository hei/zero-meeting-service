package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.cms.CmsCommonContent;
import com.imflea.zero.model.entity.qx.QxTenant;
import com.imflea.zero.model.entity.qx.dto.QxTenantDto;
import com.imflea.zero.model.entity.qx.dto.TenantPageQueryDto;
import com.imflea.zero.model.entity.qx.vo.TenantDetailVo;
import com.github.pagehelper.PageInfo;

import java.util.Map;

/**
 * <p>
 * 租户表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-24
 */
public interface IQxTenantService extends IService<QxTenant> {

    QxTenant saveOrupdateTenant(QxTenant tenant);

    PageInfo<QxTenant> queryTenant4Page(TenantPageQueryDto pageQueryReq);

    Boolean updateTenantStatusById(String id, String closeStatus);


    CmsCommonContent saveOrUpdateContent(CmsCommonContent tenantContent);

    TenantDetailVo queryDetailById(String id);
}
