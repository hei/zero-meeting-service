package com.imflea.zero.service.meeting;

import com.imflea.zero.model.entity.meeting.MeetingOrderInfo;
import com.imflea.zero.model.entity.meeting.MeetingOrderInfoItem;
import com.imflea.zero.model.entity.meeting.dto.MeetingOrderInfoItemDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.entity.meeting.dto.MeetingRoomDayTimeDto;
import com.imflea.zero.model.entity.meeting.dto.MeetingRoomTimeCheckDto;
import com.imflea.zero.model.entity.meeting.vo.DateTime4ItemStatusVO;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 空间订单表-分项表 服务类
 * </p>
 *
 * @author zzz
 * @since 2024-03-03
 */
public interface IMeetingOrderInfoItemService extends IService<MeetingOrderInfoItem> {

    MeetingOrderInfoItem saveOrUpdateMeetingOrderInfoItem(MeetingOrderInfoItem entity);

    PageInfo<MeetingOrderInfoItem> queryPage(MeetingOrderInfoItemDto dto);

    List<DateTime4ItemStatusVO> getCanOrderTimeByDateByDate(MeetingRoomDayTimeDto dayTimeDto);

    void saveOrderItemByOrder(MeetingOrderInfo order);

    Boolean removeByOrderId(String orderId);

    Boolean check(MeetingRoomTimeCheckDto dayTimeDto);
}
