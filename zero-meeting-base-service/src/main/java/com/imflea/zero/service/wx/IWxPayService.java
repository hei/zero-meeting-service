package com.imflea.zero.service.wx;

import com.imflea.zero.model.entity.pay.dto.PayOrderDetailDto;
import com.imflea.zero.model.entity.pay.dto.PayRefundOrderDto;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.service.wx
 * @ClassName: IWxPayService
 * @Description
 * @date 2021-09-26  13:26:43
 */

public interface IWxPayService {


    Map<String, Object> addOrder4Unified(PayOrderDetailDto orderDetail) throws Exception;

    Map<String, Object> updateIsPayFinish4QueryOrder(PayOrderDetailDto orderDetail) throws Exception;

    String update4WxCallBack(HttpServletRequest request) throws Exception;


    Map<String, String> doRefund(String orderNumber, BigDecimal refundAmount, BigDecimal totalFee);


    Boolean closeOrders(PayOrderDetailDto orderDetail) throws Exception;

    Map<String, String> saveDoRefund(PayRefundOrderDto refundDto);

    Map<String, String> handleRefund(PayRefundOrderDto refundDto);
}
