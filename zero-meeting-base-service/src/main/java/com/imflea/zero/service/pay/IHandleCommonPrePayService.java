package com.imflea.zero.service.pay;

import com.imflea.zero.model.entity.pay.dto.PayOrderDetailDto;
import com.imflea.zero.model.entity.pay.dto.PaySuccessDto;
import com.imflea.zero.model.entity.pay.vo.PayPreDataVo;
import com.imflea.zero.model.entity.pay.vo.PayPreRefundVo;

import java.util.List;

public interface IHandleCommonPrePayService {

    PayPreDataVo getPrePayData(PayOrderDetailDto dto) throws Exception;


    /**
     * @param
     * @author 祥保玉
     * @description 创建预支付单：预支付结果
     * @date 2021/10/15  10:38
     */

    boolean handleCreatePrePayResult(List<String> orderIds,String payType);

    /**
     * @param orderIds
     * @return boolean
     * @author 祥保玉
     * @description 支付完成，处理订单以及其他业务逻辑
     * @date 2021/10/15  11:12
     */


//    boolean handlePaySuccess(List<String> orderIds);

    boolean handlePaySuccess(List<PaySuccessDto> dto);

    /**
     *
     * @param orderId
     * @param refundOrderId
     * @param orderType
     * @return com.imflea.zero.model.entity.pay.vo.PayPreDataVo
     * @author 祥保玉
     * @description 退款数据预处理
     * @date 2021/11/2  17:00
     */


    PayPreRefundVo getPreRefundData(String refundOrderId, String orderType, String extend);

    boolean handleRefundSuccess(String refundOrderId, String orderType, String extend);
}
