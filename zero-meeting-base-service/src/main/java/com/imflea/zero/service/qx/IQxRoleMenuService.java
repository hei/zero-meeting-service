package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxMenu;
import com.imflea.zero.model.entity.qx.QxRoleMenu;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色菜单关联表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
public interface IQxRoleMenuService extends IService<QxRoleMenu> {

    Boolean saveRoleMenuRelations(String roleId, List<String> menuIds);

    List<Map<String, Object>> queryMenusByUserId(String currentUserId);

    List<QxMenu> queryRoleMenuByRoleId(String roleId);
}
