package com.imflea.zero.service.meeting;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.entity.meeting.MeetingShop;
import com.imflea.zero.model.entity.meeting.dto.MeetingShopDto;
import com.imflea.zero.model.entity.qx.QxUserInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xby
 * @since 2024-01-24
 */
public interface IMeetingShopService extends IService<MeetingShop> {

    MeetingShop saveOrUpdateMeetingShop(MeetingShop entity);

    PageInfo<MeetingShop> queryPage(MeetingShopDto dto);

    List<MeetingShop> queryAll(MeetingShopDto meetingShopDto);

    List<MeetingShop> queryByProvince(String province);

    List<MeetingShop> listAll();

    List<MeetingShop> listAll(String province, String city, String region);
}
