package com.imflea.zero.service.wx;

import java.util.Map;

public interface IWxMessageService {
    public String sendMessage(Map<String, Object> data, String accessToken, String touser, String templateId);

    public String sendOrderMessage(String statusName, String typeName, String message, String touser) throws Exception;
}
