package com.imflea.zero.service.meeting;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.entity.meeting.MeetingRoom;
import com.imflea.zero.model.entity.meeting.dto.MeetingRoomDto;
import com.imflea.zero.model.entity.meeting.dto.RoomStatProvinceDto;
import com.imflea.zero.model.entity.meeting.vo.MeetingAreaStatVo;
import com.imflea.zero.model.entity.meeting.vo.MeetingRoomDetailVO;
import com.imflea.zero.model.entity.meeting.vo.MeetingRoomStatProvinceVO;
import com.imflea.zero.model.entity.qx.BaseArea;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 空间表 服务类
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 */
public interface IMeetingRoomService extends IService<MeetingRoom> {

    MeetingRoom saveOrUpdateMeetingRoom(MeetingRoom entity);

    PageInfo<MeetingRoomDetailVO> queryPage(MeetingRoomDto dto);

    MeetingRoomDetailVO getDetailById(String id);

    List<MeetingAreaStatVo> statRoomProvinceByRoomType(RoomStatProvinceDto meetingRoomDto);

    List<MeetingRoom> queryByShopIds(List<String> shopIds);

    List<MeetingRoom> queryByShopId(String shopId);

    List<MeetingRoom> queryByShopIdsAndRoomType(List<String> shopIds, String roomType);

    Boolean updateStatusById(String id, String s);
}
