package com.imflea.zero.service.user;

import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.PageDto;
import com.imflea.zero.model.entity.user.WebUserInfo;
import com.imflea.zero.model.entity.user.dto.WebUserInfoDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.entity.user.vo.WebUserInfoDetailVo;

import java.util.Map;

/**
 * <p>
 * 微信端用户基本信息 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-13
 */
public interface IWebUserInfoService extends IService<WebUserInfo> {

    Boolean updateWebUserInfo(WebUserInfo entity);

    PageInfo<WebUserInfo> queryPage(WebUserInfoDto dto);

    WebUserInfo queryByOpenId(String openId);

    Map<String, Object>  query4Stat();

    String getOpenIdByUserId(String userId);

    WebUserInfoDetailVo getWebUserDetailById(IdDto idDto);
}
