package com.imflea.zero.service.email;

import com.imflea.zero.model.entity.invoice.dto.SendMailDto;
import org.springframework.stereotype.Service;

@Service
public interface IBusinessSendEmailService {
    Boolean updateContentAndStatusById(SendMailDto sendMailDto);
}
