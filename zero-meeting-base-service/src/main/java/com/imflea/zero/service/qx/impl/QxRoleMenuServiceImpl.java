package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.util.ZeroJsonUtils;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.qx.QxRoleMenuMapper;
import com.imflea.zero.model.entity.qx.QxMenu;
import com.imflea.zero.model.entity.qx.QxRoleMenu;
import com.imflea.zero.model.entity.qx.QxRoleUser;
import com.imflea.zero.service.qx.IQxMenuService;
import com.imflea.zero.service.qx.IQxRoleMenuService;
import com.imflea.zero.service.qx.IQxRoleUserService;
import com.imflea.zero.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色菜单关联表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
@Service
public class QxRoleMenuServiceImpl extends ServiceImpl<QxRoleMenuMapper, QxRoleMenu> implements IQxRoleMenuService {
    @Autowired
    private IQxRoleUserService roleUserService;
    @Autowired
    private IQxMenuService menuService;

    @Override
    public Boolean saveRoleMenuRelations(String roleId, List<String> menuIds) {
        QueryWrapper<QxRoleMenu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("role_id", roleId);
        long conut = this.count(queryWrapper);

        boolean delResult = this.remove(queryWrapper);

        if (conut == 0 || (conut > 0 && delResult)) {
            List<QxRoleMenu> roleMenus = new ArrayList<>();
            for (String menuId : menuIds) {
                QxRoleMenu roleMenu = new QxRoleMenu();
                roleMenu.setId(CommonUtils.generateRandomString());
                roleMenu.setRoleId(roleId);
                roleMenu.setMenuId(menuId);
                roleMenu.setTenantId(SessionUtils.getTenantId());
                roleMenus.add(roleMenu);
            }
            boolean saveResult = this.saveBatch(roleMenus);
            if (saveResult) {
                return saveResult;
            } else {
                throw new BizException("授权失败");
            }

        } else {
            throw new BizException("授权失败");
        }
    }

    @Override
    public List<Map<String, Object>> queryMenusByUserId(String currentUserId) {
        List<QxRoleUser> roleUsers = roleUserService.queryRolesByUserId(currentUserId);
        if (CommonUtils.isNull(roleUsers)) {
            return new ArrayList<>();
        }

        List<String> roleIds = roleUsers.stream().map(QxRoleUser::getRoleId).collect(Collectors.toList());
        QueryWrapper<QxRoleMenu> queryWrapper4RoleMenu = new QueryWrapper<>();
        queryWrapper4RoleMenu.in("role_id", roleIds);
        List<QxRoleMenu> roleMenus = this.list(queryWrapper4RoleMenu);
        List<String> menuIds = roleMenus.stream().map(QxRoleMenu::getMenuId).collect(Collectors.toList());
        List<QxMenu> listMenus = menuService.queryMenuByIds(menuIds);


        List<Map<String, Object>> resultMenus = null;
        try {
            resultMenus =CommonUtils.listToTree(ZeroJsonUtils.entityListToMapList(listMenus), "id", "menuPid");
        } catch (Exception e) {
            throw new BizException(e.getMessage());
        }
        return resultMenus;
    }

    @Override
    public List<QxMenu> queryRoleMenuByRoleId(String roleId) {
        QueryWrapper<QxRoleMenu> roleMenuQueryWrapper = new QueryWrapper<>();
        roleMenuQueryWrapper.eq("role_id", roleId);
        List<QxRoleMenu> result = this.list(roleMenuQueryWrapper);
        if (CommonUtils.isNull(result)) {
            return new ArrayList<>();
        } else {
            List<String> menuIds = result.stream().map(QxRoleMenu::getMenuId).collect(Collectors.toList());
            List<QxMenu> menus = menuService.listByIds(menuIds);
            return menus;
        }


    }
}
