//package com.imflea.zero.service.price.impl;
//
//import cn.hutool.core.bean.BeanUtil;
//import com.imflea.zero.util.base.CommonUtils;
//import com.imflea.zero.util.ZeroJsonUtils;
//import com.imflea.zero.constant.SemConstant;
//import com.imflea.zero.exception.BizException;
//import com.imflea.zero.model.entity.adopt.AdoptSku;
//import com.imflea.zero.model.entity.delivery.dto.ProductDeliveryRuleDto;
//import com.imflea.zero.model.entity.mall.vo.MallSkuPriceVo;
//import com.imflea.zero.model.entity.price.PriceCalculateDto;
//import com.imflea.zero.model.entity.price.SemCouponCalculateDto;
//import com.imflea.zero.model.entity.price.SkuAndAmountDto;
//import com.imflea.zero.model.entity.price.vo.PriceDetailVo;
//import com.imflea.zero.model.entity.price.vo.SkuAmountSemVo;
//import com.imflea.zero.model.entity.sem.SemCouponBusinessRelation;
//import com.imflea.zero.model.entity.sem.SemGiftCard;
//import com.imflea.zero.model.entity.sem.vo.SemCouponItemDetailVo;
//import com.imflea.zero.model.entity.sem.vo.SemItemSkuVo;
//import com.imflea.zero.model.entity.sem.vo.SkusPriceVo;
//import com.imflea.zero.service.adopt.IAdoptSkuService;
//import com.imflea.zero.service.delivery.impl.CalculateDeliveryFeeService;
//import com.imflea.zero.service.mall.IMallProductSkuService;
//import com.imflea.zero.service.price.IPriceService;
//import com.imflea.zero.service.sem.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.math.BigDecimal;
//import java.text.DecimalFormat;
//import java.time.LocalDate;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
//@Service
//public class PriceServiceImpl implements IPriceService {
//
//    private final Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    @Autowired
//    IAdoptSkuService adoptSkuService;
//    @Autowired
//    ISemAdoptCouponBizService semAdoptCouponService;
//    @Autowired
//    ISemCouponService semCouponService;
//    @Autowired
//    ISemCouponRuleService semCouponRuleService;
//    @Autowired
//    private IMallProductSkuService mallProductSkuService;
//
//    @Autowired
//    private ISemGiftCardService semGiftCardService;
//    @Autowired
//    private CalculateDeliveryFeeService calculateDeliveryFeeService;
//    @Autowired
//    private ISemCouponBusinessService semCouponBusinessService;
//
//    @Override
//    public BigDecimal calculatePrice(PriceCalculateDto priceCalculateDto) {
//        AdoptSku sku = adoptSkuService.getById(priceCalculateDto.getSkuId());
//        Integer adoptSemAdopt = 0;
//        List<String> semAdoptIdList = priceCalculateDto.getSemAdoptIdList();
//        for (String semAdoptId : semAdoptIdList) {
//            Boolean usable = semAdoptCouponService.queryCouponIsUsable(semAdoptId);
//            if (usable) {
//                adoptSemAdopt++;
//            }
//        }
//        try {
//            logger.error(ZeroJsonUtils.entityToJson(priceCalculateDto));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        Integer needPayAmount = priceCalculateDto.getAmount() - adoptSemAdopt;
//        if (needPayAmount < 0) {
//            needPayAmount = 0;
//        }
//        BigDecimal needPayAmountBigDecimal = new BigDecimal(needPayAmount);
//        BigDecimal totalPayPrice = sku.getPriceNew().multiply(needPayAmountBigDecimal);
//        return totalPayPrice;
//    }
//
//    @Override
//    public BigDecimal calculatePrice(SemCouponCalculateDto semDto) {
//
//        return null;
//    }
//
//
//    @Override
//    public PriceDetailVo listAndCalculate4SemCoupon(SemCouponCalculateDto semCouponCalculateDto) {
//        return this.listAndCalculate4SemCoupon(semCouponCalculateDto, true);
//    }
//
//    @Override
//    public PriceDetailVo listAndCalculate4Change(SemCouponCalculateDto semCouponCalculateDto) {
//        PriceDetailVo resultVo = new PriceDetailVo();
//        String useGiftCard = semCouponCalculateDto.getUseGiftCard();
//        Boolean isCalculateGiftCard = !CommonUtils.isNull(useGiftCard) && useGiftCard.equals("1");
//
//
//        BigDecimal orderAmount = new BigDecimal("0");
//        orderAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//
//        resultVo.setSemType("3");
//        DecimalFormat decimalFormat = new DecimalFormat("0.00#");
//        BigDecimal allTotalAmout = new BigDecimal("0");
//        allTotalAmout.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//        BigDecimal allDeliveryFee = new BigDecimal("0");
//        List<SkuAndAmountDto> skus = semCouponCalculateDto.getSkuList();
//        List<SkuAmountSemVo> skuAmountSemVos = new ArrayList<>();
//        List<MallSkuPriceVo> mallSkuPriceVos = mallProductSkuService.queryProductSkuPrice(skus.stream().map(item -> item.getSkuId()).collect(Collectors.toList()));
//        BigDecimal semMoney = new BigDecimal("0").setScale(2, BigDecimal.ROUND_HALF_DOWN);
//        Map<String, SkuAndAmountDto> skusMap = skus.stream().collect(Collectors.toMap(SkuAndAmountDto::getSkuId, item -> item));
//        for (SkuAndAmountDto s : skus) {
//            SkuAmountSemVo v = new SkuAmountSemVo();
//            ProductDeliveryRuleDto deliveryRuleDto = new ProductDeliveryRuleDto();
//            deliveryRuleDto.setRuleId(s.getRuleId());
//            deliveryRuleDto.setDeliveryMode(s.getDeliveryMode());
//            deliveryRuleDto.setQuantity(s.getAmount());
//            deliveryRuleDto.setReceiverCity(s.getReceiverCity());
//            deliveryRuleDto.setReceiverProvince(s.getReceiverProvince());
//            BigDecimal deliveryFee = calculateDeliveryFeeService.queryDeliveryFee(deliveryRuleDto);
//            v.setAmount(s.getAmount());
//            v.setSkuId(s.getSkuId());
//            v.setDeliveryFee(decimalFormat.format(deliveryFee));
//            allTotalAmout = allTotalAmout.add(deliveryFee);
//            allDeliveryFee = allDeliveryFee.add(deliveryFee);
//            for (MallSkuPriceVo vs : mallSkuPriceVos) {
//                if (vs.getId().equals(s.getSkuId())) {
//                    v.setTotalSemAmount(vs.getPrice().toString());
//                    Integer total = skusMap.get(vs.getId()).getAmount();
//                    BigDecimal perSukAmount = vs.getPrice().multiply(new BigDecimal(total));
//                    orderAmount = orderAmount.add(perSukAmount);
//                    semMoney = semMoney.add(vs.getPrice());
//                }
//
//            }
//            skuAmountSemVos.add(v);
//        }
//        BigDecimal giftCardAmount = new BigDecimal("0");
//        if (isCalculateGiftCard) {
//            giftCardAmount = this.calculate4GiftCard(allTotalAmout);
//        }
//        BigDecimal afterSemAmount = allTotalAmout.subtract(giftCardAmount);
//
//        resultVo.setDeliveryFee(decimalFormat.format(allDeliveryFee));
//        resultVo.setAfterSemAmount(decimalFormat.format(afterSemAmount));
//        resultVo.setSemAmount(decimalFormat.format(semMoney));
//        resultVo.setSkuAmountSemVos(skuAmountSemVos);
//        resultVo.setGiftCardAmount(decimalFormat.format(giftCardAmount));
//        resultVo.setOrderAmount(decimalFormat.format(orderAmount));
//        return resultVo;
//    }
//
//    /**
//     * @param
//     * @return com.imflea.zero.model.entity.price.vo.PriceDetailVo
//     * @author 祥保玉
//     * @description 计算可以抵扣多少礼金卡的额度
//     * @date 2021/12/13  11:29
//     */
//
//
//    public BigDecimal calculate4GiftCard(BigDecimal totalAmount) {
//        if (totalAmount.compareTo(BigDecimal.ZERO) == 0) {
//            return totalAmount;
//        }
//        List<SemGiftCard> dbGiftCards = semGiftCardService.listBindValidGiftCard();
//        if (CommonUtils.isNull(dbGiftCards)) {
//            return BigDecimal.ZERO;
//        }
//        List<SemGiftCard> giftCards = new ArrayList<>();
//        for (SemGiftCard semGiftCard : dbGiftCards) {
//            if (semGiftCard.getStatus().equals(SemConstant.SEM_GIFT_CARD_STATUS_WAIT_USE.getCode()) && semGiftCard.getDeadLine().isAfter(LocalDate.now()) && semGiftCard.getRestValue().compareTo(BigDecimal.ZERO) > 0) {
//                giftCards.add(semGiftCard);
//            }
//        }
//        if (CommonUtils.isNull(giftCards)) {
//            return BigDecimal.ZERO;
//        }
//
//
//        BigDecimal totalResetValue = new BigDecimal("0");
//
//        totalResetValue.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//        BigDecimal semMoney = new BigDecimal("0").setScale(2, BigDecimal.ROUND_HALF_DOWN);
//        if (!CommonUtils.isNull(giftCards)) {
//            totalResetValue = giftCards.stream().map(SemGiftCard::getRestValue).reduce(BigDecimal.ZERO, BigDecimal::add);
//        }
//        if (totalResetValue.compareTo(totalAmount) >= 0) {
//            semMoney = totalAmount;
//        } else {
//            semMoney = totalResetValue;
//        }
//        return semMoney;
//    }
//
//    @Deprecated
//    @Override
//    public PriceDetailVo listAndCalculate4GiftCard(SemCouponCalculateDto semCouponCalculateDto) {
//        PriceDetailVo resultVo = new PriceDetailVo();
//        resultVo.setSemType("2");
//        DecimalFormat decimalFormat = new DecimalFormat("0.00#");
//        BigDecimal allTotalAmout = new BigDecimal("0");
//        allTotalAmout.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//
//
//        BigDecimal orderAmount = new BigDecimal("0");
//        orderAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//
//
//        BigDecimal allDeliveryFee = new BigDecimal("0");
//        List<SkuAndAmountDto> skus = semCouponCalculateDto.getSkuList();
//        List<SkuAmountSemVo> skuAmountSemVos = new ArrayList<>();
//
//
//        for (SkuAndAmountDto s : skus) {
//            SkuAmountSemVo v = new SkuAmountSemVo();
//            ProductDeliveryRuleDto deliveryRuleDto = new ProductDeliveryRuleDto();
//            deliveryRuleDto.setRuleId(s.getRuleId());
//            deliveryRuleDto.setDeliveryMode(s.getDeliveryMode());
//            deliveryRuleDto.setQuantity(s.getAmount());
//            deliveryRuleDto.setReceiverCity(s.getReceiverCity());
//            deliveryRuleDto.setReceiverProvince(s.getReceiverProvince());
//            BigDecimal deliveryFee = calculateDeliveryFeeService.queryDeliveryFee(deliveryRuleDto);
//            v.setAmount(s.getAmount());
//            v.setSkuId(s.getSkuId());
//            v.setDeliveryFee(decimalFormat.format(deliveryFee));
//            allTotalAmout = allTotalAmout.add(deliveryFee);
//            allDeliveryFee = allDeliveryFee.add(deliveryFee);
//            v.setTotalSemAmount("0");
//            skuAmountSemVos.add(v);
//        }
//
//        resultVo.setDeliveryFee(decimalFormat.format(allDeliveryFee));
//
//        Map<String, SkuAndAmountDto> skusMap = skus.stream().collect(Collectors.toMap(SkuAndAmountDto::getSkuId, item -> item));
//        List<MallSkuPriceVo> mallSkuPriceVos = mallProductSkuService.queryProductSkuPrice(skus.stream().map(item -> item.getSkuId()).collect(Collectors.toList()));
//
//        BigDecimal semMoney = new BigDecimal("0").setScale(2, BigDecimal.ROUND_HALF_DOWN);
//        Map<String, BigDecimal> perSukAmountMap = new HashMap<>();
//        for (MallSkuPriceVo vo : mallSkuPriceVos) {
//            BigDecimal price = vo.getPrice();
//            Integer total = skusMap.get(vo.getId()).getAmount();
//            BigDecimal perSukAmount = price.multiply(new BigDecimal(total));
//            orderAmount = orderAmount.add(perSukAmount);
//            allTotalAmout = allTotalAmout.add(perSukAmount);
//            perSukAmountMap.put(vo.getId(), perSukAmount);
//        }
//        List<SemGiftCard> giftCards = semGiftCardService.listBindValidGiftCard();
//        BigDecimal totalResetValue = new BigDecimal("0");
//        totalResetValue.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//        if (!CommonUtils.isNull(giftCards)) {
//            totalResetValue = giftCards.stream().map(SemGiftCard::getRestValue).reduce(BigDecimal.ZERO, BigDecimal::add);
//        }
//        if (totalResetValue.compareTo(allTotalAmout) >= 0) {
//            semMoney = allTotalAmout;
//        } else {
//            semMoney = totalResetValue;
//        }
//        semMoney.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//        BigDecimal bitRate = semMoney.divide(allTotalAmout, BigDecimal.ROUND_HALF_UP);
//        allTotalAmout = allTotalAmout.subtract(semMoney);
//
//
//        for (SkuAmountSemVo vo : skuAmountSemVos) {
//            BigDecimal per = perSukAmountMap.get(vo.getSkuId());
//            vo.setTotalSemAmount(decimalFormat.format(bitRate.multiply(per)));
//        }
//        resultVo.setAfterSemAmount(decimalFormat.format(allTotalAmout));
//        resultVo.setSemAmount(decimalFormat.format(semMoney));
//        resultVo.setSkuAmountSemVos(skuAmountSemVos);
//        resultVo.setOrderAmount(decimalFormat.format(orderAmount));
//        return resultVo;
//    }
//
//    @Override
//    public PriceDetailVo listAndCalculate4No(SemCouponCalculateDto semCouponCalculateDto) {
//        PriceDetailVo resultVo = new PriceDetailVo();
//        resultVo.setSemType("0");
//        String useGiftCard = semCouponCalculateDto.getUseGiftCard();
//        Boolean isCalculateGiftCard = !CommonUtils.isNull(useGiftCard) && useGiftCard.equals("1");
//
//        DecimalFormat decimalFormat = new DecimalFormat("0.00#");
//        BigDecimal allTotalAmout = new BigDecimal("0");
//        BigDecimal allDeliveryFee = new BigDecimal("0");
//
//        BigDecimal orderAmount = new BigDecimal("0");
//        orderAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//
//        List<SkuAndAmountDto> skus = semCouponCalculateDto.getSkuList();
//        List<SkuAmountSemVo> skuAmountSemVos = new ArrayList<>();
//        for (SkuAndAmountDto s : skus) {
//            SkuAmountSemVo v = new SkuAmountSemVo();
//            ProductDeliveryRuleDto deliveryRuleDto = new ProductDeliveryRuleDto();
//            deliveryRuleDto.setRuleId(s.getRuleId());
//            deliveryRuleDto.setDeliveryMode(s.getDeliveryMode());
//            deliveryRuleDto.setQuantity(s.getAmount());
//            deliveryRuleDto.setReceiverCity(s.getReceiverCity());
//            deliveryRuleDto.setReceiverProvince(s.getReceiverProvince());
//            BigDecimal deliveryFee = calculateDeliveryFeeService.queryDeliveryFee(deliveryRuleDto);
//            v.setAmount(s.getAmount());
//            v.setSkuId(s.getSkuId());
//            v.setDeliveryFee(decimalFormat.format(deliveryFee));
//            allTotalAmout = allTotalAmout.add(deliveryFee);
//            allDeliveryFee = allDeliveryFee.add(deliveryFee);
//            v.setTotalSemAmount("0");
//            skuAmountSemVos.add(v);
//
//        }
//        List<MallSkuPriceVo> mallSkuPriceVos = mallProductSkuService.queryProductSkuPrice(skus.stream().map(item -> item.getSkuId()).collect(Collectors.toList()));
//        Map<String, SkuAndAmountDto> skusMap = skus.stream().collect(Collectors.toMap(SkuAndAmountDto::getSkuId, item -> item));
//        for (MallSkuPriceVo vo : mallSkuPriceVos) {
//            BigDecimal price = vo.getPrice();
//            Integer total = skusMap.get(vo.getId()).getAmount();
//            allTotalAmout = allTotalAmout.add(price.multiply(new BigDecimal(total)));
//            orderAmount = orderAmount.add(price.multiply(new BigDecimal(total)));
//        }
//        BigDecimal semMoney = new BigDecimal("0");
//
//
//        BigDecimal giftCardAmount = new BigDecimal("0");
//        if (isCalculateGiftCard) {
//            giftCardAmount = this.calculate4GiftCard(allTotalAmout);
//        }
//        BigDecimal afterSemAmount = allTotalAmout.subtract(giftCardAmount);
//        resultVo.setDeliveryFee(decimalFormat.format(allDeliveryFee));
//        resultVo.setAfterSemAmount(decimalFormat.format(afterSemAmount));
//        resultVo.setSemAmount(decimalFormat.format(semMoney));
//        resultVo.setSkuAmountSemVos(skuAmountSemVos);
//        resultVo.setGiftCardAmount(decimalFormat.format(giftCardAmount));
//        resultVo.setOrderAmount(decimalFormat.format(orderAmount));
//        return resultVo;
//    }
//
//    @Override
//    public PriceDetailVo listAndCalculate4SemCoupon(SemCouponCalculateDto semCouponCalculateDto, Boolean needCheckRule) {
//        PriceDetailVo resultVo = new PriceDetailVo();
//        String useGiftCard = semCouponCalculateDto.getUseGiftCard();
//        Boolean isCalculateGiftCard = !CommonUtils.isNull(useGiftCard) && useGiftCard.equals("1");
//        resultVo.setSemType("1");
//        DecimalFormat decimalFormat = new DecimalFormat("0.00#");
//        BigDecimal allDeliveryFee = new BigDecimal("0");
//        List<String> pickedSemItemIds = semCouponCalculateDto.getSemCoupItemIds();
//        List<SkuAndAmountDto> skus = semCouponCalculateDto.getSkuList();
//        BigDecimal allTotalAmout = new BigDecimal("0");
//
//        BigDecimal orderAmount = new BigDecimal("0");
//        orderAmount.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//
//        List<SkuAmountSemVo> skuAmountSemVos = new ArrayList<>();
//        for (SkuAndAmountDto s : skus) {
//            SkuAmountSemVo v = new SkuAmountSemVo();
//            ProductDeliveryRuleDto deliveryRuleDto = new ProductDeliveryRuleDto();
//            deliveryRuleDto.setRuleId(s.getRuleId());
//            deliveryRuleDto.setDeliveryMode(s.getDeliveryMode());
//            deliveryRuleDto.setQuantity(s.getAmount());
//            deliveryRuleDto.setReceiverCity(s.getReceiverCity());
//            deliveryRuleDto.setReceiverProvince(s.getReceiverProvince());
//            BigDecimal deliveryFee = calculateDeliveryFeeService.queryDeliveryFee(deliveryRuleDto);
//            v.setAmount(s.getAmount());
//            v.setSkuId(s.getSkuId());
//            v.setDeliveryFee(decimalFormat.format(deliveryFee));
//            allTotalAmout = allTotalAmout.add(deliveryFee);
//            allDeliveryFee = allDeliveryFee.add(deliveryFee);
//            v.setTotalSemAmount("0");
//            skuAmountSemVos.add(v);
//        }
//        resultVo.setDeliveryFee(decimalFormat.format(allDeliveryFee));
//        List<String> businessIds = semCouponCalculateDto.getBusinessIds();
//
//
//        Map<String, SkuAndAmountDto> skusMap = skus.stream().collect(Collectors.toMap(SkuAndAmountDto::getSkuId, item -> item));
//        List<MallSkuPriceVo> mallSkuPriceVos = mallProductSkuService.queryProductSkuPrice(skus.stream().map(item -> item.getSkuId()).collect(Collectors.toList()));
//
//        for (MallSkuPriceVo vo : mallSkuPriceVos) {
//            BigDecimal price = vo.getPrice();
//            Integer total = skusMap.get(vo.getId()).getAmount();
//            allTotalAmout = allTotalAmout.add(price.multiply(new BigDecimal(total)));
//            orderAmount = orderAmount.add(price.multiply(new BigDecimal(total)));
//        }
//        BigDecimal semMoney = new BigDecimal("0");
//
//
//        List<SemCouponItemDetailVo> dbAllCoupons = semCouponService.queryBindValidCouponItemsDetailByBusinessIds(businessIds);
//        List<SemCouponItemDetailVo> allCoupons = new ArrayList<>();
//        for (SemCouponItemDetailVo vo : dbAllCoupons) {
//            Boolean isExchange = vo.getType().equals(SemConstant.SEM_COUPON_SALE_TYPE_EXCHANGE.getCode());
//            if (!isExchange) {
//                Boolean isRestrict = !CommonUtils.isNull(vo.getRrestrict()) && vo.getRrestrict().equals(SemConstant.SEM_COUPON_RESTRICT.getCode());
//                if (isRestrict) {
//                    if (!CommonUtils.isNull(vo.getRestrictValue()) && allTotalAmout.compareTo(vo.getRestrictValue()) >= 0) {
//                        allCoupons.add(vo);
//                    }
//                } else {
//                    allCoupons.add(vo);
//                }
//            } else {
//                continue;
//            }
//
//
//        }
//
//
//        List<SemCouponItemDetailVo> pickedSemCoupons = new ArrayList<>();
//        //根据选择的券的实例，获取券对象（可重复的semCoupon 对象,数据库表中，关联查询;主为sem_coupon_item left join sem_coupon）
//        if (!CommonUtils.isNull(pickedSemItemIds)) {
//            pickedSemCoupons = semCouponService.queryBindAndValidBySemItemsIds(pickedSemItemIds);
//        }
//        Map<String, List<SemCouponItemDetailVo>> result = new HashMap<>();
//        if (needCheckRule) {
//            result = semCouponRuleService.getCouponsInRule(pickedSemCoupons, allCoupons);
//            /***
//             *  1、可选的券，不可选的券
//             *   enable
//             *   disable
//             */
//
//            resultVo.setEnable(result.get("enable"));
//            resultVo.setDisable(result.get("disable"));
//        }
//
//
//        /***
//         *  2、根据已选券（pickedSemCoupons），计算价格
//         *   每张券，每个商品
//         */
//
//
//        if (CommonUtils.isNull(pickedSemCoupons)) {
//            resultVo.setAfterSemAmount(decimalFormat.format(allTotalAmout));
//        } else {
//            Map<String, SemItemSkuVo> itemSkuVoMap = new HashMap<>();
//            for (SemCouponItemDetailVo semItemVo : pickedSemCoupons) {
//                String category = semItemVo.getCategory();
//                String type = semItemVo.getType();
//                String couponId = semItemVo.getSemCouponId();
//                String semCouponId = semItemVo.getSemCouponItemId();
//
//                List<SemCouponBusinessRelation> relations = semCouponService.queyRelationsByCouponId(couponId);
//                //品类券
//                if (category.equals(SemConstant.SEM_COUPON_CATEGORY_KIND.getCode())) {
//                    for (SemCouponBusinessRelation relation : relations) {
//                        for (MallSkuPriceVo pvo : mallSkuPriceVos) {
//                            SkusPriceVo mpvo = new SkusPriceVo();
//                            BeanUtil.copyProperties(pvo, mpvo);
//                            if (mpvo.getProductCategoryId().equals(relation.getBusinessId())) {
//                                //折扣，抵扣
//                                //品类券
//                                //抵扣（满减）
//                                if (type.equals(SemConstant.SEM_COUPON_SALE_TYPE_DEDUCT.getCode())) {
//                                    //item1+suk{itemId,skuId,0(次数),券抵扣,折扣，券的面值，券的约束}
//                                    SemItemSkuVo vo = itemSkuVoMap.get(semCouponId);
//                                    if (CommonUtils.isNull(vo)) {
//                                        vo = new SemItemSkuVo();
//                                    }
//                                    vo.setSemCouponItemId(semCouponId);
//                                    vo.setSemItem(semItemVo);
//                                    List<SkusPriceVo> mpvos = vo.getSkus();
//                                    if (CommonUtils.isNull(mpvos)) {
//                                        mpvos = new ArrayList<>();
//                                    }
//                                    Integer count = skusMap.get(mpvo.getId()).getAmount();
//                                    mpvo.setAmount(count);
//                                    mpvos.add(mpvo);
//                                    vo.setSkus(mpvos);
//                                    itemSkuVoMap.put(semCouponId, vo);
//                                    continue;
//                                }
//                                //折扣
//                                if (type.equals(SemConstant.SEM_COUPON_SALE_TYPE_DISCOUNT.getCode())) {
//                                    SemItemSkuVo vo = itemSkuVoMap.get(semCouponId);
//                                    if (CommonUtils.isNull(vo)) {
//                                        vo = new SemItemSkuVo();
//                                    }
//                                    vo.setSemCouponItemId(semCouponId);
//                                    vo.setSemItem(semItemVo);
//                                    List<SkusPriceVo> mpvos = vo.getSkus();
//                                    if (CommonUtils.isNull(mpvos)) {
//                                        mpvos = new ArrayList<>();
//                                    }
//                                    Integer count = skusMap.get(mpvo.getId()).getAmount();
//                                    mpvo.setAmount(count);
//                                    mpvos.add(mpvo);
//                                    vo.setSkus(mpvos);
//                                    itemSkuVoMap.put(semCouponId, vo);
//                                    continue;
//                                }
//                            }
//                        }
//                    }
//                }
//                //通用券
//                if (category.equals(SemConstant.SEM_COUPON_CATEGORY_COMMON.getCode())) {
//
//                    for (MallSkuPriceVo pvo : mallSkuPriceVos) {
//                        SkusPriceVo mpvo = new SkusPriceVo();
//                        BeanUtil.copyProperties(pvo, mpvo);
//                        //折扣，抵扣
//                        //抵扣（满减）
//                        if (type.equals(SemConstant.SEM_COUPON_SALE_TYPE_DEDUCT.getCode())) {
//                            //item1+suk{itemId,skuId,0(次数),券抵扣,折扣，券的面值，券的约束}
//                            SemItemSkuVo vo = itemSkuVoMap.get(semCouponId);
//                            if (CommonUtils.isNull(vo)) {
//                                vo = new SemItemSkuVo();
//                            }
//                            vo.setSemCouponItemId(semCouponId);
//                            vo.setSemItem(semItemVo);
//                            List<SkusPriceVo> mpvos = vo.getSkus();
//                            if (CommonUtils.isNull(mpvos)) {
//                                mpvos = new ArrayList<>();
//                            }
//                            Integer count = skusMap.get(mpvo.getId()).getAmount();
//                            mpvo.setAmount(count);
//                            mpvos.add(mpvo);
//                            vo.setSkus(mpvos);
//                            itemSkuVoMap.put(semCouponId, vo);
//                            continue;
//                        }
//                        //折扣
//                        if (type.equals(SemConstant.SEM_COUPON_SALE_TYPE_DISCOUNT.getCode())) {
//                            SemItemSkuVo vo = itemSkuVoMap.get(semCouponId);
//                            if (CommonUtils.isNull(vo)) {
//                                vo = new SemItemSkuVo();
//                            }
//                            vo.setSemCouponItemId(semCouponId);
//                            vo.setSemItem(semItemVo);
//                            List<SkusPriceVo> mpvos = vo.getSkus();
//                            if (CommonUtils.isNull(mpvos)) {
//                                mpvos = new ArrayList<>();
//                            }
//                            Integer count = skusMap.get(mpvo.getId()).getAmount();
//                            mpvo.setAmount(count);
//                            mpvos.add(mpvo);
//                            vo.setSkus(mpvos);
//                            itemSkuVoMap.put(semCouponId, vo);
//                            continue;
//                        }
//                    }
//                }
//                //商品券
//                if (category.equals(SemConstant.SEM_COUPON_CATEGORY_PRODUCT.getCode())) {
//                    for (SemCouponBusinessRelation relation : relations) {
//                        for (MallSkuPriceVo pvo : mallSkuPriceVos) {
//                            SkusPriceVo mpvo = new SkusPriceVo();
//                            BeanUtil.copyProperties(pvo, mpvo);
//                            if (mpvo.getProductId().equals(relation.getBusinessId())) {
//                                //折扣，抵扣
//                                //抵扣（满减）
//                                if (type.equals(SemConstant.SEM_COUPON_SALE_TYPE_DEDUCT.getCode())) {
//                                    //item1+suk{itemId,skuId,0(次数),券抵扣,折扣，券的面值，券的约束}
//                                    SemItemSkuVo vo = itemSkuVoMap.get(semCouponId);
//                                    if (CommonUtils.isNull(vo)) {
//                                        vo = new SemItemSkuVo();
//                                    }
//                                    vo.setSemCouponItemId(semCouponId);
//                                    vo.setSemItem(semItemVo);
//                                    List<SkusPriceVo> mpvos = vo.getSkus();
//                                    if (CommonUtils.isNull(mpvos)) {
//                                        mpvos = new ArrayList<>();
//                                    }
//                                    Integer count = skusMap.get(mpvo.getId()).getAmount();
//                                    mpvo.setAmount(count);
//                                    mpvos.add(mpvo);
//                                    vo.setSkus(mpvos);
//                                    itemSkuVoMap.put(semCouponId, vo);
//                                    continue;
//                                }
//                                //折扣
//                                if (type.equals(SemConstant.SEM_COUPON_SALE_TYPE_DISCOUNT.getCode())) {
//                                    SemItemSkuVo vo = itemSkuVoMap.get(semCouponId);
//                                    if (CommonUtils.isNull(vo)) {
//                                        vo = new SemItemSkuVo();
//                                    }
//                                    vo.setSemCouponItemId(semCouponId);
//                                    vo.setSemItem(semItemVo);
//                                    List<SkusPriceVo> mpvos = vo.getSkus();
//                                    if (CommonUtils.isNull(mpvos)) {
//                                        mpvos = new ArrayList<>();
//                                    }
//                                    Integer count = skusMap.get(mpvo.getId()).getAmount();
//                                    mpvo.setAmount(count);
//                                    mpvos.add(mpvo);
//                                    vo.setSkus(mpvos);
//                                    itemSkuVoMap.put(semCouponId, vo);
//                                    continue;
//                                }
//                            }
//                        }
//                    }
//                }
//                //物品券
//                if (category.equals(SemConstant.SEM_COUPON_CATEGORY_INSTANCE.getCode())) {
//                    for (SemCouponBusinessRelation relation : relations) {
//                        for (MallSkuPriceVo pvo : mallSkuPriceVos) {
//                            SkusPriceVo mpvo = new SkusPriceVo();
//                            BeanUtil.copyProperties(pvo, mpvo);
//                            String wholeBusinessId = relation.getBusinessId();
//                            String businessId = wholeBusinessId.substring(wholeBusinessId.indexOf("/") + 1);
//                            if (mpvo.getId().equals(businessId)) {
//                                //折扣，抵扣
//                                //抵扣（满减）
//                                if (type.equals(SemConstant.SEM_COUPON_SALE_TYPE_DEDUCT.getCode())) {
//                                    //item1+suk{itemId,skuId,0(次数),券抵扣,折扣，券的面值，券的约束}
//                                    SemItemSkuVo vo = itemSkuVoMap.get(semCouponId);
//                                    if (CommonUtils.isNull(vo)) {
//                                        vo = new SemItemSkuVo();
//                                    }
//                                    vo.setSemCouponItemId(semCouponId);
//                                    vo.setSemItem(semItemVo);
//                                    List<SkusPriceVo> mpvos = vo.getSkus();
//                                    if (CommonUtils.isNull(mpvos)) {
//                                        mpvos = new ArrayList<>();
//                                    }
//                                    Integer count = skusMap.get(mpvo.getId()).getAmount();
//                                    mpvo.setAmount(count);
//                                    mpvos.add(mpvo);
//                                    vo.setSkus(mpvos);
//                                    itemSkuVoMap.put(semCouponId, vo);
//                                    continue;
//                                }
//                                //折扣
//                                if (type.equals(SemConstant.SEM_COUPON_SALE_TYPE_DISCOUNT.getCode())) {
//                                    SemItemSkuVo vo = itemSkuVoMap.get(semCouponId);
//                                    if (CommonUtils.isNull(vo)) {
//                                        vo = new SemItemSkuVo();
//                                    }
//                                    vo.setSemCouponItemId(semCouponId);
//                                    vo.setSemItem(semItemVo);
//                                    List<SkusPriceVo> mpvos = vo.getSkus();
//                                    if (CommonUtils.isNull(mpvos)) {
//                                        mpvos = new ArrayList<>();
//                                    }
//                                    Integer count = skusMap.get(mpvo.getId()).getAmount();
//                                    mpvo.setAmount(count);
//                                    mpvos.add(mpvo);
//                                    vo.setSkus(mpvos);
//                                    itemSkuVoMap.put(semCouponId, vo);
//                                    continue;
//                                }
//                            }
//
//                        }
//                    }
//                }
//
//            }
//            //优惠价格
//            for (SemCouponItemDetailVo semItemVo : pickedSemCoupons) {
//                SemItemSkuVo vo = itemSkuVoMap.get(semItemVo.getSemCouponItemId());
//                if (CommonUtils.isNull(vo)) {
//                    //如果，没有这种优惠券（换购券，但是选择了换购券）
//                    continue;
//                }
//                SemCouponItemDetailVo semItem = vo.getSemItem();
//                List<SkusPriceVo> skusInSem = vo.getSkus();
//                boolean isMj = semItem.getType().equals(SemConstant.SEM_COUPON_SALE_TYPE_DEDUCT.getCode());
//                boolean isMz = semItem.getType().equals(SemConstant.SEM_COUPON_SALE_TYPE_DISCOUNT.getCode());
//                //满减券
//                if (isMj) {
//                    BigDecimal totalAmout = new BigDecimal("0");
//                    for (SkusPriceVo spvo : skusInSem) {
//                        //同一个sku的个数
//                        Integer amout = spvo.getAmount();
//                        BigDecimal perPrice = spvo.getPrice();
//                        BigDecimal totalPerSku = perPrice.multiply(new BigDecimal(amout));
//                        totalAmout = totalAmout.add(totalPerSku);
//                    }
//                    //面值
//                    BigDecimal couponValue = semItemVo.getCouponValue();
//                    BigDecimal bit = new BigDecimal("0");
//                    bit.setScale(10, BigDecimal.ROUND_HALF_UP);
//
//                    bit = couponValue.divide(totalAmout, 10, BigDecimal.ROUND_HALF_UP);
//                    for (SkusPriceVo spvo : skusInSem) {
//                        //同一个sku的个数
//                        Integer amout = spvo.getAmount();
//                        BigDecimal perPrice = spvo.getPrice();
//                        BigDecimal totalPerSku = perPrice.multiply(new BigDecimal(amout));
//                        //记录该sku在这个券上的优惠值
//                        BigDecimal tempTotal = totalPerSku.multiply(bit);
//                        SkuAmountSemVo itemSkuPriceVo = skuAmountSemVos.stream().filter(item -> item.getSkuId().equals(spvo.getId())).collect(Collectors.toList()).get(0);
//                        String tempTotalSemAmount = itemSkuPriceVo.getTotalSemAmount();
//                        itemSkuPriceVo.setTotalSemAmount(decimalFormat.format(new BigDecimal(tempTotalSemAmount).add(tempTotal)));
//                    }
//                    semMoney = semMoney.add(couponValue);
//                    allTotalAmout = allTotalAmout.subtract(couponValue);
//
//                }
//                if (isMz) {
//                    BigDecimal totalAmout = new BigDecimal("0");
//                    for (SkusPriceVo spvo : skusInSem) {
//                        //同一个sku的个数
//                        Integer amout = spvo.getAmount();
//                        BigDecimal perPrice = spvo.getPrice();
//                        BigDecimal totalPerSku = perPrice.multiply(new BigDecimal(amout));
//                        totalAmout = totalAmout.add(totalPerSku);
//                    }
//                    //面值
//                    Float disRate = (1.0F - semItemVo.getDiscountRate() / 10);
//
//                    //折扣后的值
//                    BigDecimal bit = totalAmout.multiply(BigDecimal.valueOf(disRate));
//                    bit = bit.setScale(2, BigDecimal.ROUND_HALF_DOWN);
//                    for (SkusPriceVo spvo : skusInSem) {
//                        //同一个sku的个数
//                        Integer amout = spvo.getAmount();
//                        BigDecimal perPrice = spvo.getPrice();
//                        BigDecimal totalPerSku = perPrice.multiply(new BigDecimal(amout));
//                        //记录该sku在这个券上的优惠值
//                        totalPerSku = totalPerSku.multiply(new BigDecimal(disRate));
//                        List<SkuAmountSemVo> collect = skuAmountSemVos.stream().filter(item -> item.getSkuId().equals(spvo.getId())).collect(Collectors.toList());
//                        if (!CommonUtils.isNull(collect)) {
//                            SkuAmountSemVo itemSkuPriceVo = collect.get(0);
//
//                            String tempTotalSemAmount = itemSkuPriceVo.getTotalSemAmount();
//                            itemSkuPriceVo.setTotalSemAmount(decimalFormat.format(new BigDecimal(tempTotalSemAmount).add(totalPerSku)));
//                        }
//                    }
//                    allTotalAmout = allTotalAmout.subtract(bit);
//                    semMoney = semMoney.add(bit);
//                }
//            }
//
//        }
//        if (allTotalAmout.compareTo(BigDecimal.ZERO) < 0) {
//            throw new BizException("此券不可用");
//        }
//        BigDecimal giftCardAmount = new BigDecimal("0");
//        if (isCalculateGiftCard) {
//            giftCardAmount = this.calculate4GiftCard(allTotalAmout);
//        }
//        BigDecimal afterSemAmount = allTotalAmout.subtract(giftCardAmount);
//        resultVo.setAfterSemAmount(decimalFormat.format(afterSemAmount));
//        resultVo.setSemAmount(decimalFormat.format(semMoney));
//        resultVo.setSkuAmountSemVos(skuAmountSemVos);
//        resultVo.setGiftCardAmount(decimalFormat.format(giftCardAmount));
//        List<SemCouponItemDetailVo> enable = this.semCouponBusinessService.queryBusinessName(SemConstant.BUSINESS_MODULE_MALL.getCode(), resultVo.getEnable());
//        List<SemCouponItemDetailVo> disable = this.semCouponBusinessService.queryBusinessName(SemConstant.BUSINESS_MODULE_MALL.getCode(), resultVo.getDisable());
//        resultVo.setEnable(enable);
//        resultVo.setDisable(disable);
//        resultVo.setOrderAmount(decimalFormat.format(orderAmount));
//        return resultVo;
//    }
//
//    @Override
//    public PriceDetailVo
//    calculate(SemCouponCalculateDto semCouponCalculateDto) {
//        String type = semCouponCalculateDto.getSemType();
//        if (CommonUtils.isNull(type)) {
//            //原价计算
//            return this.listAndCalculate4No(semCouponCalculateDto);
//        } else if (type.equals("1")) {
//            //默认不需要再次校验规则
//            return this.listAndCalculate4SemCoupon(semCouponCalculateDto, true);
//        } else if (type.equals("3")) {
//            return this.listAndCalculate4Change(semCouponCalculateDto);
//        }
//        throw new BizException("未知的优惠方式");
//    }
//}