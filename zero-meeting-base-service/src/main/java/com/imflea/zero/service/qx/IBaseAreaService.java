package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.BaseArea;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-24
 */
public interface IBaseAreaService extends IService<BaseArea> {

    List<BaseArea> queryByParendId(String parendId);
}
