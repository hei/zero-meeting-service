package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxUserCertification;
import com.imflea.zero.model.entity.qx.dto.QxUserCertificationDto;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * 用户认证密码表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
public interface IQxUserCertificationService extends IService<QxUserCertification> {

    boolean saveQxUserCertification(String salt, String defPassword, String userId, String tenantId);

    boolean updateQxUserCertification(String password, String userId);

    boolean query4CheckPassword(String userId, String password);

    PageInfo<QxUserCertification> queryPage(QxUserCertificationDto dto);

}
