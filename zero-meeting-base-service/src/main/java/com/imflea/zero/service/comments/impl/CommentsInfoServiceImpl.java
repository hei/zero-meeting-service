package com.imflea.zero.service.comments.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.dao.comments.CommentsInfoMapper;
import com.imflea.zero.model.entity.comments.CommentsInfo;
import com.imflea.zero.model.entity.comments.dto.CommentsInfoDto;
import com.imflea.zero.model.entity.comments.dto.SaveOrUpdateCommentsInfoDto;
import com.imflea.zero.model.entity.comments.vo.QueryListVo;
import com.imflea.zero.service.comments.ICommentsInfoService;
import com.imflea.zero.service.comments.SensitiveWordBiz;
import com.imflea.zero.service.comments.SingleAdoptSwitch;
import com.imflea.zero.service.comments.SingleAreatSwitch;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 互动评论表	 服务实现类
 * </p>
 *
 * @author guohui
 * @since 2021-10-08
 */
@Service
public class CommentsInfoServiceImpl extends ServiceImpl<CommentsInfoMapper, CommentsInfo> implements ICommentsInfoService {

    @Override
    public CommentsInfo saveOrUpdateCommentsInfo(SaveOrUpdateCommentsInfoDto pam) {
        if (CommonUtils.isNull(pam.getId())) {
            pam.setId(CommonUtils.generateRandomString());
        }
        CommentsInfo commentsInfo = new CommentsInfo();
        BeanUtils.copyProperties(pam, commentsInfo);
        commentsInfo.setTenantId(SessionUtils.getTenantId());
        commentsInfo.setUserId(SessionUtils.getUserId());
        commentsInfo.setNickName(SessionUtils.getWxNickName());
        commentsInfo.setHeadImage(SessionUtils.getAvatarUrl());
        commentsInfo.setComments(SensitiveWordBiz.filte(commentsInfo.getComments()));
        if ("0".equals(pam.getCommentBusinessType())) {
            if (SingleAreatSwitch.getInstance().getWordSwitch()) {
                commentsInfo.setStatus("1"); //审核未通过
            } else {
                commentsInfo.setStatus("0"); //审核通过
            }
        } else {
            if (SingleAdoptSwitch.getInstance().getWordSwitch()) {
                commentsInfo.setStatus("1"); //审核未通过
            } else {
                commentsInfo.setStatus("0"); //审核通过
            }
        }
        this.saveOrUpdate(commentsInfo);
        return commentsInfo;
    }

    @Override
    public PageInfo<CommentsInfo> queryPage(CommentsInfoDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        QueryWrapper<CommentsInfo> queryWrapper = this.conditionWrapper(dto);
        queryWrapper.orderByAsc("selected");
        queryWrapper.orderByDesc("create_time");
        PageHelper.startPage(pageIndex, pageSize);
        List<CommentsInfo> list = this.list(queryWrapper);
        PageInfo<CommentsInfo> pageResult = new PageInfo<>(list);
        return pageResult;
    }

    /**
     * 条件查询复用
     *
     * @param dto
     * @return queryWrapper
     */
    private QueryWrapper<CommentsInfo> conditionWrapper(CommentsInfoDto dto) {
        QueryWrapper<CommentsInfo> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(SessionUtils.getTenantId())) {
            queryWrapper.eq("tenant_id", SessionUtils.getTenantId());
        }
        if (!CommonUtils.isNull(dto.getBusinessId())) {
            queryWrapper.eq("business_id", dto.getBusinessId());
        }
        if (!CommonUtils.isNull(dto.getCommentBusinessType())) {
            queryWrapper.eq("comment_business_type", dto.getCommentBusinessType());
        }
        if (!CommonUtils.isNull(dto.getComments())) {
            queryWrapper.like("comments", dto.getComments());
        }
        if (!CommonUtils.isNull(dto.getCreateUserName())) {
            queryWrapper.like("create_user_name", dto.getCreateUserName());
        }
        if (!CommonUtils.isNull(dto.getSelected())) {
            queryWrapper.like("selected", dto.getSelected());
        }
        if (!CommonUtils.isNull(dto.getStatus())) {
            queryWrapper.like("status", dto.getStatus());
        }
        return queryWrapper;
    }

    @Override
    public Boolean updateStatusById(String id, String status) {
        UpdateWrapper<CommentsInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", status);
        updateWrapper.eq("id", id);
        return this.update(updateWrapper);
    }

    @Override
    public Boolean removeByIdAndUserId(String id) {
        QueryWrapper<CommentsInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", id);
        queryWrapper.eq("user_id", SessionUtils.getUserId());
        queryWrapper.eq("tenant_id", SessionUtils.getTenantId());
        return this.remove(queryWrapper);
    }

    @Override
    public List<QueryListVo> queryList(String id) {
        QueryWrapper<CommentsInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id", id);
        queryWrapper.eq("tenant_id", SessionUtils.getTenantId());
        queryWrapper.eq("status", "0");
        queryWrapper.orderByAsc("selected");
        queryWrapper.orderByDesc("create_time");
        List<CommentsInfo> list = this.list(queryWrapper);
        List<QueryListVo> result = new ArrayList<QueryListVo>();
        list.stream().forEach((item) -> {
            QueryListVo queryListVo = new QueryListVo();
            BeanUtils.copyProperties(item, queryListVo);
            result.add(queryListVo);
        });
        return result;
    }

    @Override
    public Boolean updateSectedById(String id, String selected) {
        UpdateWrapper<CommentsInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("selected", selected);
        updateWrapper.eq("id", id);
        return this.update(updateWrapper);
    }


}
