package com.imflea.zero.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.user.WebUserFriend;
import com.imflea.zero.model.entity.user.dto.FriendQueryDto;
import com.imflea.zero.model.entity.user.dto.WebUserFriendDto;
import com.imflea.zero.model.entity.user.vo.UserFriendVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 好友关系表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
public interface IWebUserFriendService extends IService<WebUserFriend> {

    WebUserFriend saveOrUpdateWebUserFriend(WebUserFriend entity);

    PageInfo<WebUserFriend> queryPage(WebUserFriendDto dto);

    List<UserFriendVo> queryAll(FriendQueryDto friendQueryDto);

    WebUserFriend saveFriend(WebUserFriend webUserFriend);

    boolean removeFriendsById(String id);
}
