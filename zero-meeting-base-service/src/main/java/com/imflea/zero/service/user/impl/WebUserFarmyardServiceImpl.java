package com.imflea.zero.service.user.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.user.WebUserFarmyardMapper;
import com.imflea.zero.model.entity.user.WebUserFarmyard;
import com.imflea.zero.service.user.IWebUserFarmyardService;
import com.imflea.zero.utils.SessionUtils;
import org.springframework.stereotype.Service;

/**
* <p>
    * 用户农场表 服务实现类
    * </p>
*
* @author xiangbaoyu
* @since 2021-09-24
*/
@Service
public class WebUserFarmyardServiceImpl extends ServiceImpl<WebUserFarmyardMapper, WebUserFarmyard> implements IWebUserFarmyardService {

    @Override
    public WebUserFarmyard saveOrUpdateWebUserFarmyard(WebUserFarmyard entity){
        if(CommonUtils.isNull(entity.getId())){
            entity.setId(CommonUtils.generateRandomString());
        }
        entity.setTenantId(SessionUtils.getTenantId());
        Boolean result = this.saveOrUpdate(entity);
        if(!result){
            throw new BizException("创建我的农场失败");
        }
        return entity;
    }

}
