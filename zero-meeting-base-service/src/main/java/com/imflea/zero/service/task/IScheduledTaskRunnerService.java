package com.imflea.zero.service.task;


import com.imflea.zero.model.entity.task.ScheduleTask;

import java.util.List;


public interface IScheduledTaskRunnerService {

    /**
     * 根据任务key 启动任务
     */
    Boolean start(String taskKey, ScheduleTask scheduled);

    /**
     * 根据任务key 停止任务
     */
    Boolean stop(String taskKey);

    /**
     * 根据任务key 重启任务
     */
    Boolean restart(String taskKey, ScheduleTask scheduled);

    /**
     * 初始化  ==> 启动所有正常状态的任务
     */
    void initAllTask(List<ScheduleTask> scheduleds);
}