package com.imflea.zero.service.invoice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.invoice.InvoiceInfo;
import com.imflea.zero.model.entity.invoice.dto.InsertWxApplayDto;
import com.imflea.zero.model.entity.invoice.dto.InvoiceInfoDto;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 发票信息 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 */
public interface IInvoiceInfoService extends IService<InvoiceInfo> {

    InvoiceInfo saveOrUpdateInvoiceInfo(InvoiceInfo entity);


    InvoiceInfo insertWxApplay(InsertWxApplayDto insertWxApplayDto);

    List<InvoiceInfo> listByOrderIdAndUserIdAndTentantId(String id);

    Boolean insertBath(List<InsertWxApplayDto> list);

    PageInfo<InvoiceInfo> selectPage(InvoiceInfoDto dto);

    InvoiceInfo queryByOrderId(String id);
}
