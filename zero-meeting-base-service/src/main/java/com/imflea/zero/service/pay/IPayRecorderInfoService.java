package com.imflea.zero.service.pay;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.pay.PayRecorderInfo;
import com.imflea.zero.model.entity.pay.dto.PayRecorder4QueryDto;
import com.imflea.zero.model.entity.pay.dto.PayRecorderInfoDto;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 支付记录 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-27
 */
public interface IPayRecorderInfoService extends IService<PayRecorderInfo> {

    PayRecorderInfo saveOrUpdatePayRecoderInfo(PayRecorderInfo entity);

    PageInfo<PayRecorderInfo> queryPage(PayRecorderInfoDto dto);

    PayRecorderInfo queryByOrderSn(String sn);

    Boolean updateStatus(String id, String status);

    PayRecorderInfo getByOrderSn(String orderSn);

    PageInfo<PayRecorderInfo> queryPage(PayRecorder4QueryDto orderQeryDto);

    PayRecorderInfo updatePayRecorderStatus2Finish(String recoderId);

    boolean removeByOrderIds(List<String> orderIds);

    List<PayRecorderInfo> queryByOrderIds(List<String> orderIds);

    PayRecorderInfo queryByPaySn(String paySn);

    List<PayRecorderInfo> queryWaitHandleRecorder();
}
