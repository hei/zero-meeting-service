//package com.imflea.zero.service.pay.impl;
//
//import com.github.jaemon.dinger.DingerSender;
//import com.github.jaemon.dinger.core.entity.DingerRequest;
//import com.github.jaemon.dinger.core.entity.enums.DingerType;
//import com.github.jaemon.dinger.core.entity.enums.MessageSubType;
//import com.imflea.zero.util.base.CommonUtils;
//import com.imflea.zero.constant.AdoptConstant;
//import com.imflea.zero.constant.PayConstant;
//import com.imflea.zero.exception.BizException;
//import com.imflea.zero.model.entity.adopt.AdoptOrderInfo;
//import com.imflea.zero.model.entity.adopt.AdoptedInfo;
//import com.imflea.zero.model.entity.adopt.AdoptedReturnApply;
//import com.imflea.zero.model.entity.pay.dto.PayOrderDetailDto;
//import com.imflea.zero.model.entity.pay.dto.PayPreRecorderDto;
//import com.imflea.zero.model.entity.pay.dto.PaySuccessDto;
//import com.imflea.zero.model.entity.pay.vo.PayPreDataVo;
//import com.imflea.zero.model.entity.pay.vo.PayPreRefundVo;
//import com.imflea.zero.service.adopt.IAdoptOrderInfoService;
//import com.imflea.zero.service.adopt.IAdoptedInfoService;
//import com.imflea.zero.service.adopt.IAdoptedReturnApplyService;
//import com.imflea.zero.service.pay.IHandleCommonPrePayService;
//import com.imflea.zero.utils.OrderUtils;
//import com.imflea.zero.utils.SessionUtils;
//import com.imflea.zero.utils.TaskTimeOutUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.math.BigDecimal;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * @author 祥保玉
// * @PackageName: com.imflea.zero.service.pay.impl
// * @ClassName: HandleAdoptPayServiceimpl
// * @Description
// * @date 2021-10-14  19:12:31
// */
//@Service("adoptPrePayService")
//@Slf4j
//public class HandleAdoptPrePayServiceImpl implements IHandleCommonPrePayService {
//    @Autowired
//    private IAdoptOrderInfoService adoptOrderInfoService;
//    @Autowired
//    private IAdoptedReturnApplyService adoptedReturnApplyService;
//    @Autowired
//    private IAdoptedInfoService adoptedInfoService;
//    @Autowired
//    private DingerSender dingerSender;
//
//    //计算金额
//    @Override
//    public PayPreDataVo getPrePayData(PayOrderDetailDto payPreOrderDto) {
//        PayPreDataVo vo = new PayPreDataVo();
//        List<String> orderIds = payPreOrderDto.getOrderIds();
//
//        StringBuffer goodNamesBuf = new StringBuffer("");
//        //统一支付流水号
//        String paySn = OrderUtils.generateOrderSn();
//        List<PayPreRecorderDto> preRecorders = new ArrayList<>();
//        if (CommonUtils.isNull(orderIds)) {
//            throw new BizException("订单id为空");
//        }
//        List<AdoptOrderInfo> orderInfos = adoptOrderInfoService.listByIds(orderIds);
//        if (CommonUtils.isNull(orderInfos)) {
//            throw new BizException("未找到有效的订单");
//        }
//        if (orderIds.size() != orderIds.size()) {
//            throw new BizException("订单不匹配");
//        }
//        BigDecimal totalPay = new BigDecimal(0);
//        for (AdoptOrderInfo info : orderInfos) {
//            totalPay = totalPay.add(info.getPayAmount());
//            PayPreRecorderDto dto = new PayPreRecorderDto();
//            dto.setOrderId(info.getId());
//            dto.setTenantId(info.getTenantId());
//            dto.setOrderSn(info.getOrderSn());
//            dto.setOrderType(PayConstant.ORDER_TYPE_ADOPT.getCode());
//            dto.setTotalAmount(info.getPayAmount());
//            dto.setPayTime(LocalDateTime.now());
//            dto.setPayUserId(SessionUtils.getUserId());
//            dto.setPayUserName(SessionUtils.getWxNickName());
//            dto.setPayUserOpenId(SessionUtils.getOpenId());
//            goodNamesBuf.append(info.getAdoptName());
//            goodNamesBuf.append(",");
//            preRecorders.add(dto);
//        }
//        vo.setPayType(payPreOrderDto.getPayType());
//        String goodsName = goodNamesBuf.toString();
//        vo.setGoodsName(goodsName.substring(0, goodsName.length() - 1));
//        vo.setPaySn(paySn);
//        vo.setPayTotal(totalPay);
//        vo.setPreRecorders(preRecorders);
//        return vo;
//    }
//
//    /**
//     * @param orderIds
//     * @return boolean
//     * @author 祥保玉
//     * @description 发起微信支付，转账支付，此时并未支付成功
//     * @date 2021/10/15  17:20
//     */
//
//
//    @Override
//    public boolean handleCreatePrePayResult(List<String> orderIds, String payType) {
//
//        boolean result = adoptOrderInfoService.updatePayTypeByIds(orderIds, payType);
//        for (String id : orderIds) {
//            if (payType.equals(PayConstant.PAY_TYPE_WX.getCode())) {
//                TaskTimeOutUtil.enqueue(AdoptConstant.ADOPT_TIME_OUT_SQU.getCode(), id, Integer.valueOf(AdoptConstant.ADOPT_TIME_OUT_TIME_SECONDS.getCode()));
//            } else {
//                TaskTimeOutUtil.enqueue(AdoptConstant.ADOPT_TIME_OUT_SQU.getCode(), id, Integer.valueOf(AdoptConstant.ADOPT_TIME_OUT_TIME_SECONDS_TRANSFER.getCode()));
//            }
//        }
//
//        //是否更新
//
//        return true;
//    }
//
//
//    @Override
//    public boolean handlePaySuccess(List<PaySuccessDto> dtos) {
//        try {
//            List<AdoptOrderInfo> orderInfos = adoptOrderInfoService.listByIds(dtos.stream().map(item -> item.getOrderId()).collect(Collectors.toList()));
//            List<String> orders = orderInfos.stream().map(item -> item.getOrderSn()).collect(Collectors.toList());
//            int size = orders.size();
//            StringBuffer snos = new StringBuffer("");
//            for (int i = 0; i < size; i++) {
//                String sno = orders.get(i);
//                snos.append(sno);
//                if (i + 1 < size) {
//                    snos.append(",");
//                }
//            }
//            dingerSender.send(DingerType.WETALK, MessageSubType.TEXT, DingerRequest.request("您有" + size + "条新的认养订单，订单号为：" + snos.toString()));
//        } catch (Exception e) {
//            log.error("订单推送异常", e.getMessage());
//        }
//        boolean order2CompletedResult = adoptOrderInfoService.changeOrder2Complete(dtos, dtos.get(0).getPaySn(), dtos.get(0).getPayType());
//        return order2CompletedResult;
//    }
//
//    @Override
//    public PayPreRefundVo getPreRefundData(String refundOrderId, String orderType, String extend) {
//
//        if (CommonUtils.isNull(refundOrderId)) {
//            throw new BizException("退单id为空");
//        }
//        AdoptedReturnApply apply = adoptedReturnApplyService.getById(refundOrderId);
//        if (CommonUtils.isNull(apply)) {
//            throw new BizException("未找到待退款的申请");
//        }
//        String orderId = apply.getOrderId();
//        if (CommonUtils.isNull(orderId)) {
//            throw new BizException("订单id为空");
//        }
//        String status = apply.getStatus();
//        if (!status.equals(AdoptConstant.ADOPTED_RETURN_STATUS_RETURN_PAY.getCode())) {
//            throw new BizException("尚未同意同款，请联系管理员");
//        }
//        String adoptedId = apply.getAdoptedId();
//
//        AdoptedInfo adoptedInfo = this.adoptedInfoService.getById(adoptedId);
//        if (CommonUtils.isNull(adoptedInfo)) {
//            throw new BizException("已认养物不存在");
//        }
//        Boolean harvaested = adoptedInfo.getAdoptedStatus().equals(AdoptConstant.ADOPTED_BASE_STATUS_HAVE_HARVEST.getCode());
//        if (harvaested) {
//            throw new BizException("已收获不可退款");
//        }
//        BigDecimal refundAmount = apply.getReturnAmount();
//        if (CommonUtils.isNull(refundAmount)) {
//            throw new BizException("退款金额为空，请联系管理员");
//        }
//        //单只单价
//        BigDecimal regionPrice = apply.getRealPrice();
//        if (CommonUtils.isNull(regionPrice)) {
//            throw new BizException("原单价为空");
//        }
//
//        if (refundAmount.compareTo(regionPrice) > 0) {
//            throw new BizException("退款金额大于原支付金额，请联系管理员");
//        }
//        AdoptOrderInfo orderInfo = adoptOrderInfoService.getById(orderId);
//        if (CommonUtils.isNull(orderInfo)) {
//            throw new BizException("原订单不存在");
//        }
//        String paySn = orderInfo.getPaySn();
//        if (CommonUtils.isNull(paySn)) {
//            throw new BizException("未获取到支付单号");
//        }
//        String payType = orderInfo.getPayType();
//
//        if (CommonUtils.isNull(payType)) {
//            throw new BizException("未获取到支付方式");
//        }
//        PayPreRefundVo vo = new PayPreRefundVo();
//        BigDecimal totalFee = orderInfo.getPayAmount();
//        vo.setPayType(payType);
//        vo.setPaySn(paySn);
//        vo.setRefundAmount(refundAmount);
//        vo.setTotalFee(totalFee);
//        return vo;
//    }
//
//    @Override
//    public boolean handleRefundSuccess(String refundOrderId, String orderType, String extend) {
//        boolean result = this.adoptedReturnApplyService.update2RefundSuccess(refundOrderId);
//        return result;
//    }
//}
