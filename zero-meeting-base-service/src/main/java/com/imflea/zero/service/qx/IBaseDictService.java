package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.BaseDict;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-25
 */
public interface IBaseDictService extends IService<BaseDict> {

    List<BaseDict> queryByPcode(String pcode) ;

    BaseDict saveOrUpdateBaseDict(BaseDict baseDict);

    Boolean queryDictByCode4Validate(String code) ;

    Boolean removeBaseDictById(String id);

    List<BaseDict> queryRootDict(String name);
}
