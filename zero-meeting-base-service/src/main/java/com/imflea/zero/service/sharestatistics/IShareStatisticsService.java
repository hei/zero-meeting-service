package com.imflea.zero.service.sharestatistics;

import com.imflea.zero.model.entity.sharestatistics.ShareStatistics;
import com.imflea.zero.model.entity.sharestatistics.dto.ShareStatisticsDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * 分享的统计分析 服务类
 * </p>
 *
 * @author guohui
 * @since 2021-10-22
 */
public interface IShareStatisticsService extends IService<ShareStatistics> {

    ShareStatistics saveOrUpdateShareStatistics(ShareStatistics entity);

    PageInfo<ShareStatistics> queryPage(ShareStatisticsDto dto);

}
