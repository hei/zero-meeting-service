package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxCacheInfo;
import com.imflea.zero.model.entity.qx.dto.QxCacheInfoDto;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 缓存基本信息 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-17
 */
public interface IQxCacheInfoService extends IService<QxCacheInfo> {

    QxCacheInfo saveOrUpdateQxCacheInfo(QxCacheInfo entity);

    PageInfo<QxCacheInfo> queryPage(QxCacheInfoDto dto);

    List<Map<String, Object>> queryTableDataByQuerySql(String dataQuerySql);

    boolean refreshCacheByTableName(List<String> ids);

    List<QxCacheInfo> queryByTablesNames(List<String> tableNames);

    QxCacheInfo queryByTablesName(String tableNames);

    boolean queryAndRefreshCache(List<QxCacheInfo> redisGrid);

    boolean refreshCacheByTableName(String tableName);

    List<QxCacheInfo> queryAll(String tableName);
}
