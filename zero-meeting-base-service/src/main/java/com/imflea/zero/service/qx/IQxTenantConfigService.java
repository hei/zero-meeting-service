package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxTenantConfig;
import com.imflea.zero.model.entity.qx.dto.QxTenantConfigDto;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * 租户配置表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-23
 */
public interface IQxTenantConfigService extends IService<QxTenantConfig> {

    QxTenantConfig saveOrUpdateQxTenantConfig(QxTenantConfig entity);

    PageInfo<QxTenantConfig> queryPage(QxTenantConfigDto dto);

    QxTenantConfig queryByTenantId(String tenantId);


    QxTenantConfig queryTransferInfoBankByTenantId(String tenantId);

    QxTenantConfig queryByMchId(String mchId);

    String queryOrderTemplateId(String tenantId);

    String queryDefaultComment(String tenantId);
}
