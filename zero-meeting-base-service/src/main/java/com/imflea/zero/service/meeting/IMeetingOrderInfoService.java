package com.imflea.zero.service.meeting;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.entity.meeting.MeetingOrderInfo;
import com.imflea.zero.model.entity.meeting.dto.*;
import com.imflea.zero.model.entity.meeting.vo.DateTime4ItemStatusVO;
import com.imflea.zero.model.entity.pay.dto.PaySuccessDto;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 订单主表 服务类
 * </p>
 *
 * @author xiangbaoyu
 */
public interface IMeetingOrderInfoService extends IService<MeetingOrderInfo> {
    MeetingOrderInfo saveOrUpdateMeetingRoomInfo(MeetingOrderAppSaveDto meetingOrderAppSaveDto);
    MeetingOrderInfo saveOrUpdateRechargeInfo(RechargeOrderAppSaveDto meetingOrderAppSaveDto);

    MeetingOrderInfo saveOrUpdateMeetingOrderInfo(MeetingOrderInfo entity);

    PageInfo<MeetingOrderInfo> queryPage(MeetingOrderInfoDto dto);

    PageInfo<MeetingOrderInfo> queryPage4App(MeetingOrderInfoDto dto);

    MeetingOrderInfo queryOrderByOrderSn(String orderSn);

    Boolean changeOrder2Confirming(String orderId);

    Boolean changeOrder2Confirmed(String orderId);

    Boolean changeOrder2Complete(String orderId);

    Boolean changeOrder2Complete(List<String> orderId);

    Boolean changeOrder2Cancel(String orderId);

    Boolean changeOrder2Invalid(String orderId);



    Boolean changeOrder2Complete(List<PaySuccessDto> dtos, String paySn, String payType);

    Map<String, Object> query4Stat(String orderType);

    Boolean updateCancelUnpayOrder(String orderId);

    Boolean updatePayTypeByIds(List<String> orderIds, String payType);


    Boolean updateSemInfo(MeetingOrderInfo orderInfo);

    boolean updatePay2RoomOrder(String orderId);

    List<DateTime4ItemStatusVO> getCanOrderTimeByDate(MeetingRoomDayTimeDto dayTimeDto);

    Boolean updateWebUserAccountInfo(MeetingOrderInfo orderInfo);

    Boolean check(MeetingRoomTimeCheckDto dayTimeDto);
}
