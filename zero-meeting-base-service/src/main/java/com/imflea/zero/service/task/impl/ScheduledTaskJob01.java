package com.imflea.zero.service.task.impl;


import com.imflea.zero.service.task.IScheduledTaskJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class ScheduledTaskJob01 implements IScheduledTaskJobService {

    @Override
    public void run() {
        // TODO 要处理的业务逻辑
        log.info("ScheduledTask => 01  run  当前线程名称 {} ", Thread.currentThread().getName());
    }
}