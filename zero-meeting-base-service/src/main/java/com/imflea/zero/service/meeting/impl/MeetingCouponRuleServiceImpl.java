package com.imflea.zero.service.meeting.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.dao.meeting.MeetingCouponRuleMapper;
import com.imflea.zero.model.entity.meeting.MeetingCouponRule;
import com.imflea.zero.model.entity.meeting.dto.MeetingCouponRuleDto;
import com.imflea.zero.service.meeting.IMeetingCouponRuleService;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.utils.SessionUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 规则表 服务实现类
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 */
@Service
public class MeetingCouponRuleServiceImpl extends ServiceImpl<MeetingCouponRuleMapper, MeetingCouponRule> implements IMeetingCouponRuleService {

    @Override
    public MeetingCouponRule saveOrUpdateMeetingCouponRule(MeetingCouponRule entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
        }
        entity.setTenantId(SessionUtils.getTenantId());
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<MeetingCouponRule> queryPage(MeetingCouponRuleDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<MeetingCouponRule> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tenant_id", tenantId);
        queryWrapper.orderByDesc("condition_base");
        PageHelper.startPage(pageIndex, pageSize);
        List<MeetingCouponRule> adoptInfos = this.list(queryWrapper);
        PageInfo<MeetingCouponRule> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

    @Override
    public List<MeetingCouponRule> listAllRules() {

        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<MeetingCouponRule> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tenant_id", tenantId);
//        queryWrapper.eq("status", "0");
        queryWrapper.orderByDesc("condition_base");
        List<MeetingCouponRule> adoptInfos = this.list(queryWrapper);
        return adoptInfos;
    }

    @Override
    public BigDecimal getSemAmount(BigDecimal prePayAmount) {
        List<MeetingCouponRule> meetingCouponRules = this.listAllRules();
        MeetingCouponRule condtionRule = null;
        if (CommonUtils.isNull(meetingCouponRules)) {
            return BigDecimal.ZERO;
        } else {

            for (MeetingCouponRule rule : meetingCouponRules) {
                if (prePayAmount.compareTo(new BigDecimal(rule.getConditionBase())) >= 0) {
                    condtionRule = rule;
                    break;
                }
            }

        }

        if (!CommonUtils.isNull(condtionRule)) {
            return new BigDecimal(condtionRule.getConditionResult());
        }
        return BigDecimal.ZERO;
    }

}
