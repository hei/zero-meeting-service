package com.imflea.zero.service.meeting.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.dao.meeting.MeetingAlbumMapper;
import com.imflea.zero.model.entity.meeting.MeetingAlbum;
import com.imflea.zero.model.entity.meeting.dto.MeetingAlbumDto;
import com.imflea.zero.service.meeting.IMeetingAlbumService;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.utils.SessionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 画廊 服务实现类
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 */
@Service
public class MeetingAlbumServiceImpl extends ServiceImpl<MeetingAlbumMapper, MeetingAlbum> implements IMeetingAlbumService {

    @Override
    public MeetingAlbum saveOrUpdateMeetingAlbum(MeetingAlbum entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
        }
        entity.setTenantId(SessionUtils.getTenantId());
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<MeetingAlbum> queryPage(MeetingAlbumDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        String bizId = dto.getBizId();
        QueryWrapper<MeetingAlbum> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        queryWrapper.eq("biz_id", bizId);
        queryWrapper.orderByDesc("create_time");
        PageHelper.startPage(pageIndex, pageSize);
        List<MeetingAlbum> adoptInfos = this.list(queryWrapper);
        PageInfo<MeetingAlbum> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

}
