package com.imflea.zero.service.pay.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.constant.AdoptConstant;
import com.imflea.zero.constant.PayConstant;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.pay.PayTransportProofMapper;
import com.imflea.zero.model.entity.pay.PayRecorderInfo;
import com.imflea.zero.model.entity.pay.PayTransportProof;
import com.imflea.zero.model.entity.pay.dto.PayProofQueryDto;
import com.imflea.zero.model.entity.pay.dto.PayTransportProofDto;
import com.imflea.zero.model.entity.pay.vo.PayRecorderDetailVo;
import com.imflea.zero.service.pay.IPayRecorderInfoService;
import com.imflea.zero.service.pay.IPayTransportProofService;
import com.imflea.zero.utils.SessionUtils;
import com.imflea.zero.utils.TaskTimeOutUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 转账支付凭证 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-27
 */
@Service
public class PayTransportProofServiceImpl extends ServiceImpl<PayTransportProofMapper, PayTransportProof> implements IPayTransportProofService {
    @Autowired
    private IPayRecorderInfoService payRecorderInfoService;

    @Override
    public PayTransportProof savePayTransportProof(PayTransportProofDto payTransportProofDto) {

        String orderSn = payTransportProofDto.getOrderSn();

        PayRecorderInfo recorderInfo = payRecorderInfoService.queryByOrderSn(orderSn);
        if (CommonUtils.isNull(recorderInfo)) {
            throw new BizException("不存在的支付记录");
        }
        String orderId = recorderInfo.getOrderId();
        String proofId =CommonUtils.generateRandomString();
        PayTransportProof entity = new PayTransportProof();
        String payRecorderId = recorderInfo.getId();
        entity.setId(proofId);
        //只有转账记录才可以进行进行管理
        String payType = recorderInfo.getPayType();
        if (!PayConstant.PAY_TYPE_TRANSFER.getCode().equals(payType)) {
            throw new BizException("非银行转账类订单，不可上传转账证明");
        }
        String payStatus = recorderInfo.getPayStatus();
        //订单支付记录已经完成。则不可以再进行上传和修改支付证明
        if (PayConstant.PAY_STATUS_FINISH.getCode().equals(payStatus)) {
            throw new BizException("订单支付记录已经完成,不可更新证明");
        }

        String proofType = payTransportProofDto.getProofType();
        String recorderStatus = "";
        Boolean isPayProof = proofType.equals(PayConstant.PAY_RECODER_PROOF_TYPE_PAY.getCode());
        //上传支付凭证
        if (isPayProof) {
            if (!(PayConstant.PAY_STATUS_WAIT_UPLOAD_PAY_PROOF.getCode().equals(payStatus) || PayConstant.PAY_STATUS_WAIT_UPLOAD_RECEIVE_PROOF.getCode().equals(payStatus))) {
                throw new BizException("该状态为：" + PayConstant.getName(payStatus) + "不可上传证明");
            }
            recorderStatus = PayConstant.PAY_STATUS_WAIT_UPLOAD_RECEIVE_PROOF.getCode();

        }

        //上传收款证明
        if (proofType.equals(PayConstant.PAY_RECODER_PROOF_TYPE_RECEIVE.getCode())) {
            if (!PayConstant.PAY_STATUS_WAIT_UPLOAD_RECEIVE_PROOF.getCode().equals(payStatus)) {
                throw new BizException("该状态为：" + PayConstant.getName(payStatus) + "不可上传证明");
            }
            recorderStatus = PayConstant.PAY_STATUS_WAIT_CONFIRM.getCode();
        }
        //支付凭证，收款凭证
        //支付记录状态
        //支付类型


        entity.setTenantId(SessionUtils.getTenantId());

        entity.setOrderSn(orderSn);
        entity.setPayRecorderId(payRecorderId);
        entity.setProofId(payTransportProofDto.getProofId());
        entity.setProofUrl(payTransportProofDto.getProofUrl());
        entity.setProofType(proofType);


        boolean result = this.save(entity);
        if (!result) {
            throw new BizException("上传凭证失败");
        }
        Boolean isOk = payRecorderInfoService.updateStatus(payRecorderId, recorderStatus);
        if (isOk && isPayProof) {
            TaskTimeOutUtil.dequeue(AdoptConstant.ADOPT_TIME_OUT_SQU.getCode(), orderId);
        }
        return entity;
    }

    @Override
    public PayTransportProof getByOrderSn(String orderSn, String proofType) {
        QueryWrapper<PayTransportProof> proofQueryWrapper = new QueryWrapper<>();
        proofQueryWrapper.eq("order_sn", orderSn);
        proofQueryWrapper.eq("tenant_id", SessionUtils.getTenantId());
        proofQueryWrapper.eq("proof_type", proofType);
        proofQueryWrapper.orderByDesc("create_time");
        List<PayTransportProof> result = this.list(proofQueryWrapper);
        if (!CommonUtils.isNull(result)) {
            return result.get(0);
        }
        return null;
    }

    @Override
    public PayRecorderDetailVo queryPayDetailRecorderInfo(String orderSn) {
        QueryWrapper<PayRecorderInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_sn", orderSn);
        PayRecorderInfo recorderInfo = payRecorderInfoService.getOne(queryWrapper);
        PayTransportProof proof = this.getByOrderSn(orderSn, PayConstant.PAY_RECODER_PROOF_TYPE_PAY.getCode());
        PayRecorderDetailVo vo = new PayRecorderDetailVo();
        BeanUtil.copyProperties(recorderInfo, vo);
        vo.setPayTransportProof(proof);
        return vo;
    }

    @Override
    public List<PayTransportProof> getByOrderSn(String orderSn) {
        QueryWrapper<PayTransportProof> proofQueryWrapper = new QueryWrapper<>();
        proofQueryWrapper.eq("order_sn", orderSn);
        proofQueryWrapper.eq("tenant_id", SessionUtils.getTenantId());
        List<PayTransportProof> proofs = this.list(proofQueryWrapper);
        return proofs;
    }

    @Override
    public PayTransportProof saveOrUpdatePayTransportProof(PayTransportProof entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
        }
        String orderSn = entity.getOrderSn();
        String payRecorderId = entity.getPayRecorderId();
        PayRecorderInfo recorderInfo = payRecorderInfoService.queryByOrderSn(orderSn);
        if (CommonUtils.isNull(recorderInfo)) {
            throw new BizException("不存在的支付记录");
        }

        //只有转账记录才可以进行进行管理
        String payType = recorderInfo.getPayType();
        if (!PayConstant.PAY_TYPE_TRANSFER.getCode().equals(payType)) {
            throw new BizException("银行转账类订单，不可上传转账证明");
        }
        String payStatus = recorderInfo.getPayStatus();
        //订单支付记录已经完成。则不可以再进行上传和修改支付证明
        if (PayConstant.PAY_STATUS_FINISH.getCode().equals(payStatus)) {
            throw new BizException("订单支付记录已经完成,不可更新证明");
        }

        String proofType = entity.getProofType();
        String recorderStatus = "";
        //上传支付凭证
        if (proofType.equals(PayConstant.PAY_RECODER_PROOF_TYPE_PAY.getCode())) {
            if (!PayConstant.PAY_STATUS_WAIT_UPLOAD_PAY_PROOF.getCode().equals(payStatus)) {
                throw new BizException("该状态为：" + PayConstant.getName(payStatus) + "不可上传证明");
            }
            recorderStatus = PayConstant.PAY_STATUS_WAIT_UPLOAD_RECEIVE_PROOF.getCode();

        }

        //上传收款证明
        if (proofType.equals(PayConstant.PAY_RECODER_PROOF_TYPE_RECEIVE.getCode())) {
            if (!PayConstant.PAY_STATUS_WAIT_UPLOAD_RECEIVE_PROOF.getCode().equals(payStatus)) {
                throw new BizException("该状态为：" + PayConstant.getName(payStatus) + "不可上传证明");
            }
            recorderStatus = PayConstant.PAY_STATUS_WAIT_CONFIRM.getCode();
        }
        //支付凭证，收款凭证
        //支付记录状态
        //支付类型
        entity.setTenantId(SessionUtils.getTenantId());
        boolean result = this.saveOrUpdate(entity);
        if (!result) {
            throw new BizException("上传支付凭证失败");
        }
        payRecorderInfoService.updateStatus(payRecorderId, recorderStatus);
        return entity;
    }

    @Override
    public PageInfo<PayTransportProof> queryPage(PayProofQueryDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<PayTransportProof> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<PayTransportProof> adoptInfos = this.list(queryWrapper);
        PageInfo<PayTransportProof> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }


}
