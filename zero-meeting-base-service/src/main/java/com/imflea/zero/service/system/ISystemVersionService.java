package com.imflea.zero.service.system;

import com.imflea.zero.model.entity.system.SystemVersion;
import com.imflea.zero.model.entity.system.dto.SystemVersionDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * 系统发布版本 服务类
 * </p>
 *
 * @author xby
 * @since 2022-04-06
 */
public interface ISystemVersionService extends IService<SystemVersion> {

    SystemVersion saveOrUpdateSystemVersion(SystemVersion entity);

    PageInfo<SystemVersion> queryPage(SystemVersionDto dto);

}
