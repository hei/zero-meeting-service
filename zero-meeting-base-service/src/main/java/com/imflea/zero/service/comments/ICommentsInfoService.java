package com.imflea.zero.service.comments;

import com.imflea.zero.model.entity.comments.CommentsInfo;
import com.imflea.zero.model.entity.comments.dto.CommentsInfoDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.comments.dto.SaveOrUpdateCommentsInfoDto;
import com.imflea.zero.model.entity.comments.vo.QueryListVo;
import com.imflea.zero.model.entity.comments.vo.QueryPageVo;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 互动评论表	 服务类
 * </p>
 *
 * @author guohui
 * @since 2021-10-08
 */
public interface ICommentsInfoService extends IService<CommentsInfo> {

    CommentsInfo saveOrUpdateCommentsInfo(SaveOrUpdateCommentsInfoDto pam);

    PageInfo<CommentsInfo> queryPage(CommentsInfoDto dto);

    Boolean updateStatusById(String id,String status);

    Boolean removeByIdAndUserId(String id);

    List<QueryListVo> queryList(String id);

    Boolean updateSectedById(String id, String selected);
}
