package com.imflea.zero.service.user;

import com.imflea.zero.model.entity.user.WebUserFeedback;
import com.imflea.zero.model.entity.user.dto.WebUserFeedbackDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-24
 */
public interface IWebUserFeedbackService extends IService<WebUserFeedback> {

    WebUserFeedback saveUserFeedback(WebUserFeedback entity);

    WebUserFeedback updateUserFeedback(WebUserFeedback entity);

    PageInfo<WebUserFeedback> queryPage(WebUserFeedbackDto dto);

}
