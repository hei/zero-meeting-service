//package com.imflea.zero.service.price;
//
//import com.imflea.zero.model.entity.price.PriceCalculateDto;
//import com.imflea.zero.model.entity.price.SemCouponCalculateDto;
//import com.imflea.zero.model.entity.price.vo.PriceDetailVo;
//
//import java.math.BigDecimal;
//
//public interface IPriceService {
//
//    BigDecimal calculatePrice(PriceCalculateDto priceCalculateDto);
//
//    BigDecimal calculatePrice(SemCouponCalculateDto semCouponCalculateDto);
//
//    PriceDetailVo listAndCalculate4SemCoupon(SemCouponCalculateDto semCouponCalculateDto);
//
//    BigDecimal calculate4GiftCard(BigDecimal totalAmount);
//
//    PriceDetailVo listAndCalculate4GiftCard(SemCouponCalculateDto semCouponCalculateDto);
//
//    PriceDetailVo listAndCalculate4SemCoupon(SemCouponCalculateDto semCouponCalculateDto, Boolean isCheckRule);
//
//    PriceDetailVo listAndCalculate4No(SemCouponCalculateDto semCouponCalculateDto);
//
//    /**
//     * @param semCouponCalculateDto
//     * @return com.imflea.zero.model.entity.price.vo.PriceDetailVo
//     * @author 祥保玉
//     * @description 换购
//     * @date 2021/12/8  13:45
//     */
//
//
//    PriceDetailVo listAndCalculate4Change(SemCouponCalculateDto semCouponCalculateDto);
//
//    /**
//     * @param semCouponCalculateDto
//     * @return com.imflea.zero.model.entity.price.vo.PriceDetailVo
//     * @author 祥保玉
//     * @description 供下单时再次计算价格，返回价格，且返回每种sku的分摊
//     * @date 2021/11/19  13:57
//     */
//
//
//    PriceDetailVo calculate(SemCouponCalculateDto semCouponCalculateDto);
//
//
//}
