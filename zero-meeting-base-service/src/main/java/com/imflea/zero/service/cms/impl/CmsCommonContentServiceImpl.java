package com.imflea.zero.service.cms.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.dao.cms.CmsCommonContentMapper;
import com.imflea.zero.model.entity.cms.CmsCommonContent;
import com.imflea.zero.service.cms.ICmsCommonContentService;
import com.imflea.zero.utils.AssertUtil;
import com.imflea.zero.utils.RestUtil;
import com.imflea.zero.utils.SessionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 公共富文本信息表 服务实现类
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-23
 */
@Service
public class CmsCommonContentServiceImpl extends ServiceImpl<CmsCommonContentMapper, CmsCommonContent> implements ICmsCommonContentService {

    @Override
    public CmsCommonContent saveOrUpdateCmsCommonContent(CmsCommonContent entity) {

        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
            entity.setTenantId(SessionUtils.getTenantId());
        }

        boolean success = this.saveOrUpdate(entity);

        return RestUtil.orElseThrow(success, entity, "正文保存失败");
    }


    @Override
    public Boolean removeCmsCommonContentsByBizIds(List<String> bizIds) {

        AssertUtil.mustNotEmpty(bizIds, "业务主键不能为空");

        QueryWrapper<CmsCommonContent> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("biz_id", bizIds);

        boolean success = this.remove(queryWrapper);
        return RestUtil.orElseThrow(success, "正文删除失败");
    }


    @Override
    public CmsCommonContent queryCmsCommonContentByBizId(String bizId) {

        AssertUtil.mustNotEmpty(bizId, "业务主键不能为空");

        QueryWrapper<CmsCommonContent> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("biz_id", bizId);

        return this.getOne(queryWrapper);
    }
}
