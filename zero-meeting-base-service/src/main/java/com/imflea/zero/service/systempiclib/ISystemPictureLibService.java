package com.imflea.zero.service.systempiclib;

import com.imflea.zero.model.entity.systempiclib.SystemPictureLib;
import com.imflea.zero.model.entity.systempiclib.dto.SystemPictureLibDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * 系统图库 服务类
 * </p>
 *
 * @author guohui
 * @since 2021-10-28
 */
public interface ISystemPictureLibService extends IService<SystemPictureLib> {

    SystemPictureLib saveOrUpdateSystemPictureLib(SystemPictureLib entity);

    PageInfo<SystemPictureLib> queryPage(SystemPictureLibDto dto);

}
