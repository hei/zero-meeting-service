package com.imflea.zero.service.keywords.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.dao.keywords.KeywordsMapper;
import com.imflea.zero.model.entity.keywords.Keywords;
import com.imflea.zero.model.entity.keywords.dto.KeywordsDto;
import com.imflea.zero.service.comments.SensitiveWordBiz;
import com.imflea.zero.service.keywords.IKeywordsService;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 敏感词表 服务实现类
 * </p>
 *
 * @author guohui
 * @since 2021-10-09
 */
@Service
public class KeywordsServiceImpl extends ServiceImpl<KeywordsMapper, Keywords> implements IKeywordsService {

    @Override
    public Keywords saveOrUpdateKeywords(Keywords entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
        }
        entity.setTenantId(SessionUtils.getTenantId());
        this.saveOrUpdate(entity);
        SensitiveWordBiz.initKeyWordsCache();
        return entity;
    }

    @Override
    public PageInfo<Keywords> queryPage(KeywordsDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();

        QueryWrapper<Keywords> queryWrapper = new QueryWrapper<>();

        if (!CommonUtils.isNull(dto.getWords())) {
            queryWrapper.like("words", dto.getWords());
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<Keywords> adoptInfos = this.list(queryWrapper);
        PageInfo<Keywords> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

    @Override
    public List<Keywords> queryAll() {
        List<Keywords> result = this.list();
        return result;
    }

}
