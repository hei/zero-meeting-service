package com.imflea.zero.service.user;

import com.imflea.zero.model.entity.user.WebUserAccount;
import com.imflea.zero.model.entity.user.dto.WebUserAccountDto;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;

import java.math.BigDecimal;

/**
 * <p>
 * 微信端用户账户 服务类
 * </p>
 *
 * @author zzz
 * @since 2024-02-22
 */
public interface IWebUserAccountService extends IService<WebUserAccount> {

    WebUserAccount saveOrUpdateWebUserAccount(WebUserAccount entity);

    PageInfo<WebUserAccount> queryPage(WebUserAccountDto dto);


    WebUserAccount getAccountByUserId(String userId);

    Boolean updateAountByUserId(String userId, BigDecimal semAmout);
}
