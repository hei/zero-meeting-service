package com.imflea.zero.service.invoice.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.dao.invoice.InvoiceHeaderMapper;
import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.imflea.zero.model.entity.invoice.dto.InvoiceHeaderDto;
import com.imflea.zero.model.entity.invoice.dto.SaveDto;
import com.imflea.zero.service.invoice.IInvoiceHeaderService;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * 发表抬头 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 */
@Service
public class InvoiceHeaderServiceImpl extends ServiceImpl<InvoiceHeaderMapper, InvoiceHeader> implements IInvoiceHeaderService {

    @Override
    public InvoiceHeader saveOrUpdateInvoiceHeader(SaveDto saveDto) {
        InvoiceHeader result = this.toInvoiceHeader(saveDto);
        if (CommonUtils.isNull(result.getId())) {
            saveDto.setId(CommonUtils.generateRandomString());
            this.setUserInfo(result, 0);
        } else {
            this.setUserInfo(result, 1);
        }
        result.setTenantId(SessionUtils.getTenantId());
        this.saveOrUpdate(result);
        return result;
    }

    private InvoiceHeader toInvoiceHeader(SaveDto saveDto) {
        InvoiceHeader result = new InvoiceHeader();
        BeanUtils.copyProperties(saveDto, result);
        return result;
    }

    public static void setUserInfo(InvoiceHeader pam, int flag) {
        if (flag == 0) {
            pam.setCreateTime(LocalDateTime.now());
            pam.setCreateUserId(SessionUtils.getUserId());
            pam.setCreateUserName(SessionUtils.getWxNickName());
            pam.setUserId(SessionUtils.getUserId());
        }
        pam.setUpdateTime(LocalDateTime.now());
        pam.setUpdateUserId(SessionUtils.getUserId());
        pam.setUpdateUserName(SessionUtils.getWxNickName());
    }

    /**
     * @param : InvoiceHeaderDto
     * @return : InvoiceHeader
     * @throws Exception
     * @name : InvoiceHeaderDto 转 InvoiceHeader
     * @description 相关说明
     * @time 创建时间:2021年9月23日上午9:29:46
     * @author 作者：郭辉
     * @history 修订历史（历次修订内容、修订人、修订时间等）
     */
    private InvoiceHeader toInvoiceHeader(InvoiceHeaderDto pam) {
        InvoiceHeader invoiceHeader = new InvoiceHeader();
        BeanUtils.copyProperties(pam, invoiceHeader);
        return invoiceHeader;
    }

    @Override
    public PageInfo<InvoiceHeader> queryPage(InvoiceHeaderDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<InvoiceHeader> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<InvoiceHeader> adoptInfos = this.list(queryWrapper);
        PageInfo<InvoiceHeader> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

    @Override
    public List<InvoiceHeader> queryList() {
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<InvoiceHeader> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        if (!CommonUtils.isNull(SessionUtils.getUserId())) {
            queryWrapper.eq("user_id", SessionUtils.getUserId());
        }
        List<InvoiceHeader> result = this.list(queryWrapper);
        return result;
    }

    public static boolean isEmail(String email) {
        String str = "^" +
                "([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)*@" +
                "([a-zA-Z0-9]*[-_]?[a-zA-Z0-9]+)+" +
                "[\\.][A-Za-z]{2,3}" +
                "([\\.][A-Za-z]{2})?" +
                "$";
        Pattern p = Pattern.compile(str);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    @Override
    public InvoiceHeader queryByOrderIdAndOrderType(String orderId, String orderType) {
        if (CommonUtils.isNull(orderId) ||CommonUtils.isNull(orderType)) {
            return null;
        }
        return this.baseMapper.queryByOrderIdAndOrderType(orderId, orderType);
    }
}
