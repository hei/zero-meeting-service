package com.imflea.zero.service.sharestatistics.impl;

import com.imflea.zero.model.entity.sharestatistics.ShareStatistics;
import com.imflea.zero.model.dao.sharestatistics.ShareStatisticsMapper;
import com.imflea.zero.service.sharestatistics.IShareStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.model.entity.sharestatistics.dto.ShareStatisticsDto;
import org.springframework.stereotype.Service;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.util.base.CommonUtils;
import java.util.List;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
* <p>
    * 分享的统计分析 服务实现类
    * </p>
*
* @author guohui
* @since 2021-10-22
*/
@Service
public class ShareStatisticsServiceImpl extends ServiceImpl<ShareStatisticsMapper, ShareStatistics> implements IShareStatisticsService {

    @Override
    public ShareStatistics saveOrUpdateShareStatistics(ShareStatistics entity){
        if(CommonUtils.isNull(entity.getId())){
            entity.setId(CommonUtils.generateRandomString());
        }
        entity.setTenantId(SessionUtils.getTenantId());
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<ShareStatistics> queryPage(ShareStatisticsDto dto){
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<ShareStatistics> queryWrapper = new QueryWrapper<>();
        if(!CommonUtils.isNull(tenantId)){
            queryWrapper.eq("tenant_id", tenantId);
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<ShareStatistics> adoptInfos = this.list(queryWrapper);
        PageInfo<ShareStatistics> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

}
