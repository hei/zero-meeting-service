package com.imflea.zero.service.comments;


import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;


/**
 * @功能职责: 认养物品类的开关控制
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
public class SingleAdoptSwitch {

    static {
        try {
            if (CommonUtils.isNull(ZeroCacheUtils.getString("Switch:SingleAdoptSwitch"))){
               ZeroCacheUtils.setString("Switch:SingleAdoptSwitch", String.valueOf(false));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //立即声明
    private static SingleAdoptSwitch SensitiveSwitch = new SingleAdoptSwitch();

    private boolean wordSwitch;
    //私有化构造方法
    private SingleAdoptSwitch(){
        try {
            if (CommonUtils.isNull(ZeroCacheUtils.getString("Switch:SingleAdoptSwitch"))){
               ZeroCacheUtils.setString("Switch:SingleAdoptSwitch", String.valueOf(false));
                this.wordSwitch = false;
            }else {
                this.wordSwitch = Boolean.valueOf(ZeroCacheUtils.getString("Switch:SingleAdoptSwitch"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static SingleAdoptSwitch getInstance(){
        return SensitiveSwitch;
    }

    public boolean getWordSwitch(){
        try {
            return Boolean.valueOf(ZeroCacheUtils.getString("Switch:SingleAdoptSwitch"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.wordSwitch;
    }

    public void setWordSwitch(boolean wordSwitch){
        try {
           ZeroCacheUtils.setString("Switch:SingleAdoptSwitch", String.valueOf(wordSwitch));
            this.wordSwitch = wordSwitch;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

