//package com.imflea.zero.service.pay.impl;
//
//import com.github.jaemon.dinger.DingerSender;
//import com.github.jaemon.dinger.core.entity.DingerRequest;
//import com.github.jaemon.dinger.core.entity.enums.DingerType;
//import com.github.jaemon.dinger.core.entity.enums.MessageSubType;
//import com.imflea.zero.constant.MallContant;
//import com.imflea.zero.constant.MallPromptContant;
//import com.imflea.zero.constant.PayConstant;
//import com.imflea.zero.model.dao.mall.MallOrderInfoMapper;
//import com.imflea.zero.model.entity.mall.MallOrderInfo;
//import com.imflea.zero.model.entity.mall.dto.MallCancelPayHistoryDto;
//import com.imflea.zero.model.entity.mall.dto.ReturnApplyStatusDto;
//import com.imflea.zero.model.entity.mall.vo.MallPayOrderVo;
//import com.imflea.zero.model.entity.pay.dto.PayOrderDetailDto;
//import com.imflea.zero.model.entity.pay.dto.PayPreRecorderDto;
//import com.imflea.zero.model.entity.pay.dto.PaySuccessDto;
//import com.imflea.zero.model.entity.pay.vo.PayPreDataVo;
//import com.imflea.zero.model.entity.pay.vo.PayPreRefundVo;
//import com.imflea.zero.service.mall.IMallOrderInfoService;
//import com.imflea.zero.service.mall.IMallOrderStatusForwardService;
//import com.imflea.zero.service.mall.IMallReturnOrderService;
//import com.imflea.zero.service.pay.IHandleCommonPrePayService;
//import com.imflea.zero.utils.AssertUtil;
//import com.imflea.zero.utils.OrderUtils;
//import com.imflea.zero.utils.SessionUtils;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.time.LocalDateTime;
//import java.util.Collections;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * @author 祥保玉
// * @PackageName: com.imflea.zero.service.pay.impl
// * @ClassName: HandleMallPrePayServiceImpl
// * @Description
// * @date 2021-10-18  13:56:14
// */
//
//@Service("mallPrePayService")
//@Slf4j
//public class HandleMallPrePayServiceImpl implements IHandleCommonPrePayService {
//
//    @Autowired
//    private IMallOrderInfoService iMallOrderInfoService;
//
//
//    @Autowired
//    private IMallOrderStatusForwardService iMallOrderStatusForwardService;
//
//    @Autowired
//    private IMallReturnOrderService iMallReturnOrderService;
//
//    @Autowired
//    private DingerSender dingerSender;
//
//
//    /*
//            NOTE:对于商城订单支付时一定是按照一笔订单进行支付的
//                   所以订单号一定是有且只有一个的
//     */
//    @Override
//    public PayPreDataVo getPrePayData(PayOrderDetailDto dto) {
//
//        List<String> orderIds = dto.getOrderIds();
//        AssertUtil.mustNotEmpty(orderIds, MallPromptContant.ORDER_ID_NOT_EMPTY);
//
//        final MallOrderInfoMapper mapper = (MallOrderInfoMapper) iMallOrderInfoService.getBaseMapper();
//
//        // 商城订单的结算一定是一单一结
//        String orderId = orderIds.get(MallContant.ZERO);
//        MallPayOrderVo mallPayOrderVo = mapper.selectPayOrder(orderId);
//
//        PayPreDataVo vo = new PayPreDataVo();
//
//        vo.setPayType(dto.getPayType());
//
//        vo.setPaySn(OrderUtils.generateOrderSn());
//
//        vo.setPayTotal(mallPayOrderVo.getPayAmount());
//        vo.setGoodsName(mallPayOrderVo.getGoodsName());
//
//        List<PayPreRecorderDto> preRecorders = getPreRecorderList(mallPayOrderVo);
//        vo.setPreRecorders(preRecorders);
//
//        return vo;
//    }
//
//
//    @Override
//    public boolean handleCreatePrePayResult(List<String> orderIds, String payType) {
//        return false;
//    }
//
//
//    /*
//        NOTE: 支付成功的后置处理
//               更新订单状态
//     */
//    @Override
//    public boolean handlePaySuccess(List<PaySuccessDto> dtos) {
//
//        AssertUtil.mustNotEmpty(dtos, "支付信息不能为空");
//        boolean result = iMallOrderStatusForwardService.updateHasPayStatus(dtos.get(MallContant.ZERO));
//        try {
//            List<String> orderIds = dtos.stream().map(item -> item.getOrderId()).collect(Collectors.toList());
//            List<MallOrderInfo> mallOrderDetailVos = this.iMallOrderInfoService.listByIds(orderIds);
//            List<String> orderSns = mallOrderDetailVos.stream().map(item -> item.getOrderSn()).collect(Collectors.toList());
//            int size = orderSns.size();
//            StringBuffer snos = new StringBuffer("");
//            for (int i = 0; i < size; i++) {
//                String sno = orderSns.get(i);
//                snos.append(sno);
//                if (i + 1 < size) {
//                    snos.append(",");
//                }
//            }
//            dingerSender.send(DingerType.WETALK, MessageSubType.TEXT, DingerRequest.request("您有" + size + "条新的商城订单，订单号为：" + snos.toString()));
//        } catch (Exception e) {
//            log.error("订单推送异常", e.getMessage());
//        }
//        return result;
//    }
//
//    @Override
//    public PayPreRefundVo getPreRefundData(String refundOrderId, String orderType, String extend) {
//
//        PayPreRefundVo vo = new PayPreRefundVo();
//
//        vo.setPayType("PAY_TYPE_WX");
//
//        switch (extend) {
//
//            case "HAS_PAY_REFUND": // NOTE: 已支付查询订单
//
//                vo = iMallOrderInfoService.selectRefundData(refundOrderId);
//                break;
//
//            case "BACK_ORDER_REFUND":  // NOTE: 售后退款查询订单
//
//                vo = iMallReturnOrderService.selectRefundData(refundOrderId);
//                break;
//        }
//
//        return vo;
//    }
//
//
//    /*
//        NOTE: 已支付后退款行为
//                已支付未发货退款
//                售后退款
//
//     */
//    @Override
//    public boolean handleRefundSuccess(String refundOrderId, String orderType, String extend) {
//
//        switch (extend) {
//
//            case "HAS_PAY_REFUND":   // NOTE: 已支付未发货退款
//
//                MallCancelPayHistoryDto payHistoryDto = new MallCancelPayHistoryDto();
//
//                payHistoryDto.setOrderId(refundOrderId);
//
//                return iMallOrderStatusForwardService.updateHaspayRefund(payHistoryDto);
//
//            case "BACK_ORDER_REFUND": // NOTE: 售后退款
//
//                // 更新订单状态
//                ReturnApplyStatusDto dto = new ReturnApplyStatusDto();
//
//                dto.setId(refundOrderId);
//
//                return iMallReturnOrderService.updateHandleStatus(dto);
//        }
//
//        return false;
//    }
//
//
//    private List<PayPreRecorderDto> getPreRecorderList(MallPayOrderVo mallPayOrderVo) {
//
//        PayPreRecorderDto recorderDto = new PayPreRecorderDto();
//
//        recorderDto.setOrderType(PayConstant.ORDER_TYPE_MALL.getCode());
//
//        recorderDto.setOrderId(mallPayOrderVo.getOrderId());
//        recorderDto.setOrderSn(mallPayOrderVo.getOrderSn());
//        recorderDto.setTotalAmount(mallPayOrderVo.getPayAmount());
//        recorderDto.setTenantId(mallPayOrderVo.getTenantId());
//
//        recorderDto.setPayUserId(SessionUtils.getUserId());
//        recorderDto.setPayUserName(SessionUtils.getWxNickName());
//        recorderDto.setPayUserOpenId(SessionUtils.getOpenId());
//        recorderDto.setPayTime(LocalDateTime.now());
//
//        return Collections.singletonList(recorderDto);
//    }
//}
