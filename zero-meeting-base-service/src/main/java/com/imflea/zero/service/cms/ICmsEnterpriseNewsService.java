package com.imflea.zero.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.dto.PageDto;
import com.imflea.zero.model.entity.cms.CmsEnterpriseNews;
import com.imflea.zero.model.entity.cms.dto.CmsEnterpriseNewsDto;
import com.imflea.zero.model.entity.cms.vo.EnterpriseNewsVo;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 企业资讯 服务类
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-24
 */
public interface ICmsEnterpriseNewsService extends IService<CmsEnterpriseNews> {

    /**
     * 管理端: 保存企业新闻
     */
    CmsEnterpriseNews saveOrUpdateCmsEnterpriseNews(CmsEnterpriseNews entity);

    /**
     * 管理端: 发布企业新闻到ES中
     */
    Boolean updatePublishStatus(CmsEnterpriseNewsDto dto);

    /**
     * 管理端: 企业新闻分页
     */
    PageInfo<CmsEnterpriseNews> queryPage(CmsEnterpriseNewsDto dto);

    /**
     * 移动端: 分页查询企业新闻从ES查询
     */
    EnterpriseNewsVo queryPageFormEs(PageDto pageDto);

    /**
     * 移动端: 查询新闻内容从ES查询
     */
    Map<String, Object> queryNewsFromEs(String newsId);

    boolean removeEntity(List<String> ids);
}
