package com.imflea.zero.service.meeting;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.entity.meeting.MeetingCouponRule;
import com.imflea.zero.model.entity.meeting.dto.MeetingCouponRuleDto;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 规则表 服务类
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 */
public interface IMeetingCouponRuleService extends IService<MeetingCouponRule> {

    MeetingCouponRule saveOrUpdateMeetingCouponRule(MeetingCouponRule entity);

    PageInfo<MeetingCouponRule> queryPage(MeetingCouponRuleDto dto);

    List<MeetingCouponRule> listAllRules();


    BigDecimal getSemAmount(BigDecimal prePayAmount);

}
