package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.util.ZeroJsonUtils;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.qx.QxMenuMapper;
import com.imflea.zero.model.entity.qx.QxMenu;
import com.imflea.zero.service.qx.IQxMenuService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
@Service
public class QxMenuServiceImpl extends ServiceImpl<QxMenuMapper, QxMenu> implements IQxMenuService {

    @Override
    public QxMenu saveOrUpdateMenu(QxMenu qxMenu) {

        String id = qxMenu.getId();
        boolean result = false;
        if (CommonUtils.isNull(id)) {
            id =CommonUtils.generateRandomString();
            qxMenu.setId(id);
            qxMenu.setStatus(QxContant.getMenuOpenStatus());
            result = this.save(qxMenu);
        } else {
            result = this.updateById(qxMenu);
        }
        if (result) {
            return qxMenu;
        } else {
            throw new BizException("操作异常");
        }
    }

    @Override
    public Boolean removeMenuById(String id) {
        boolean result = this.removeById(id);
        return result;

    }

    @Override
    public Boolean updateMenuStatus(List<String> ids, String status) {
        UpdateWrapper<QxMenu> updateWrapper = new UpdateWrapper<QxMenu>();
        updateWrapper.set("status", status);
        updateWrapper.in("id", ids);
        boolean result = this.update(updateWrapper);
        return result;
    }

    /**
     * @param menuPid
     * @return com.css.aegis.entity.JsonResult<java.util.List < com.imflea.zero.model.entity.QxMenu>>
     * @author 祥保玉
     * @description 根据id，获取所有的已经启用的子节点
     * @date 2021/8/23  11:26
     */


    @Override
    public List<QxMenu> queryByParentId(String menuPid) {
        QueryWrapper<QxMenu> qxMenuQueryWrapper = new QueryWrapper<>();
        qxMenuQueryWrapper.eq("status", QxContant.getMenuOpenStatus());
        qxMenuQueryWrapper.eq("menu_pid", menuPid).orderByDesc("order_num");
        List<QxMenu> result = this.list(qxMenuQueryWrapper);
        return result;
    }

    @Override
    public List<QxMenu> queryMenuByIds(List<String> menuIds) {
        QueryWrapper<QxMenu> qxMenuQueryWrapper = new QueryWrapper<>();
        qxMenuQueryWrapper.eq("status", QxContant.getMenuOpenStatus());
        qxMenuQueryWrapper.in("id", menuIds);
        List<QxMenu> result = this.list(qxMenuQueryWrapper);
        return result;
    }

    @Override
    public List<Map<String, Object>> queryMenuTree() {
        QueryWrapper<QxMenu> qxMenuQueryWrapper = new QueryWrapper<>();
        qxMenuQueryWrapper.eq("status", QxContant.getMenuOpenStatus());
        List<QxMenu> result = this.list(qxMenuQueryWrapper);
        List<Map<String, Object>> treeMap = null;
        try {
            treeMap =CommonUtils.listToTree(ZeroJsonUtils.entityListToMapList(result), "id", "menuPid");
        } catch (Exception e) {
            throw new BizException(e.getMessage());
        }
        return treeMap;
    }

    @Override
    public List<Map<String, Object>> queryMenuTreeNoStatus() {
        QueryWrapper<QxMenu> qxMenuQueryWrapper = new QueryWrapper<>();
        List<QxMenu> result = this.list(qxMenuQueryWrapper);
        List<Map<String, Object>> treeMap = null;
        try {
            treeMap =CommonUtils.listToTree(ZeroJsonUtils.entityListToMapList(result), "id", "menuPid");
        } catch (Exception e) {
            throw new BizException(e.getMessage());
        }
        return treeMap;
    }
}
