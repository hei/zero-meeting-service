package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.model.dao.qx.QxUserAccessMapper;
import com.imflea.zero.model.entity.qx.QxUserAccess;
import com.imflea.zero.model.entity.qx.dto.QxUserAccessDto;
import com.imflea.zero.service.qx.IQxUserAccessService;
import com.imflea.zero.utils.SessionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 用户每天访问数量表 服务实现类
 * </p>
 *
 * @author zhaoyang
 * @since 2022-01-13
 */
@Service
public class QxUserAccessServiceImpl extends ServiceImpl<QxUserAccessMapper, QxUserAccess> implements IQxUserAccessService {

    @Override
    public QxUserAccess saveOrUpdateQxUserAccess(QxUserAccess entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
        }
        if (CommonUtils.isNull(entity.getTenantId())) {
            entity.setTenantId(SessionUtils.getTenantId());
        }
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<QxUserAccess> queryPage(QxUserAccessDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<QxUserAccess> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<QxUserAccess> adoptInfos = this.list(queryWrapper);
        PageInfo<QxUserAccess> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

    @Override
    public List<QxUserAccess> listAllAccessByTenantId(String tenantId) {

        QueryWrapper<QxUserAccess> qxUserAccessQueryWrapper = new QueryWrapper<>();
        qxUserAccessQueryWrapper.eq("tenant_id", tenantId);

        return this.list(qxUserAccessQueryWrapper);
    }

    @Override
    public Boolean saveOrUpdateByTenantAndDate(QxUserAccess access) {
        String tenantId = access.getTenantId();
        QueryWrapper<QxUserAccess> qxUserAccessQueryWrapper = new QueryWrapper<>();
        qxUserAccessQueryWrapper.eq("tenant_id", tenantId);
        qxUserAccessQueryWrapper.eq("access_day", access.getAccessDay());
        qxUserAccessQueryWrapper.orderByDesc("total");
        List<QxUserAccess> list = this.list(qxUserAccessQueryWrapper);
        if (CommonUtils.isNull(list)) {
            this.saveOrUpdateQxUserAccess(access);
        } else {
            QxUserAccess entity = list.get(0);
            if (entity.getTotal() < access.getTotal()) {
                entity.setTotal(access.getTotal());
                this.updateById(entity);
            }
        }
        try {
            String key = QxContant.getWxAccessUserNumPrefix() + tenantId + ":" + "*";
            Set<String> keys =ZeroCacheUtils.getKeys(key);
            if (!CommonUtils.isNull(keys)) {
               ZeroCacheUtils.deleteKey(keys.toArray(new String[keys.size()]));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

}
