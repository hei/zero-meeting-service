package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.dto.SearchPageDto;
import com.imflea.zero.model.entity.qx.QxUserInfo;
import com.imflea.zero.model.entity.qx.vo.QxUserInfoVo;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 管理端客户用户 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-18
 */
public interface IQxUserInfoService extends IService<QxUserInfo> {

    QxUserInfo saveOrUpdateUser(QxUserInfo userInfo);

    PageInfo<QxUserInfoVo> queryByTenantId4Page(SearchPageDto req);

    Boolean updateUserStatusById(String id, String lockStatus);

    Boolean removeUserById(String id);

    Map<String, Object> handleQueryUser4Login(String userName, String password);

    QxUserInfo saveOrUpdateUser4Tenant(QxUserInfo userInfo);

    QxUserInfo queryById(String id);

    Boolean updateUserPassword(String newPassword, String oldPassword);

    boolean updatePassword2Def(String id);

    boolean updatePasswordBatch();

    List<QxUserInfoVo> queryAllTenantUsers();

}
