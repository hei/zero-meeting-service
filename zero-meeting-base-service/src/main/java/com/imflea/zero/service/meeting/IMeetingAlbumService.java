package com.imflea.zero.service.meeting;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.entity.meeting.MeetingAlbum;
import com.imflea.zero.model.entity.meeting.dto.MeetingAlbumDto;

/**
 * <p>
 * 画廊 服务类
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 */
public interface IMeetingAlbumService extends IService<MeetingAlbum> {

    MeetingAlbum saveOrUpdateMeetingAlbum(MeetingAlbum entity);

    PageInfo<MeetingAlbum> queryPage(MeetingAlbumDto dto);

}
