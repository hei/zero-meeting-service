package com.imflea.zero.service.invoice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.imflea.zero.model.entity.invoice.dto.InvoiceHeaderDto;
import com.imflea.zero.model.entity.invoice.dto.SaveDto;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 发表抬头 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 */
public interface IInvoiceHeaderService extends IService<InvoiceHeader> {

    InvoiceHeader saveOrUpdateInvoiceHeader(SaveDto saveDto);

    PageInfo<InvoiceHeader> queryPage(InvoiceHeaderDto dto);

    List<InvoiceHeader> queryList();

    InvoiceHeader queryByOrderIdAndOrderType(String orderId, String orderType);
}
