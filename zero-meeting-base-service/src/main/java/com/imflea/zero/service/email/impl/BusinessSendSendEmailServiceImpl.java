package com.imflea.zero.service.email.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.imflea.zero.model.entity.invoice.InvoiceInfo;
import com.imflea.zero.model.entity.invoice.dto.SendMailDto;
import com.imflea.zero.service.email.IBusinessSendEmailService;
import com.imflea.zero.service.email.IEmailService;
import com.imflea.zero.service.invoice.IInvoiceHeaderService;
import com.imflea.zero.service.invoice.IInvoiceInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * @功能职责:
 * @描述：
 * @作者: 郭辉
 * @创建时间: 2020-12-02
 * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
 * @company 中国软件与技术服务股份有限公司
 */
@Service
public class BusinessSendSendEmailServiceImpl implements IBusinessSendEmailService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    IInvoiceHeaderService iInvoiceHeaderService;

    @Autowired
    IInvoiceInfoService iInvoiceInfoService;

    @Autowired
    IEmailService iEmailService;

    @Value("${fileserver.domain:https://iampeiyuan.com/}")
    public String FILE_DOMAIN;
    @Override
    public Boolean updateContentAndStatusById(SendMailDto sendMailDto) {
        InvoiceInfo invoiceInfo = iInvoiceInfoService.getById(sendMailDto.getId());
        UpdateWrapper<InvoiceInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("content_url",sendMailDto.getContentUrl());
        updateWrapper.set("content_id",sendMailDto.getContentId());
        updateWrapper.set("status","0");
        updateWrapper.eq("id", sendMailDto.getId());
        iInvoiceInfoService.update(updateWrapper);
        InvoiceHeader header = iInvoiceHeaderService.getById(invoiceInfo.getInvoiceHeaderId());
        if (CommonUtils.isNull(header) ||CommonUtils.isNull(header.getEmail()) ||CommonUtils.isNull(header.getId())){
            return false;
        }
        Boolean result = iEmailService.sendSimpleMail(header.getEmail(), "发送附件", "发票地址:\n" + this.FILE_DOMAIN+ sendMailDto.getContentUrl());
        return result;
    }
}

