package com.imflea.zero.service.meeting;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.entity.meeting.MeetingShopUser;
import com.imflea.zero.model.entity.meeting.dto.MeetingShopUserDto;
import com.imflea.zero.model.entity.qx.QxUserInfo;
import com.imflea.zero.model.entity.qx.vo.QxUserInfoVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xby
 * @since 2024-01-25
 */
public interface IMeetingShopUserService extends IService<MeetingShopUser> {

    MeetingShopUser saveOrUpdateMeetingShopUser(MeetingShopUser entity);

    PageInfo<MeetingShopUser> queryPage(MeetingShopUserDto dto);

    Map<String, List<QxUserInfoVo>> queryByShopId(String id);

    List<MeetingShopUser> saveOrUpdateMeetingShopUserBatch(List<MeetingShopUser> meetingShopUsers);

    List<MeetingShopUser> queryByUserId(String userId);
    List<String> queryShopIdsByUserId(String userId);
}
