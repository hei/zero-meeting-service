package com.imflea.zero.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.dao.system.SystemVersionMapper;
import com.imflea.zero.model.entity.system.SystemVersion;
import com.imflea.zero.model.entity.system.dto.SystemVersionDto;
import com.imflea.zero.service.system.ISystemVersionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 系统发布版本 服务实现类
 * </p>
 *
 * @author xby
 * @since 2022-04-06
 */
@Service
public class SystemVersionServiceImpl extends ServiceImpl<SystemVersionMapper, SystemVersion> implements ISystemVersionService {

    @Override
    public SystemVersion saveOrUpdateSystemVersion(SystemVersion entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
//            entity.setPostTime(LocalDate.now());
        }
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<SystemVersion> queryPage(SystemVersionDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        QueryWrapper<SystemVersion> queryWrapper = new QueryWrapper<>();

        PageHelper.startPage(pageIndex, pageSize);
        List<SystemVersion> adoptInfos = this.list(queryWrapper);
        PageInfo<SystemVersion> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

}
