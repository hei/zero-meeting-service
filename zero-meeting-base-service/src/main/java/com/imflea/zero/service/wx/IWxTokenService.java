package com.imflea.zero.service.wx;

public interface IWxTokenService {
    public String getAccessToken() throws Exception;
}
