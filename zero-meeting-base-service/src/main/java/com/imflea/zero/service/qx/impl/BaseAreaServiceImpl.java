package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.model.dao.qx.BaseAreaMapper;
import com.imflea.zero.model.entity.qx.BaseArea;
import com.imflea.zero.service.qx.IBaseAreaService;
import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-24
 */
@Service
public class BaseAreaServiceImpl extends ServiceImpl<BaseAreaMapper, BaseArea> implements IBaseAreaService {

    @Override
    public List<BaseArea> queryByParendId(String parendId) {
        QueryWrapper<BaseArea> queryWrapper = new QueryWrapper<>();

        if (CommonUtils.isNull(parendId)) {
            parendId = "00000000000000000000000000000";
            queryWrapper.eq("parent_id", parendId);
        }

        List<BaseArea> result = null;
        try {
            result = ZeroCacheUtils.getEntityListCacheValueByTableNameAndGroupIndexAndIndexValue("base_area", "parentId", parendId, BaseArea.class);
            if (CommonUtils.isNull(result)) {
                result = this.list(queryWrapper);
            }
        } catch (Exception e) {
            result = this.list(queryWrapper);
        }
        return result;
    }
}
