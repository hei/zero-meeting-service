package com.imflea.zero.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.user.WebUserFarmyard;

/**
 * <p>
 * 用户农场表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-24
 */
public interface IWebUserFarmyardService extends IService<WebUserFarmyard> {

    WebUserFarmyard saveOrUpdateWebUserFarmyard(WebUserFarmyard entity);

}
