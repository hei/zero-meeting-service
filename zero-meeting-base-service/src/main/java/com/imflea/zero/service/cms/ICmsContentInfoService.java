package com.imflea.zero.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.cms.CmsContentInfo;

import java.util.List;

/**
 * <p>
 * 发布正文信息表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-17
 */
public interface ICmsContentInfoService extends IService<CmsContentInfo> {

    /**
     * 管理端: 删除CMS模板信息正文
     */
    Boolean removeByTemplateId(String templateId);

    /**
     * 管理端: 列表查询模板下的所有正文信息(对于图片可能存在多个)
     */
    List<CmsContentInfo> queryListByTemplateId(String templateId);

}
