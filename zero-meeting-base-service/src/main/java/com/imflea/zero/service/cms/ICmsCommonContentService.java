package com.imflea.zero.service.cms;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.cms.CmsCommonContent;

import java.util.List;

/**
 * <p>
 * 当前接口仅提供了保存,删除,查询功能。后续如果需要需要其他接口可联系我添加.
 * </p>
 *
 * @author zhaoyang
 * @since 2021-09-23
 */
public interface ICmsCommonContentService extends IService<CmsCommonContent> {

    /**
     * 内部接口: 新增(更新)富文本信息
     */
    CmsCommonContent saveOrUpdateCmsCommonContent(CmsCommonContent entity);

    /**
     * 内部接口: 根据业务键删除
     */
    Boolean removeCmsCommonContentsByBizIds(List<String> bizIds);

    /**
     * 内部接口: 根据业务键获取内容
     */
    CmsCommonContent queryCmsCommonContentByBizId(String bizId);

}
