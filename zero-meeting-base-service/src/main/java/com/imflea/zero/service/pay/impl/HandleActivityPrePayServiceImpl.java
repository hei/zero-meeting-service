//package com.imflea.zero.service.pay.impl;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
//import com.imflea.zero.util.base.CommonUtils;
//import com.imflea.zero.exception.BizException;
//import com.imflea.zero.model.entity.activity.ActivityInfo;
//import com.imflea.zero.model.entity.activity.ActivityUserRelation;
//import com.imflea.zero.model.entity.pay.dto.PayOrderDetailDto;
//import com.imflea.zero.model.entity.pay.dto.PayPreRecorderDto;
//import com.imflea.zero.model.entity.pay.dto.PaySuccessDto;
//import com.imflea.zero.model.entity.pay.vo.PayPreDataVo;
//import com.imflea.zero.model.entity.pay.vo.PayPreRefundVo;
//import com.imflea.zero.service.activity.IActivityInfoService;
//import com.imflea.zero.service.activity.IActivityUserRelationService;
//import com.imflea.zero.service.pay.IHandleCommonPrePayService;
//import com.imflea.zero.utils.OrderUtils;
//import com.imflea.zero.utils.SessionUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
///**
// * @功能职责:
// * @描述：
// * @作者: 郭辉
// * @创建时间: 2020-12-02
// * @copyright Copyright (c) 2020 中国软件与技术服务股份有限公司
// * @company 中国软件与技术服务股份有限公司
// */
//@Service("activityPrePayService")
//public class HandleActivityPrePayServiceImpl implements IHandleCommonPrePayService {
//
//    @Autowired
//    private IActivityInfoService iActivityInfoService;
//
//    @Autowired
//    private IActivityUserRelationService iActivityUserRelationService;
//
//    //计算金额
//    @Override
//    public PayPreDataVo getPrePayData(PayOrderDetailDto dto) throws Exception {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        PayPreDataVo result = new PayPreDataVo();
//        List<String> relationIds = dto.getOrderIds();
//        List<String> activityIds = new ArrayList<>();
//        List<ActivityUserRelation> relationList = iActivityUserRelationService.listByIds(relationIds);
//        relationList.stream().forEach(item -> activityIds.add(item.getActivityId()));
//        if (CommonUtils.isNull(activityIds)) {
//            throw new BizException("没有该订单");
//        }
//        List<ActivityInfo> activityInfos = iActivityInfoService.listByIds(activityIds);
//        if (CommonUtils.isNull(activityInfos)) {
//            throw new BizException("没有该订单");
//        }
//        //校验是否有名额
//        QueryWrapper<ActivityUserRelation> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("activity_id", activityIds.get(0));
//        queryWrapper.ge("create_time", df.format(new Date().getTime() - (5 * 60 * 1000)));
//        long count = iActivityUserRelationService.count(queryWrapper);
//        long last = activityInfos.get(0).getScopeUser() - count;
//        if (last <= 0) {
//            throw new BizException("没名额");
//        }
//
//        List<PayPreRecorderDto> preRecorders = new ArrayList<>();
//        PayPreRecorderDto payPreRecorderDto = new PayPreRecorderDto();
//        payPreRecorderDto.setOrderSn(activityInfos.get(0).getId());
//        payPreRecorderDto.setOrderId(activityInfos.get(0).getId());
//        payPreRecorderDto.setOrderType(dto.getOrderType());
//        payPreRecorderDto.setPayTime(LocalDateTime.now());
//        payPreRecorderDto.setTenantId(SessionUtils.getTenantId());
//        payPreRecorderDto.setTotalAmount(activityInfos.get(0).getCharge());
//        payPreRecorderDto.setPayUserId(SessionUtils.getUserId());
//        payPreRecorderDto.setPayUserName(SessionUtils.getWxNickName());
//        payPreRecorderDto.setPayUserOpenId(SessionUtils.getOpenId());
//        payPreRecorderDto.setPaySn(activityInfos.get(0).getId());
//        payPreRecorderDto.setPayType(dto.getPayType());
//        preRecorders.add(payPreRecorderDto);
//        result.setPayTotal(activityInfos.get(0).getCharge());
//        result.setGoodsName(activityInfos.get(0).getTitle());
//        result.setTenantId(SessionUtils.getTenantId());
//        result.setPaySn(OrderUtils.generateOrderSn());
//        result.setPreRecorders(preRecorders);
//        result.setPayType(dto.getPayType());
//        return result;
//    }
//
//    //支付成功后更新支付状态
//    @Override
//    public     boolean handleCreatePrePayResult(List<String> orderIds,String payType) {
//        return true;
//    }
//
//    @Override
//    public boolean handlePaySuccess(List<PaySuccessDto> dto) {
//
//
//        UpdateWrapper<ActivityUserRelation> userRelationUpdateWrapper = new UpdateWrapper<>();
//        userRelationUpdateWrapper.set("is_charge", "0");
//        userRelationUpdateWrapper.in("activity_id", dto.get(0).getOrderId());
//        boolean result = iActivityUserRelationService.update(userRelationUpdateWrapper);
//        return result;
//
//    }
//
//    @Override
//    public PayPreRefundVo getPreRefundData(String orderId, String refundOrderId, String orderType) {
//        return null;
//    }
//
//    @Override
//    public boolean handleRefundSuccess(String orderId, String refundOrderId, String orderType) {
//        return false;
//    }
//}
