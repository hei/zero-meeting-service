package com.imflea.zero.service.qx;

import com.baomidou.mybatisplus.extension.service.IService;
import com.imflea.zero.model.entity.qx.QxAttachment;
import com.imflea.zero.model.entity.qx.dto.AttachmentDetailDto;

import java.util.List;

/**
 * <p>
 * 附件表 服务类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-27
 */
public interface IQxAttachmentService extends IService<QxAttachment> {

    QxAttachment saveOrUpdateAttachment(QxAttachment attchment);

    Boolean saveAttachmentBatch(List<QxAttachment> attchments);

    Boolean removeByAttachmentById(String id);

    Boolean removeByBusinessId(AttachmentDetailDto delReq);

    List<QxAttachment> queryAttachmentsByBuinessId(String businessId);

    List<QxAttachment> queryAttachmentsByBuinessIdAndModule(AttachmentDetailDto queryReq);

    QxAttachment queryAttachmentsById(String id);
}
