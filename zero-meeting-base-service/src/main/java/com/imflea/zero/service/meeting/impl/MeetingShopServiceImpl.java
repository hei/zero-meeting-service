package com.imflea.zero.service.meeting.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.model.dao.meeting.MeetingShopMapper;
import com.imflea.zero.model.entity.meeting.MeetingShop;
import com.imflea.zero.model.entity.meeting.MeetingShopUser;
import com.imflea.zero.model.entity.meeting.dto.MeetingShopDto;
import com.imflea.zero.service.meeting.IMeetingShopService;
import com.imflea.zero.service.meeting.IMeetingShopUserService;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author xby
 * @since 2024-01-24
 */
@Service
public class MeetingShopServiceImpl extends ServiceImpl<MeetingShopMapper, MeetingShop> implements IMeetingShopService {
    @Autowired
    private IMeetingShopUserService meetingShopUserService;

    @Override
    public MeetingShop saveOrUpdateMeetingShop(MeetingShop entity) {
        if (CommonUtils.isNull(entity.getId())) {
            entity.setId(CommonUtils.generateRandomString());
        }
        entity.setTenantId(SessionUtils.getTenantId());
        this.saveOrUpdate(entity);
        return entity;
    }

    @Override
    public PageInfo<MeetingShop> queryPage(MeetingShopDto dto) {
        Integer pageIndex = dto.getPageNum();
        Integer pageSize = dto.getPageSize();
        String tenantId = SessionUtils.getTenantId();
        QueryWrapper<MeetingShop> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }
        if (!CommonUtils.isNull(dto.getCompanyName())) {
            queryWrapper.like("company_name", dto.getCompanyName());
        }
        List<MeetingShopUser> shopUsers = new ArrayList<>();
        List<String> shopIds = new ArrayList<>();
        if (SessionUtils.isMdAdmin()) {
            shopUsers = meetingShopUserService.queryByUserId(SessionUtils.getUserId());
        }
        if (!CommonUtils.isNull(shopUsers)) {
            shopIds = shopUsers.stream().map(item -> item.getMeetingShopId()).collect(Collectors.toList());

        }
        queryWrapper.in(!CommonUtils.isNull(shopIds), "id", shopIds);
        PageHelper.startPage(pageIndex, pageSize);
        List<MeetingShop> adoptInfos = this.list(queryWrapper);
        PageInfo<MeetingShop> pageResult = new PageInfo<>(adoptInfos);
        return pageResult;
    }

    @Override
    public List<MeetingShop> queryAll(MeetingShopDto meetingShopDto) {
        QueryWrapper<MeetingShop> queryWrapper = new QueryWrapper<>();
        String tenantId = SessionUtils.getTenantId();
        if (!CommonUtils.isNull(tenantId)) {
            queryWrapper.eq("tenant_id", tenantId);
        }

        List<MeetingShopUser> shopUsers = new ArrayList<>();
        List<String> shopIds = new ArrayList<>();
        if (SessionUtils.isMdAdmin()) {
            shopUsers = meetingShopUserService.queryByUserId(SessionUtils.getUserId());
        }
        if (!CommonUtils.isNull(shopUsers)) {
            shopIds = shopUsers.stream().map(item -> item.getMeetingShopId()).collect(Collectors.toList());

        }
        queryWrapper.in(!CommonUtils.isNull(shopIds), "id", shopIds);
        List<MeetingShop> list = this.list(queryWrapper);
        return list;
    }

    @Override
    public List<MeetingShop> queryByProvince(String province) {
        QueryWrapper<MeetingShop> queryWrapper = new QueryWrapper<>();

        List<MeetingShopUser> shopUsers = new ArrayList<>();
        List<String> shopIds = new ArrayList<>();
        if (SessionUtils.isMdAdmin()) {
            shopUsers = meetingShopUserService.queryByUserId(SessionUtils.getUserId());
        }
        if (!CommonUtils.isNull(shopUsers)) {
            shopIds = shopUsers.stream().map(item -> item.getMeetingShopId()).collect(Collectors.toList());

        }
        queryWrapper.in(!CommonUtils.isNull(shopIds), "id", shopIds);
        queryWrapper.eq(!CommonUtils.isNull(province), "province", province);
        List<MeetingShop> list = this.list(queryWrapper);
        return list;
    }

    @Override
    public List<MeetingShop> listAll() {
        List<MeetingShopUser> shopUsers = new ArrayList<>();
        List<String> shopIds = new ArrayList<>();
        if (SessionUtils.isMdAdmin()) {
            shopUsers = meetingShopUserService.queryByUserId(SessionUtils.getUserId());
        }
        QueryWrapper<MeetingShop> queryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(shopUsers)) {
            shopIds = shopUsers.stream().map(item -> item.getMeetingShopId()).collect(Collectors.toList());

        }
        queryWrapper.in(!CommonUtils.isNull(shopIds), "id", shopIds);
        List<MeetingShop> list = this.list(queryWrapper);
        return list;
    }

    @Override
    public List<MeetingShop> listAll(String province, String city,String region) {
        QueryWrapper<MeetingShop> queryWrapper = new QueryWrapper<>();

        List<MeetingShopUser> shopUsers = new ArrayList<>();
        List<String> shopIds = new ArrayList<>();
        if (SessionUtils.isMdAdmin()) {
            shopUsers = meetingShopUserService.queryByUserId(SessionUtils.getUserId());
        }
        if (!CommonUtils.isNull(shopUsers)) {
            shopIds = shopUsers.stream().map(item -> item.getMeetingShopId()).collect(Collectors.toList());

        }
        queryWrapper.in(!CommonUtils.isNull(shopIds), "id", shopIds);
        queryWrapper.eq(!CommonUtils.isNull(province), "province", province);
        queryWrapper.eq(!CommonUtils.isNull(city), "city", city);
        queryWrapper.eq(!CommonUtils.isNull(region), "region", region);
        List<MeetingShop> list = this.list(queryWrapper);
        return list;
    }

}
