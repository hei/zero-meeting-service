package com.imflea.zero.service.qx.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.dao.qx.QxTenantMapper;
import com.imflea.zero.model.entity.cms.CmsCommonContent;
import com.imflea.zero.model.entity.qx.QxTenant;
import com.imflea.zero.model.entity.qx.dto.TenantPageQueryDto;
import com.imflea.zero.model.entity.qx.vo.TenantDetailVo;
import com.imflea.zero.service.cms.ICmsCommonContentService;
import com.imflea.zero.service.qx.IQxTenantService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 租户表 服务实现类
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-08-24
 */
@Service
public class QxTenantServiceImpl extends ServiceImpl<QxTenantMapper, QxTenant> implements IQxTenantService {
    @Autowired
    private ICmsCommonContentService contentService;

    @Override
    public QxTenant saveOrupdateTenant(QxTenant tenant) {
        boolean result = false;
        String id = tenant.getId();
        if (CommonUtils.isNull(id)) {
            id =CommonUtils.generateRandomString();
            tenant.setTenantId(id);
            tenant.setId(id);
            tenant.setStatus(QxContant.getTenantStatusOpen());
            result = this.saveOrUpdate(tenant);
        } else {
            result = this.updateById(tenant);
        }
        if (result) {
            return tenant;

        } else {
            throw new BizException("租户信息更新失败");
        }

    }

    @Override
    public PageInfo<QxTenant> queryTenant4Page(TenantPageQueryDto pageQueryReq) {

        Integer pageIndex = pageQueryReq.getPageNum();
        Integer pageSize = pageQueryReq.getPageSize();
        String status = pageQueryReq.getStatus();
        String tenantName = pageQueryReq.getTenantName();
        QueryWrapper<QxTenant> qxTenantQueryWrapper = new QueryWrapper<>();
        if (!CommonUtils.isNull(status)) {
            qxTenantQueryWrapper.eq("status", status);
        }
        if (!CommonUtils.isNull(tenantName)) {
            qxTenantQueryWrapper.like("tenant_name", tenantName);
        }
        PageHelper.startPage(pageIndex, pageSize);
        List<QxTenant> tenants = this.list(qxTenantQueryWrapper);
        PageInfo<QxTenant> pageResult = new PageInfo<>(tenants);
        return pageResult;
    }

    @Override
    public Boolean updateTenantStatusById(String id, String closeStatus) {
        UpdateWrapper<QxTenant> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", closeStatus);
        updateWrapper.eq("id", id);
        boolean result = this.update(updateWrapper);
        return result;
    }


    @Override
    public CmsCommonContent saveOrUpdateContent(CmsCommonContent tenantContent) {
        boolean result = false;
        String bizId = tenantContent.getBizId();
        if (CommonUtils.isNull(bizId)) {
            throw new BizException("企业信息id为空，请确认");
        }

        String id = tenantContent.getId();
        if (CommonUtils.isNull(id)) {
            id =CommonUtils.generateRandomString();
            tenantContent.setTenantId(id);
            tenantContent.setId(id);
            contentService.saveOrUpdateCmsCommonContent(tenantContent);
            result = true;
        } else {
            contentService.saveOrUpdateCmsCommonContent(tenantContent);
            result = true;
        }
        if (result) {
            return tenantContent;
        } else {
            throw new BizException("企业信息简介操作失败");
        }
    }

    @Override
    public TenantDetailVo queryDetailById(String id) {
        QxTenant tenant = this.getById(id);
        CmsCommonContent content = contentService.queryCmsCommonContentByBizId(id);
        TenantDetailVo vo = new TenantDetailVo();
        vo.setContent(content);
        vo.setTenant(tenant);
        return vo;
    }
}
