//package com.imflea.zero;
//
//import com.alibaba.fastjson.JSONObject;
//import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
//import com.baomidou.mybatisplus.core.toolkit.StringPool;
//import com.baomidou.mybatisplus.core.toolkit.StringUtils;
//import com.baomidou.mybatisplus.generator.AutoGenerator;
//import com.baomidou.mybatisplus.generator.config.InjectionConfig;
//import com.baomidou.mybatisplus.generator.config.*;
//import com.baomidou.mybatisplus.generator.config.po.TableInfo;
//import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
//import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
//import freemarker.template.Configuration;
//import freemarker.template.Template;
//import freemarker.template.TemplateExceptionHandler;
//import lombok.Data;
//
//import java.io.*;
//import java.sql.*;
//import java.util.*;
//
///**
// * <p>
// * mysql 代码生成器演示例子
// * </p>
// *
// * @author jobob
// * @since 2018-09-12
// */
//public class SpringBootGenerator {
//
//    /**
//     * <p>
//     * 读取控制台内容
//     * </p>
//     */
//    public static String scanner(String tip) {
//        @SuppressWarnings("resource")
//        Scanner scanner = new Scanner(System.in);
//        StringBuilder help = new StringBuilder();
//        help.append("请输入" + tip + "：");
//        System.out.println(help.toString());
//        if (scanner.hasNext()) {
//            String ipt = scanner.next();
//            if (StringUtils.isNotBlank(ipt)) {
//                return ipt;
//            }
//        }
//        throw new MybatisPlusException("请输入正确的" + tip + "！");
//    }
//
//    /**
//     * RUN THIS
//     */
//    public static void main(String[] args) throws Exception {
//        // 代码生成器
//        AutoGenerator mpg = new AutoGenerator();
//
//
//        // 全局配置
//        GlobalConfig gc = new GlobalConfig();
//        gc.setSwagger2(true);
//        String projectBasePath = System.getProperty("user.dir");
//        String projectPath = projectBasePath + "/farm-plus-base-service";
//        gc.setOutputDir(projectPath + "/src/main/java");
//
//        gc.setAuthor("xby");
////        gc.setAuthor("liyingbo");
//        gc.setOpen(false);
//        gc.setFileOverride(false);// 是否覆盖同名文件，默认是false
//        gc.setActiveRecord(true);// 不需要ActiveRecord特性的请改为false
//        gc.setEnableCache(false);// XML 二级缓存
//        gc.setBaseResultMap(true);// XML ResultMap
//        gc.setBaseColumnList(true);// XML columList
//        /* 自定义文件命名，注意 %s 会自动填充表实体属性！ */
//        // gc.setMapperName("%sDao");
//        // gc.setXmlName("%sDao");
//        // gc.setServiceName("MP%sService");
//        // gc.setServiceImplName("%sServiceDiy");
//        // gc.setControllerName("%sAction")
//        mpg.setGlobalConfig(gc);
//
//        // 数据源配置
//        DataSourceConfig dsc = new DataSourceConfig();
////        dsc.setUrl("jdbc:mysql://10.2.57.153:3306/farmplus?useUnicode=true&characterEncoding=utf-8");
//        dsc.setUrl("jdbc:mysql://101.200.160.238:4406/farmplus_test?useUnicode=true&characterEncoding=utf-8");
//        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
//        dsc.setUsername("root");
//        dsc.setPassword("root");
//        dsc.setPassword("Css@2021_0908");
//
//        mpg.setDataSource(dsc);
//
//        // 包配置
//        PackageConfig pc = new PackageConfig();
//        String moduleName = scanner("模块名");
////        pc.setModuleName(scanner("模块名"));
////        pc.setParent("com.baomidou.mybatisplus.samples.generator");
//
//        pc.setParent("com.css.farmplus");
////        pc.setEntity("model.entity");
////        pc.setMapper("model.dao");
//        pc.setEntity("model.entity." + moduleName);
//        pc.setMapper("model.dao." + moduleName);
//        pc.setService("service." + moduleName);
//        pc.setServiceImpl("service." + moduleName + ".impl");
//        pc.setController("controller");
//        pc.setXml("classpath*:mapper");
//
//
//        mpg.setPackageInfo(pc);
//
//
//        // 自定义配置
//        InjectionConfig cfg = new InjectionConfig() ;
//        List<FileOutConfig> focList = new ArrayList<>();
//        focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                // 自定义输入文件名称
//                return projectPath + "/src/main/resources/mapper/" + moduleName + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
//            }
//        });
//        cfg.setFileOutConfigList(focList);
//        mpg.setCfg(cfg);
//        TemplateConfig templateConfig = new TemplateConfig();
//
//        //控制 不生成 controller
////        templateConfig.setController("");
//        templateConfig.setXml(null);
//        mpg.setTemplate(templateConfig);
//
//        // 策略配置
//        StrategyConfig strategy = new StrategyConfig();
//        strategy.setNaming(NamingStrategy.underline_to_camel);
//        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
////        strategy.setSuperEntityClass("com.baomidou.mybatisplus.samples.generator.common.BaseEntity");
//        strategy.setEntityLombokModel(true);
////        strategy.setSuperControllerClass("com.baomidou.mybatisplus.samples.generator.common.BaseController");
//
//        String[] tableNames = scanner("表名，多个英文逗号分割").split(",");
//
//        strategy.setInclude(tableNames);
////        strategy.setSuperEntityColumns("id");
//        strategy.setControllerMappingHyphenStyle(true);
//        strategy.setTablePrefix(pc.getModuleName() + "_");
//        mpg.setStrategy(strategy);
//        // 选择 freemarker 引擎需要指定如下加，注意 pom 依赖必须有！
//        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
//        mpg.execute();
//
//        String dtoFilePath = projectPath + "/src/main/java/com/css/farmplus" +
//                "/model/entity/" + moduleName + "/dto";
//        SpringBootGenerator springBootGenerator = new SpringBootGenerator();
//        for (String tableName : tableNames) {
//            List<Attr> attrList = springBootGenerator.queryTable(tableName);
////            String entityTotalPath = entityPkgPath + "." + camelName(tableName);
//            springBootGenerator.generateDto(
//                    "com.imflea.zero.model.entity." + moduleName + ".dto",
//                    dtoFilePath,
//                    tableName,
//                    attrList);
//        }
//
//    }
//
//    private void generateDto(String pkgName, String dtoFilePath, String tableName, List<Attr> attrList) throws Exception {
//        Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
//        String projectBasePath = System.getProperty("user.dir");
//        String projectTemplatePath = projectBasePath
//                + "/farm-plus-manager"
//                + "/src/main/resources/templates";
//        cfg.setDirectoryForTemplateLoading(new File(projectTemplatePath));
//        cfg.setDefaultEncoding("UTF-8");
//        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
//
//        Template temp = cfg.getTemplate("dto.java.ftl");
//
//        Map<String, Object> root = new HashMap<>();
////        root.put("packageName", "com.ricky.java");
//        root.put("packageName", pkgName);
//        root.put("className", bigCamelName(tableName) + "Dto");
//        root.put("author", "xby");
//
//        List<Map<String, Object>> listMap = new ArrayList<>();
//        for (Attr attr : attrList) {
//            Map<String, Object> map = JSONObject.parseObject(JSONObject.toJSONString(attr));
//            listMap.add(map);
//        }
//
//        root.put("attrs", listMap);
//
//        File dir = new File(dtoFilePath);
//        if (!dir.exists()) {
//            dir.mkdirs();
//        }
//        OutputStream fos = new FileOutputStream(new File(dir, bigCamelName(tableName) + "Dto.java")); //java文件的生成目录
//        Writer out = new OutputStreamWriter(fos);
//        temp.process(root, out);
//
//        fos.flush();
//        fos.close();
//
//    }
//
//    @Data
//    private class Attr {
//        String type;
//        String name;
//        String desc;
//    }
//
//    private static String smallCamelName(String name) {
//        StringBuilder result = new StringBuilder();
//        // 快速检查
//        if (name == null || name.isEmpty()) {
//            // 没必要转换
//            return "";
//        } else if (!name.contains("_")) {
//            // 不含下划线，仅将首字母小写
//            return name.substring(0, 1).toLowerCase() + name.substring(1);
//        }
//        // 用下划线将原始字符串分割
//        String camels[] = name.split("_");
//        for (String camel : camels) {
//            // 跳过原始字符串中开头、结尾的下换线或双重下划线
//            if (camel.isEmpty()) {
//                continue;
//            }
//            // 处理真正的驼峰片段
//            if (result.length() == 0) {
//                // 第一个驼峰片段，全部字母都小写
//                result.append(camel.toLowerCase());
//            } else {
//                // 其他的驼峰片段，首字母大写
//                result.append(camel.substring(0, 1).toUpperCase());
//                result.append(camel.substring(1).toLowerCase());
//            }
//        }
//        return result.toString();
//    }
//
//    private static String bigCamelName(String name) {
//        StringBuilder result = new StringBuilder();
//        // 快速检查
//        if (name == null || name.isEmpty()) {
//            // 没必要转换
//            return "";
//        } else if (!name.contains("_")) {
//            // 不含下划线，仅将首字母小写
//            return name.substring(0, 1).toUpperCase() + name.substring(1);
//        }
//        // 用下划线将原始字符串分割
//        String camels[] = name.split("_");
//        for (String camel : camels) {
//            // 跳过原始字符串中开头、结尾的下换线或双重下划线
//            if (camel.isEmpty()) {
//                continue;
//            }
//            // 处理真正的驼峰片段
//            result.append(camel.substring(0, 1).toUpperCase());
//            result.append(camel.substring(1).toLowerCase());
//        }
//        return result.toString();
//    }
//
//    private List<Attr> queryTable(String tableName) {
//        try {
//            // 加载数据库驱动，注册到驱动管理器
//            Class.forName("com.mysql.jdbc.Driver");
//            // 数据库连接字符串
//            String url = "jdbc:mysql://101.200.160.238:4406/farmplus?useUnicode=true&characterEncoding=utf-8";
//            // 数据库用户名
//            String username = "root";
//            // 数据库密码
//            String password = "Css@2021_0908";
//            // 创建Connection连接
//            Connection conn = DriverManager.getConnection(url, username, password);
//            // 获取Statement
//            Statement stmt = conn.createStatement();
//            // 添加图书信息的SQL语句
//            String sql = "SELECT COLUMN_NAME,COLUMN_TYPE,COLUMN_COMMENT FROM information_schema.`COLUMNS` " +
//                    "WHERE TABLE_NAME = '" + tableName + "'";
//            // 执行查询
//            ResultSet rs = stmt.executeQuery(sql);
//            // 实例化List对象
//            List<Attr> list = new ArrayList<>();
//            // 判断光标向后移动，并判断是否有效
//            while (rs.next()) {
//                // 实例化Book对象
//                Attr attr = new Attr();
//                // 对id属性赋值
//                attr.setName(smallCamelName(rs.getString("COLUMN_NAME")));
//                // 对name属性赋值
//                String oriType = rs.getString("COLUMN_TYPE");
//                String type = "";
//                if (oriType.contains("varchar")) {
//                    type = "String";
//                }
//                if (oriType.contains("int")) {
//                    type = "Integer";
//                }
//                if (oriType.contains("datetime")) {
//                    type = "LocalDateTime";
//                }
//                attr.setType(type);
//                // 对price属性赋值
//                attr.setDesc(rs.getString("COLUMN_COMMENT"));
//                // 将图书对象添加到集合中
//                list.add(attr);
//            }
//            // 将图书集合放置到request之中
//            rs.close();        // 关闭ResultSet
//            stmt.close();    // 关闭Statement
//            conn.close();    // 关闭Connection
//            return list;
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//}
