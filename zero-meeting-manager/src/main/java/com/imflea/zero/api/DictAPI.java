package com.imflea.zero.api;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.PCodeDto;
import com.imflea.zero.model.entity.qx.BaseDict;
import com.imflea.zero.service.qx.IBaseDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.api
 * @ClassName: DictAPI
 * @Description
 * @date 2021-08-25  14:10:46
 */
@RestController
@RequestMapping("/api/dictAPI")
@Api(value = "字典API-基础公共api", tags = "基础公共api")
public class DictAPI {
    @Autowired
    private IBaseDictService baseDictService;


    @ApiOperation(value = "根据pcode获取字典子项", notes = "根据pcode获取字典子项")
    @PostMapping("/queryByPcode")
    @ApiOperationSupport(order = 1)
    public JsonResult<List<BaseDict>> queryByPcode(@RequestBody PCodeDto pCodeReq) {
        String pcode = pCodeReq.getPcode();
        return new JsonResult<>(ZeroContant.getSuccessCode(), baseDictService.queryByPcode(pcode));
    }

}
