package com.imflea.zero.api;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.PidDto;
import com.imflea.zero.model.entity.qx.BaseArea;
import com.imflea.zero.service.qx.IBaseAreaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.api
 * @ClassName: AreaAPI
 * @Description
 * @date 2021-08-24  16:51:28
 */
@RestController
@RequestMapping("/api/areaAPI")
@Api(value = "地区字典库-基础公共api", tags = "基础公共api")
public class AreaAPI {

    @Autowired
    private IBaseAreaService baseAreaService;


    @ApiOperation(value = "根据parentId获取地区信息，parendId 为空则为省份", notes = "根据parentId获取地区信息")
    @PostMapping("/queryByParendId")
    @ApiOperationSupport(order = 1)
    public JsonResult<List<BaseArea>> queryByParendId(@RequestBody PidDto pidReq) {

        String parendId = pidReq.getPid();

        JsonResult<List<BaseArea>> areas  = new JsonResult<>();
        try {
            areas.setInfo(baseAreaService.queryByParendId(parendId));
            areas.setCode(ZeroContant.getSuccessCode());
        } catch (Exception e) {
            areas.setCode(ZeroContant.getFailCode());
            areas.setMsg(e.getMessage());
        }
        return areas;
    }




}
