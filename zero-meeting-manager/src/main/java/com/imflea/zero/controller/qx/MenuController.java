package com.imflea.zero.controller.qx;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.dto.PidDto;
import com.imflea.zero.model.entity.qx.QxMenu;
import com.imflea.zero.service.qx.IQxMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: MenuController
 * @Description
 * @date 2021-08-23  10:01:49
 */
@RestController
@RequestMapping("/qx/menu")
@Api(value = "菜单管理", tags = "菜单管理-权限部分")
public class MenuController {
    @Autowired
    private IQxMenuService menuService;

    @PostMapping("/saveOrUpdateMenu")
    @ApiOperation(value = "菜单保存与修改接口", notes = "菜单管理-权限管理")
    @ApiOperationSupport(order = 1)
    public JsonResult<QxMenu> saveOrUpdateMenu(@RequestBody QxMenu qxMenu) {

        return new JsonResult<>(ZeroContant.getSuccessCode(), this.menuService.saveOrUpdateMenu(qxMenu));

    }

    @PostMapping("/removeMenuById")
    @ApiOperation(value = "根据id删除菜单", notes = "删除菜单-权限管理")
    @ApiOperationSupport(order = 2)
    public JsonResult<Boolean> removeMenuById(@RequestBody IdDto idReq) {

        String id = idReq.getId();
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.menuService.removeMenuById(id));

    }

    @PostMapping("/updateMenuStatusByIds2Close")
    @ApiOperation(value = "菜单批量禁用", notes = "菜单-禁用-权限管理")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> updateMenuStatusByIds2Close(@RequestBody IdsDto idsReq) {
        String status = QxContant.getMenuCloseStatus();
        List<String> ids = idsReq.getIds();
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.menuService.updateMenuStatus(ids, status));
    }

    @PostMapping("/updateMenuStatusByIds2Open")
    @ApiOperation(value = "菜单批量启用", notes = "菜单-启用-权限管理")
    @ApiOperationSupport(order = 4)
    public JsonResult<Boolean> updateMenuStatusByIds2Open(@RequestBody IdsDto idsReq) {
        String status = QxContant.getMenuOpenStatus();
        List<String> ids = idsReq.getIds();

        return new JsonResult<>(ZeroContant.getSuccessCode(), this.menuService.updateMenuStatus(ids, status));

    }

    @ApiOperation(value = "根据菜单获取下一级：无权限（全部下一级）", notes = "根据菜单获取下一级-权限管理")
    @PostMapping("/queryMenuByPid")
    @ApiOperationSupport(order = 5)
    public JsonResult<List<QxMenu>> queryMenuByPid(@RequestBody PidDto pidReq) {
        String menuPid = pidReq.getPid();
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.menuService.queryByParentId(menuPid));
    }

    @ApiOperation(value = "获取全量已打开菜单树", notes = "获取全量菜单树-已打开状态-权限管理")
    @PostMapping("/queryMenuTree")
    @ApiOperationSupport(order = 6)
    public JsonResult<List<Map<String, Object>>> queryMenuTree() {
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.menuService.queryMenuTree());
    }

    @ApiOperation(value = "获取全量菜单树", notes = "获取全量菜单树-权限管理")
    @PostMapping("/queryMenuTreeNoStatus")
    @ApiOperationSupport(order = 7)
    public JsonResult<List<Map<String, Object>>> queryMenuTreeNoStatus() {
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.menuService.queryMenuTreeNoStatus());
    }


}
