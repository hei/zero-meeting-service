package com.imflea.zero.controller.qx;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.qx.QxMenu;
import com.imflea.zero.model.entity.qx.dto.RoleMenuRelationDto;
import com.imflea.zero.service.qx.IQxRoleMenuService;
import com.imflea.zero.utils.SessionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: RoleMenuController
 * @Description
 * @date 2021-08-23  10:40:06
 */
@RestController
@RequestMapping("/qx/roleMenu")
@Api(value = "角色与菜单授权管理", tags = "角色菜单关联关系-权限管理")
public class RoleMenuController {
    @Autowired
    private IQxRoleMenuService roleMenuService;

    @ApiOperation(value = "角色菜单授权", notes = "角色菜单关联-权限管理")
    @PostMapping("/saveRoleMenuRelation")
    @ApiOperationSupport(order = 1)
    public JsonResult<Boolean> saveRoleMenuRelation(@RequestBody RoleMenuRelationDto roleMenuRelationReq) {
        String roleId = roleMenuRelationReq.getRoleId();
        List<String> menuIds = roleMenuRelationReq.getMenuIds();
        return new JsonResult(ZeroContant.getSuccessCode(), this.roleMenuService.saveRoleMenuRelations(roleId, menuIds));

    }

    @ApiOperation(value = "获取当前登录人的所有菜单-权限管理", notes = "获取当前登录人的所有菜单-权限管理")
    @PostMapping("/queryAuthMenu")
    @ApiOperationSupport(order = 2)
    public JsonResult<List<Map<String, Object>>> queryRoleMenus() {
        String currentUserId = SessionUtils.getUserId();

        return new JsonResult<>(ZeroContant.getSuccessCode(), this.roleMenuService.queryMenusByUserId(currentUserId));

    }

    @ApiOperation(value = "根据角色获取授权菜单-权限管理", notes = "根据角色获取授权菜单-权限管理")
    @PostMapping("/queryRoleMenuByRoleId")
    @ApiOperationSupport(order = 3)
    public JsonResult<List<QxMenu>> queryRoleMenuByRoleId(@RequestBody IdDto idReq) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.roleMenuService.queryRoleMenuByRoleId(idReq.getId()));

    }

}
