package com.imflea.zero.controller.qx;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.utils.SessionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: LogoutController
 * @Description
 * @date 2021-08-23  14:27:04
 */
@RestController
@RequestMapping("/qx/token/")
@Api(value = "退出", tags = "认证相关")
public class LogoutController {
//    @Autowired
//    private IQxUserInfoService userInfoService;


    @ApiOperation(value = "退出登录", notes = "退出登录")
    @PostMapping("/logout")
    @ApiOperationSupport(order = 1)
    public JsonResult<Boolean> logout() {
        String accessToken = SessionUtils.getAccessToken();
        if (CommonUtils.isNull(accessToken)) {
            return new JsonResult<>(ZeroContant.getSuccessCode(), true);
        } else {
            try {
               ZeroCacheUtils.deleteKey(accessToken);
                return new JsonResult<>(ZeroContant.getSuccessCode(), true);
            } catch (Exception e) {
                e.printStackTrace();
                return new JsonResult<>(ZeroContant.getFailCode(), e.getMessage());
            }
        }
    }


}
