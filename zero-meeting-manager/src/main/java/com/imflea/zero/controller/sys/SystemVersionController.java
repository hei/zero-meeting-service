package com.imflea.zero.controller.sys;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.entity.system.SystemVersion;
import com.imflea.zero.service.system.ISystemVersionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;

/**
 * <p>
 * 系统发布版本 前端控制器
 * </p>
 *
 * @author xby
 * @PackageName: com.imflea.zero.controller
 * @ClassName: SystemVersionController
 * @Description
 * @since 2022-04-06
 */
@RestController
@Api(value = "系统发布版本管理", tags = "系统发布版本管理")
@RequestMapping("/sys/version")
public class SystemVersionController {

    @Autowired
    private ISystemVersionService systemVersionService;

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改系统发布版本接口")
    @ApiOperationSupport(order = 1)
    public JsonResult<SystemVersion> save(@RequestBody SystemVersion systemVersion) {
        SystemVersion entity = systemVersionService.saveOrUpdateSystemVersion(systemVersion);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询系统发布版本列表")
    @ApiOperationSupport(order = 2)
    public JsonResult<List<SystemVersion>> list() {
        List<SystemVersion> list = systemVersionService.list();
        if (!CommonUtils.isNull(list)) {
            list.sort(Comparator.comparing(SystemVersion::getPostTime).reversed());
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), list);
    }

}
