package com.imflea.zero.controller.task;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.task.ScheduleTask;
import com.imflea.zero.model.entity.task.dto.ScheduleTaskDto;
import com.imflea.zero.service.task.IScheduleTaskService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 定时任务配置表 前端控制器
 * </p>
 *
 * @author zhaoyang
 * @PackageName: com.imflea.zero.controller
 * @ClassName: ScheduleTaskController
 * @Description
 * @since 2022-01-14
 */
@RestController
@Api(value = "定时任务配置表管理", tags = "定时任务配置表管理")
@RequestMapping("/schedule/task")
public class ScheduleTaskController {

    @Autowired
    private IScheduleTaskService scheduleTaskService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<ScheduleTask> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        ScheduleTask entity = scheduleTaskService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改定时任务配置表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<ScheduleTask> save(@RequestBody ScheduleTask scheduleTask) {
        ScheduleTask entity = scheduleTaskService.saveOrUpdateScheduleTask(scheduleTask);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/updateJobToOpen")
    @ApiOperation(value = "开启任务")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> updateJobToOpen(@RequestBody IdDto taskId) {
        String status = "1";
        Boolean result = scheduleTaskService.updateJobStatus(taskId, status);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "任务启动成功！", result);
    }

    @PostMapping("/updateJobToClose")
    @ApiOperation(value = "关闭任务")
    @ApiOperationSupport(order = 4)
    public JsonResult<Boolean> updateJobToClose(@RequestBody IdDto taskId) {
        String status = "0";
        Boolean result = scheduleTaskService.updateJobStatus(taskId, status);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "任务启动成功！", result);
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询定时任务配置表分页列表")
    @ApiOperationSupport(order = 5)
    public JsonResult<PageInfo<ScheduleTask>> queryPage(@RequestBody ScheduleTaskDto scheduleTaskDto) {
        PageInfo<ScheduleTask> pageInfo = scheduleTaskService.queryPage(scheduleTaskDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
