package com.imflea.zero.controller.invoice;


import com.imflea.zero.model.entity.invoice.dto.SaveDto;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.imflea.zero.model.entity.invoice.InvoiceHeader;
import com.imflea.zero.model.entity.invoice.dto.InvoiceHeaderDto;
import com.imflea.zero.service.invoice.IInvoiceHeaderService;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.dto.IdDto;
import java.util.List;

/**
 * <p>
 * 发表抬头 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 * @PackageName: com.imflea.zero.controller
 * @ClassName: InvoiceHeaderController
 * @Description
 */
@RestController
@Api(value = "发表抬头管理", tags = "发表抬头管理")
@RequestMapping("/invoice/header")
public class InvoiceHeaderController {

    @Autowired
    private IInvoiceHeaderService invoiceHeaderService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<InvoiceHeader> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        InvoiceHeader entity = invoiceHeaderService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改发表抬头接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<InvoiceHeader> save(@RequestBody SaveDto saveDto) {
        InvoiceHeader entity = invoiceHeaderService.saveOrUpdateInvoiceHeader(saveDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除发表抬头")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        invoiceHeaderService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询发表抬头分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<InvoiceHeader>> queryPage(@RequestBody InvoiceHeaderDto invoiceHeaderDto) {
        PageInfo<InvoiceHeader> pageInfo = invoiceHeaderService.queryPage(invoiceHeaderDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
