package com.imflea.zero.controller.qx;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.CodeDto;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.NameDto;
import com.imflea.zero.dto.PCodeDto;
import com.imflea.zero.model.entity.qx.BaseDict;
import com.imflea.zero.service.qx.IBaseDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: DictController
 * @Description
 * @date 2021-08-25  14:18:27
 */
@RestController
@RequestMapping("/qx/dict")
@Api(value = "字典管理-公共部分", tags = "字典管理-公共部分")
public class DictController {
    @Autowired
    private IBaseDictService baseDictService;

    @PostMapping("/saveOrUpdateBaseDict")
    @ApiOperation(value = "字典保存与修改", notes = "字典保存与修改")
    @ApiOperationSupport(order = 1)
    public JsonResult<BaseDict> saveOrUpdateBaseDict(@RequestBody BaseDict baseDict) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.baseDictService.saveOrUpdateBaseDict(baseDict));

    }

    @PostMapping("/queryDictByCode4Validate")
    @ApiOperation(value = "校验字典code是否已经存在:true表示code不存在，false：code已经存在", notes = "校验字典code是否已经存在")
    @ApiOperationSupport(order = 2)
    public JsonResult<Boolean> queryDictByCode4Validate(@RequestBody CodeDto codeReq) {

        String code = codeReq.getCode();
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.baseDictService.queryDictByCode4Validate(code));


    }

    @PostMapping("/removeById")
    @ApiOperation(value = "根据id删除字典项", notes = "根据id删除字典项")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> removeById(@RequestBody IdDto idReq) {
        String id = idReq.getId();
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.baseDictService.removeBaseDictById(id));

    }

    @PostMapping("/queryRootDict")
    @ApiOperation(value = "查询字典根下的字典项，可根据字典名模糊查询", notes = "查询字典根下的字典项，可根据字典名模糊查询")
    @ApiOperationSupport(order = 4)
    public JsonResult<List<BaseDict>> queryRootDict(@RequestBody NameDto nameReq) {
        String name = nameReq.getName();
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.baseDictService.queryRootDict(name));

    }

    @ApiOperation(value = "根据pcode获取字典子项", notes = "根据pcode获取字典子项")
    @PostMapping("/queryByPcode")
    @ApiOperationSupport(order = 5)
    public JsonResult<List<BaseDict>> queryByPcode(@RequestBody PCodeDto pCodeReq) {
        String pcode = pCodeReq.getPcode();
        return new JsonResult<>(ZeroContant.getSuccessCode(), baseDictService.queryByPcode(pcode));
    }


}
