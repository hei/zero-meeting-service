package com.imflea.zero.controller.qx;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.qx.QxNotice;
import com.imflea.zero.model.entity.qx.dto.QxNoticeDto;
import com.imflea.zero.model.entity.qx.dto.QxNoticeSaveDto;
import com.imflea.zero.model.entity.qx.vo.QxNoticeDetailVo;
import com.imflea.zero.service.qx.IQxNoticeService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 系统通知 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: QxNoticeController
 * @Description
 * @since 2021-09-22
 */
@RestController
@Api(value = "系统通知管理", tags = "系统通知管理")
@RequestMapping("/qx/notice")
public class QxNoticeController {

    @Autowired
    private IQxNoticeService qxNoticeService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<QxNotice> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        QxNotice entity = qxNoticeService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改系统通知接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<QxNoticeDetailVo> save(@RequestBody QxNoticeSaveDto qxNoticeSaveDto) {
        QxNoticeDetailVo entity = qxNoticeService.saveOrUpdateQxNotice(qxNoticeSaveDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除系统通知")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        qxNoticeService.removeBatchByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询系统通知分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<QxNotice>> queryPage(@RequestBody QxNoticeDto qxNoticeDto) {
        PageInfo<QxNotice> pageInfo = qxNoticeService.queryPage(qxNoticeDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/queryDetailById")
    @ApiOperation(value = "根据ID获取通知详细信息")
    @ApiOperationSupport(order = 5)
    public JsonResult<QxNoticeDetailVo> queryDetailById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        QxNoticeDetailVo entity = qxNoticeService.queryDetailById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/updateStatus2Post")
    @ApiOperation(value = "修改通知状态，通知发布")
    @ApiOperationSupport(order = 5)
    public JsonResult<Boolean> updateStatus2Post(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        String status = QxContant.getNoticeStatusPost();
        boolean result = qxNoticeService.updateStatus(id, status, true);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }

    @PostMapping("/updateStatus2UnPost")
    @ApiOperation(value = "修改通知状态，取消发布")
    @ApiOperationSupport(order = 6)
    public JsonResult<Boolean> updateStatus2UnPost(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        String status = QxContant.getNoticeStatusUnpost();
        boolean result = qxNoticeService.updateStatus(id, status, false);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", result);
    }


}
