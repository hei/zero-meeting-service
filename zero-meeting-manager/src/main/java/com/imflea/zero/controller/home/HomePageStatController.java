package com.imflea.zero.controller.home;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.service.meeting.IMeetingOrderInfoService;
import com.imflea.zero.service.user.IWebUserInfoService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.adopt
 * @ClassName: HomePageStatController
 * @Description
 * @date 2021-12-28  13:51:59
 */
@RestController
@Api(value = "首页-统计", tags = "首页-统计")
@RequestMapping("/home/stat")
public class HomePageStatController {
    @Autowired
    private IMeetingOrderInfoService meetingOrderInfoService;
    @Autowired
    private IWebUserInfoService webUserInfoService;

    @PostMapping("/roomOrderInfo")
    @ApiOperation(value = "统计-空间订单信息")
    @ApiOperationSupport(order = 1)
    public JsonResult<Map<String, Object>> queryOrderStatistics() {
        Map<String, Object> map = meetingOrderInfoService.query4Stat("1");
        return new JsonResult<>(ZeroContant.getSuccessCode(), map);
    }

    @PostMapping("/chargeOrderInfo")
    @ApiOperation(value = "统计-充值订单信息")
    @ApiOperationSupport(order = 2)
    public JsonResult<Map<String, Object>> chargeOrderInfo() {
        Map<String, Object> map = meetingOrderInfoService.query4Stat("2");
        return new JsonResult<>(ZeroContant.getSuccessCode(), map);
    }


    @PostMapping("/userInfo")
    @ApiOperation(value = "统计-统计用户信息")
    @ApiOperationSupport(order = 1)
    public JsonResult<Map<String, Object>> queryUserStatistics() {
        Map<String, Object> map = webUserInfoService.query4Stat();
        return new JsonResult<>(ZeroContant.getSuccessCode(), map);
    }
}
