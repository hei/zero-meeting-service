package com.imflea.zero.controller.pay;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.constant.PayConstant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.pay.PayTransportProof;
import com.imflea.zero.model.entity.pay.dto.OrderSnDto;
import com.imflea.zero.model.entity.pay.dto.PayTransportProofDto;
import com.imflea.zero.service.pay.IPayTransportProofService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 转账支付凭证 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: PayTransportProofController
 * @Description
 * @since 2021-09-27
 */
@RestController
@Api(value = "转账支付凭证管理", tags = "转账支付凭证管理")
@RequestMapping("/pay/proof")
public class PayTransportProofController {

    @Autowired
    private IPayTransportProofService payTransportProofService;


    @PostMapping("/save")
    @ApiOperation(value = "保存账支付凭证接口")
    @ApiOperationSupport(order = 1)
    public JsonResult<PayTransportProof> save(@RequestBody PayTransportProofDto payTransportProof) {
        payTransportProof.setProofType(PayConstant.PAY_RECODER_PROOF_TYPE_RECEIVE.getCode());
        PayTransportProof entity = payTransportProofService.savePayTransportProof(payTransportProof);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/queryByOrderSn")
    @ApiOperation(value = "根据订单号查询转账支付证明")
    @ApiOperationSupport(order = 2)
    public JsonResult<List<PayTransportProof>> queryBySn(@RequestBody OrderSnDto orderSnDto) {
        String orderSn = orderSnDto.getOrderSn();
        List<PayTransportProof> entity = payTransportProofService.getByOrderSn(orderSn);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

}
