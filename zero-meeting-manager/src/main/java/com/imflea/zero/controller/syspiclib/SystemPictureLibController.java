package com.imflea.zero.controller.syspiclib;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.entity.systempiclib.dto.QueryListDto;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.imflea.zero.model.entity.systempiclib.SystemPictureLib;
import com.imflea.zero.model.entity.systempiclib.dto.SystemPictureLibDto;
import com.imflea.zero.service.systempiclib.ISystemPictureLibService;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.dto.IdDto;
import java.util.List;

/**
 * <p>
 * 系统图库 前端控制器
 * </p>
 *
 * @author guohui
 * @since 2021-10-28
 * @PackageName: com.imflea.zero.controller
 * @ClassName: SystemPictureLibController
 * @Description
 */
@RestController
@Api(value = "系统图库管理", tags = "系统图库管理")
@RequestMapping("/system-picture-lib")
public class SystemPictureLibController {

    @Autowired
    private ISystemPictureLibService systemPictureLibService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<SystemPictureLib> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        SystemPictureLib entity = systemPictureLibService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改系统图库接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<SystemPictureLib> save(@RequestBody SystemPictureLib systemPictureLib) {
        SystemPictureLib entity = systemPictureLibService.saveOrUpdateSystemPictureLib(systemPictureLib);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除系统图库")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        systemPictureLibService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询系统图库分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<SystemPictureLib>> queryPage(@RequestBody SystemPictureLibDto systemPictureLibDto) {
        PageInfo<SystemPictureLib> pageInfo = systemPictureLibService.queryPage(systemPictureLibDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/list")
    @ApiOperation(value = "查询系统图库列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<List<SystemPictureLib>> queryList(@RequestBody QueryListDto queryListDto) {
        if (CommonUtils.isNull(queryListDto) ||CommonUtils.isNull(queryListDto.getBusinessType())){
            return new JsonResult<>(ZeroContant.getFailCode(), "参数为空");
        }
        QueryWrapper<SystemPictureLib> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_type",queryListDto.getBusinessType());
        queryWrapper.orderByAsc("sort");
        List<SystemPictureLib> result = systemPictureLibService.list(queryWrapper);
        return new JsonResult<>(ZeroContant.getSuccessCode(), result);
    }

}
