package com.imflea.zero.controller.qx;

import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.SearchPageDto;
import com.imflea.zero.model.entity.qx.QxUserInfo;
import com.imflea.zero.model.entity.qx.dto.UpdatePasswordDto;
import com.imflea.zero.model.entity.qx.vo.QxUserInfoVo;
import com.imflea.zero.service.qx.IQxUserInfoService;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.utils.SessionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: UserController
 * @Description
 * @date 2021-08-18  16:21:18
 */
@RestController
@RequestMapping("/qx/user")
@Api(value = "客户（用户）管理", tags = "用户管理-管理端权限")
public class UserController {
    @Autowired
    private IQxUserInfoService userInfoService;

    @ApiOperation(value = "客户基本信息保存与更新", notes = "客户基本信息保存与更新")
    @PostMapping("/saveOrUpdateUser")
    @ApiOperationSupport(order = 1)
    public JsonResult<QxUserInfo> saveOrUpdateUser(@RequestBody QxUserInfo userInfo) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.saveOrUpdateUser(userInfo));

    }

    @ApiOperation(value = "分页查询，提供当前租户下的所有用户", notes = "分页查询，提供当前租户下的所有用户")
    @PostMapping("/queryTenantUsers")
    @ApiOperationSupport(order = 2)
    public JsonResult<PageInfo<QxUserInfoVo>> queryUsers(@RequestBody SearchPageDto req) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.queryByTenantId4Page(req));
    }

    @ApiOperation(value = "冻结用户", notes = "冻结用户")
    @PostMapping("/lockAccount")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> updateUserStatus2Lock(@RequestBody IdDto req) {

        String lockStatus = QxContant.getLockStauts();
        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.updateUserStatusById(req.getId(), lockStatus));

    }

    @ApiOperation(value = "解冻用户", notes = "用户解锁")
    @PostMapping("/unlockAccount")
    @ApiOperationSupport(order = 4)
    public JsonResult<Boolean> updateUserStatus2UnLock(@RequestBody IdDto req) {

        String unLockStatus = QxContant.getUnlockStatus();
        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.updateUserStatusById(req.getId(), unLockStatus));

    }

    @ApiOperation(value = "删除用户", notes = "删除用户")
    @PostMapping("/removeById")
    @ApiOperationSupport(order = 5)
    public JsonResult<Boolean> removeById(@RequestBody IdDto req) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.removeUserById(req.getId()));

    }


    @ApiOperation(value = "修改密码", notes = "修改密码")
    @PostMapping("/updatePassword")
    @ApiOperationSupport(order = 6)
    public JsonResult<Boolean> updatePassword(@RequestBody UpdatePasswordDto updatePasswordReq) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.userInfoService.updateUserPassword(updatePasswordReq.getNewPassword(), updatePasswordReq.getOldPassword()));
        //待确认登录方式，以及用户的加密方式

    }

    @ApiOperation(value = "租户-客户基本信息保存与更新", notes = "租户-客户基本信息保存与更新")
    @PostMapping("/saveOrUpdateUser4Tenant")
    @ApiOperationSupport(order = 7)
    public JsonResult<QxUserInfo> saveOrUpdateUser4Tenant(@RequestBody QxUserInfo userInfo) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.saveOrUpdateUser4Tenant(userInfo));

    }

    @ApiOperation(value = "根据id获取用户信息-用户信息", notes = "用户信息")
    @PostMapping("/queryById")
    @ApiOperationSupport(order = 8)
    public JsonResult<QxUserInfo> queryById(@RequestBody IdDto req) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.queryById(req.getId()));

    }

    @ApiOperation(value = "重置密码", notes = "重置密码")
    @PostMapping("/restPassword")
    @ApiOperationSupport(order = 9)
    public JsonResult<Boolean> updatePassword2Def(@RequestBody IdDto req) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.updatePassword2Def(req.getId()));
    }

    @ApiOperation(value = "isMdadmin", notes = "isMdadmin")
    @PostMapping("/isMdadmin")
    @ApiOperationSupport(order = 10)
    public JsonResult<Boolean> isMdadmin() {
        return new JsonResult<>(ZeroContant.getSuccessCode(), SessionUtils.isMdAdmin());
    }
//    @ApiOperation(value = "handlePass", notes = "handlePass")
//    @PostMapping("/handlePass")
//    @ApiOperationSupport(order = 10)
//    public JsonResult<Boolean> handlePass() {
//        return new JsonResult<>(ZeroContant.getSuccessCode(), userInfoService.updatePasswordBatch());
//    }


}
