package com.imflea.zero.controller.pay;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.pay.PayRecorderInfo;
import com.imflea.zero.model.entity.pay.dto.OrderSnDto;
import com.imflea.zero.model.entity.pay.dto.PayRecorder4QueryDto;
import com.imflea.zero.service.pay.IPayRecorderInfoService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 支付记录 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: PayRecoderInfoController
 * @Description
 * @since 2021-09-27
 */
@RestController
@Api(value = "支付记录管理", tags = "支付记录管理")
@RequestMapping("/pay/recoderInfo")
public class PayRecoderInfoController {

    @Autowired
    private IPayRecorderInfoService payRecoderInfoService;

    @PostMapping("/queryByOrderSn")
    @ApiOperation(value = "根据订单号，查询支付信息")
    @ApiOperationSupport(order = 1)
    public JsonResult<PayRecorderInfo> queryByOrderSn(@RequestBody OrderSnDto orderSnDto) {
        String orderSn = orderSnDto.getOrderSn();
        PayRecorderInfo entity = payRecoderInfoService.getByOrderSn(orderSn);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询支付记录分页列表")
    @ApiOperationSupport(order = 2)
    public JsonResult<PageInfo<PayRecorderInfo>> queryPage(@RequestBody PayRecorder4QueryDto orderQueryDto) {
        PageInfo<PayRecorderInfo> pageInfo = payRecoderInfoService.queryPage(orderQueryDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/updatePayRecorderStatus2Finish")
    @ApiOperation(value = "根据交易记录id，确认支付状态：转账专用")
    @ApiOperationSupport(order = 2)
    public JsonResult<PayRecorderInfo> updatePayRecorderStatus2Finish(@RequestBody IdDto idDto) {
        String recoderId = idDto.getId();
        PayRecorderInfo entity = payRecoderInfoService.updatePayRecorderStatus2Finish(recoderId);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }


}
