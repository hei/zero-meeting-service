package com.imflea.zero.controller.task;

//import com.imflea.zero.service.adopt.impl.AdoptUnPayOrderTimeOutTask;

import com.imflea.zero.constant.AdoptConstant;
import com.imflea.zero.service.meeting.impl.MeetingUnPayOrderTimeOutTask;
import com.imflea.zero.service.task.IScheduledTaskJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.task
 * @ClassName: AdoptUnPayOrderTimeOutTasJob
 * @Description
 * @date 2022-01-14  13:15:21
 */
@Component("meetingUnPayOrderTimeOutTaskJob")
@Slf4j
public class MeetingUnPayOrderTimeOutTaskJob implements IScheduledTaskJobService {
    @Autowired
    private MeetingUnPayOrderTimeOutTask meetingUnPayOrderTimeOutTask;

    @Override
    public void run() {
        log.error("订单取消任务");
        meetingUnPayOrderTimeOutTask.cancel(AdoptConstant.MEETING_ORDER_TIME_OUT_SQU.getCode());
    }
}
