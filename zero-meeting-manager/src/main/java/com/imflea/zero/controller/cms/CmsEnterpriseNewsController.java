package com.imflea.zero.controller.cms;


import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.cms.CmsEnterpriseNews;
import com.imflea.zero.model.entity.cms.dto.CmsEnterpriseNewsDto;
import com.imflea.zero.service.cms.ICmsEnterpriseNewsService;
import com.imflea.zero.utils.RestUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 企业资讯 前端控制器
 * </p>
 *
 * @author zhaoyang
 * @PackageName: com.imflea.zero.controller
 * @ClassName: CmsEnterpriseNewsController
 * @Description
 * @since 2021-09-24
 */
@RestController
@Api(value = "内容发布-企业资讯", tags = "内容发布-企业资讯")
@RequestMapping("/cms/enterprise/news")
public class CmsEnterpriseNewsController {

    @Autowired
    private ICmsEnterpriseNewsService cmsEnterpriseNewsService;

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改企业资讯接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<CmsEnterpriseNews> save(@RequestBody CmsEnterpriseNews cmsEnterpriseNews) {
        CmsEnterpriseNews entity = cmsEnterpriseNewsService.saveOrUpdateCmsEnterpriseNews(cmsEnterpriseNews);
        return RestUtil.success(entity);
    }

    @PostMapping("/publish")
    @ApiOperation(value = "新闻发布")
    @ApiOperationSupport(order = 2)
    public JsonResult<Boolean> publish(@RequestBody CmsEnterpriseNewsDto dto) {
        Boolean success = cmsEnterpriseNewsService.updatePublishStatus(dto);
        return RestUtil.success(success);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除企业资讯")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        boolean success = cmsEnterpriseNewsService.removeEntity(ids);
        return RestUtil.success(success);
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询企业资讯分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<CmsEnterpriseNews>> queryPage(@RequestBody CmsEnterpriseNewsDto cmsEnterpriseNewsDto) {
        PageInfo<CmsEnterpriseNews> pageInfo = cmsEnterpriseNewsService.queryPage(cmsEnterpriseNewsDto);
        return RestUtil.success(pageInfo);
    }

}
