package com.imflea.zero.controller.cms;


import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.cms.CmsTemplate;
import com.imflea.zero.model.entity.cms.dto.CmsTemplateDto;
import com.imflea.zero.service.cms.ICmsTemplateService;
import com.imflea.zero.utils.RestUtil;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 发布模板信息 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: CmsTemplateController
 * @Description
 * @since 2021-09-17
 */
@RestController
@Api(value = "内容发布-发布模板", tags = "内容发布-发布模板")
@RequestMapping("/cms/template")
public class CmsTemplateController {

    @Autowired
    private ICmsTemplateService cmsTemplateService;

    @ApiOperation(value = "新增接口")
    @ApiOperationSupport(order = 1)
    @PostMapping("/add")
    public JsonResult<CmsTemplate> save(@RequestBody CmsTemplate cmsTemplate) {
        CmsTemplate entity = cmsTemplateService.saveEntity(cmsTemplate);
        return RestUtil.success(entity);
    }

    @ApiOperation(value = "修改接口")
    @ApiOperationSupport(order = 2)
    @PostMapping("/update")
    public JsonResult<CmsTemplate> update(@RequestBody CmsTemplate cmsTemplate) {
        CmsTemplate entity = cmsTemplateService.updateEntity(cmsTemplate);
        return RestUtil.success(entity);
    }

    @ApiOperation(value = "根据id集合删除发布模板信息")
    @ApiOperationSupport(order = 3)
    @PostMapping("/remove")
    public JsonResult<Boolean> remove(@RequestBody IdDto idReq) {

        Boolean success = cmsTemplateService.removeEntity(idReq.getId());

        return RestUtil.success(success);
    }

    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 4)
    @PostMapping("/template")
    public JsonResult<CmsTemplate> queryById(@RequestBody IdDto idDto) {
        CmsTemplate cmsContentVo = cmsTemplateService.queryEntity(idDto.getId());
        return RestUtil.success(cmsContentVo);
    }

    @ApiOperation(value = "查询发布模板信息分页列表")
    @ApiOperationSupport(order = 5)
    @PostMapping("/page")
    public JsonResult<PageInfo<CmsTemplate>> queryPage(@RequestBody CmsTemplateDto cmsTemplateDto) {
        PageInfo<CmsTemplate> pageInfo = cmsTemplateService.queryPage(cmsTemplateDto);
        return RestUtil.success(pageInfo);
    }

    @ApiOperation(value = "名称查询模板列表")
    @ApiOperationSupport(order = 6)
    @PostMapping("/list")
    public JsonResult<List<CmsTemplate>> queryList(@RequestBody CmsTemplateDto cmsTemplateDto) {
        List<CmsTemplate> list = cmsTemplateService.queryList(cmsTemplateDto);
        return RestUtil.success(list);
    }

}
