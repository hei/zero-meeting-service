package com.imflea.zero.controller.invoice;


import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.entity.invoice.dto.SendMailDto;
import com.imflea.zero.model.entity.invoice.vo.QueryPageVo;
import com.imflea.zero.service.email.IBusinessSendEmailService;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.imflea.zero.model.entity.invoice.InvoiceInfo;
import com.imflea.zero.model.entity.invoice.dto.InvoiceInfoDto;
import com.imflea.zero.service.invoice.IInvoiceInfoService;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.dto.IdDto;
import java.util.List;

/**
 * <p>
 * 发票信息 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @since 2021-09-25
 * @PackageName: com.imflea.zero.controller
 * @ClassName: InvoiceInfoController
 * @Description
 */
@RestController
@Api(value = "发票信息管理", tags = "发票信息管理")
@RequestMapping("/invoice/info")
public class InvoiceInfoController {

    @Autowired
    private IInvoiceInfoService invoiceInfoService;

    @Autowired
    private IBusinessSendEmailService iBusinessSendEmailService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<InvoiceInfo> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        InvoiceInfo entity = invoiceInfoService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改发票信息接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<InvoiceInfo> save(@RequestBody InvoiceInfo invoiceInfo) {
        InvoiceInfo entity = invoiceInfoService.saveOrUpdateInvoiceInfo(invoiceInfo);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除发票信息")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        invoiceInfoService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }


    @PostMapping("/page")
    @ApiOperation(value = "查询发票信息分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<InvoiceInfo>> queryPage(@RequestBody InvoiceInfoDto invoiceInfoDto) {
        PageInfo<InvoiceInfo> pageInfo = invoiceInfoService.selectPage(invoiceInfoDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/send")
    @ApiOperation(value = "发票上传关联文件服务发送邮箱")
    @ApiOperationSupport(order = 5)
    public JsonResult<Boolean> updateContentAndStatusById(@RequestBody SendMailDto sendMailDto) {
        Boolean result = iBusinessSendEmailService.updateContentAndStatusById(sendMailDto);
        if (result == false){
            return new JsonResult<>(ZeroContant.getFailCode(), result);
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), result);
    }

    @PostMapping("/list")
    @ApiOperation(value = "根据订单Id查发票信息")
    @ApiOperationSupport(order = 6)
    public JsonResult<List<InvoiceInfo>> listByOrderIdAndUserIdAndTentantId(@RequestBody IdDto idDto){
        if (CommonUtils.isNull(idDto) ||CommonUtils.isNull(idDto.getId())){
            return new JsonResult<>(ZeroContant.getSuccessCode(),"订单Id是空");
        }else {
            return new JsonResult<>(ZeroContant.getSuccessCode(),"查询成功",invoiceInfoService.listByOrderIdAndUserIdAndTentantId(idDto.getId()));
        }
    }

}
