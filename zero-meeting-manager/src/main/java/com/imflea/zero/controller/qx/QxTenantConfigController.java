package com.imflea.zero.controller.qx;


import cn.hutool.core.bean.BeanUtil;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.qx.QxTenantConfig;
import com.imflea.zero.model.entity.qx.vo.QxTenantConfigVo;
import com.imflea.zero.service.qx.IQxTenantConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 租户配置表 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: QxTenantConfigController
 * @Description
 * @since 2021-09-23
 */
@RestController
@Api(value = "租户配置表管理", tags = "租户配置表管理")
@RequestMapping("/tenant/config")
public class QxTenantConfigController {

    @Autowired
    private IQxTenantConfigService qxTenantConfigService;



    @PostMapping("/save")
    @ApiOperation(value = "保存与修改租户配置表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<QxTenantConfigVo> save(@RequestBody QxTenantConfig qxTenantConfig) {
        QxTenantConfig entity = qxTenantConfigService.saveOrUpdateQxTenantConfig(qxTenantConfig);

        QxTenantConfigVo vo = new QxTenantConfigVo();
        if(!CommonUtils.isNull(entity)){
            BeanUtil.copyProperties(entity,vo);
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", vo);
    }

    @PostMapping("/queryByTenantId")
    @ApiOperation(value = "根据租户id获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<QxTenantConfigVo> queryByTenantId(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        QxTenantConfigVo vo = new QxTenantConfigVo();
        QxTenantConfig entity = qxTenantConfigService.queryByTenantId(id);
        if(!CommonUtils.isNull(entity)){
            BeanUtil.copyProperties(entity,vo);
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", vo);
    }

}
