package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.meeting.MeetingRoom;
import com.imflea.zero.model.entity.meeting.dto.MeetingRoomDto;
import com.imflea.zero.model.entity.meeting.vo.MeetingRoomDetailVO;
import com.imflea.zero.service.meeting.IMeetingRoomService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 空间表 前端控制器
 * </p>
 *
 * @author xby
 * @PackageName: com.imflea.zero.controller
 * @ClassName: MeetingRoomController
 * @Description
 * @since 2024-01-25
 */
@RestController
@Api(value = "空间表管理", tags = "空间表管理")
@RequestMapping("/meeting/room")
public class MeetingRoomController {

    @Autowired
    private IMeetingRoomService meetingRoomService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<MeetingRoomDetailVO> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        MeetingRoomDetailVO entity = meetingRoomService.getDetailById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改空间表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<MeetingRoom> save(@RequestBody MeetingRoom meetingRoom) {
        MeetingRoom entity = meetingRoomService.saveOrUpdateMeetingRoom(meetingRoom);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除空间表")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        meetingRoomService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询空间表分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<MeetingRoomDetailVO>> queryPage(@RequestBody MeetingRoomDto meetingRoomDto) {
        PageInfo<MeetingRoomDetailVO> pageInfo = meetingRoomService.queryPage(meetingRoomDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/update2Up")
    @ApiOperation(value = "上架接口")
    @ApiOperationSupport(order = 5)
    public JsonResult<Boolean> update2Up(@RequestBody IdDto idDto) {
        Boolean entity = meetingRoomService.updateStatusById(idDto.getId(), "1");
        return new JsonResult<>(ZeroContant.getSuccessCode(), "上架成功！", entity);
    }

    @PostMapping("/update2Down")
    @ApiOperation(value = "下架接口")
    @ApiOperationSupport(order = 6)
    public JsonResult<Boolean> update2Down(@RequestBody IdDto idDto) {
        Boolean entity = meetingRoomService.updateStatusById(idDto.getId(), "0");
        return new JsonResult<>(ZeroContant.getSuccessCode(), "下架成功！", entity);
    }

}
