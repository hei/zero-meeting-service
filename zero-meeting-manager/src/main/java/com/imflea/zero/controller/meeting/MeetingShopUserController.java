package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.meeting.MeetingShopUser;
import com.imflea.zero.model.entity.meeting.dto.MeetingShopUserDto;
import com.imflea.zero.model.entity.meeting.dto.MeetingShopUserObjsDto;
import com.imflea.zero.model.entity.qx.vo.QxUserInfoVo;
import com.imflea.zero.service.meeting.IMeetingShopUserService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xby
 * @PackageName: com.imflea.zero.controller
 * @ClassName: MeetingShopUserController
 * @Description
 * @since 2024-01-25
 */
@RestController
@Api(value = "门店人员管理", tags = "门店人员管理")
@RequestMapping("/meeting/shopUser")
public class MeetingShopUserController {

    @Autowired
    private IMeetingShopUserService meetingShopUserService;


    @PostMapping("/queryByShopId")
    @ApiOperation(value = "根据门店ID获取穿梭框左（已关联）：右（未关联人员：门店管理员）")
    @ApiOperationSupport(order = 1)
    public JsonResult<Map<String, List<QxUserInfoVo>>> queryByShopId(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        Map<String, List<QxUserInfoVo>> entity = meetingShopUserService.queryByShopId(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }


    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<MeetingShopUser> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        MeetingShopUser entity = meetingShopUserService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改接口(穿梭框，单条保存)")
    @ApiOperationSupport(order = 2)
    public JsonResult<MeetingShopUser> save(@RequestBody MeetingShopUser meetingShopUser) {
        MeetingShopUser entity = meetingShopUserService.saveOrUpdateMeetingShopUser(meetingShopUser);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        meetingShopUserService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<MeetingShopUser>> queryPage(@RequestBody MeetingShopUserDto meetingShopUserDto) {
        PageInfo<MeetingShopUser> pageInfo = meetingShopUserService.queryPage(meetingShopUserDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/saveBatch")
    @ApiOperation(value = "保存与修改接口(穿梭框，单条保存)-多条")
    @ApiOperationSupport(order = 6)
    public JsonResult<List<MeetingShopUser>> saveBath(@RequestBody MeetingShopUserObjsDto objs) {
        List<MeetingShopUser> meetingShopUsers = objs.getObjs();
        List<MeetingShopUser> entity = meetingShopUserService.saveOrUpdateMeetingShopUserBatch(meetingShopUsers);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }


}
