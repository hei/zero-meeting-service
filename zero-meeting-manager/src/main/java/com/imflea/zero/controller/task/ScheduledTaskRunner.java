package com.imflea.zero.controller.task;


import com.imflea.zero.service.task.IScheduleTaskService;
import com.imflea.zero.service.task.IScheduledTaskRunnerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class ScheduledTaskRunner implements ApplicationRunner {

    @Autowired
    private IScheduleTaskService scheduleTaskService;
    @Autowired
    private IScheduledTaskRunnerService scheduledTaskRunnerService;

    /**
     * 程序启动完毕后,需要自启的任务
     */
    @Override
    public void run(ApplicationArguments applicationArguments) {
        log.info(" >>>>>> 项目启动完毕, 开启 => 需要自启的任务 开始!");
        scheduledTaskRunnerService.initAllTask(scheduleTaskService.list());
        log.info(" >>>>>> 项目启动完毕, 开启 => 需要自启的任务 结束！");
    }
}