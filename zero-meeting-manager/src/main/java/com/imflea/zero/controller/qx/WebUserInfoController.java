package com.imflea.zero.controller.qx;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.model.entity.user.WebUserInfo;
import com.imflea.zero.model.entity.user.dto.WebUserInfoDto;
import com.imflea.zero.service.user.IWebUserInfoService;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.utils.RandomNameUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 微信端用户基本信息 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: WebUserInfoController
 * @Description
 * @since 2021-09-13
 */
@RestController
@Api(value = "微信端用户基本信息管理", tags = "微信端用户基本信息管理")
@RequestMapping("/qx/webuser")
public class WebUserInfoController {

    @Autowired
    private IWebUserInfoService webUserInfoService;


    @PostMapping("/page")
    @ApiOperation(value = "查询微信端用户基本信息分页列表")
    @ApiOperationSupport(order = 3)
    public JsonResult<PageInfo<WebUserInfo>> queryPage(@RequestBody WebUserInfoDto webUserInfoDto) {
        PageInfo<WebUserInfo> pageInfo = webUserInfoService.queryPage(webUserInfoDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @ApiOperation(value = "handleNickName", notes = "handleNickName")
    @PostMapping("/handleNickName")
    @ApiOperationSupport(order = 11)
    public JsonResult<Boolean> handleNickName() {


        List<WebUserInfo> all = webUserInfoService.list();
        List<WebUserInfo> allA = new ArrayList<>();


        for (WebUserInfo userInfo : all) {
            String randomName = userInfo.getNickName();
            String url = userInfo.getAvatarUrl();
            boolean isUpdate = false;
            if (CommonUtils.isNull(randomName) || CommonUtils.isNull(url)) {
                isUpdate = true;
            }
            if (CommonUtils.isNull(randomName)) {
                randomName = RandomNameUtils.randomName(false, 4);
                userInfo.setNickName(randomName);
            }
            if (CommonUtils.isNull(url)) {
                userInfo.setAvatarUrl(RandomNameUtils.generateImg(randomName));
            }
            if (isUpdate) {
                allA.add(userInfo);
            }

        }
        boolean b = webUserInfoService.updateBatchById(allA);

        return new JsonResult<>(ZeroContant.getSuccessCode(), b);
    }


    @ApiOperation(value = "getRandUrl", notes = "getRandUrl")
    @PostMapping("/getRandUrl")
    @ApiOperationSupport(order = 12)
    public JsonResult<String> getRandUrl() {


        String url = RandomNameUtils.generateImg(RandomNameUtils.randomName(false,4));


        return new JsonResult<>(ZeroContant.getSuccessCode(), url);
    }



}
