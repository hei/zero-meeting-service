package com.imflea.zero.controller.qx;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.util.ZeroCacheUtils;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.entity.qx.dto.LoginDto;
import com.imflea.zero.service.qx.IQxUserInfoService;
import com.imflea.zero.utils.RandomValidateCodeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: LoginController
 * @Description
 * @date 2021-08-23  14:27:04
 */
@RestController
@RequestMapping("/qx/login")
@Api(value = "登录-认证", tags = "认证相关")
public class LoginController {
    @Autowired
    private IQxUserInfoService userInfoService;


    @ApiOperation(value = "登录认证-认证", notes = "登录认证-认证")
    @PostMapping("/login")
    @ApiOperationSupport(order = 1)
    public JsonResult<Map<String, Object>> login(@RequestBody LoginDto loginData) {

        String userName = loginData.getUserName();
        String password = loginData.getPassword();
        String serialNum = loginData.getSerialNum();
        String captchaCode = loginData.getCaptchaCode();

        if (CommonUtils.isNull(captchaCode)) {

            return new JsonResult<>(ZeroContant.getFailCode(), "请输入验证码");

        } else {
            String captchaFromRedis = null;
            try {
                captchaFromRedis =ZeroCacheUtils.getString(QxContant.getSingleSignOnCaptcha() + serialNum);
               ZeroCacheUtils.expire(QxContant.getSingleSignOnCaptcha() + serialNum, 1L);
            } catch (Exception e) {
                e.printStackTrace();
                return new JsonResult<>(ZeroContant.getFailCode(), "验证码异常");
            }
            if (!captchaCode.equals(captchaFromRedis)) {
                return new JsonResult<>(ZeroContant.getFailCode(), "验证码错误");
            }
        }
        Map<String, Object> result = userInfoService.handleQueryUser4Login(userName, password);
        if (!CommonUtils.isNull(result) && !CommonUtils.isNull(result.get("msg"))) {
            return new JsonResult<>(ZeroContant.getFailCode(), result.get("msg").toString());
        }

        return new JsonResult<>(ZeroContant.getSuccessCode(), result);
    }

    @ApiOperation(value = "登录认证-获取图形验证码", notes = "登录认证-获取图形验证码")
    @PostMapping("/getCaptcha")
    @ApiOperationSupport(order = 2)
    public JsonResult<Map<String, String>> getCaptchaCode() {

        Map<String, String> result = null;
        try {
            Map<String, String> randomResult = RandomValidateCodeUtil.getRandCode();
            result = new HashMap<String, String>();
            String captchaCode = randomResult.get("value");
            String serialNum =CommonUtils.generateRandomString();
           ZeroCacheUtils.setString(QxContant.getSingleSignOnCaptcha() + serialNum, captchaCode);
           ZeroCacheUtils.expire(QxContant.getSingleSignOnCaptcha() + serialNum, 5 * 60);
            result.put("image", randomResult.get("image"));
            result.put("serialNum", serialNum);
            return new JsonResult<>(ZeroContant.getSuccessCode(), result);
        } catch (Exception e) {
            throw new BizException("图形验证码获取异常");
        }

    }

}
