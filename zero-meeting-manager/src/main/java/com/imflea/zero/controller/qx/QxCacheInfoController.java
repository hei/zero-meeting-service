package com.imflea.zero.controller.qx;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.qx.QxCacheInfo;
import com.imflea.zero.model.entity.qx.dto.CacheInfoTableNameDto;
import com.imflea.zero.model.entity.qx.dto.CacheInfoTableNamesDto;
import com.imflea.zero.model.entity.qx.dto.QxCacheInfoDto;
import com.imflea.zero.service.qx.IQxCacheInfoService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 缓存基本信息 前端控制器
 * </p>
 *
 * @author xiangbaoyu
 * @PackageName: com.imflea.zero.controller
 * @ClassName: QxCacheInfoController
 * @Description
 * @since 2021-09-17
 */
@RestController
@Api(value = "缓存基本信息管理", tags = "缓存基本信息管理")
@RequestMapping("/qx/cacheInfo")
public class QxCacheInfoController {

    @Autowired
    private IQxCacheInfoService qxCacheInfoService;

/*    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<QxCacheInfo> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        QxCacheInfo entity = qxCacheInfoService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }*/

/*    @PostMapping("/save")
    @ApiOperation(value = "保存与修改缓存基本信息接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<QxCacheInfo> save(@RequestBody QxCacheInfo qxCacheInfo) {
        QxCacheInfo entity = qxCacheInfoService.saveOrUpdateQxCacheInfo(qxCacheInfo);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }*/
/*
    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除缓存基本信息")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        qxCacheInfoService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }*/

/*
    @PostMapping("/page")
    @ApiOperation(value = "查询缓存基本信息表且分页")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<QxCacheInfo>> queryPage(@RequestBody QxCacheInfoDto qxCacheInfoDto) {
        PageInfo<QxCacheInfo> pageInfo = qxCacheInfoService.queryPage(qxCacheInfoDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }
*/

    @PostMapping("/refreshCache")
    @ApiOperation(value = "刷新缓存")
    @ApiOperationSupport(order = 5)
    public JsonResult<Boolean> query4RefreshCache(@RequestBody CacheInfoTableNamesDto namesDto) {
        List<String> tableNames = namesDto.getTableNames();
        return new JsonResult<>(ZeroContant.getSuccessCode(), qxCacheInfoService.refreshCacheByTableName(tableNames));
    }

    @PostMapping("/queryAll")
    @ApiOperation(value = "查询缓存基本信息表")
    @ApiOperationSupport(order = 6)
    public JsonResult<List<QxCacheInfo>> queryAll(@RequestBody CacheInfoTableNameDto qxCacheInfoDto) {
        String tableName = qxCacheInfoDto.getTableName();
        List<QxCacheInfo> pageInfo = qxCacheInfoService.queryAll(tableName);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }
}
