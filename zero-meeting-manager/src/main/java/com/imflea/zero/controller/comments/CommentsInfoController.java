package com.imflea.zero.controller.comments;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.comments.CommentsInfo;
import com.imflea.zero.model.entity.comments.dto.*;
import com.imflea.zero.service.comments.ICommentsInfoService;
import com.imflea.zero.service.comments.SingleAdoptSwitch;
import com.imflea.zero.service.comments.SingleAreatSwitch;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 互动评论表前端控制器  
 * </p>
 *
 * @author guohui
 * @since 2021-10-08
 * @PackageName: com.imflea.zero.controller
 * @ClassName: CommentsInfoController
 * @Description
 */
@Slf4j
@RestController
@Api(value = "互动评论表管理", tags = "互动评论表管理")
@RequestMapping("/comments-info")
public class CommentsInfoController {

    @Autowired
    private ICommentsInfoService commentsInfoService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<CommentsInfo> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        CommentsInfo entity = commentsInfoService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改互动评论表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<CommentsInfo> save(@RequestBody SaveOrUpdateCommentsInfoDto pam) {
        if (CommonUtils.isNull(pam) ||CommonUtils.isNull(pam.getComments()) ||CommonUtils.isNull(pam.getCommentBusinessType())){
            return new JsonResult<>(ZeroContant.getFailCode(), "内容为空！");
        }
        CommentsInfo entity = commentsInfoService.saveOrUpdateCommentsInfo(pam);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除互动评论表")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        commentsInfoService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询互动评论表分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<CommentsInfo>> queryPage(@RequestBody CommentsInfoDto commentsInfoDto) {
        PageInfo<CommentsInfo> pageInfo = commentsInfoService.queryPage(commentsInfoDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/updateStatusById")
    @ApiOperation(value = "评论审核是否通过")
    @ApiOperationSupport(order = 5)
    public JsonResult<Boolean> updateStatusById(@RequestBody UpdateStatusByIdDto updateStatusByIdDto) {
        if (CommonUtils.isNull(updateStatusByIdDto) ||CommonUtils.isNull(updateStatusByIdDto.getId()) ||CommonUtils.isNull(updateStatusByIdDto.getStatus())){
            return new JsonResult<>(ZeroContant.getFailCode(), "参数为空");
        }
        Boolean result = commentsInfoService.updateStatusById(updateStatusByIdDto.getId(), updateStatusByIdDto.getStatus());
        return new JsonResult<>(ZeroContant.getSuccessCode(), "更新成功！",result);
    }

    @PostMapping("/isSelected")
    @ApiOperation(value = "是否精选")
    @ApiOperationSupport(order = 6)
    public JsonResult<Boolean> isSelected(@RequestBody IsSelectedDto isSelectedDto) {
        if (CommonUtils.isNull(isSelectedDto) ||CommonUtils.isNull(isSelectedDto.getId()) ||CommonUtils.isNull(isSelectedDto.getSelected())){
            return new JsonResult<>(ZeroContant.getFailCode(), "参数为空！");
        }
        Boolean result = commentsInfoService.updateSectedById(isSelectedDto.getId(),isSelectedDto.getSelected());
        return new JsonResult<>(ZeroContant.getSuccessCode(), "更新成功！",result);
    }

    @PostMapping("/switch")
    @ApiOperation(value = "评论开关是否开启")
    @ApiOperationSupport(order = 7)
    public JsonResult<Boolean> isSwitch(@RequestBody SwitchDto switchDto) {
        if (CommonUtils.isNull(switchDto) ||CommonUtils.isNull(switchDto.getIsSwitch()) ||CommonUtils.isNull(switchDto.getCommentBusinessType())){
            return new JsonResult<>(ZeroContant.getFailCode(), "参数为空！");
        }
        if ("0".equals(switchDto.getCommentBusinessType())){//点位
            SingleAreatSwitch.getInstance().setWordSwitch(switchDto.getIsSwitch());
            log.info(SingleAreatSwitch.getInstance().getWordSwitch()? "点位评论开关已经打开" : "评论开关已经关闭");
        }
        if ("1".equals(switchDto.getCommentBusinessType())){//认养物品类
            SingleAdoptSwitch.getInstance().setWordSwitch(switchDto.getIsSwitch());
            log.info(SingleAdoptSwitch.getInstance().getWordSwitch()? "点位评论开关已经打开" : "评论开关已经关闭");
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "更新成功！");
    }

    @PostMapping("/querySwitch")
    @ApiOperation(value = "查询评论的开关")
    @ApiOperationSupport(order = 7)
    public JsonResult<Boolean> querySwitch(@RequestBody QuerySwitchDto querySwitchDto) {
        if ("0".equals(querySwitchDto.getCommentBusinessType())){
            return new JsonResult<>(ZeroContant.getSuccessCode(), "查询成功！",SingleAreatSwitch.getInstance().getWordSwitch());
        }
        if ("1".equals(querySwitchDto.getCommentBusinessType())){
            return new JsonResult<>(ZeroContant.getSuccessCode(), "查询成功！",SingleAdoptSwitch.getInstance().getWordSwitch());
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "条件穿透");
    }
}
