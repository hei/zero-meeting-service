package com.imflea.zero.controller.qx;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.qx.QxRole;
import com.imflea.zero.service.qx.IQxRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: RoleController
 * @Description 角色相关的主要操作
 * @date 2021-08-19  14:54:54
 */
@RestController
@RequestMapping("/qx/role")
@Api(value = "角色管理", tags = "角色管理-管理端权限")
public class RoleController {
    @Autowired
    private IQxRoleService roleService;

    @ApiOperation(value = "角色信息保存与更新", notes = "角色信息保存与更新")
    @PostMapping("/saveOrUpdateRole")
    @ApiOperationSupport(order = 1)
    public JsonResult<QxRole> saveOrUpdateROle(@RequestBody QxRole role) {

        return new JsonResult<>(ZeroContant.getSuccessCode(), roleService.saveOrUpdateRole(role));

    }

    @ApiOperation(value = "查询所有角色列表", notes = "查询所有角色列表")
    @PostMapping("/queryRoles")
    @ApiOperationSupport(order = 2)
    public JsonResult<List<QxRole>> queryRoles() {
        return new JsonResult<>(ZeroContant.getSuccessCode(), roleService.queryRoles());

    }


    @ApiOperation(value = "删除角色", notes = "删除角色")
    @PostMapping("/removeById")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> removeById(@RequestBody IdDto req) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), roleService.removeRoleById(req.getId()));
    }

    @ApiOperation(value = "查询所有租户可用角色列表", notes = "查询所有租户可用角色列表")
    @PostMapping("/queryTenantRoles")
    @ApiOperationSupport(order = 4)
    public JsonResult<List<QxRole>> queryTenantRoles() {
        return new JsonResult<>(ZeroContant.getSuccessCode(), roleService.queryTenantRoles());

    }


}
