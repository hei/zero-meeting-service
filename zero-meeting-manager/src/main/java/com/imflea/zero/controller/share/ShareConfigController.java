package com.imflea.zero.controller.share;



import com.imflea.zero.util.base.CommonUtils;
import com.imflea.zero.model.entity.share.dto.PosterDto;
import com.imflea.zero.model.entity.share.vo.QueryPageVo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.imflea.zero.model.entity.share.ShareConfig;
import com.imflea.zero.model.entity.share.dto.ShareConfigDto;
import com.imflea.zero.service.share.IShareConfigService;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.dto.IdDto;

import java.util.List;

/**
 * <p>
 * 分享界面图片配置表 前端控制器
 * </p>
 *
 * @author guohui
 * @since 2021-10-21
 * @PackageName: com.imflea.zero.controller
 * @ClassName: ShareConfigController
 * @Description
 */
@RestController
@Api(value = "分享界面图片配置表管理", tags = "分享界面图片配置表管理")
@RequestMapping("/shareConfig")
public class ShareConfigController {

    @Autowired
    private IShareConfigService shareConfigService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<ShareConfig> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        ShareConfig entity = shareConfigService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改分享界面图片配置表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<ShareConfigDto> save(@RequestBody ShareConfigDto shareConfig) {
        if (CommonUtils.isNull(shareConfig) ||CommonUtils.isNull(shareConfig.getList())) {
            return new JsonResult<>(ZeroContant.getFailCode(), "内容为空");
        }
        ShareConfigDto entity = shareConfigService.saveOrUpdateShareConfig(shareConfig);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除分享界面图片配置表")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        shareConfigService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询分享界面图片配置表分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<QueryPageVo>> queryPage(@RequestBody ShareConfigDto shareConfigDto) {
        PageInfo<QueryPageVo> pageInfo = shareConfigService.queryPage(shareConfigDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/poster")
    @ApiOperation(value = "生成分享海报")
    @ApiOperationSupport(order = 5)
    public JsonResult<String> poster(@RequestBody PosterDto posterDto) {
        if (CommonUtils.isNull(posterDto)
                ||CommonUtils.isNull(posterDto.getGoodsUrl())
                ||CommonUtils.isNull(posterDto.getJumpUrl())
                ||CommonUtils.isNull(posterDto.getDescription())
        ||CommonUtils.isNull(posterDto.getGoodsName())
        ||CommonUtils.isNull(posterDto.getGoodsTitle())){
            return new JsonResult<>(ZeroContant.getSuccessCode(),"参数为空");
        }
        String result = "";
        try {
            result = shareConfigService.poster(posterDto);
        } catch (Exception e) {
            e.printStackTrace();
            return new JsonResult<>(ZeroContant.getFailCode(),"生成失败",result);
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(),"成功",result);
    }
}
