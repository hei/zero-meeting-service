package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.meeting.MeetingAlbum;
import com.imflea.zero.model.entity.meeting.dto.MeetingAlbumDto;
import com.imflea.zero.model.entity.qx.QxAttachment;
import com.imflea.zero.model.entity.qx.dto.AttachmentDetailDto;
import com.imflea.zero.service.meeting.IMeetingAlbumService;
import com.imflea.zero.service.qx.IQxAttachmentService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 画廊 前端控制器
 * </p>
 *
 * @author xby
 * @PackageName: com.imflea.zero.controller
 * @ClassName: MeetingAlbumController
 * @Description
 * @since 2024-01-25
 */
@RestController
@Api(value = "画廊管理", tags = "画廊管理")
@RequestMapping("/meeting/album")
public class MeetingAlbumController {

    @Autowired
    private IMeetingAlbumService meetingAlbumService;

    @Autowired
    private IQxAttachmentService attachmentService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<MeetingAlbum> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        MeetingAlbum entity = meetingAlbumService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改画廊接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<MeetingAlbum> save(@RequestBody MeetingAlbum meetingAlbum) {
        MeetingAlbum entity = meetingAlbumService.saveOrUpdateMeetingAlbum(meetingAlbum);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！", entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除画廊")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        meetingAlbumService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/pageAlb")
    @ApiOperation(value = "查询画廊分页列表-正确的接口")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<MeetingAlbum>> pageAlb(@RequestBody MeetingAlbumDto meetingAlbumDto) {
        PageInfo<MeetingAlbum> pageInfo = meetingAlbumService.queryPage(meetingAlbumDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询画廊分页列表-错误的接口")
    @ApiOperationSupport(order = 4)
    public JsonResult<List<QxAttachment>> queryPage(@RequestBody MeetingAlbumDto meetingAlbumDto) {
        AttachmentDetailDto dto = new AttachmentDetailDto();
        dto.setBusinessId(meetingAlbumDto.getBizId());
        dto.setBusinessSysCode("FARM_PLUS");
        dto.setBusinessSysModuleCode("MEET_MEDIA");
        List<QxAttachment> qxAttachments = attachmentService.queryAttachmentsByBuinessIdAndModule(dto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), qxAttachments);
    }

}
