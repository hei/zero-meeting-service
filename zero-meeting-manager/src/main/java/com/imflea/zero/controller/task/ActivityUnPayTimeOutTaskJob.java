package com.imflea.zero.controller.task;

import com.imflea.zero.constant.ActivityConstant;
import com.imflea.zero.service.task.IScheduledTaskJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.task
 * @ClassName: ActivityUnPayTimeOutTask
 * @Description
 * @date 2022-01-14  13:10:19
 */
@Component
@Slf4j
public class ActivityUnPayTimeOutTaskJob implements IScheduledTaskJobService {
//    @Autowired
//    private ActivityUnPayTimeOutTask activityUnPayTimeOutTask;

    @Override
    public void run() {
        log.error("活动订单取消任务");
//        activityUnPayTimeOutTask.cancel(ActivityConstant.getActivitySquKey());
    }


}
