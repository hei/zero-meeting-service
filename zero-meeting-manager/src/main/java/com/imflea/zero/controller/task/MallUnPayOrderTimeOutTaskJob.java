package com.imflea.zero.controller.task;

import com.imflea.zero.constant.MallContant;
//import com.imflea.zero.service.mall.impl.IMallUnPayOrderTimeOutTask;
import com.imflea.zero.service.task.IScheduledTaskJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.controller.task
 * @filename 创建时间: 2022/1/19
 * @description
 * @copyright Copyright (c) 2022 中国软件与技术服务股份有限公司
 */
@Component
@Slf4j
public class MallUnPayOrderTimeOutTaskJob implements IScheduledTaskJobService {
//
//    @Autowired
//    private IMallUnPayOrderTimeOutTask iMallUnPayOrderTimeOutTask;

    @Override
    public void run() {

//        iMallUnPayOrderTimeOutTask.cancel(MallContant.UN_PAY_ORDER);
    }

}
