package com.imflea.zero.controller.keywords;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.keywords.Keywords;
import com.imflea.zero.model.entity.keywords.dto.KeywordsDto;
import com.imflea.zero.service.comments.SensitiveWordBiz;
import com.imflea.zero.service.keywords.IKeywordsService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 敏感词表 前端控制器
 * </p>
 *
 * @author guohui
 * @since 2021-10-09
 * @PackageName: com.imflea.zero.controller
 * @ClassName: KeywordsController
 * @Description
 */
@RestController
@Api(value = "敏感词表管理", tags = "敏感词表管理")
@RequestMapping("/keywords")
public class KeywordsController {

    @Autowired
    private IKeywordsService keywordsService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<Keywords> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        Keywords entity = keywordsService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改敏感词表接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<Keywords> save(@RequestBody Keywords keywords) {
        Keywords entity = keywordsService.saveOrUpdateKeywords(keywords);
        SensitiveWordBiz.initKeyWordsCache();
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除敏感词表")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        keywordsService.removeByIds(ids);
        SensitiveWordBiz.initKeyWordsCache();
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询敏感词表分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<Keywords>> queryPage(@RequestBody KeywordsDto keywordsDto) {
        PageInfo<Keywords> pageInfo = keywordsService.queryPage(keywordsDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
