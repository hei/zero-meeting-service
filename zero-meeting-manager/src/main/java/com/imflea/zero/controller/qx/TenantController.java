package com.imflea.zero.controller.qx;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.constant.QxContant;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.cms.CmsCommonContent;
import com.imflea.zero.model.entity.qx.QxTenant;
import com.imflea.zero.model.entity.qx.dto.TenantPageQueryDto;
import com.imflea.zero.model.entity.qx.vo.TenantDetailVo;
import com.imflea.zero.service.qx.IQxTenantService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: TenantController
 * @Description
 * @date 2021-08-24  15:03:37
 */
@RestController
@RequestMapping("/qx/tenant")
@Api(value = "租户管理", tags = "租户管理-权限管理")
public class TenantController {
    @Autowired
    private IQxTenantService tenantService;

    @PostMapping("/saveOrUpdateTenant")
    @ApiOperation(value = "租户保存与修改接口", notes = "租户管理-权限管理")
    @ApiOperationSupport(order = 1)
    public JsonResult<QxTenant> saveOrUpdateTenant(@RequestBody QxTenant tenant) {

        return new JsonResult<>(ZeroContant.getSuccessCode(), tenantService.saveOrupdateTenant(tenant));

    }

    @PostMapping("/queryTenant")
    @ApiOperation(value = "租户分页，条件查询", notes = "租户分页-权限管理")
    @ApiOperationSupport(order = 2)
    public JsonResult<PageInfo<QxTenant>> queryTenant(@RequestBody TenantPageQueryDto pageQueryReq) {

        return new JsonResult<>(ZeroContant.getSuccessCode(), tenantService.queryTenant4Page(pageQueryReq));

    }

    @PostMapping("/lockTenant")
    @ApiOperation(value = "冻结租户", notes = "冻结租户-权限管理")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> updateTenantStatus2Close(@RequestBody IdDto idReq) {

        String closeStatus = QxContant.getTenantStatusClose();
        return new JsonResult<>(ZeroContant.getSuccessCode(), tenantService.updateTenantStatusById(idReq.getId(), closeStatus));

    }

    @PostMapping("/unlockTenant")
    @ApiOperation(value = "解冻租户", notes = "解冻租户-权限管理")
    @ApiOperationSupport(order = 4)
    public JsonResult<Boolean> updateTenantStatus2Open(@RequestBody IdDto idReq) {
        String openStatus = QxContant.getTenantStatusOpen();
        return new JsonResult<>(ZeroContant.getSuccessCode(), tenantService.updateTenantStatusById(idReq.getId(), openStatus));
    }

    @PostMapping("/saveOrUpdateContent")
    @ApiOperation(value = "保存或修改企业简介信息", notes = "保存或修改企业简介信息")
    @ApiOperationSupport(order = 5)
    public JsonResult<CmsCommonContent> saveOrUpdateContent(@RequestBody CmsCommonContent tenantContent) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), tenantService.saveOrUpdateContent(tenantContent));
    }

    @PostMapping("/queryTenantDetailById")
    @ApiOperation(value = "根据企业（商户id）获取详细信息", notes = "根据企业（商户id）获取详细信息")
    @ApiOperationSupport(order = 6)
    public JsonResult<TenantDetailVo> queryTenantDetailById(@RequestBody IdDto idDto) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), tenantService.queryDetailById(idDto.getId()));
    }


}
