package com.imflea.zero.controller.meeting;


import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.exception.BizException;
import com.imflea.zero.model.entity.meeting.MeetingOrderInfo;
import com.imflea.zero.model.entity.meeting.dto.MeetingOrderInfoDto;
import com.imflea.zero.service.meeting.IMeetingOrderInfoService;
import com.imflea.zero.util.base.JsonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "订单", tags = "空间租赁-充值订单订单")
@RequestMapping("/meeting/order")
public class MeetingOrderController {

    @Autowired
    IMeetingOrderInfoService meetingOrderInfoService;


    @PostMapping("/queryAllOrders")
    @ApiOperation(value = "获取所有订单")
    @ApiOperationSupport(order = 1)
    public JsonResult<PageInfo<MeetingOrderInfo>> queryAllOrders(@RequestBody MeetingOrderInfoDto dto) {
        PageInfo<MeetingOrderInfo> meetingOrderInfoPageInfo = meetingOrderInfoService.queryPage(dto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", meetingOrderInfoPageInfo);
    }

    @PostMapping("/queryDetailById")
    @ApiOperation(value = "查看订单详细信息")
    @ApiOperationSupport(order = 2)
    public JsonResult<MeetingOrderInfo> queryDetailById(@RequestBody MeetingOrderInfoDto dto) {
        MeetingOrderInfo info = meetingOrderInfoService.getById(dto.getId());
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", info);
    }

    @PostMapping("/removeByIds")
    @ApiOperation(value = "删除订单")
    @ApiOperationSupport(order = 2)
    public JsonResult<Boolean> removeByIds(@RequestBody IdsDto dto) {
        Boolean info = meetingOrderInfoService.removeByIds(dto.getIds());
        if (!info) {
            throw new BizException("订单删除失败！");
        }
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", info);
    }

}
