package com.imflea.zero.controller.qx;


import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.model.entity.user.WebUserFeedback;
import com.imflea.zero.model.entity.user.dto.WebUserFeedbackDto;
import com.imflea.zero.service.user.IWebUserFeedbackService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zhaoyang
 * @PackageName: com.imflea.zero.controller
 * @ClassName: WebUserFeedbackController
 * @Description
 * @since 2021-09-24
 */
@RestController
@Api(value = "个人中心-产品建议", tags = "个人中心-产品建议")
@RequestMapping("/customer/feedback")
public class UserFeedbackController {

    @Autowired
    private IWebUserFeedbackService webUserFeedbackService;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<WebUserFeedback> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        WebUserFeedback entity = webUserFeedbackService.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    // TODO 未来的回复信息告知用户,可以调用站内信接口发送消息通知
    @PostMapping("/reply")
    @ApiOperation(value = "管理端回复反馈信息")
    @ApiOperationSupport(order = 2)
    public JsonResult<WebUserFeedback> reply(@RequestBody WebUserFeedback webUserFeedback) {
        WebUserFeedback entity = webUserFeedbackService.updateUserFeedback(webUserFeedback);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "", entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        webUserFeedbackService.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<WebUserFeedback>> queryPage(@RequestBody WebUserFeedbackDto webUserFeedbackDto) {
        PageInfo<WebUserFeedback> pageInfo = webUserFeedbackService.queryPage(webUserFeedbackDto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
