package com.imflea.zero.controller.pay;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.pay
 * @ClassName: WxPayController
 * @Description
 * @date 2021-11-02  14:11:32
 */

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.model.entity.pay.dto.PayRefundOrderDto;
import com.imflea.zero.service.wx.IWxPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@Api(value = "支付", tags = "支付")
@RequestMapping("/wx/pay")
public class WxPayController {
    @Autowired
    IWxPayService wxPayService;

    @ApiOperation(value = "API10005：退款", notes = "微信支付")
    @ApiOperationSupport(order = 1)
    @PostMapping("/refund")
    public JsonResult<Map<String, String>> refund(@RequestBody PayRefundOrderDto refundDto) {


//        String orderNumber, BigDecimal refundAmount, BigDecimal totalFee
        Map<String, String> result = wxPayService.saveDoRefund(refundDto);
//        return null;

        return new JsonResult(ZeroContant.getSuccessCode(), result);
    }



    @ApiOperation(value = "API10002：退款-手动", notes = "退款-手动")
    @ApiOperationSupport(order = 2)
    @PostMapping("/HandleRefund")
    public JsonResult<Map<String, String>> HandleRefund(@RequestBody PayRefundOrderDto refundDto) {

        String id = "";
//        String orderNumber, BigDecimal refundAmount, BigDecimal totalFee
        Map<String, String> result = wxPayService.handleRefund(refundDto);
//        return null;

        return new JsonResult(ZeroContant.getSuccessCode(), result);
    }


}
