//package com.imflea.zero.controller.task;
//
//
//import com.imflea.zero.util.base.CommonUtils;
//import com.imflea.zero.util.ZeroJsonUtils;
//import com.imflea.zero.constant.PayConstant;
//import com.imflea.zero.model.entity.adopt.AdoptedInfo;
//import com.imflea.zero.model.entity.mall.vo.MallAlertVo;
//import com.imflea.zero.model.entity.pay.PayRecorderInfo;
//import com.imflea.zero.server.WebSocketServer;
//import com.imflea.zero.service.adopt.IAdoptedInfoService;
//import com.imflea.zero.service.mall.IMallAlertService;
//import com.imflea.zero.service.pay.IPayRecorderInfoService;
//import com.imflea.zero.service.task.IScheduledTaskJobService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.util.*;
//import java.util.stream.Collectors;
//
//@Component
//@Slf4j
//public class OrderNoticeTaskJob implements IScheduledTaskJobService {
//    @Autowired
//    private IAdoptedInfoService adoptedInfoService;
//    @Autowired
//    private IPayRecorderInfoService payRecorderInfoService;
//
//    @Autowired
//    private IMallAlertService iMallAlertService;
//
//    public void run() {
//        log.error("用户待处理事件通知");
//        List<AdoptedInfo> adoptInfoList = adoptedInfoService.queryWaitConfirmed();
//
//        List<PayRecorderInfo> recorderInfos = payRecorderInfoService.queryWaitHandleRecorder();
//
//        List<MallAlertVo> payOrderCount = iMallAlertService.queryPayOrderCount();
//        List<MallAlertVo> cancelPayOrderCount = iMallAlertService.queryCancelPayOrderCount();
//        List<MallAlertVo> applyReturnOrderCount = iMallAlertService.queryApplyReturnOrderCount();
//
//        Set<String> payOrderCountTenantIds = payOrderCount.stream().map(MallAlertVo::getTenantId).collect(Collectors.toSet());
//        Set<String> cancelPayOrderCountTenantIds = cancelPayOrderCount.stream().map(MallAlertVo::getTenantId).collect(Collectors.toSet());
//        Set<String> applyReturnOrderCountTenantIds = applyReturnOrderCount.stream().map(MallAlertVo::getTenantId).collect(Collectors.toSet());
//
//        Map<String, List<PayRecorderInfo>> recorderMap = recorderInfos.stream().collect(Collectors.groupingBy(item -> item.getTenantId()));
//
//        Map<String, List<AdoptedInfo>> adopted = adoptInfoList.stream().collect(Collectors.groupingBy(item -> item.getTenantId()));
//
//        Set<String> tenantIds = new HashSet<>();
//        tenantIds.addAll(recorderMap.keySet());
//        tenantIds.addAll(adopted.keySet());
//        tenantIds.addAll(payOrderCountTenantIds);
//        tenantIds.addAll(cancelPayOrderCountTenantIds);
//        tenantIds.addAll(applyReturnOrderCountTenantIds);
//        for (String tenantId : tenantIds) {
//            try {
//                List<Map<String, Object>> resultList = new ArrayList<>();
//                if (!CommonUtils.isNull(adopted)&&!CommonUtils.isNull(adopted.get(tenantId))) {
//                    List<String> orderIds = adopted.get(tenantId).stream().map(item -> item.getOrderId()).distinct().collect(Collectors.toList());
//                    if (!CommonUtils.isNull(orderIds)) {
//                        Map<String, Object> messageBody = new HashMap<>();
//                        messageBody.put("num", orderIds.size());
//                        messageBody.put("message", "您有" + orderIds.size() + "笔新的待处理认养订单");
//                        messageBody.put("flag", "adoptOrder");
////                        resultList.add(messageBody);
//                        Map<String, Object> messageBody2 = new HashMap<>();
//
//                        messageBody2.put("num", adopted.get(tenantId).size());
//                        messageBody2.put("message", "您有" + adopted.get(tenantId).size() + "只已认养物待建档");
//                        messageBody2.put("flag", "adopted");
//                        resultList.add(messageBody2);
//                    }
//
//                }
//                if (!CommonUtils.isNull(recorderMap)) {
//                    List<PayRecorderInfo> waitUploadRecifeProofs = recorderMap.get(tenantId).stream().filter(item -> item.getPayStatus().equals(PayConstant.PAY_STATUS_WAIT_UPLOAD_RECEIVE_PROOF.getCode())).collect(Collectors.toList());
//                    if (!CommonUtils.isNull(waitUploadRecifeProofs)) {
//                        Map<String, Object> messageBody = new HashMap<>();
//                        messageBody.put("num", waitUploadRecifeProofs.size());
//                        messageBody.put("message", "您有" + waitUploadRecifeProofs.size() + "笔新的认养订单待上传收款证明");
//                        messageBody.put("flag", "uploadProof");
//                        resultList.add(messageBody);
//                    }
//                    List<PayRecorderInfo> waitConfirms = recorderMap.get(tenantId).stream().filter(item -> item.getPayStatus().equals(PayConstant.PAY_STATUS_WAIT_CONFIRM.getCode())).collect(Collectors.toList());
//                    if (!CommonUtils.isNull(waitConfirms)) {
//                        Map<String, Object> messageBody = new HashMap<>();
//                        messageBody.put("num", waitConfirms.size());
//                        messageBody.put("message", "您有" + waitConfirms.size() + "笔新的认养订单待确认已支付");
//                        messageBody.put("flag", "waitConfirm");
//                        resultList.add(messageBody);
//                    }
//                }
//                // 已支付未发货订单
//                if (!CommonUtils.isNull(payOrderCount)) {
//                    payOrderCount.forEach(vo -> {
//                        if (tenantId.equals(vo.getTenantId())) {
//                            Map<String, Object> messageBody = new HashMap<>();
//                            messageBody.put("num", vo.getCount());
//                            messageBody.put("message", "您有" + vo.getCount() + "笔订单待发货");
//                            messageBody.put("flag", "mallPayOrder");
//                            resultList.add(messageBody);
//                        }
//                    });
//
//                }
//                // 已支付申请款订单
//                if (!CommonUtils.isNull(cancelPayOrderCount)) {
//                    cancelPayOrderCount.forEach(vo -> {
//                        if (tenantId.equals(vo.getTenantId())) {
//                            Map<String, Object> messageBody = new HashMap<>();
//                            messageBody.put("num", vo.getCount());
//                            messageBody.put("message", "您有" + vo.getCount() + "笔订单待退款");
//                            messageBody.put("flag", "mallCancelPayOrder");
//                            resultList.add(messageBody);
//                        }
//                    });
//
//                }
//                // 已收货申请退换订单
//                if (!CommonUtils.isNull(applyReturnOrderCount)) {
//                    applyReturnOrderCount.forEach(vo -> {
//                        if (tenantId.equals(vo.getTenantId())) {
//                            Map<String, Object> messageBody = new HashMap<>();
//                            messageBody.put("num", vo.getCount());
//                            messageBody.put("message", "您有" + vo.getCount() + "笔售后单待处理");
//                            messageBody.put("flag", "mallApplyReturnOrder");
//                            resultList.add(messageBody);
//                        }
//                    });
//
//                }
//
//                WebSocketServer.sendInfo(ZeroJsonUtils.listToJson(resultList), tenantId);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//}