package com.imflea.zero.controller.qx;

import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.imflea.zero.dto.IdDto;
import com.imflea.zero.model.entity.qx.QxRoleUser;
import com.imflea.zero.model.entity.qx.dto.RoleUserSaveDto;
import com.imflea.zero.service.qx.IQxRoleUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author 祥保玉
 * @PackageName: com.imflea.zero.controller.qx
 * @ClassName: RoleUserController
 * @Description 角色人员关联关系
 * @date 2021-08-19  15:45:58
 */
@Api(value = "角色用户关联", tags = "角色用户关联-用户权限")
@RestController
@RequestMapping("/qx/roleUser")
public class RoleUserController {
    @Autowired
    private IQxRoleUserService roleUserService;

    @ApiOperation(value = "人员角色授权", notes = "人员角色授权")
    @PostMapping("/saveRoleUserBatch")
    @ApiOperationSupport(order = 1)
    public JsonResult<Boolean> saveRoleUserBatch(@RequestBody RoleUserSaveDto req) {
        return new JsonResult<>(ZeroContant.getSuccessCode(), this.roleUserService.saveRoleUserBatch(req.getUserId(), req.getRoleIds()));

    }

    @ApiOperation(value = "根据userId获取人员角色", notes = "人员角色授权")
    @PostMapping("/queryRolesByUserId")
    @ApiOperationSupport(order = 2)
    public JsonResult<List<QxRoleUser>> queryRolesByUserId(@RequestBody IdDto idReq) {
        String userId = idReq.getId();
        return new JsonResult<>(ZeroContant.getSuccessCode(), roleUserService.queryRolesByUserId(userId));

    }
}
