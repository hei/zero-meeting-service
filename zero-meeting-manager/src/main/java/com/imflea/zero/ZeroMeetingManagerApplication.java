package com.imflea.zero;


import com.imflea.zero.filter.AuthticationFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


@SpringBootApplication
@EnableTransactionManagement
public class ZeroMeetingManagerApplication {

    public static void main(String[] args) {

        SpringApplication.run(ZeroMeetingManagerApplication.class, args);


    }

    @Bean
    public FilterRegistrationBean<AuthticationFilter> registSSOAuthenticationFilter() {
        FilterRegistrationBean<AuthticationFilter> registration = new FilterRegistrationBean<>();
        // 注入过滤器
        registration.setFilter(new AuthticationFilter());
        // 初始化参数
        registration.addInitParameter("ignorePattern", ".*(doc.html).*|.*(login).*|.*(api).*|.*(noticeTask).*|.*(websocket).*");
        // 拦截规则
        registration.addUrlPatterns("/*");
        // 过滤器名称
        registration.setName("AuthticationFilter");
        // 过滤器顺序
        registration.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE);
        return registration;
    }

    private CorsConfiguration buildConfig() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        return corsConfiguration;
    }

    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", buildConfig());
        return new CorsFilter(source);
    }

}
