package com.imflea.zero.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author zhaoyang
 * @version 1.0
 * @project farmplus-service
 * @package com.imflea.zero.config
 * @filename 创建时间: 2021/10/20
 * @description 定时任务调度配置
 * @copyright Copyright (c) 2021 中国软件与技术服务股份有限公司
 */
@Configuration
@EnableScheduling
public class SpringTaskConfig {
}
