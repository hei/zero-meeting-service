package com.imflea.zero.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.imflea.zero.utils.SessionUtils;
import lombok.SneakyThrows;
import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class MetaHandler implements MetaObjectHandler {

    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(MetaHandler.class);

    /**
     * 新增数据执行
     *
     * @param metaObject
     */
    @SneakyThrows
    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("delFlag", "0", metaObject);

        this.setFieldValByName("createTime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("createUserId", SessionUtils.getUserId(), metaObject);
        this.setFieldValByName("createUserName", SessionUtils.getUserName(), metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("updateUserId", SessionUtils.getUserId(), metaObject);
        this.setFieldValByName("updateUserName",  SessionUtils.getUserName(), metaObject);

    }

    /**
     * 更新数据执行
     *
     * @param metaObject
     */
    @SneakyThrows
    @Override
    public void updateFill(MetaObject metaObject) {

        this.setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
        this.setFieldValByName("updateUserId", SessionUtils.getUserId(), metaObject);
        this.setFieldValByName("updateUserName",  SessionUtils.getUserName(), metaObject);

    }

}