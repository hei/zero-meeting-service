package ${package.Controller};


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
<#if swagger2>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
</#if>
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ${package.Entity}.${entity};
import ${package.Entity}.dto.${entity}Dto;
import ${package.Service}.I${entity}Service;
import com.imflea.zero.constants.ZeroContant;
import com.imflea.zero.util.base.JsonResult;
import com.github.pagehelper.PageInfo;
import com.imflea.zero.dto.IdsDto;
import com.imflea.zero.dto.IdDto;
import java.util.List;

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 * @PackageName: ${package.Controller}
 * @ClassName: ${table.controllerName}
 * @Description
 */
@RestController
<#if swagger2>
@Api(value = "${table.comment!}管理", tags = "${table.comment!}管理")
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

    @Autowired
    private ${table.serviceName} ${table.entityPath}Service;

    @PostMapping("/queryById")
    @ApiOperation(value = "根据ID获取实体")
    @ApiOperationSupport(order = 1)
    public JsonResult<${entity}> queryById(@RequestBody IdDto idDto) {
        String id = idDto.getId();
        ${entity} entity = ${table.entityPath}Service.getById(id);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "",entity);
    }

    @PostMapping("/save")
    @ApiOperation(value = "保存与修改${table.comment!}接口")
    @ApiOperationSupport(order = 2)
    public JsonResult<${entity}> save(@RequestBody ${entity} ${table.entityPath}) {
        ${entity} entity = ${table.entityPath}Service.saveOrUpdate${entity}(${table.entityPath});
        return new JsonResult<>(ZeroContant.getSuccessCode(), "保存成功！",entity);
    }

    @PostMapping("/remove")
    @ApiOperation(value = "根据id集合删除${table.comment!}")
    @ApiOperationSupport(order = 3)
    public JsonResult<Boolean> remove(@RequestBody IdsDto idsReq) {
        List<String> ids = idsReq.getIds();
        ${table.entityPath}Service.removeByIds(ids);
        return new JsonResult<>(ZeroContant.getSuccessCode(), "删除成功！");
    }

    @PostMapping("/page")
    @ApiOperation(value = "查询${table.comment!}分页列表")
    @ApiOperationSupport(order = 4)
    public JsonResult<PageInfo<${entity}>> queryPage(@RequestBody ${entity}Dto ${table.entityPath}Dto) {
        PageInfo<${entity}> pageInfo = ${table.entityPath}Service.queryPage(${table.entityPath}Dto);
        return new JsonResult<>(ZeroContant.getSuccessCode(), pageInfo);
    }

}
</#if>