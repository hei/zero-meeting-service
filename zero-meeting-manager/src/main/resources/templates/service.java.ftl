package ${package.Service};

import ${package.Entity}.${entity};
import ${package.Entity}.dto.${entity}Dto;
import ${superServiceClassPackage};
import com.github.pagehelper.PageInfo;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    ${entity} saveOrUpdate${entity}(${entity} entity);

    PageInfo<${entity}> queryPage(${entity}Dto dto);

}
</#if>
