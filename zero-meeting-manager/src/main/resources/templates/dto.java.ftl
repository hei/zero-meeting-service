package ${packageName};

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.time.LocalDateTime;
import com.imflea.zero.dto.PageDto;

/**
 * <p>
 * ${className}DTO
 * </p>
 *
 * @author ${author}
 */
@Data
@ApiModel(value="${className}对象", description="${className}")
public class ${className} extends PageDto {

<#list attrs as attr>
    @ApiModelProperty(value = "${attr.desc}",example = "")
    private ${attr.type} ${attr.name};

</#list>

}
